﻿using Aura.Models;
using Aura.Services;
using OpenAI;
using OpenAI.Chat;
using OpenAI.Models;
using System.Text.Json;
using System.Windows.Threading;

namespace Coatl.Aura.ChatGPT
{
    public class ChatGPTService : AuraServiceBase, IChatbotService
    {
        public override string ServiceID => "svc.coatl.chatgpt";
        public override string ServiceName => "ChatGPT";

        public OpenAIClient? Client { get; private set; }

        public async Task<string?> ProcessMessageAsync(string message)
        {
            if (Client == null) return null;
            
            Model model = Model.GPT3_5_Turbo;
            switch(Config?.Model)
            {
                case "gpt4":
                    model = Model.GPT4;
                    break;
                case "gpt4-32k":
                    model = Model.GPT4_32K;
                    break;
                case "moderation-latest":
                    model = Model.Moderation_Latest;
                    break;
            }

            var chatRequest = new ChatRequest(new List<Message> { new Message(Role.User, message) }, model);
            var result = await Client.ChatEndpoint.GetCompletionAsync(chatRequest);
            return result.FirstChoice.Message.Content;
        }

        public override bool AutoStart => true;

        public override bool CanUserShutdown => true;

        public override bool HasConfiguration => true;

        public override async Task OpenConfiguration()
        {
            if (Manager != null)
            {
                try
                {
                    var msg = this.CreateMessage(InternalServices.CoreService, Path.GetFullPath(configPath));
                    msg["action"] = "open";
                    await Manager.BroadcastMessage(msg);
                }
                catch { }
            }
        }

        public ChatGPTConfig? Config { get; private set; }

        const string configPath = "data/chatgpt3.json";
        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                if (!File.Exists(configPath))
                {
                    try
                    {
                        var json = JsonSerializer.Serialize(new ChatGPTConfig());
                        File.WriteAllText(configPath, json);
                    }
                    catch { }
                }
            });
        }

        protected override async Task OnStart()
        {
            await Task.Run(() =>
            {
                var json = File.ReadAllText(configPath);
                Config = JsonSerializer.Deserialize<ChatGPTConfig>(json);
                if (Config == null || string.IsNullOrWhiteSpace(Config.APIKey))
                {
                    throw new Exception("Invalid configuration");
                }
                GenerateClient(Config.APIKey);
            });
        }

        protected override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
                Client = null;
            });
        }

        public void GenerateClient(string apiKey)
        {
            Client = new OpenAIClient(apiKey);
        }
    }
}