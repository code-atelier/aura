﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.ChatGPT
{
    public class ChatGPTConfig
    {
        [JsonPropertyName("api_key")]
        public string APIKey { get; set; } = string.Empty;

        [JsonPropertyName("model")]
        public string Model { get; set; } = "gpt3.5-turbo";
    }
}
