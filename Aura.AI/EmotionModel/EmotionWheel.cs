﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Aura.AI.EmotionModel
{
    public class EmotionWheel : IEmotion
    {
        public static double BaseDecayRate { get; } = 0.005;
        public IntrinsicEmotion IntrinsicEmotion { get; set; } = new IntrinsicEmotion();

        Vector2 _position = Vector2.Zero;
        public Vector2 Position
        {
            get => _position;
            set
            {
                var distance = value.Length();
                if (distance > 1)
                {
                    _position = value / distance;
                }
                else
                {
                    _position = value;
                }
            }
        }

        public float X { get => Position.X; }

        public float Y { get => Position.Y; }

        public double Joy
        {
            get
            {
                var maxPoint = new Vector2(0, 1);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public double Trust
        {
            get
            {
                var sin45 = (float)Math.Sin(45 * Math.PI / 180);
                var maxPoint = new Vector2(sin45, sin45);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public double Fear
        {
            get
            {
                var maxPoint = new Vector2(1, 0);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public double Surprise
        {
            get
            {
                var sin45 = (float)Math.Sin(45 * Math.PI / 180);
                var maxPoint = new Vector2(sin45, -sin45);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public double Sadness
        {
            get
            {
                var maxPoint = new Vector2(0, -1);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public double Disgust
        {
            get
            {
                var sin45 = (float)Math.Sin(45 * Math.PI / 180);
                var maxPoint = new Vector2(-sin45, -sin45);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public double Anger
        {
            get
            {
                var maxPoint = new Vector2(-1, 0);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public double Anticipation
        {
            get
            {
                var sin45 = (float)Math.Sin(45 * Math.PI / 180);
                var maxPoint = new Vector2(-sin45, sin45);
                var point = new Vector2(X, Y);

                var value = 1 - Vector2.Distance(maxPoint, point);
                var neutralDistance = Vector2.Distance(Vector2.Zero, point);
                if (neutralDistance <= IntrinsicEmotion.NeutralZone) return 0;
                return Math.Max(0, value);
            }
        }

        public List<KeyValuePair<EmotionEnum, double>> GetDominantEmotions()
        {
            var res = new List<KeyValuePair<EmotionEnum, double>>()
            {
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Joy, Joy),
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Trust, Trust),
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Fear, Fear),
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Surprise, Surprise),
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Sadness, Sadness),
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Disgust, Disgust),
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Anger, Anger),
                new KeyValuePair<EmotionEnum, double>(EmotionEnum.Anticipation, Anticipation),
            };

            res.Sort((a, b) => a.Value.CompareTo(b.Value) * -1);
            return res;
        }

        public List<string> GetActivatedEmotions()
        {
            var res = new List<string>();
            var domEmo = GetDominantEmotions();
            foreach(var emo in domEmo) {
                if (emo.Key == EmotionEnum.Joy && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("happiness");
                    res.Add("joy");
                    res.Add("happy");
                }
                if (emo.Key == EmotionEnum.Trust && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("trust");
                    res.Add("confident");
                }
                if (emo.Key == EmotionEnum.Fear && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("fear");
                    res.Add("scared");
                }
                if (emo.Key == EmotionEnum.Surprise && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("surprise");
                    res.Add("surprised");
                }
                if (emo.Key == EmotionEnum.Sadness && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("sad");
                    res.Add("sadness");
                }
                if (emo.Key == EmotionEnum.Disgust && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("disgusted");
                    res.Add("disgust");
                }
                if (emo.Key == EmotionEnum.Anger && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("anger");
                    res.Add("angry");
                }
                if (emo.Key == EmotionEnum.Anticipation && emo.Value >= IntrinsicEmotion.ActuationThreshold)
                {
                    res.Add("anticipation");
                    res.Add("anticipating");
                    res.Add("excited");
                }
            }
            return res;
        }

        public void Reset(IntrinsicEmotion? intrinsicEmotion = null)
        {
            Position = Vector2.Zero;
            IntrinsicEmotion = intrinsicEmotion ?? new IntrinsicEmotion();
        }

        public void Add(IEmotion emotion)
        {
            var currentPoint = new Vector2(X, Y);
            float x1 = X, y1 = Y;
            var sin45 = (float)Math.Sin(45 * Math.PI / 180);
            var centerPoints = new List<Vector2>()
            {
                new Vector2(0, 1),
                new Vector2(sin45, sin45),
                new Vector2(1, 0),
                new Vector2(sin45, -sin45),
                new Vector2(0, -1),
                new Vector2(-sin45, -sin45),
                new Vector2(-1, 0),
                new Vector2(-sin45, sin45),
            };
            for (var i = 0; i < 8; i++)
            {
                float value = 0;
                if (i == 0) value = (float)emotion.Joy * (float)IntrinsicEmotion.EmotionRates.Joy;
                if (i == 1) value = (float)emotion.Trust * (float)IntrinsicEmotion.EmotionRates.Trust;
                if (i == 2) value = (float)emotion.Fear * (float)IntrinsicEmotion.EmotionRates.Fear;
                if (i == 3) value = (float)emotion.Surprise * (float)IntrinsicEmotion.EmotionRates.Surprise;
                if (i == 4) value = (float)emotion.Sadness * (float)IntrinsicEmotion.EmotionRates.Sadness;
                if (i == 5) value = (float)emotion.Disgust * (float)IntrinsicEmotion.EmotionRates.Disgust;
                if (i == 6) value = (float)emotion.Anger * (float)IntrinsicEmotion.EmotionRates.Anger;
                if (i == 7) value = (float)emotion.Anticipation * (float)IntrinsicEmotion.EmotionRates.Anticipation;

                var centerPoint = centerPoints[i];
                var vector = centerPoint - currentPoint;
                var len = vector.Length();
                var identity = len != 0 ? vector / len : Vector2.Zero;
                var move = identity * value;
                x1 += move.X;
                y1 += move.Y;
            }

            Position = new Vector2(x1, y1);
        }

        public void Decay(float deltaSeconds)
        {
            var decay = (float)Math.Max(0, deltaSeconds * IntrinsicEmotion.DecayRate * BaseDecayRate);
            if (decay == 0) return;
            var currentPoint = new Vector2(X, Y);
            var len = currentPoint.Length();
            if (len != 0)
            {
                var decayed = currentPoint / len * Math.Max(0, len - decay);
                Position = decayed;
            }
        }
    }
}
