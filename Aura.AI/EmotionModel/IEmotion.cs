﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.AI.EmotionModel
{
    public interface IEmotion
    {
        double Joy { get; }

        double Trust { get; }

        double Fear { get; }

        double Surprise { get; }

        double Sadness { get; }

        double Disgust { get; }

        double Anger { get; }

        double Anticipation { get; }
    }
    public interface IWriteableEmotion
    {
        double Joy { get; set; }

        double Trust { get; set; }

        double Fear { get; set; }

        double Surprise { get; set; }

        double Sadness { get; set; }

        double Disgust { get; set; }

        double Anger { get; set; }

        double Anticipation { get; set; }
    }
}
