﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.AI.EmotionModel
{
    public class Emotion: IEmotion, IWriteableEmotion
    {
        [JsonPropertyName("joy")]
        public double Joy { get; set; }

        [JsonPropertyName("trust")]
        public double Trust { get; set; }

        [JsonPropertyName("fear")]
        public double Fear { get; set; }

        [JsonPropertyName("surprise")]
        public double Surprise { get; set; }

        [JsonPropertyName("sadness")]
        public double Sadness { get; set; }

        [JsonPropertyName("disgust")]
        public double Disgust { get; set; }

        [JsonPropertyName("anger")]
        public double Anger { get; set; }

        [JsonPropertyName("anticipation")]
        public double Anticipation { get; set; }
    }
}
