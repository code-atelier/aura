﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.AI.EmotionModel
{
    public class IntrinsicEmotion
    {
        /// <summary>
        /// Neutral emotion rate
        /// </summary>
        [JsonPropertyName("neutral_zone")]
        public double NeutralZone { get; set; } = 0.1;

        /// <summary>
        /// Emotion decay rate multiplier
        /// </summary>
        [JsonPropertyName("decay_rate")]
        public double DecayRate { get; set; } = 1;

        [JsonPropertyName("actuation_threshold")]
        public double ActuationThreshold { get; set; } = 0.5;

        [JsonPropertyName("emotion_rates")]
        public Emotion EmotionRates { get; set; } = new Emotion()
        {
            Anger = 1,
            Anticipation = 1,
            Disgust = 1,
            Fear = 1,
            Joy = 1,
            Sadness = 1,
            Surprise = 1,
            Trust = 1,
        };

        [JsonPropertyName("emotion_tags")]
        public EmotionTags Tags { get; set; } = new EmotionTags();
    }
    public class EmotionTags
    {

        public List<string> Joy { get; set; } = new List<string>();

        public List<string> Trust { get; set; } = new List<string>();

        public List<string> Fear { get; set; } = new List<string>();

        public List<string> Surprise { get; set; } = new List<string>();

        public List<string> Sadness { get; set; } = new List<string>();

        public List<string> Disgust { get; set; } = new List<string>();

        public List<string> Anger { get; set; } = new List<string>();

        public List<string> Anticipation { get; set; } = new List<string>();
    }
}
