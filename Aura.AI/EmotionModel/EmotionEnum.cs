﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.AI.EmotionModel
{
    public enum EmotionEnum
    {
        Joy,

        Trust,

        Fear,

        Surprise,

        Sadness,

        Disgust,

        Anger,

        Anticipation,
    }
}
