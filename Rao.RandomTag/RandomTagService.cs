﻿using Aura.Models;
using Aura.Services;
using System.IO;
using System.Text.Json;
using System.Windows.Threading;

namespace Rao.RandomTag
{
    public class RandomTagService : AuraServiceBase
    {
        public override string ServiceID => "rao.randomtag";

        public override string ServiceName => "Random Tag";

        public override string? ServiceIcon => "/Rao.RandomTag;Component/res/icons/layers.png";

        public RandomTagConfig Config { get; set; } = new RandomTagConfig();

        const string ConfigPath = "data/randomtag.json";
        public override bool HasConfiguration => true;

        public override bool CanUserShutdown => true;

        FileSystemWatcher Watcher { get; set; } = new FileSystemWatcher();

        DispatcherTimer ReloadTimer { get; } = new DispatcherTimer();
        DateTime? LastChanged { get; set; }

        public RandomTagService()
        {
            Watcher.Changed += Watcher_Changed;
            Watcher.Filter = "randomtag.json";
            ReloadTimer.Interval = new TimeSpan(0, 0, 0, 1);
            ReloadTimer.Tick += ReloadTimer_Tick;
        }

        private void ReloadTimer_Tick(object? sender, EventArgs e)
        {
            if (LastChanged == null) return;
            if ((DateTime.Now - LastChanged.Value).TotalSeconds >= 2)
            {
                ReloadConfig();
                ReloadTimer.Stop();
            }
        }


        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            LastChanged = DateTime.Now;
            ReloadTimer.Start();
        }

        protected override async Task OnStart()
        {
            await Task.Run(() =>
            {
                ReloadConfig();
                Watcher.Path = Path.GetFullPath("data");
                Watcher.EnableRaisingEvents = true;
            });
        }

        protected override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
                Watcher.EnableRaisingEvents = false;
            });
        }

        public override async Task OpenConfiguration()
        {
            if (Manager != null)
            {
                try
                {
                    var msg = this.CreateMessage(InternalServices.CoreService, Path.GetFullPath(ConfigPath));
                    msg["action"] = "open";
                    await Manager.BroadcastMessage(msg);
                }
                catch { }
            }
        }

        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                if (Manager != null)
                {
                    Manager.OnMainUIClockTick += Manager_OnMainUIClockTick;
                }
                ReloadConfig();
                try
                {
                    var json = JsonSerializer.Serialize(Config, new JsonSerializerOptions() { WriteIndented = true });
                    File.WriteAllText(ConfigPath, json);
                }
                catch { }
            });
        }

        private void ReloadConfig()
        {
            try
            {
                var json = File.ReadAllText(ConfigPath);
                Config = JsonSerializer.Deserialize<RandomTagConfig>(json) ?? Config;
            }
            catch { }
        }

        private void Manager_OnMainUIClockTick(object? sender, EventArgs e)
        {
            foreach (var set in Config.Sets)
            {
                if (set.Every != null)
                {
                    if (set.LastEvery == null)
                    {
                        set.LastEvery = DateTime.Now;
                    }

                    if (set.LastEvery != null)
                    {
                        var delta = DateTime.Now - set.LastEvery.Value;
                        if (delta.TotalSeconds > set.Every)
                        {
                            InvokeChangeTag(set);
                            set.LastEvery = DateTime.Now;
                        }
                    }
                }
            }
        }

        private async void InvokeChangeTag(RandomTagSet set)
        {
            if (Manager == null) return;
            var svcTag = Manager.GetService<TagService>();
            if (svcTag == null) return;

            if (set.PickRandom)
            {
                var ri = new Random().Next(set.Tags.Count);
                if (set.Tags.Count > ri)
                {
                    svcTag.SetExpiringTag(ServiceID + set.Id, set.Tags[ri], new TimeSpan(0, 0, set.Duration));
                }
            }
            else
            {
                foreach (var tag in set.Tags)
                {
                    svcTag.SetExpiringTag(ServiceID + set.Id, tag, new TimeSpan(0, 0, set.Duration));
                }
            }

            if (!string.IsNullOrWhiteSpace(set.Say))
            {
                var msg = this.CreateMessage(InternalServices.MessageService,
                    set.Say);
                await Manager.BroadcastMessage(msg);
            }

            var svcEmo = Manager.GetService<EmotionService>();
            if (svcEmo != null && set.Emotion != null)
            {
                svcEmo.EmotionWheel.Add(set.Emotion);
            }
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            await Task.Run(() =>
            {
                if (message.From(InternalSensors.Region) && message.Body is string region)
                {
                    foreach (var set in Config.Sets)
                    {
                        if (set.Region == region)
                        {
                            InvokeChangeTag(set);
                        }
                    }
                }
            });
        }
    }
}