﻿using Aura.AI.EmotionModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Rao.RandomTag
{
    public class RandomTagConfig
    {
        [JsonPropertyName("sets")]
        public List<RandomTagSet> Sets { get; set; } = new List<RandomTagSet>();
    }

    public class RandomTagSet
    {
        [JsonPropertyName("duration")]
        public int Duration { get; set; }

        [JsonPropertyName("region")]
        public string? Region { get; set; }

        [JsonPropertyName("say")]
        public string? Say { get; set; }

        [JsonPropertyName("every")]
        public int? Every { get; set; }

        [JsonPropertyName("pickRandom")]
        public bool PickRandom { get; set; }

        [JsonPropertyName("emotion")]
        public Emotion? Emotion { get; set; }

        [JsonPropertyName("tags")]
        public List<string> Tags { get; set; } = new List<string>();

        [JsonIgnore]
        public DateTime? LastEvery { get; set; }

        [JsonIgnore]
        public string Id { get; set; } = Guid.NewGuid().ToString("n");
    }
}
