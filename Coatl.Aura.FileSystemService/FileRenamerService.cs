﻿using Aura.Models;
using Aura.Services;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Interop;

namespace Coatl.Aura.FileSystemService
{
    public class FileRenamerService : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.filerenamer";
        public override string ServiceName => "File Renamer";
        public override AuraServiceMessagePriority MessagePriority => AuraServiceMessagePriority.Low;

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            await Task.Run(() =>
            {
                if (message.Sender == InternalSensors.DragDrop)
                {
                    if (message.HeaderIs("event", "drop")
                        && message.Body is IDataObject drop)
                    {
                        try
                        {
                            if (drop.GetDataPresent(DataFormats.FileDrop))
                            {
                                var files = drop.GetData(DataFormats.FileDrop) as string[];
                                if (files != null && Manager != null)
                                {
                                    if (message["tag"] == "rename")
                                    {
                                        message.Handled = true;
                                        Application.Current.Dispatcher.Invoke(() =>
                                        {
                                            var f = new FileRenamerWindow();
                                            f.LoadFiles(files);
                                            f.Show();
                                        });
                                    }
                                }
                            }
                        }
                        catch { }
                    }
                    if (message.HeaderIs("event", "preview")
                        && message.Body is PoolRequest request
                        && request.RequestData is IDataObject data)
                    {
                        try
                        {
                            if (data.GetDataPresent(DataFormats.FileDrop))
                            {
                                var fileList = data.GetData(DataFormats.FileDrop) as string[];
                                if (fileList != null)
                                {
                                    request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                    {
                                        Text = "File Renamer",
                                        Tag = "rename"
                                    });
                                }
                            }
                        }
                        catch { }
                    }
                }
            });
        }
    }
}