﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coatl.Aura.FileSystemService
{
    public class RenameStep
    {
        public string Pattern { get; set; } = string.Empty;

        public bool UseRegex { get; set; }

        public string Value { get; set; } = string.Empty;

        public RenameStepAction Action { get; set; }

        public RenameStepPatternPosition Position { get; set; }

        public RenameStepTarget Target { get; set; }

        public int SwapSeparatorOccurence { get; set; } = 1;

        public int ActionIndex { get; set; } = 0;
        public int ActionLength { get; set; } = 0;

        public string Display
        {
            get
            {
                return "Action: " + Action;
            }
        }

        public string Rename(string fileName)
        {
            try
            {
                var name = Path.GetFileNameWithoutExtension(fileName);
                var ext = Path.GetExtension(fileName)?.Substring(1);

                var subject = Target == RenameStepTarget.Name ? name : ext;
                if (subject == null) return fileName;

                switch (Action)
                {
                    case RenameStepAction.NewName:
                        subject = Value;
                        break;
                    case RenameStepAction.Replace:
                        subject = Replace(subject);
                        break;
                    case RenameStepAction.Remove:
                        subject = Remove(subject);
                        break;
                    case RenameStepAction.Trim:
                        subject = Position == RenameStepPatternPosition.Start ? subject.TrimStart(Pattern.ToCharArray())
                            : Position == RenameStepPatternPosition.End ? subject.TrimEnd(Pattern.ToCharArray())
                            : subject.Trim(Value.ToCharArray());
                        break;
                    case RenameStepAction.Cut:
                        try
                        {
                            subject = subject.Substring(0, ActionIndex) + subject.Substring(ActionIndex + ActionLength);
                        }
                        catch { }
                        break;
                    case RenameStepAction.Prefix:
                        subject = Value + subject;
                        break;
                    case RenameStepAction.Suffix:
                        subject += Value;
                        break;
                    case RenameStepAction.Swap:
                        var chr = Pattern.ToCharArray().First();
                        var delimIndex = subject.IndexOf(chr);
                        for (var i = 2; i <= SwapSeparatorOccurence && delimIndex >= 0; i++)
                        {
                            delimIndex = subject.IndexOf(chr, delimIndex);
                        }

                        if (delimIndex >= 0)
                        {
                            subject = subject.Substring(delimIndex + 1) + chr + subject.Substring(0, delimIndex);
                        }
                        break;
                    case RenameStepAction.InsertAt:
                        subject = subject.Substring(0, ActionIndex) + Value + subject.Substring(ActionIndex);
                        break;
                    case RenameStepAction.Uppercase:
                        subject = subject.ToUpper();
                        break;
                    case RenameStepAction.Lowercase:
                        subject = subject.ToLower();
                        break;
                }

                return Target == RenameStepTarget.Name ? subject + "." + ext : name + "." + subject;
            }
            catch
            {
                return fileName;
            }
        }

        public string Rename(string fileName, int index)
        {
            var res = Rename(fileName);
            var i = index.ToString();
            res = res.Replace("{i}", i);
            for(var w = 2; w <= 5; w++)
                res = res.Replace("{i:" + w + "}", i.PadLeft(w, '0'));
            return res;
        }

        private string Replace(string subject)
        {
            try
            {
                if (UseRegex)
                {
                    var pat = Pattern;
                    if (Position == RenameStepPatternPosition.Start)
                        pat = "^" + pat;
                    if (Position == RenameStepPatternPosition.End)
                        pat = pat + "$";
                    return Regex.Replace(subject, Pattern, Value);
                }
                else
                {
                    return subject.Replace(Pattern, Value);
                }
            }
            catch
            {
                return subject;
            }
        }
        private string Remove(string subject)
        {
            try
            {
                if (UseRegex)
                {
                    var pat = Pattern;
                    if (Position == RenameStepPatternPosition.Start)
                        pat = "^" + pat;
                    if (Position == RenameStepPatternPosition.End)
                        pat = pat + "$";
                    return Regex.Replace(subject, Pattern, "");
                }
                else
                {
                    return subject.Replace(Pattern, "");
                }
            }
            catch
            {
                return subject;
            }
        }
    }

    public enum RenameStepPatternPosition
    {
        Anywhere,
        Start,
        End,
    }

    public enum RenameStepTarget
    {
        Name,
        Extension
    }

    public enum RenameStepAction
    {
        NewName,
        Replace,
        Remove,
        Trim,
        Cut,
        Prefix,
        Suffix,
        Swap, // swap name
        InsertAt,
        Uppercase,
        Lowercase,
    }
}
