﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Markup;

namespace Coatl.Aura.FileSystemService
{
    public class ImageDiffService : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.imagediff";

        public override string ServiceName => "Image Diff";

        public static string[] SupportedExtensions => new string[]
        {
            ".png",
            ".bmp",
            ".jpg",
            ".jpeg"
        };

        private bool MatchExtension(IEnumerable<string> fileList, IEnumerable<string> extensions)
        {
            var exts = extensions.ToList();
            foreach (var file in fileList)
            {
                var ext = Path.GetExtension(file)?.ToLower() ?? "";
                if (!exts.Contains(ext)) return false;
            }
            return true;
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            if (message.Sender == InternalSensors.DragDrop)
            {
                if (message.HeaderIs("event", "preview"))
                {
                    await HandleFileDropPreview(message);
                }
                if (message.HeaderIs("event", "drop"))
                {
                    await HandleFileDrop(message);
                }
            }
        }

        private async Task HandleFileDrop(ServiceMessage message)
        {
            if (message.HeaderIs("event", "drop") && message.Body is IDataObject drop)
            {
                try
                {
                    if (drop.GetDataPresent(DataFormats.FileDrop))
                    {
                        var files = drop.GetData(DataFormats.FileDrop) as string[];
                        if (files != null && files.Length > 1)
                        {
                            var win = new ImageDiffWindow();
                            win.Show();
                            win.Topmost = true;
                            win.LoadFiles(files);
                            win.Topmost = false;
                        }
                    }
                }
                catch { }
            }
        }

        private async Task HandleFileDropPreview(ServiceMessage message)
        {
            if (message.Body is PoolRequest request && request.RequestData is IDataObject data)
            {
                try
                {
                    if (data.GetDataPresent(DataFormats.FileDrop))
                    {
                        var fileList = data.GetData(DataFormats.FileDrop) as string[];
                        if (fileList != null && fileList.Length > 0)
                        {
                            if (MatchExtension(fileList, SupportedExtensions))
                            {
                                await Task.Run(() =>
                                {
                                    request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                    {
                                        Text = "Image Diff",
                                        Tag = "imgdiff"
                                    });
                                });
                            }
                        }
                    }
                }
                catch
                {

                }
            }
        }
    }
}

