﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using File = System.IO.File;

namespace Coatl.Aura.FileSystemService
{
    public class ImageConverterService : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.imageconverter";
        public override string ServiceName => "Image Converter";

        public static string[] SupportedExtensions => new string[]
        {
            ".png",
            ".bmp",
            ".jpg",
            ".jpeg",
            ".ico",
            ".gif"
        };

        private bool MatchExtension(IEnumerable<string> fileList, IEnumerable<string> extensions)
        {
            var exts = extensions.ToList();
            foreach (var file in fileList)
            {
                var ext = Path.GetExtension(file)?.ToLower() ?? "";
                if (!exts.Contains(ext)) return false;
            }
            return true;
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            if (message.Sender == InternalSensors.DragDrop)
            {
                if (message.HeaderIs("event", "preview")
                    && message.Body is PoolRequest request
                    && request.RequestData is IDataObject data)
                {
                    try
                    {
                        if (data.GetDataPresent(DataFormats.FileDrop))
                        {
                            var fileList = data.GetData(DataFormats.FileDrop) as string[];
                            if (fileList != null)
                            {
                                if (MatchExtension(fileList, SupportedExtensions))
                                {
                                    request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                    {
                                        Text = "Convert to JPG",
                                        Tag = "conv:jpg"
                                    });
                                    request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                    {
                                        Text = "Convert to PNG",
                                        Tag = "conv:png"
                                    });
                                }
                                if (MatchExtension(fileList, new string[] { ".png" }))
                                {
                                    request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                    {
                                        Text = "Convert to ICO",
                                        Tag = "conv:ico"
                                    });
                                }
                                if (MatchExtension(fileList, new string[] { ".png", ".jpg", ".jpeg", ".bmp" }))
                                {
                                    request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                    {
                                        Text = "Remove Background (beta)",
                                        Tag = "removebg"
                                    });
                                }
                            }
                        }
                    }
                    catch { }
                }


                if (message.HeaderIs("event", "drop")
                    && message.Body is IDataObject drop)
                {
                    try
                    {
                        if (drop.GetDataPresent(DataFormats.FileDrop))
                        {
                            var files = drop.GetData(DataFormats.FileDrop) as string[];
                            if (files != null && files.Length > 0 && Manager != null)
                            {
                                var tag = message["tag"];
                                if (tag != null && tag?.StartsWith("conv:") == true)
                                {
                                    var ext = tag.Substring(5);
                                    message.Handled = true;
                                    string lastFile = "";
                                    List<string> successFiles = new List<string>();
                                    foreach (var file in files)
                                    {
                                        try
                                        {
                                            var res = HandleConversion(file, ext);
                                            if (res != null)
                                            {
                                                lastFile = Path.GetFileName(file);
                                                successFiles.Add(res);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = this.CreateToastNotificationMessage(
                                                "Aura Image Converter",
                                                 $"Failed to convert '{Path.GetFileName(file)}': " + ex.Message,
                                                 ServiceID);
                                            await Manager.BroadcastMessage(msg);
                                        }
                                    }

                                    if (successFiles.Count > 0)
                                    {
                                        var msg = this.CreateToastNotificationMessage(
                                                "Aura Image Converter",
                                                $"{(successFiles.Count > 1 ? successFiles.Count + " files" : "'" + lastFile + "'")} converted successfully",
                                                ServiceID,
                                                new Dictionary<string, string>()
                                                {
                                                    { "$receiver", InternalServices.CoreService },
                                                    { "action", "show-in-explorer" },
                                                    { "paths", string.Join(";", successFiles) }
                                                }
                                            );
                                        await Manager.BroadcastMessage(msg);
                                    }
                                }

                                if (tag == "removebg")
                                {
                                    message.Handled = true;
                                    string lastFile = "";
                                    List<string> successFiles = new List<string>();
                                    foreach (var file in files)
                                    {
                                        try
                                        {
                                            var res = RemoveBackground(file);
                                            if (res != null)
                                            {
                                                lastFile = Path.GetFileName(file);
                                                successFiles.Add(res);
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = this.CreateToastNotificationMessage(
                                                "Aura Image Converter",
                                                 $"Failed to remove background of '{Path.GetFileName(file)}': " + ex.Message,
                                                 ServiceID);
                                            await Manager.BroadcastMessage(msg);
                                        }
                                    }

                                    if (successFiles.Count > 0)
                                    {
                                        var msg = this.CreateToastNotificationMessage(
                                                "Aura Image Converter",
                                                $"{(successFiles.Count > 1 ? successFiles.Count + " files" : "'" + lastFile + "'")} background removed successfully",
                                                ServiceID,
                                                new Dictionary<string, string>()
                                                {
                                                    { "$receiver", InternalServices.CoreService },
                                                    { "action", "show-in-explorer" },
                                                    { "paths", string.Join(";", successFiles) }
                                                }
                                            );
                                        await Manager.BroadcastMessage(msg);
                                    }
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
        }

        private string? HandleConversion(string sourceFile, string targetExtension)
        {
            Dictionary<string, ImageFormat> ImageFormats = new Dictionary<string, ImageFormat>()
            {
                { "ico", ImageFormat.Icon },
                { "png", ImageFormat.Png },
                { "bmp", ImageFormat.Bmp },
                { "gif", ImageFormat.Gif },
                { "jpg", ImageFormat.Jpeg },
                { "jpeg", ImageFormat.Jpeg },
                { "tiff", ImageFormat.Tiff },
                { "exif", ImageFormat.Exif },
                { "wmf", ImageFormat.Wmf },
                { "emf", ImageFormat.Emf },
            };

            var destDirectory = Path.GetDirectoryName(sourceFile);
            if (destDirectory != null)
            {
                using (var stream = File.OpenRead(sourceFile))
                {
                    Bitmap bitmap = (Bitmap)Image.FromStream(stream);

                    var fName = Path.GetFileNameWithoutExtension(sourceFile) + "." + targetExtension;
                    var dest = Path.Combine(destDirectory, fName);
                    if (File.Exists(dest))
                        throw new Exception("File already exist '" + fName + "'");
                    bitmap.Save(dest, ImageFormats[targetExtension.ToLower()]);
                    return dest;
                }
            }
            return null;
        }

        private string? RemoveBackground(string sourceFile)
        {

            var destDirectory = Path.GetDirectoryName(sourceFile);
            if (destDirectory != null)
            {
                using (var stream = File.OpenRead(sourceFile))
                {
                    Bitmap bitmap = (Bitmap)Image.FromStream(stream);

                    var fName = Path.GetFileNameWithoutExtension(sourceFile) + ".bgremoved.png";
                    var dest = Path.Combine(destDirectory, fName);
                    if (File.Exists(dest))
                        throw new Exception("File already exist '" + fName + "'");

                    Bitmap? output = null;

                    if (output == null) output = RemoveBackgroundFromMask(bitmap);

                    if (output != null)
                    {
                        output.Save(dest, ImageFormat.Png);
                        return dest;
                    }
                    else
                        return null;
                }
            }
            return null;
        }

        private Bitmap? RemoveBackgroundFromMask(Bitmap bitmap)
        {
            var halfW = (int)Math.Ceiling(bitmap.Width / 2.0);
            var halfH = (int)Math.Ceiling(bitmap.Height / 2.0);
            var rects = new Rectangle[]
            {
                new Rectangle(0, 0, bitmap.Width, halfH),
                new Rectangle(0, halfH, bitmap.Width, bitmap.Height - halfH),

                new Rectangle(0, 0, halfW, bitmap.Height),
                new Rectangle(halfW, 0, bitmap.Width - halfW, bitmap.Height),
            };

            var fullRect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
            var index = 0;
            foreach (var rect in rects)
            {
                var pxTL = bitmap.GetPixel(rect.Left, rect.Top);
                if (IsMaskArea(bitmap, rect))
                {
                    var srcRect = rects[index == 1 ? 0 : index == 0 ? 1 : index == 2 ? 3 : 2];
                    var bmp = new Bitmap(srcRect.Width, srcRect.Height, PixelFormat.Format32bppArgb);
                    using (var gDst = Graphics.FromImage(bmp))
                    {
                        gDst.DrawImage(bitmap, new Rectangle(0, 0, bmp.Width, bmp.Height), srcRect, GraphicsUnit.Pixel);
                    }

                    for (var x = 0; x < bmp.Width; x++)
                    {
                        for (var y = 0; y < bmp.Height; y++)
                        {
                            var maskPixel = bitmap.GetPixel(rect.Left + x, rect.Top + y);
                            var pixel = bmp.GetPixel(x, y);
                            var alpha = maskPixel.GetBrightness();
                            if (pxTL.ToArgb() == Color.White.ToArgb())
                            {
                                alpha = 1 - alpha;
                            }
                            var bAlpha = (byte)Math.Round(255 * alpha);
                            var nPixel = Color.FromArgb(bAlpha, pixel);
                            bmp.SetPixel(x, y, nPixel);
                        }
                    }

                    return bmp;
                }

                index++;
            }

            return null;
        }

        private bool IsMaskArea(Bitmap bitmap, Rectangle rect)
        {
            var pxTL = bitmap.GetPixel(rect.Left, rect.Top);
            var pxTR = bitmap.GetPixel(rect.Right - 1, rect.Top);
            var pxBL = bitmap.GetPixel(rect.Left, rect.Bottom - 1);
            var pxBR = bitmap.GetPixel(rect.Right - 1, rect.Bottom - 1);
            if (pxTL == pxTR && pxBL == pxBR && pxTL == pxBL && (pxTL.ToArgb() == Color.White.ToArgb() || pxTL.ToArgb() == Color.Black.ToArgb()))
            {
                var ptCenter = new System.Drawing.Point(rect.Left + rect.Width / 2, rect.Top + rect.Height / 2);
                var rand = new Random();
                for (var i = 0; i < 30; i++)
                {
                    var x = ptCenter.X + rand.Next(bitmap.Width / 2) - bitmap.Width / 4;
                    var y = ptCenter.Y + rand.Next(bitmap.Height / 2) - bitmap.Height / 4;

                    var px = bitmap.GetPixel((int)x, (int)y);
                    var diff = Math.Abs(px.R - px.G) + Math.Abs(px.R - px.B) + Math.Abs(px.B - px.G);
                    if (diff > 10)
                        return false;
                }
                return true;
            }
            return false;
        }
    }
}
