﻿using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Windows.Storage.Streams;

namespace Coatl.Aura.FileSystemService
{
    /// <summary>
    /// Interaction logic for ImageDiffWindow.xaml
    /// </summary>
    public partial class ImageDiffWindow : Window
    {
        const string ResultDir = "diffres";
        const int ThumbnailSize = 90;
        const int ThumbnailThread = 20;
        static readonly SolidColorBrush TransclurentBackground = new SolidColorBrush(Color.FromArgb(0x99, 0, 0, 0));
        static readonly SolidColorBrush SolidBackground = new SolidColorBrush(Color.FromArgb(0xff, 60, 60, 60));
        static readonly SolidColorBrush SelectedBackground = new SolidColorBrush(Color.FromArgb(0x66, 0x55, 0xaa, 0xff));

        public ImageDiffWindow()
        {
            InitializeComponent();
        }

        Border? SelectedBaseImage { get; set; }

        public async void LoadFiles(IEnumerable<string> files)
        {
            foreach (var file in files)
            {
                var img = new Image();
                img.Stretch = Stretch.Uniform;
                img.Tag = file;
                img.HorizontalAlignment = HorizontalAlignment.Stretch;
                img.VerticalAlignment = VerticalAlignment.Stretch;

                var brd = new Border();
                brd.Width = ThumbnailSize;
                brd.Height = ThumbnailSize;
                img.Margin = new Thickness(4, 4, 4, 4);
                brd.Cursor = Cursors.Hand;
                brd.Child = img;
                brd.Tag = file;
                brd.MouseLeftButtonDown += Brd_MouseLeftButtonDown;

                wpPaging.Children.Add(brd);
            }
            svSelector.ScrollToTop();

            await Task.Run(() =>
            {
                for (var i = 0; i < files.Count(); i += ThumbnailThread)
                {
                    var tasks = new List<Task>();
                    for (var x = i; x < files.Count() && x - i < ThumbnailThread; x++)
                    {
                        tasks.Add(LoadThumbnail(files.ElementAt(x)));
                    }
                    if (tasks.Count > 0)
                        Task.WaitAll(tasks.ToArray());
                }
            });
        }
        public async Task LoadThumbnail(string path)
        {
            await Task.Run(() =>
            {
                try
                {
                    using (var img = System.Drawing.Image.FromFile(path))
                    {
                        var imW = img.Width;
                        var imH = img.Height;
                        var scl = Math.Min((double)ThumbnailSize / imW, (double)ThumbnailSize / imH);
                        imW = (int)Math.Round(imW * scl);
                        imH = (int)Math.Round(imH * scl);
                        using (var thumb = img.GetThumbnailImage(imW, imH, () => false, IntPtr.Zero))
                        {
                            using (var ms = new MemoryStream())
                            {
                                thumb.Save(ms, System.Drawing.Imaging.ImageFormat.Png);

                                ms.Seek(0, SeekOrigin.Begin);
                                var buf = new byte[ms.Length];
                                ms.Read(buf);

                                Dispatcher.Invoke(() =>
                                {
                                    var localBuffer = new byte[buf.Length];
                                    Array.Copy(buf, localBuffer, buf.Length);
                                    BitmapImage bmp = new BitmapImage();
                                    bmp.BeginInit();
                                    bmp.StreamSource = new MemoryStream(localBuffer);
                                    bmp.EndInit();

                                    foreach (var brd in wpPaging.Children.OfType<Border>())
                                    {
                                        if (brd.Tag is string p && p == path && brd.Child is Image img)
                                        {
                                            img.Source = bmp;
                                            break;
                                        }
                                    }
                                });
                            }
                        }

                    }
                }
                catch { }
            });
        }


        private void Brd_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            foreach (var brd in wpPaging.Children.OfType<Border>())
            {
                brd.Background = null;
            }
            if (sender is Border sbrd && sbrd.Tag is string p && File.Exists(p))
            {
                SelectedBaseImage = sbrd;
                sbrd.Background = SelectedBackground;
                btnStart.IsEnabled = true;
                LoadImage(p);
            }
        }

        private async void LoadImage(string path)
        {
            try
            {
                await Task.Run(() =>
                {
                    var buffer = File.ReadAllBytes(path);
                    Dispatcher.Invoke(() =>
                    {
                        var localBuffer = new byte[buffer.Length];
                        Array.Copy(buffer, localBuffer, buffer.Length);
                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = new MemoryStream(localBuffer);
                        bmp.EndInit();

                        imgBase.Source = bmp;
                    });
                });
            }
            catch { }
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedBaseImage != null && SelectedBaseImage.Tag is string imgBase && File.Exists(imgBase))
            {
                svSelector.IsEnabled = false;
                btnStart.IsEnabled = false;
                btnStart.Content = "Running...";
                var files = new Dictionary<Border, string>();
                foreach (var brd in wpPaging.Children.OfType<Border>())
                {
                    if (brd != SelectedBaseImage && brd.Tag is string p && File.Exists(p))
                    {
                        files.Add(brd, p);
                    }
                }
                RunDiff(imgBase, files);
            }
        }

        private async void RunDiff(string baseImage, Dictionary<Border, string> others)
        {
            try
            {
                var buffer = File.ReadAllBytes(baseImage);
                string targetFile = "";
                using (System.Drawing.Image bmpBase = System.Drawing.Bitmap.FromStream(new MemoryStream(buffer)))
                {
                    foreach (var other in others)
                    {
                        Dispatcher.Invoke(() =>
                        {
                            foreach (var brd in wpPaging.Children.OfType<Border>())
                            {
                                brd.Background = null;
                                if (brd == other.Key)
                                {
                                    brd.Background = SelectedBackground;
                                    brd.BringIntoView();
                                }
                            }
                        });

                        await Task.Run(async () =>
                        {
                            targetFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(other.Value) ?? "", ResultDir, System.IO.Path.GetFileName(other.Value));
                            await LoadResultImage((System.Drawing.Bitmap)bmpBase, other.Value);
                            Thread.Sleep(1000);
                        });
                    }
                }

                Dispatcher.Invoke(() =>
                {
                    MessageBox.Show("Diff completed!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                    try
                    {
                        System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", targetFile));
                    }
                    catch { }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private async Task LoadResultImage(System.Drawing.Bitmap baseImage, string path)
        {
            var targetDir = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(path) ?? "", ResultDir);
            await Task.Run(() =>
            {
                if (!Directory.Exists(targetDir))
                    Directory.CreateDirectory(targetDir);
                var targetFile = System.IO.Path.Combine(targetDir, System.IO.Path.GetFileName(path));
                var buffer = File.ReadAllBytes(path);
                using (var ms = new MemoryStream(buffer))
                {
                    using (System.Drawing.Bitmap bmpTarget = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms))
                    {

                        if (bmpTarget.Width != baseImage.Width || bmpTarget.Height != baseImage.Height || bmpTarget.PixelFormat != baseImage.PixelFormat)
                            return;

                        using (var nBmp = new System.Drawing.Bitmap(bmpTarget.Width, bmpTarget.Height, bmpTarget.PixelFormat))
                        {
                            for (var x = 0; x < nBmp.Width; x++)
                            {
                                for (var y = 0; y < nBmp.Height; y++)
                                {
                                    var pxBase = baseImage.GetPixel(x, y);
                                    var pxTarget = bmpTarget.GetPixel(x, y);
                                    if (pxBase != pxTarget)
                                    {
                                        nBmp.SetPixel(x, y, pxTarget);
                                    }
                                    else
                                    {
                                        nBmp.SetPixel(x, y, System.Drawing.Color.Transparent);
                                    }
                                }
                            }
                            using (var mst = new MemoryStream())
                            {
                                nBmp.Save(mst, System.Drawing.Imaging.ImageFormat.Png);
                                mst.Seek(0, SeekOrigin.Begin);
                                var msBuffer = mst.GetBuffer();
                                File.WriteAllBytes(targetFile, msBuffer);

                                Dispatcher.Invoke(() =>
                                {
                                    var localBuffer = new byte[msBuffer.Length];
                                    Array.Copy(msBuffer, localBuffer, msBuffer.Length);
                                    var bmp = new BitmapImage();
                                    bmp.BeginInit();
                                    bmp.StreamSource = new MemoryStream(localBuffer);
                                    bmp.EndInit();

                                    imgResult.Source = bmp;
                                });
                            }
                        }
                    }
                }
            });
        }
    }
}
