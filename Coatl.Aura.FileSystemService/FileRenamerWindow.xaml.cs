﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static System.Net.WebRequestMethods;
using File = System.IO.File;
using Path = System.IO.Path;

namespace Coatl.Aura.FileSystemService
{
    /// <summary>
    /// Interaction logic for FileRenamerWindow.xaml
    /// </summary>
    public partial class FileRenamerWindow : Window
    {
        Dictionary<RenameStepAction, RenamerUIVisibility> VisibilitySettings { get; } = new Dictionary<RenameStepAction, RenamerUIVisibility>()
        {
            { RenameStepAction.NewName, RenamerUIVisibility.Value },
            { RenameStepAction.Replace, RenamerUIVisibility.Value | RenamerUIVisibility.Pattern | RenamerUIVisibility.Position | RenamerUIVisibility.PositionAnywhere | RenamerUIVisibility.Regex },
            { RenameStepAction.Remove, RenamerUIVisibility.Pattern | RenamerUIVisibility.Regex },
            { RenameStepAction.Trim, RenamerUIVisibility.Pattern },
            { RenameStepAction.Lowercase, RenamerUIVisibility.None },
            { RenameStepAction.Uppercase, RenamerUIVisibility.None },
            { RenameStepAction.Prefix, RenamerUIVisibility.Value },
            { RenameStepAction.Suffix, RenamerUIVisibility.Value },
        };
        public FileRenamerWindow()
        {
            InitializeComponent();
            var vals = Enum.GetValues<RenameStepAction>();
            foreach (var val in vals)
            {
                cbAction.Items.Add(val);
            }
            cbAction.SelectionChanged += CbAction_SelectionChanged;
            cbAction.SelectedIndex = 0;
        }

        private void CbAction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbAction.SelectedItem is RenameStepAction rsa)
            {
                RenamerUIVisibility res = RenamerUIVisibility.None;
                if (VisibilitySettings.ContainsKey(rsa))
                {
                    res = VisibilitySettings[rsa];
                    btnAddStep.Visibility = Visibility.Visible;
                }
                else
                {
                    btnAddStep.Visibility = Visibility.Collapsed;
                }
                UpdateVisibility(res);
            }
        }

        private void UpdateVisibility(RenamerUIVisibility visibility)
        {
            grTarget.Visibility = (visibility & RenamerUIVisibility.Target) > 0 ? Visibility.Visible : Visibility.Collapsed;
            cbTarget.SelectedIndex = 0;

            grPosition.Visibility = (visibility & RenamerUIVisibility.Position) > 0 ? Visibility.Visible : Visibility.Collapsed;
            cbiPositionAnywhere.Visibility = (visibility & RenamerUIVisibility.PositionAnywhere) > 0 ? Visibility.Visible : Visibility.Collapsed;
            if (cbiPositionAnywhere.Visibility == Visibility.Visible)
                cbPosition.SelectedIndex = 0;
            else
                cbPosition.SelectedIndex = 1;

            grPattern.Visibility = (visibility & RenamerUIVisibility.Pattern) > 0 ? Visibility.Visible : Visibility.Collapsed;
            txPattern.Clear();

            grValue.Visibility = (visibility & RenamerUIVisibility.Value) > 0 ? Visibility.Visible : Visibility.Collapsed;
            txValue.Clear();

            grRegex.Visibility = (visibility & RenamerUIVisibility.Regex) > 0 ? Visibility.Visible : Visibility.Collapsed;
            cxRegex.IsChecked = false;

            grSwapOccurence.Visibility = (visibility & RenamerUIVisibility.Occurence) > 0 ? Visibility.Visible : Visibility.Collapsed;
            cbSwapOccurence.SelectedIndex = 0;

            grIndex.Visibility = (visibility & RenamerUIVisibility.AtIndex) > 0 ? Visibility.Visible : Visibility.Collapsed;
            txIndex.Clear();

            grLength.Visibility = (visibility & RenamerUIVisibility.Length) > 0 ? Visibility.Visible : Visibility.Collapsed;
            txLength.Clear();
        }

        List<RenameStep> renameSteps = new List<RenameStep>();
        List<DataItem> dataItems = new List<DataItem>();
        public void LoadFiles(IEnumerable<string> files)
        {
            var fls = files.ToList();
            fls.Sort();

            dgvFiles.Items.Clear();
            foreach (var f in fls)
            {
                var dir = Path.GetDirectoryName(f);
                var name = Path.GetFileName(f);
                var data = new DataItem()
                {
                    Name = name,
                    Renamed = name,
                    Directory = dir,
                    Status = "Skip"
                };
                dgvFiles.Items.Add(data);
                dataItems.Add(data);
            }

            RebindSteps();
        }

        public void RebindSteps()
        {
            ActionsList.Items.Clear();
            foreach (var rs in renameSteps)
            {
                ActionsList.Items.Add(rs);
            }
            RefreshNames();
        }

        private void btnDelAction_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn && btn.Tag is RenameStep rs)
            {
                if (MessageBox.Show("Are you sure to delete this step?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes) return;
                renameSteps.Remove(rs);
                RebindSteps();
            }
        }

        public void RefreshNames()
        {
            var idx = 1;
            foreach (var itmData in dataItems)
            {
                itmData.Renamed = DoRename(itmData.Name ?? "", idx++);
            }
            foreach (var itmData in dataItems)
            {
                itmData.Status = itmData.Name == itmData.Renamed ? "Skip"
                    : string.IsNullOrWhiteSpace(itmData.Renamed) ? "Error"
                    : dataItems.Any(x => x != itmData && x.Renamed?.Trim().ToLower() == itmData.Renamed?.Trim().ToLower()) ? "Duplicate"
                    : "OK";
            }
            dgvFiles.Items.Clear();
            foreach (var f in dataItems)
            {
                dgvFiles.Items.Add(f);
            }
        }

        private string DoRename(string name, int index)
        {
            foreach (var rs in renameSteps)
            {
                name = rs.Rename(name, index);
            }
            return name;
        }

        private void btnExecute_Click(object sender, RoutedEventArgs e)
        {
            if (renameSteps.Count == 0) return;
            if (dataItems.Any(x => x.Status == "Error"))
            {
                MessageBox.Show("You have some error(s) for new file name, please recheck the table.", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (dataItems.Any(x => x.Status == "Duplicate"))
            {
                MessageBox.Show("You have some duplicate(s) for new file name, please recheck the table.", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (MessageBox.Show("Start renaming those files?\r\nThis action cannot be undone.", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.Yes) return;

            foreach(var di in dataItems)
            {
                try
                {
                    if (di.Directory != null && di.Name != null && di.Renamed != null)
                    {
                        var ipath = Path.Combine(di.Directory, di.Name);
                        var opath = Path.Combine(di.Directory, di.Renamed);
                        if (File.Exists(ipath))
                        {
                            File.Move(ipath, opath, true);
                            di.Name = di.Renamed;
                            di.Status = "Skip";
                        }
                    }
                }
                catch(Exception ex)
                {
                    di.Status = "Failed";
                    MessageBox.Show("Failed to rename file: " + di.Name + Environment.NewLine + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
            MessageBox.Show("Success!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            Close();
        }

        private void btnAddStep_Click(object sender, RoutedEventArgs e)
        {
            if (cbAction.SelectedItem is RenameStepAction rsa)
            {
                try
                {
                    var step = new RenameStep()
                    {
                        Action = rsa,
                        Pattern = txPattern.Text,
                        Value = txValue.Text,
                    };
                    if (grPattern.Visibility == Visibility.Visible && string.IsNullOrEmpty(step.Pattern))
                        throw new Exception("Pattern cannot be empty");
                    if (grValue.Visibility == Visibility.Visible && string.IsNullOrEmpty(step.Value))
                        throw new Exception("Value cannot be empty");
                    if (grIndex.Visibility == Visibility.Visible)
                        step.ActionIndex = int.Parse(txIndex.Text);
                    if (grLength.Visibility == Visibility.Visible)
                        step.ActionLength = int.Parse(txLength.Text);
                    if (grTarget.Visibility == Visibility.Visible)
                        step.Target = Enum.Parse<RenameStepTarget>(cbTarget.Text);
                    if (grRegex.Visibility == Visibility.Visible)
                        step.UseRegex = cxRegex.IsChecked == true;
                    if (grPosition.Visibility == Visibility.Visible)
                        step.Position = Enum.Parse<RenameStepPatternPosition>(cbPosition.Text);
                    if (grSwapOccurence.Visibility == Visibility.Visible)
                        step.SwapSeparatorOccurence = int.Parse(cbSwapOccurence.Text);

                    renameSteps.Add(step);
                    RebindSteps();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
    }

    class DataItem
    {
        public string? Name { get; set; }
        public string? Renamed { get; set; }
        public string? Directory { get; set; }

        public string Status { get; set; } = String.Empty;
    }

    enum RenamerUIVisibility
    {
        None = 0,
        Target = 1,
        Position = 2,
        PositionAnywhere = 4,
        Pattern = 8,
        Value = 16,
        Regex = 32,
        Occurence = 64,
        AtIndex = 128,
        Length = 256,
    }
}
