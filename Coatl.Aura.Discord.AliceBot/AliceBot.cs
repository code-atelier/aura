﻿using Aura.Services;
using Coatl.Aura.Models;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.IO;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Threading;

namespace Coatl.Aura
{
    public class AliceBot : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.discord.alice";

        public override string ServiceName => "Alice Bot";

        public DiscordSocketClient? _client;

        public AliceBotConfig? Config { get; private set; }

        public MessageManager Message { get; } = new MessageManager();
        DispatcherTimer ReloadTimer { get; } = new DispatcherTimer();
        DateTime? LastChanged { get; set; }
        FileSystemWatcher Watcher { get; set; } = new FileSystemWatcher();

        public override bool AutoStart => true;

        public override bool HasConfiguration => true;

        public AliceBot()
        {
            Watcher.Changed += Watcher_Changed;
            Watcher.Filter = "*.json";
            ReloadTimer.Interval = new TimeSpan(0, 0, 0, 1);
            ReloadTimer.Tick += ReloadTimer_Tick;
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            LastChanged = DateTime.Now;
            ReloadTimer.Start();
        }

        private void ReloadTimer_Tick(object? sender, EventArgs e)
        {
            if (LastChanged == null) return;
            if ((DateTime.Now - LastChanged.Value).TotalSeconds >= 2)
            {
                ReloadTemplates();
                ReloadTimer.Stop();
            }
        }

        public override async Task OpenConfiguration()
        {
            await Task.Run(() =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    ConfigurationWindow conf = new ConfigurationWindow(this);
                    //conf.Owner = Manager?.MainWindow;
                    conf.ShowDialog();
                });
            });
        }

        public void SaveConfig()
        {
            if (Config != null)
            {
                var configFile = Path.Combine(AliceBotStatics.DataPath, AliceBotStatics.ConfigName);
                var json = JsonSerializer.Serialize(Config);
                File.WriteAllText(configFile, json);
            }
        }

        protected override async Task OnInitialize()
        {
            await Task.Run(() => {
                if (!Directory.Exists(AliceBotStatics.DataPath))
                    Directory.CreateDirectory(AliceBotStatics.DataPath);
                if (!Directory.Exists(AliceBotStatics.TemplatePath))
                    Directory.CreateDirectory(AliceBotStatics.TemplatePath);
                if (!Directory.Exists(AliceBotStatics.LogPath))
                    Directory.CreateDirectory(AliceBotStatics.LogPath);

                _client = new DiscordSocketClient();
                _client.Log += _client_Log;
                var configFile = Path.Combine(AliceBotStatics.DataPath, AliceBotStatics.ConfigName);
                if (File.Exists(configFile))
                {
                    var json = File.ReadAllText(configFile);
                    Config = JsonSerializer.Deserialize<AliceBotConfig>(json);
                }
                else
                {
                    var cfg = new AliceBotConfig();
                    var json = JsonSerializer.Serialize(cfg);
                    File.WriteAllText(configFile, json);
                }

                var globalTemplateFile = Path.Combine(AliceBotStatics.TemplatePath, AliceBotStatics.GlobalTemplateName);
                if (!File.Exists(globalTemplateFile))
                {
                    var def = new AliceBotMessageTrigger()
                    {
                        Conditions =
                        {
                            new AliceBotMessageTriggerCondition()
                            {
                                ContainsString = new string[] { "hi", "hello", "helo", "hai" },
                            }
                        },
                        Replies = new List<MessageTemplate>() { 
                            new MessageTemplate()
                            {
                                Kind = MessageTemplateKind.Text,
                                Text = "Hi, {sender}!"
                            } 
                        }
                    };
                    var global = new AliceBotMessageTemplate()
                    {
                        GuildId = "global",
                        GuildName= "Global",
                        Triggers = new List<AliceBotMessageTrigger>() { def }
                    };
                    var json = JsonSerializer.Serialize(global);
                    File.WriteAllText(globalTemplateFile, json);
                }

                // load templates
                ReloadTemplates();

                //load data store
                Message.LoadDataStore();
            });
        }

        public void ReloadTemplates()
        {
            var files = Directory.EnumerateFiles(AliceBotStatics.TemplatePath, "*.json");

            Message.Templates.Clear();

            foreach (var file in files)
            {
                try
                {
                    var json = File.ReadAllText(file);
                    var obj = JsonSerializer.Deserialize<AliceBotMessageTemplate>(json);
                    if (obj != null)
                    {
                        Message.Templates[obj.GuildId] = obj;
                    }
                }
                catch { }
            }
        }

        public async void SetStatus(string? status = null, string statusKind = "playing")
        {
            if (_client != null)
            {
                statusKind = statusKind.ToLower();
                var kind = ActivityType.Playing;
                if (statusKind == "watching")
                    kind = ActivityType.Watching;
                if (statusKind == "streaming")
                    kind = ActivityType.Streaming;
                if (statusKind == "listening")
                    kind = ActivityType.Listening;
                await _client.SetGameAsync(status, null, kind);
            }
        }

        private async Task _client_Log(LogMessage arg)
        {
            
        }

        protected override async Task OnStart()
        {
            if (Config == null) throw new Exception("AliceBot configuration is not found or invalid");
            if (_client == null) throw new Exception("DiscordSocketClient is not initialized");

            _client.MessageReceived += _client_MessageReceived;

            await _client.LoginAsync(TokenType.Bot, Config.Token);
            await _client.StartAsync();

            if (!string.IsNullOrWhiteSpace(Config.Status))
            {
                SetStatus(Config.Status, Config.StatusKind);
            }

            Watcher.Path = Path.GetFullPath(AliceBotStatics.TemplatePath);
            Watcher.EnableRaisingEvents = true;
        }

        protected override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
                Watcher.EnableRaisingEvents = false;
            });
        }

        private async Task _client_MessageReceived(SocketMessage arg)
        {
            if (_client == null || Config == null) return;
            if (arg is SocketUserMessage userMessage)
            {
                if (await Message.Handle(_client, userMessage)) return;

                int pos = 0;
                var ch = userMessage.Channel as SocketChannel;
                if (Config.UseChatbot && userMessage.Author.IsBot == false && (userMessage.HasMentionPrefix(_client.CurrentUser, ref pos) || ch.GetChannelType() == ChannelType.DM))
                {
                    try
                    {
                        var svc = Manager?.Services.Where(x => x is IChatbotService && x.State == AuraServiceState.Running).FirstOrDefault();
                        if (svc is IChatbotService chatbotService)
                        {
                            var msg = userMessage.CleanContent;

                            string pattern = @"@[a-zA-Z0-9]+#[0-9]+";
                            RegexOptions options = RegexOptions.Multiline;
                            Regex regex = new Regex(pattern, options);
                            msg = regex.Replace(msg, "").Trim();

                            var res = await chatbotService.ProcessMessageAsync(msg);
                            if (!string.IsNullOrWhiteSpace(res))
                            {
                                await userMessage.ReplyAsync(res);
                            }
                        }
                    }
                    catch { }
                }
            }
        }
    }
}