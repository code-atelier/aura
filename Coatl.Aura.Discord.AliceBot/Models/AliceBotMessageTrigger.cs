﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coatl.Aura.Models
{
    public class AliceBotMessageTrigger
    {
        /// <summary>
        /// At least one of the optional condition and all required conditions must be satisfied.
        /// </summary>
        [JsonPropertyName("conditions")]
        public List<AliceBotMessageTriggerCondition> Conditions { get; set; } = new List<AliceBotMessageTriggerCondition>();

        [JsonPropertyName("replies")]
        public List<MessageTemplate> Replies { get; set; } = new List<MessageTemplate>();

        [JsonPropertyName("store")]
        public List<AliceBotStore> Stores { get; set; } = new List<AliceBotStore>();

        public bool CheckCondition(DiscordSocketClient client, SocketUserMessage userMessage)
        {
            int count = 0;
            foreach (var condition in Conditions)
            {
                if (condition.Check(client, userMessage))
                {
                    count++;
                }
                else if (!condition.Optional)
                {
                    return false;
                }
            }
            return count > 0;
        }

        public Dictionary<string, string> GetFormatInputs(SocketUserMessage userMessage)
        {
            var res = new Dictionary<string, string>();
            foreach(var cond in Conditions)
            {
                var inputs = cond.GetFormatInputs(userMessage);
                if (inputs != null) { 
                    foreach(var kv in inputs)
                    {
                        res[kv.Key] = kv.Value;
                    }
                }
            }
            return res;
        }
    }

    public class AliceBotMessageTriggerCondition
    {
        [JsonPropertyName("from")]
        public string? From { get; set; }

        [JsonPropertyName("optional")]
        public bool Optional { get; set; } = false;

        [JsonPropertyName("tagged")]
        public bool Tagged { get; set; } = true;

        [JsonPropertyName("not_bot")]
        public bool NotBot { get; set; } = true;

        [JsonPropertyName("contains")]
        public string[]? ContainsString { get; set; } = null;

        [JsonPropertyName("format")]
        public string? Format { get; set; } = null;

        [JsonPropertyName("case_sensitive")]
        public bool ContainsStringCaseSensitive { get; set; } = false;

        public bool Check(DiscordSocketClient client, SocketUserMessage userMessage)
        {
            var res = false;

            // Author related conditions
            if (!string.IsNullOrEmpty(From) && From != userMessage.Author.Id.ToString())
            {
                return res;
            }
            if (NotBot && userMessage.Author.IsBot)
            {
                return res;
            }
            int arg = 0;
            if (Tagged)
            {
                if (!userMessage.HasMentionPrefix(client.CurrentUser, ref arg))
                    return res;
            }
            else
            {
                // must be a DM channel and not bot
                var ch = userMessage.Channel as SocketChannel;
                if (userMessage.Author.IsBot || ch == null || ch.GetChannelType() != ChannelType.DM)
                    return res;
            }

            // Message related conditions
            var text = userMessage.CleanContent;
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            text = rgx.Replace(text, "");

            string pattern = @"@[a-zA-Z0-9]+#[0-9]+";
            RegexOptions options = RegexOptions.Multiline;
            Regex regex = new Regex(pattern, options);
            text = regex.Replace(text, "").Trim();

            if (!ContainsStringCaseSensitive)
            {
                text = text.ToLower();
            }
            var words = text.Split(new char[] { ' ' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            if (ContainsString != null && ContainsString.Length > 0)
            {
                var passed = false;
                foreach(var tofind in ContainsString)
                {
                    var find = tofind;
                    if (!ContainsStringCaseSensitive)
                    {
                        find = find.ToLower();
                    }

                    if (words.Contains(find))
                    {
                        passed = true;
                        break;
                    }
                }
                if (!passed)
                {
                    return res;
                }
                else
                {
                    res = true;
                }
            }
            if (Format != null)
            {
                if (GetFormatInputs(userMessage) == null)
                {
                    return res;
                }
                else
                {
                    res = true;
                }
            }

            return res;
        }

        public Dictionary<string, string>? GetFormatInputs(SocketUserMessage userMessage)
        {
            if (Format == null) return null;

            var text = userMessage.CleanContent;
            if (!ContainsStringCaseSensitive) text = text.ToLower();

            string pattern = @"@[a-zA-Z0-9]+#[0-9]+";
            RegexOptions options = RegexOptions.Multiline;
            Regex regex = new Regex(pattern, options);
            text = regex.Replace(text, "").Trim();

            var words = text.Split(new char[] { ' ' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries).ToList();
            Regex rgx = new Regex("[^a-zA-Z0-9 -]");
            for (int i = 0; i < words.Count; i++)
            {
                words[i] = rgx.Replace(words[i], "");
            }

            var fspl = Format.Split(new char[] { ' ' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            var wi = 0;
            var inputs = new Dictionary<string, string>();
            for (var i = 0; i < fspl.Length; i++)
            {
                if (words.Count - 1 < wi) break;
                var part = fspl[i];
                var word = words[wi++];

                //optional
                if (part.StartsWith("[") && part.EndsWith("]"))
                {
                    var spart = part.Substring(1, part.Length - 2);
                    if (spart.ToLower() != word.ToLower())
                    {
                        wi--;
                    }
                    continue;
                }

                // input
                if (part.StartsWith("{") && part.EndsWith("}"))
                {
                    var spart = part.Substring(1, part.Length - 2);
                    inputs[spart] = word;
                    continue;
                }

                if (part.ToLower() != word.ToLower())
                    return null;
            }
            return inputs;
        }
    }
}
