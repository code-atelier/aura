﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.Models
{
    public class MessageTemplate
    {
        [JsonPropertyName("kind")]
        public MessageTemplateKind Kind { get; set; }

        [JsonPropertyName("text")]
        public string Text { get; set; } = string.Empty;

        [JsonPropertyName("to")]
        public string? To { get; set; } = null;
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum MessageTemplateKind
    {
        Text = 0,
    }
}
