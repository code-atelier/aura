﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.Models
{
    public class AliceBotMessageTemplate
    {
        [JsonPropertyName("guild_id")]
        public string GuildId { get; set; } = string.Empty;
        
        [JsonPropertyName("guild_name")]
        public string GuildName { get; set; } = string.Empty;

        [JsonPropertyName("triggers")]
        public List<AliceBotMessageTrigger> Triggers { get; set; } = new List<AliceBotMessageTrigger>();
    }
}
