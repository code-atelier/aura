﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.Models
{
    public class AliceBotConfig
    {
        [JsonPropertyName("token")]
        public string Token { get; set; } = string.Empty;

        [JsonPropertyName("status")]
        public string Status { get; set; } = string.Empty;

        [JsonPropertyName("status_kind")]
        public string StatusKind { get; set; } = "playing";

        [JsonPropertyName("use_chatbot")]
        public bool UseChatbot { get; set; } = false;
    }
}
