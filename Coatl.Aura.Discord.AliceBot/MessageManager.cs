﻿using Coatl.Aura.Models;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Coatl.Aura
{
    public class MessageManager
    {
        public Dictionary<string, AliceBotMessageTemplate> Templates { get; } = new Dictionary<string, AliceBotMessageTemplate>();
        public Dictionary<string, string> DataStore { get; } = new Dictionary<string, string>();

        public void SaveDataStore()
        {
            try
            {
                var json = JsonSerializer.Serialize(DataStore);
                var path = Path.Combine(AliceBotStatics.DataPath, AliceBotStatics.MessageDataStoreName);
                File.WriteAllText(path, json);
            }
            catch { }
        }
        public void LoadDataStore()
        {
            try
            {
                DataStore.Clear();
                var path = Path.Combine(AliceBotStatics.DataPath, AliceBotStatics.MessageDataStoreName);
                var json = File.ReadAllText(path);
                var dic = JsonSerializer.Deserialize<Dictionary<string, string>>(json);
                if (dic != null)
                {
                    foreach (var kv in dic)
                    {
                        DataStore[kv.Key] = kv.Value;
                    }
                }
            }
            catch { }
        }

        public AliceBotMessageTrigger? GetTriggeredMessage(DiscordSocketClient client, SocketUserMessage userMessage)
        {
            var channel = userMessage.Channel as SocketGuildChannel;
            if (channel != null && channel.Guild != null)
            {
                var guildId = channel.Guild.Id.ToString();
                if (Templates.ContainsKey(guildId))
                {
                    var template = Templates[guildId];
                    var msg = template.Triggers.FirstOrDefault(x => x.CheckCondition(client, userMessage));
                    if (msg != null)
                        return msg;
                }
            }
            if (Templates.ContainsKey("global"))
            {
                var template = Templates["global"];
                var msg = template.Triggers.FirstOrDefault(x => x.CheckCondition(client, userMessage));
                if (msg != null)
                    return msg;
            }
            return null;
        }

        public string FormatString(string text, SocketUser? sender, SocketGuildChannel? channel, Dictionary<string, string> inputs)
        {
            if (sender != null)
            {
                text = text.Replace("{sender}", sender.Mention);
            }
            if (channel != null)
            {
                text = text.Replace("{channel}", channel.Name);
            }
            if (channel?.Guild != null)
            {
                text = text.Replace("{guild}", channel.Guild.Name);
            }
            if (inputs != null)
            {
                foreach (var kv in inputs)
                {
                    text = text.Replace("{" + kv.Key + "}", kv.Value);
                }
            }
            foreach(var ds in DataStore)
            {
                text = text.Replace("{store:" + ds.Key + "}", ds.Value);
            }
            string pattern = @"\{store\:[^}]+\}";
            Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
            text = regex.Replace(text, "null");
            return text;
        }

        public async Task<bool> Handle(DiscordSocketClient client, SocketUserMessage userMessage)
        {
            var triggeredMessage = GetTriggeredMessage(client, userMessage);
            if (triggeredMessage != null)
            {
                var inputs = triggeredMessage.GetFormatInputs(userMessage);
                foreach(var store in triggeredMessage.Stores)
                {
                    var id = FormatString(store.Id, userMessage.Author, userMessage.Channel as SocketGuildChannel, inputs);
                    var value = FormatString(store.Value ?? "``not set``", userMessage.Author, userMessage.Channel as SocketGuildChannel, inputs);
                    if (!string.IsNullOrWhiteSpace(id))
                        DataStore[id] = value;
                }
                if (triggeredMessage.Stores.Count > 0)
                    SaveDataStore();

                foreach(var reply in triggeredMessage.Replies)
                {
                    var text = FormatString(reply.Text, userMessage.Author, userMessage.Channel as SocketGuildChannel, inputs);
                    if (string.IsNullOrWhiteSpace(reply.To))
                    {
                        await userMessage.ReplyAsync(text);
                    }
                    else
                    {
                        try
                        {
                            var spl = reply.To.Split('#');
                            var user = client.GetUser(spl[0], spl[1]);
                            var dm = await user.CreateDMChannelAsync();
                            await dm.SendMessageAsync(text);
                        }
                        catch { }
                    }
                }
                return true;
            }
            return false;
        }
    }
}
