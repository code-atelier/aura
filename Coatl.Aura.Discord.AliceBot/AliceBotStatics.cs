﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura
{
    public static class AliceBotStatics
    {
        public static Version Version { get; } = new Version(1, 0, 1);

        public const string DataPath = "data/alicebot";
        public const string LogPath = "data/alicebot/logs";
        public const string TemplatePath = "data/alicebot/templates";

        public const string ConfigName = "config.json";
        public const string MessageDataStoreName = "messagedata.json";
        public const string GlobalTemplateName = "global.json";
    }
}
