﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coatl.Aura
{
    /// <summary>
    /// Interaction logic for ConfigurationWindow.xaml
    /// </summary>
    public partial class ConfigurationWindow : Window
    {
        public AliceBot Service { get; set; }

        public ConfigurationWindow(AliceBot service)
        {
            InitializeComponent();
            Service = service;

            cxChatbot.IsChecked = service.Config?.UseChatbot ?? false;
            lbDesc.Content = "AliceBot v" + AliceBotStatics.Version;
        }

        private void btnReloadTemplates_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Service.ReloadTemplates();
                MessageBox.Show("Message templates reloaded!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void cxChatbot_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void btnSaveStatus_Click(object sender, RoutedEventArgs e)
        {
            if (Service.Config != null)
            {
                try
                {
                    Service.Config.UseChatbot = cxChatbot.IsChecked == true;
                    Service.SaveConfig();
                    Service.ReloadTemplates();
                    MessageBox.Show("Configuration Saved!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private async void btnopenTemplate_Click(object sender, RoutedEventArgs e)
        {
            if (Service.Manager != null)
            {
                try
                {
                    var msg = Service.CreateMessage(InternalServices.CoreService, System.IO.Path.GetFullPath(AliceBotStatics.TemplatePath));
                    msg["action"] = "open";
                    await Service.Manager.BroadcastMessage(msg);
                }
                catch { }
            }
        }
    }
}
