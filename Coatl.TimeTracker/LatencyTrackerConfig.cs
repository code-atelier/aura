﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.TimeTracker
{
    public class LatencyTrackerConfig
    {
        public List<LatencyTrackerItem> Trackers { get; set; } = new List<LatencyTrackerItem>();
    }

    public class LatencyTrackerItem
    {
        public string Name { get; set; } = "Tracker";

        public string Host { get; set; } = "google.com";
    }
}
