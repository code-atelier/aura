﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coatl.TimeTracker
{
    /// <summary>
    /// Interaction logic for WLatencyTrackerConfig.xaml
    /// </summary>
    public partial class WLatencyTrackerConfig : Window
    {
        public WLatencyTrackerConfig()
        {
            InitializeComponent();
        }

        LatencyTrackerViewModel? ViewModel { get; set; }

        public void Initialize(LatencyTrackerViewModel model)
        {
            ViewModel = model;
            txHost.Text = model.Host;
            txName.Text = model.Name;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null)
            {
                ViewModel.Name = txName.Text.Trim();
                ViewModel.Host = txHost.Text.Trim();
                Close();
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
