﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using LiveCharts;
using LiveCharts.Wpf;
using System.Windows.Threading;
using LiveCharts.Defaults;
using WinRT;
using System.Windows.Xps.Serialization;
using System.Net.NetworkInformation;
using System.IO;

namespace Coatl.TimeTracker
{
    /// <summary>
    /// Interaction logic for WLatencyTracker.xaml
    /// </summary>
    public partial class WLatencyTracker : Window
    {
        const int MaxData = 10;
        public ServiceManager? ServiceManager { get; set; }
        DispatcherTimer ChartRefresher { get; set; } = new DispatcherTimer(DispatcherPriority.Render);
        public double?[] LastPing { get; } = new double?[6];

        public List<LatencyTrackerViewModel> ViewModels { get; } = new List<LatencyTrackerViewModel>() {
            new LatencyTrackerViewModel() { Name = "Tracker #1" },
            new LatencyTrackerViewModel() { Name = "Tracker #2" },
            new LatencyTrackerViewModel() { Name = "Tracker #3" },
            new LatencyTrackerViewModel() { Name = "Tracker #4" },
            new LatencyTrackerViewModel() { Name = "Tracker #5" },
            new LatencyTrackerViewModel() { Name = "Tracker #6" },
        };

        public List<Thread> PingThreads { get; } = new List<Thread>();

        public WLatencyTracker()
        {
            InitializeComponent();
            ChartRefresher.Interval = new TimeSpan(0, 0, 0, 0, 1000);
            ChartRefresher.Tick += ChartRefresher_Tick;
            ChartRefresher.Start();

            InitGraph();

            try
            {
                if (File.Exists(ConfigPath))
                {
                    var json = File.ReadAllText(ConfigPath);
                    var cfg = System.Text.Json.JsonSerializer.Deserialize<LatencyTrackerConfig>(json);
                    if (cfg != null)
                    {
                        var i = 0;
                        foreach(var item in cfg.Trackers)
                        {
                            ViewModels[i].Name = item.Name;
                            ViewModels[i].Host = item.Host;
                            i++;
                        }
                    }
                }
            }
            catch { }
            tabControl.ItemsSource = ViewModels;

            for (var i = 0; i < ViewModels.Count; i++)
            {
                var thread = new Thread(new ParameterizedThreadStart(PingThread));
                thread.Start(i);
                PingThreads.Add(thread);
            }
        }

        private void ChartRefresher_Tick(object? sender, EventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                ChartRefresher.Stop();
                try
                {
                    DrawGraph();
                }
                finally
                {
                    ChartRefresher.Start();
                }
            });
        }

        private void InitGraph()
        {
            chart.ToolTip = null;
            chart.Zoom = ZoomingOptions.None;
            chart.HideTooltip();

            chart.SeriesColors = new ColorsCollection
            {
                Colors.MistyRose,
                Colors.Honeydew,
                Colors.LightCyan,
                Colors.Red,
                Colors.LimeGreen,
                Colors.Blue,
                Colors.Orange,
                Colors.DimGray,
                Colors.Fuchsia,
            };

            for (int i = 0; i < ViewModels.Count; i++)
            {
                ViewModels[i].Color = chart.SeriesColors[i + 3];
            }

            chart.AxisY.Add(new Axis()
            {
                MinValue = 0,
                MaxValue = 150,
                LabelFormatter = (d) => Math.Round(d, 1) + "ms"
            });
            chart.AxisX.Add(new Axis()
            {
                MinValue = 1 - MaxData,
                MaxValue = 0,
                LabelFormatter = (d) => Math.Round(d, 1) + "s"
            });

            var series = new SeriesCollection
            {
                // baselines
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>()
                    {
                        new ObservablePoint(-9, 300),
                        new ObservablePoint(0, 300),
                    },
                    DataLabels = false,
                    Title = "orange limit",
                    PointGeometry = null,
                    Stroke = null,
                    Fill = new SolidColorBrush(chart.SeriesColors[0]),
                    StrokeThickness = 0,
                },
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>()
                    {
                        new ObservablePoint(-9, 70),
                        new ObservablePoint(0, 70),
                    },
                    DataLabels = false,
                    Title = "green limit",
                    PointGeometry = null,
                    Stroke = null,
                    Fill = new SolidColorBrush(chart.SeriesColors[1]),
                    StrokeThickness = 0,
                },
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>()
                    {
                        new ObservablePoint(-9, 30),
                        new ObservablePoint(0, 30),
                    },
                    DataLabels = false,
                    Title = "blue limit",
                    PointGeometry = null,
                    Stroke = null,
                    Fill = new SolidColorBrush(chart.SeriesColors[2]),
                    StrokeThickness = 0,
                },

                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>(),
                    Fill = new SolidColorBrush(Colors.Transparent),
                    DataLabels = false,
                    PointGeometry = null,
                },
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>(),
                    Fill = new SolidColorBrush(Colors.Transparent),
                    DataLabels = false,
                    PointGeometry = null,
                },
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>(),
                    Fill = new SolidColorBrush(Colors.Transparent),
                    DataLabels = false,
                    PointGeometry = null,
                },
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>(),
                    Fill = new SolidColorBrush(Colors.Transparent),
                    DataLabels = false,
                    PointGeometry = null,
                },
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>(),
                    Fill = new SolidColorBrush(Colors.Transparent),
                    DataLabels = false,
                    PointGeometry = null,
                },
                new LineSeries()
                {
                    Values = new ChartValues<ObservablePoint>(),
                    Fill = new SolidColorBrush(Colors.Transparent),
                    DataLabels = false,
                    PointGeometry = null,
                }
            };
            chart.Series = series;

            DrawGraph();
        }

        private void DrawGraph()
        {
            for (var s = 3; s < chart.Series.Count; s++)
            {
                var series = chart.Series[s];
                var viewModel = ViewModels[s - 3];

                if (!viewModel.Active)
                {
                    if (series.Values.Count > 0)
                        series.Values.Clear();
                    continue;
                }
                if (series is LineSeries lineSeries)
                    lineSeries.Title = viewModel.Name;

                while (series.Values.Count >= MaxData)
                {
                    series.Values.RemoveAt(0);
                }
                for (var i = 0; i < series.Values.Count; i++)
                {
                    var op = series.Values[i].As<ObservablePoint>();
                    op.X--;
                }

                var newValue = LastPing[s - 3];
                LastPing[s - 3] = null;
                if (newValue == null)
                {
                    newValue = 150;
                    viewModel.Add(-1);
                }
                else
                {
                    viewModel.Add(newValue.Value);
                }
                series.Values.Add(new ObservablePoint(0, newValue.Value));
            }
        }

        bool __wantStop = false;
        private void PingThread(object? index)
        {
            try
            {
                if (index == null) return;
                var idx = (int)index;
                var icmp = new System.Net.NetworkInformation.Ping();
                while (!__wantStop)
                {
                    try
                    {
                        if (ViewModels[idx].Active)
                        {
                            var host = ViewModels[idx].Host;
                            if (!string.IsNullOrWhiteSpace(host))
                            {
                                var reply = icmp.Send(host);
                                LastPing[idx] = reply?.RoundtripTime;
                            }
                        }
                        else
                        {
                            LastPing[idx] = null;
                        }
                    }
                    catch { }
                    Thread.Sleep(500);
                }
            }
            catch
            {
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            __wantStop = true;
        }

        const string ConfigPath = "data/latencytracker/config.json";
        private void btnConfigTracker_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn && btn.Tag is LatencyTrackerViewModel vm)
            {
                var f = new WLatencyTrackerConfig();
                f.Owner = this;
                f.Initialize(vm);
                f.ShowDialog();

                try
                {
                    // save config
                    var cfg = new LatencyTrackerConfig();
                    cfg.Trackers.AddRange(ViewModels.Select(x => new LatencyTrackerItem()
                    {
                        Host = x.Host,
                        Name = x.Name,
                    }));
                    var json = System.Text.Json.JsonSerializer.Serialize(cfg);
                    var cfDir = System.IO.Path.GetDirectoryName(ConfigPath);
                    if (cfDir != null && !Directory.Exists(cfDir))
                        Directory.CreateDirectory(cfDir);
                    File.WriteAllText(ConfigPath, json);
                }
                catch { }
            }
        }
    }
}
