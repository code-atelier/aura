﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics.Arm;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Coatl.TimeTracker
{
    /// <summary>
    /// Interaction logic for WTimeTracker.xaml
    /// </summary>
    public partial class WTimeTracker : Window
    {
        DispatcherTimer RefresherTimer { get; } = new DispatcherTimer(DispatcherPriority.Render);

        public ServiceManager? ServiceManager { get; set; }

        public WTimeTracker()
        {
            InitializeComponent();
            RefresherTimer.Interval = new TimeSpan(0, 0, 0, 0, 32);
            RefresherTimer.Tick += RefresherTimer_Tick;
            RefresherTimer.Start();
        }

        DateTime LastTick = DateTime.Now;
        bool IsPaused = false;
        SolidColorBrush brRed = new SolidColorBrush(Colors.Crimson);
        SolidColorBrush brDarkRed = new SolidColorBrush(Colors.DarkRed);
        SolidColorBrush brGreen = new SolidColorBrush(Colors.Green);
        SolidColorBrush brBlack = new SolidColorBrush(Colors.Black);
        private void RefresherTimer_Tick(object? sender, EventArgs e)
        {
            var now = DateTime.Now;
            if (TotalTimeRemaining != null && EndTime != null && Started)
            {
                if (!IsPaused)
                {
                    var delta = EndTime - now;
                    TotalTimeRemaining = delta;

                    if (TotalTimeRemaining.Value.TotalSeconds <= 0)
                    {
                        Stop(true);
                    }
                    else
                    {
                        var txUpdate = "";
                        txUpdate += TotalTimeRemaining.Value.Hours.ToString().PadLeft(2, '0') + ":";
                        txUpdate += TotalTimeRemaining.Value.Minutes.ToString().PadLeft(2, '0') + ":";
                        txUpdate += TotalTimeRemaining.Value.Seconds.ToString().PadLeft(2, '0') + ".";
                        txUpdate += TotalTimeRemaining.Value.Milliseconds.ToString().PadLeft(3, '0');
                        Dispatcher.Invoke(() =>
                        {
                            lbTimeRemaining.Content = txUpdate;
                            pbTracker.Value = TotalTimeRemaining.Value.TotalSeconds;
                            if (pbTracker.Value <= 10)
                            {
                                lbTimeRemaining.Foreground = TotalTimeRemaining.Value.TotalSeconds % 1 >= 0.5 ? brRed : brDarkRed;
                                pbTracker.Foreground = brRed;
                            }
                            else
                            {
                                lbTimeRemaining.Foreground = brBlack;
                                pbTracker.Foreground = brGreen;
                            }
                        });
                    }
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        lbTimeRemaining.Content = "00:00:00.000";
                    });
                }
                LastTick = now;
            };
        }

        private void Stop(bool sendNotif = false)
        {
            Started = false;
            IsPaused = false;
            TotalTimeRemaining = null;
            EndTime = null;
            lbTimeRemaining.Content = "00:00:00.000";
            grConfig.Visibility = Visibility.Visible;
            grControl.Visibility = Visibility.Collapsed;
            pbTracker.Value = 0;
            pbTracker.Foreground = brGreen;
            lbTimeRemaining.Foreground = brBlack;
            if (sendNotif && ServiceManager != null)
            {
                var msg = new ServiceMessage("svc.coatl.timetracker", InternalServices.MessageService,
                    (string.IsNullOrWhiteSpace(txTitle.Text) ? "Your timer" : txTitle.Text) + " has finished!");
                msg["as"] = "toast";
                msg["toast-title"] = "Time Tracker";
                ServiceManager.BroadcastMessage(msg);
            }
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Stop the timer and close?", "Question", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes) return;
            Close();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            RefresherTimer.Stop();
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Cursor = Cursors.Hand;
                DragMove();
                Cursor = Cursors.Arrow;
            }
        }

        private void txTitle_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        private TimeSpan? TotalTimeRemaining = null;
        private DateTime? EndTime = null;
        private bool Started = false;

        private TimeSpan? ParseTime()
        {
            var txt = txTime.Text.Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            TimeSpan res = TimeSpan.Zero;
            foreach (var t in txt)
            {
                if (t.Length < 2) return null;
                var amt = t.Substring(0, t.Length - 1);
                int val;
                if (!int.TryParse(amt, out val)) return null;
                var unit = t.Substring(t.Length - 1);
                if (unit == "s")
                {
                    res += new TimeSpan(0, 0, val);
                }
                else if (unit == "m")
                {
                    res += new TimeSpan(0, val, 0);
                }
                else if (unit == "h")
                {
                    res += new TimeSpan(val, 0, 0);
                }
                else
                {
                    return null;
                }
            }
            return res;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (Started) return;
            var rem = ParseTime();
            if (rem == null)
            {
                MessageBox.Show("Invalid time expression", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            TotalTimeRemaining = rem;
            pbTracker.Maximum = rem.Value.TotalSeconds;
            pbTracker.Value = rem.Value.TotalSeconds;
            EndTime = DateTime.Now.Add(rem.Value);
            pbTracker.Foreground = brGreen;
            lbTimeRemaining.Foreground = brBlack;
            IsPaused = false;
            Started = true;
            btnPause.Content = "Pause";
            grConfig.Visibility = Visibility.Collapsed;
            grControl.Visibility = Visibility.Visible;
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            if (!IsPaused)
            {
                IsPaused = true;
                btnPause.Content = "Resume";
            }
            else
            {
                if (TotalTimeRemaining.HasValue)
                    EndTime = DateTime.Now.Add(TotalTimeRemaining.Value);
                IsPaused = false;
                btnPause.Content = "Pause";
            }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            Stop();
        }

        private void txTime_MouseMove(object sender, MouseEventArgs e)
        {
            e.Handled = true;
        }

        List<SolidColorBrush> Brushes { get; } = new List<SolidColorBrush>()
        {
            new SolidColorBrush(Colors.White),
            new SolidColorBrush(Colors.MistyRose),
            new SolidColorBrush(Colors.Honeydew),
            new SolidColorBrush(Colors.AliceBlue),
            new SolidColorBrush(Colors.LightGray),
        };
        int BrushIndex { get; set; } = 0;

        private void btnBackground_Click(object sender, RoutedEventArgs e)
        {
            BrushIndex++;
            var brush = Brushes[BrushIndex % Brushes.Count];
            grBG.Background = brush;
        }
    }
}
