﻿using Aura.Models;
using Aura.Services;

namespace Coatl.TimeTracker
{
    public class TimeTrackerService : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.timetracker";
        public override string ServiceName => "Time Tracker";

        public override string? ServiceIcon => "/Coatl.TimeTracker;Component/res/icons/clock.png";

        public override IReadOnlyList<IAuraWidget> Widgets { get; } = new List<IAuraWidget>()
        {
            new CountdownTimerWidget(),
            new LatencyTrackerWidget(),
        };
    }
}