﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Coatl.TimeTracker
{
    public class LatencyTrackerViewModel : INotifyPropertyChanged
    {
        const int MaxData = 10;
        private string name = "New Tracker";
        private string host = "google.com";
        private bool active;
        private Color color = Colors.Black;

        public string Name
        {
            get => name; set
            {
                name = value;
                NotifyPropertyChanged("Name");
            }
        }
        public string Host
        {
            get => host; set
            {
                host = value;
                NotifyPropertyChanged("Host");
            }
        }

        public bool Active
        {
            get => active; set
            {
                if (active != value)
                {
                    active = value;
                    NotifyPropertyChanged("ActiveValue");
                    NotifyPropertyChanged("Active");
                    NotifyPropertyChanged("SwitchBackground");
                }
            }
        }

        public double ActiveValue { get => Active ? 1 : 0; set => Active = value == 1; }

        public LatencyTrackerViewModel()
        {
            Latencies.CollectionChanged += Latencies_CollectionChanged;
        }

        private void Latencies_CollectionChanged(object? sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            NotifyPropertyChanged(nameof(Average));
            NotifyPropertyChanged(nameof(Min));
            NotifyPropertyChanged(nameof(Max));
            NotifyPropertyChanged(nameof(AverageMS));
            NotifyPropertyChanged(nameof(MinMS));
            NotifyPropertyChanged(nameof(MaxMS));
        }

        public void Add(double latency)
        {
            while (Latencies.Count >= MaxData)
            {
                Latencies.RemoveAt(0);
            }
            Latencies.Add(latency);
        }

        public ObservableCollection<double> Latencies { get; } = new ObservableCollection<double>();
        public double Average
        {
            get {
                var nonEmpty = Latencies.Where(x => x > 0);
                return nonEmpty.Count() > 0 ?
                    Math.Round(nonEmpty.Average()) :
                    -1;
            }
        }
        public string AverageMS
        {
            get => Average > 0 ? Math.Round(Average, 1) + " ms" : "- ms";
        }
        public double Min
        {
            get
            {
                var nonEmpty = Latencies.Where(x => x > 0);
                return nonEmpty.Count() > 0 ?
                    Math.Round(nonEmpty.Min()) :
                    -1;
            }
        }
        public string MinMS
        {
            get => Min > 0 ? Math.Round(Min, 1) + " ms" : "- ms";
        }
        public double Max
        {
            get
            {
                var nonEmpty = Latencies.Where(x => x > 0);
                return nonEmpty.Count() > 0 ?
                    Math.Round(nonEmpty.Max()) :
                    -1;
            }
        }
        public string MaxMS
        {
            get => Max > 0 ? Math.Round(Max, 1) + " ms" : "- ms";
        }

        public Color Color
        {
            get => color; set
            {
                color = value;
                NotifyPropertyChanged("Color");
                NotifyPropertyChanged("Foreground");
            }
        }
        public SolidColorBrush Foreground { get => new SolidColorBrush(Color); }

        public SolidColorBrush SwitchBackground { get => new SolidColorBrush(Active ? Colors.LimeGreen: Colors.Red); }

        private void NotifyPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        public event PropertyChangedEventHandler? PropertyChanged;
    }
}
