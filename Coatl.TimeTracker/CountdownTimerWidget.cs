﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Coatl.TimeTracker
{
    public class CountdownTimerWidget : IAuraWidget
    {
        public Guid Guid => Guid.Parse("896ecea951134159a723b2c04cf56bc7");

        public string Name => "Countdown Timer";

        public string IconUri => "/Coatl.TimeTracker;Component/res/icons/clock.png";

        public bool AllowMultiple => true;

        public ServiceManager? ServiceManager { get; set; }

        public Window CreateWidgetWindow()
        {
            return new WTimeTracker() { Tag = this, ServiceManager = ServiceManager };
        }
    }
}
