﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Coatl.TimeTracker
{
    public class LatencyTrackerWidget : IAuraWidget
    {
        public Guid Guid => Guid.Parse("ab9f89b84d7041298a6eb26e32b27e37");

        public string Name => "Latency Tracker";

        public string IconUri => "/Coatl.TimeTracker;Component/res/icons/monitor.png";

        public bool AllowMultiple => false;

        public ServiceManager? ServiceManager { get; set; }

        public Window CreateWidgetWindow()
        {
            return new WLatencyTracker() { Tag = this, ServiceManager = ServiceManager };
        }
    }
}
