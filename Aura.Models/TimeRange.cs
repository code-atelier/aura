﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public struct TimeRange
    {
        [JsonPropertyName("start")]
        public TimeSpan Start { get; set; }

        [JsonPropertyName("end")]
        public TimeSpan End { get; set; }

        [JsonIgnore]
        public bool IsAcrossDay { get => Start > End; }

        public bool Contains(TimeSpan timeOfDay)
        {
            if (!IsAcrossDay)
            {
                return timeOfDay >= Start && timeOfDay < End;
            }
            else
            {
                return timeOfDay >= Start || timeOfDay < End;
            }
        }

        public bool IntersectsWith(TimeRange timeRange)
        {
            return Contains(timeRange.Start) 
                || Contains(timeRange.End) 
                || timeRange.Contains(Start) 
                || timeRange.Contains(End);
        }

        public bool Contains(DateTime dateTime)
        {
            return Contains(dateTime.TimeOfDay);
        }
    }

    public struct DateOfYearRange
    {
        [JsonPropertyName("start")]
        public DateTime Start { get; set; }

        [JsonPropertyName("end")]
        public DateTime End { get; set; }

        [JsonIgnore]
        public bool IsAcrossYear { get => ToIntDate(Start) > ToIntDate(End); }

        public bool Contains(DateTime dateOfyear)
        {
            if (!IsAcrossYear)
            {
                return ToIntDate(dateOfyear) >= ToIntDate(Start) && ToIntDate(dateOfyear) < ToIntDate(End);
            }
            else
            {
                return ToIntDate(dateOfyear) >= ToIntDate(Start) || ToIntDate(dateOfyear) < ToIntDate(End);
            }
        }

        public bool IntersectsWith(DateOfYearRange dateOfYearRange)
        {
            return Contains(dateOfYearRange.Start)
                || Contains(dateOfYearRange.End)
                || dateOfYearRange.Contains(Start)
                || dateOfYearRange.Contains(End);
        }

        public static int ToIntDate(DateTime date)
        {
            return date.Day + date.Month * 100;
        }
    }
}
