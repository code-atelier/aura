﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class TagCollection : ICollection<string>
    {
        public TagCollection(List<string> tags)
        {
            this.tags = tags;
        }

        List<string> tags = new List<string>();

        public double CalculateWeight(Dictionary<string, int> weightedTags, bool positiveOnly = false)
        {
            double sum = 0;
            foreach (var kv in weightedTags)
            {
                var tag = kv.Key.Trim().ToLower();
                if (Contains(tag))
                {
                    sum += kv.Value;
                }
                else if (!positiveOnly)
                {
                    sum -= kv.Value;
                }
            }
            return sum;
        }

        public int Count => tags.Count;

        public bool IsReadOnly => true;

        public void Add(string item)
        {
            throw new NotSupportedException();
        }

        public void Clear()
        {
            throw new NotSupportedException();
        }

        public bool Contains(string item)
        {
            return tags.Contains(item);
        }

        public void CopyTo(string[] array, int arrayIndex)
        {
            tags.CopyTo(array, arrayIndex);
        }

        public IEnumerator<string> GetEnumerator()
        {
            return tags.GetEnumerator();
        }

        public bool Remove(string item)
        {
            throw new NotSupportedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
