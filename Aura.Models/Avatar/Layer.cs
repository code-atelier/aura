﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models.Avatar
{
    public class Layer
    {
        [JsonPropertyName("path")]
        public string Path { get; set; } = string.Empty;

        [JsonPropertyName("default")]
        public string? Default { get; set; }

        [JsonPropertyName("materials")]
        public List<string> Materials { get; set; } = new List<string>();

        [JsonPropertyName("zIndex")]
        public int ZIndex { get; set; }

        public string? GetMaterialPath(string material)
        {
            return System.IO.Path.Combine(Path, material).Replace("\\", "/");
        }
    }
}
