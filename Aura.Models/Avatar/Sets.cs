﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models.Avatar
{
    public class Sets : Dictionary<string, Set>
    {
        public IEnumerable<string> GetCategories(bool withColon = false)
        {
            var cats = new List<string>();
            foreach (var key in Keys)
            {
                if (key.Contains(':'))
                {
                    var spl = key.Split(':', 2, StringSplitOptions.TrimEntries);
                    cats.Add(spl[0].Trim() + (withColon ? ":" : ""));
                }
            }
            return cats.Distinct();
        }

        public Dictionary<string, Set> GetByCategory(string category)
        {
            var cat = category.Trim() + ":";
            var noCat = string.IsNullOrWhiteSpace(category);
            return this
                .Where(x => (noCat && !x.Key.Contains(":")) || x.Key.StartsWith(cat))
                .ToDictionary(x => x.Key, x => x.Value);
        }

        public Dictionary<string, Tuple<Set, int>> GetMostActiveSets(TagCollection tags)
        {
            var res = new Dictionary<string, Tuple<Set, int>>();
            var cats = GetCategories().ToList();

            foreach (var cat in cats)
            {
                var sets = GetByCategory(cat);
                var wMax = 0;
                KeyValuePair<string, Set>? maxSet = null;
                foreach (var set in sets)
                {
                    var w = tags.CalculateWeight(set.Value.Tags);
                    if (w > wMax)
                    {
                        wMax = (int)w;
                        maxSet = set;
                    }
                }
                if (maxSet != null)
                {
                    res.Add(maxSet.Value.Key, new Tuple<Set, int>(maxSet.Value.Value, wMax));
                }
            }

            // no cat
            var ncSets = GetByCategory("");
            foreach (var set in ncSets)
            {
                var w = tags.CalculateWeight(set.Value.Tags);
                if (w > 0)
                {
                    res.Add(set.Key, new Tuple<Set, int>(set.Value, (int)w));
                }
            }

            res = res.OrderBy(x => x.Value.Item2).ToDictionary(x => x.Key, x => x.Value);
            return res;
        }
    }
}
