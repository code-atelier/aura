﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models.Avatar
{
    public class AvatarManifest
    {
        [JsonPropertyName("layers")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public Layers Layers { get; set; } = new Layers();

        [JsonPropertyName("sets")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public Sets Sets { get; set; } = new Sets();

        [JsonPropertyName("regions")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public Dictionary<string, Regions> Regions { get; set; } = new Dictionary<string, Regions>();

        [JsonPropertyName("display")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public AvatarDisplay Display { get; set; } = new AvatarDisplay();
    }
}
