﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models.Avatar
{
    public class Set
    {
        [JsonPropertyName("layers")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public Dictionary<string, string?> Layers { get; set; } = new Dictionary<string, string?>();

        [JsonPropertyName("tags")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public Dictionary<string, int> Tags { get; set; } = new Dictionary<string, int>();
    }

    public static class SetExtension
    {
        public static string GetCategory(this KeyValuePair<string, Set> kv)
        {
            var spl = kv.Key.Split(':', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            if (spl.Length > 0) return spl[0].Trim();
            return "";
        }
    }
}
