﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models.Avatar
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum RegionType
    {
        Radius,
        Rectangle,
        Point
    }
}
