﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class ServiceMessage
    {
        public string Sender { get; }

        public Guid Guid { get; } = Guid.NewGuid();

        public ServiceMessageKind Kind { get; set; } = ServiceMessageKind.Event;

        public string IntendedReceiver { get; } = "*";

        public Dictionary<string, string> Headers { get; } = new Dictionary<string, string>();

        public object? Body { get; set; }

        public bool Handled { get; set; }

        public ServiceMessage? ReplyOf { get; set; }

        public ServiceMessage(string sender, string receiver = "*", object? body = null)
        {
            Sender = sender;
            IntendedReceiver = receiver;
            Body = body;
        }

        public ServiceMessage? FindMessageInChain(Guid guid)
        {
            if (ReplyOf == null) return null;
            if (ReplyOf.Guid == guid) return ReplyOf;
            return ReplyOf.FindMessageInChain(guid);
        }

        public string? this[string headerKey]
        {
            get
            {
                return Headers.ContainsKey(headerKey) ? Headers[headerKey] : null;
            }

            set
            {
                if (value != null)
                    Headers[headerKey] = value;
                else
                {
                    if (Headers.ContainsKey(headerKey))
                        Headers.Remove(headerKey);
                }
            }
        }

        public bool ContainsHeader(string headerKey)
        {
            return Headers.ContainsKey(headerKey);
        }

        public T? BodyIs<T>(Func<T, bool>? condition = null)
        {
            if (Body is T inst)
            {
                if (condition == null || condition.Invoke(inst))
                {
                    return inst;
                }
            }
            return default;
        }

        public ServiceMessage SetHeader(string key, string value = "true")
        {
            Headers[key] = value;
            return this;
        }

        public bool From(string sender)
        {
            return Sender == sender;
        }

        public bool HeaderIs(string key, string value = "true")
        {
            return ContainsHeader(key) && this[key] == value;
        }
    }

    public enum ServiceMessageKind
    {
        Event,
        Pool
    }

    public class PendingServiceMessageCollection : ICollection<PendingServiceMessage>
    {
        List<PendingServiceMessage> messages { get; } = new List<PendingServiceMessage>();

        public int Count => messages.Count;

        public bool IsReadOnly => false;

        public void Add(PendingServiceMessage item)
        {
            RemoveTimedOut();
            messages.Add(item);
        }

        public void Add(Guid queryGuid, ServiceMessage message, int? timeoutInMilliseconds = null)
        {
            RemoveTimedOut();
            var psm = new PendingServiceMessage(queryGuid, message, timeoutInMilliseconds);
            messages.Add(psm);
        }

        public void Clear()
        {
            messages.Clear();
        }

        public void RemoveTimedOut()
        {
            messages.RemoveAll(x => x.Expire <= DateTime.Now);
        }

        public bool Contains(PendingServiceMessage item)
        {
            RemoveTimedOut();
            return messages.Any(x => x.Guid == item.Guid);
        }

        public bool Contains(Guid guid)
        {
            RemoveTimedOut();
            return messages.Any(x => x.Guid == guid);
        }

        public bool Contains(string guid)
        {
            RemoveTimedOut();
            Guid iguid;
            if (Guid.TryParse(guid, out iguid)) {
                return messages.Any(x => x.Guid == iguid);
            }
            return false;
        }

        public void CopyTo(PendingServiceMessage[] array, int arrayIndex)
        {
            RemoveTimedOut();
            messages.CopyTo(array, arrayIndex);
        }

        public IEnumerator<PendingServiceMessage> GetEnumerator()
        {
            RemoveTimedOut();
            return messages.GetEnumerator();
        }

        public bool Remove(PendingServiceMessage item)
        {
            RemoveTimedOut();
            return messages.Remove(item);
        }

        public bool Remove(Guid guid)
        {
            RemoveTimedOut();
            return messages.RemoveAll(x => x.Guid == guid) > 0;
        }
        public bool Remove(string? guid)
        {
            RemoveTimedOut();
            Guid iguid;
            if (guid != null && Guid.TryParse(guid, out iguid))
                return Remove(iguid);
            return false;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public PendingServiceMessage? this[Guid guid]
        {
            get
            {
                if (!Contains(guid)) return null;
                return messages.FirstOrDefault(x => x.Guid == guid);
            }
        }

        public PendingServiceMessage? this[string? guid]
        {
            get
            {
                if (guid == null) return null;
                if (!Contains(guid)) return null;
                Guid iguid;
                if (Guid.TryParse(guid, out iguid))
                    return messages.FirstOrDefault(x => x.Guid == iguid);
                return null;
            }
        }
    }

    public class PendingServiceMessage
    {
        public ServiceMessage Message { get; set; }

        public Guid Guid { get; set; }

        public DateTime? Expire { get; set; }

        public PendingServiceMessage(Guid queryGuid, ServiceMessage message, int? timeoutMillisecond = null)
        {
            Message = message;
            Guid = queryGuid;
            Expire = timeoutMillisecond != null ? DateTime.Now.AddMilliseconds(timeoutMillisecond.Value) : null;
        }

        public object? Body => Message.Body;

        public string? this[string headerKey]
        {
            get => Message.Headers.ContainsKey(headerKey) ? Message.Headers[headerKey] : null;
        }
    }
}
