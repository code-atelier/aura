﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class AuraArchive
    {
        public AuraArchiveVirtualFileSystem FileSystem { get; } = new AuraArchiveVirtualFileSystem();
    }

    public class AuraArchiveVirtualFileSystem: IEnumerable<KeyValuePair<string, MemoryStream>>
    {
        private Dictionary<string, MemoryStream> _fs = new Dictionary<string, MemoryStream>();

        public MemoryStream this[string path]
        {
            get
            {
                if (!Exists(path))
                    throw new Exception("File not found in virtual file system: " + path);
                return _fs[_fs.Keys.First(x => x.ToLower() == path.ToLower())];
            }

            set
            {
                _fs[path.Trim()] = value;
            }
        }

        public bool Exists(string path)
        {
            return _fs.Keys.Any(x => x.ToLower() == path.ToLower().Trim());
        }

        public void Delete(string path)
        {
            MemoryStream? ms = this[path];
            try
            {
                ms.Dispose();
            }
            catch { }
            _fs.Remove(path);
        }

        public void Add(string path, Stream stream, long position = 0, SeekOrigin seekOrigin = SeekOrigin.Begin, long length = -1)
        {
            stream.Seek(position, seekOrigin);            
            var buf = new byte[length < 0 ? stream.Length - stream.Position : length];
            stream.Read(buf, 0, buf.Length);
            var ms = new MemoryStream(buf);
            if (Exists(path))
            {
                Delete(path);
            }
            _fs[path.Trim()] = ms;
        }

        public IEnumerator<KeyValuePair<string, MemoryStream>> GetEnumerator()
        {
            return _fs.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
