﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class ToastActivationData
    {
        public Dictionary<string, string> Arguments { get; set; } = new Dictionary<string, string>();

        public Dictionary<string, object> UserInput { get; set; } = new Dictionary<string, object>();
    }
}
