﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public static class InternalServices
    {
        public const string CoreService = "svc.coatl.core";

        public const string MessageService = "svc.coatl.message";

        public const string ScheduleService = "svc.coatl.schedule";

        public const string AvatarService = "svc.coatl.avatar";

        public const string ConsoleService = "svc.coatl.console";

        public const string EmotionService = "svc.coatl.emotion";

        public const string UI = "svc.aura.ui";
    }
}
