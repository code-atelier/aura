﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class ScheduleEntry
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("time")]
        public TimeRange? Time { get; set; }

        [JsonPropertyName("dateOfYear")]
        public DateOfYearRange? DateOfYear { get; set; }

        [JsonPropertyName("days")]
        public List<DayOfWeek>? Days { get; set; }

        [JsonPropertyName("date")]
        public DateTime? Date { get; set; }

        [JsonPropertyName("tags")]
        public List<string> Tags { get; set; } = new List<string>();

        public bool Contains(DateTime dateTime)
        {
            if (Days != null && Days.Count > 0)
            {
                if (!Days.Contains(dateTime.DayOfWeek))
                    return false;
            }
            if (Date != null)
            {
                if (Date.Value.Date != dateTime.Date)
                    return false;
            }
            if (Time != null && !Time.Value.Contains(dateTime))
            {
                return false;
            }
            if (DateOfYear != null && !DateOfYear.Value.Contains(dateTime))
            {
                return false;
            }
            return true;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
