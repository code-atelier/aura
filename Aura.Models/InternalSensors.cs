﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public static class InternalSensors
    {
        public const string DragDrop = "sensor.dragdrop";
        public const string Toast = "sensor.toastnotif";
        public const string ContextMenu = "sensor.contextmenu";
        public const string Region = "sensor.region";
        public const string RegionDrag = "sensor.regiondrag";
    }
}
