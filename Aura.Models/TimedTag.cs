﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class TimedTag
    {
        public string Tag { get; set; } = string.Empty;

        public DateTime Expire { get; set; } =  DateTime.MinValue;
    }
}
