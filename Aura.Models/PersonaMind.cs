﻿using Aura.AI.EmotionModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class PersonaMind
    {
        [JsonPropertyName("personality")]
        public IntrinsicEmotion Personality { get; set; } = new IntrinsicEmotion();
    }
}
