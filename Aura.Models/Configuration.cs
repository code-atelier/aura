﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Configuration
    {
        [JsonPropertyName("persona")]
        public string? Persona { get; set; }

        [JsonPropertyName("stayOnTop")]
        public bool StayOnTop { get; set; } = true;

        [JsonPropertyName("lockPosition")]
        public bool LockPosition { get; set; } = false;

        [JsonPropertyName("snapToScreen")]
        public bool SnapToScreen { get; set; } = true;

        [JsonPropertyName("positionLeft")]
        public double? PositionLeft { get; set; }

        [JsonPropertyName("positionTop")]
        public double? PositionTop { get; set; }

        [JsonPropertyName("allowAutoStart")]
        public Dictionary<string, bool> AllowAutoStart { get; set; } = new Dictionary<string, bool>();
    }
}
