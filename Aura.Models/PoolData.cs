﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class PoolData
    {
        public string ServiceID { get; }

        public object? Data { get; }

        public PoolData(string serviceID, object? data = null)
        {
            ServiceID = serviceID;
            Data = data;
        }
    }

    public class PoolDataCollection: Collection<PoolData>
    {
        public void Add(string serviceID, object? data = null)
        {
            Add(new PoolData(serviceID, data));
        }
    }

    public class PoolRequest
    {
        public PoolDataCollection PoolData { get; set; } = new PoolDataCollection();

        public object? RequestData { get; set; }
    }
}
