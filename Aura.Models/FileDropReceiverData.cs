﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class FileDropReceiverData
    {
        public string? Text { get; set; }

        public string? IconUri { get; set; }

        public string? Tag { get; set; }
    }
}
