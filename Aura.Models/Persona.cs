﻿using Aura.Models.Avatar;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Models
{
    public class Persona
    {
        public AvatarManifest Avatar { get; set; } = new AvatarManifest();

        public PersonaManifest Manifest { get; set; } = new PersonaManifest();

        public AuraArchive Archive { get; set; } = new AuraArchive();

        public Schedule Schedule { get; set; } = new Schedule();

        public PersonaMind Mind { get; set; } = new PersonaMind();

        public string? LastSavePath { get; set; }

        public const string PersonaPreviewPath = "res/persona.preview.png";
        const string PersonaManifestPath = "persona.manifest.json";
        const string AvatarManifestPath = "avatar.manifest.json";
        const string SchedulePath = "schedule.json";
        const string MindPath = "personality.json";

        public void Save(string? path = null)
        {
            var savePath = path ?? LastSavePath;
            if (savePath == null)
            {
                throw new Exception("Path must not be null");
            }

            var tempPath = "_temp_" + Guid.NewGuid().ToString("n");
            var tempDir = Directory.CreateDirectory(tempPath);
            tempDir.Attributes = FileAttributes.Hidden;

            try
            {
                var personaManifestJson = System.Text.Json.JsonSerializer.Serialize(Manifest);
                File.WriteAllText(Path.Combine(tempPath, PersonaManifestPath), personaManifestJson);
                var avatarManifestJson = System.Text.Json.JsonSerializer.Serialize(Avatar);
                File.WriteAllText(Path.Combine(tempPath, AvatarManifestPath), avatarManifestJson);
                var scheduleJson = System.Text.Json.JsonSerializer.Serialize(Schedule);
                File.WriteAllText(Path.Combine(tempPath, SchedulePath), scheduleJson);
                var mindJson = System.Text.Json.JsonSerializer.Serialize(Mind);
                File.WriteAllText(Path.Combine(tempPath, MindPath), mindJson);

                // write all files in archive
                foreach (var f in Archive.FileSystem)
                {
                    var fpath = Path.Combine(tempPath, Path.GetDirectoryName(f.Key) ?? "");
                    if (!Directory.Exists(fpath)) Directory.CreateDirectory(fpath);
                    fpath = Path.Combine(fpath, Path.GetFileName(f.Key));

                    var buf = new byte[f.Value.Length];
                    f.Value.Seek(0, SeekOrigin.Begin);
                    f.Value.Read(buf, 0, buf.Length);
                    File.WriteAllBytes(fpath, buf);
                }

                ZipFile.CreateFromDirectory(tempPath, savePath + ".new");
                if (File.Exists(savePath)) File.Delete(savePath);
                File.Move(savePath + ".new", savePath);
                LastSavePath = savePath;
            }
            finally
            {
                Directory.Delete(tempPath, true);
            }
        }

        public static Persona Load(string path)
        {
            if (!File.Exists(path)) throw new FileNotFoundException(path + " is not found");

            var tempPath = "_temp_" + Guid.NewGuid().ToString("n");
            var tempDir = Directory.CreateDirectory(tempPath);
            tempDir.Attributes = FileAttributes.Hidden;

            try
            {
                ZipFile.ExtractToDirectory(path, tempPath);

                var persona = new Persona();

                var personaManifestJson = File.ReadAllText(Path.Combine(tempPath, PersonaManifestPath));
                var manifest = System.Text.Json.JsonSerializer.Deserialize<PersonaManifest>(personaManifestJson);
                if (manifest == null) throw new Exception("Failed to read Persona manifest");
                persona.Manifest = manifest;

                var avatarManifestJson = File.ReadAllText(Path.Combine(tempPath, AvatarManifestPath));
                var avatar = System.Text.Json.JsonSerializer.Deserialize<AvatarManifest>(avatarManifestJson);
                if (avatar == null) throw new Exception("Failed to read Avatar manifest");
                persona.Avatar = avatar;

                if (File.Exists(Path.Combine(tempPath, SchedulePath)))
                {
                    var scheduleJson = File.ReadAllText(Path.Combine(tempPath, SchedulePath));
                    var schedule = System.Text.Json.JsonSerializer.Deserialize<Schedule>(scheduleJson);
                    if (schedule == null) throw new Exception("Failed to read Schedule");
                    persona.Schedule = schedule;
                }

                var resdir = Path.Combine(tempPath, "res");
                var files = Directory.EnumerateFiles(resdir, "*", SearchOption.AllDirectories).ToList();
                foreach (var file in files)
                {
                    var fpath = file.Trim('\\');
                    if (fpath.StartsWith(tempPath)) fpath = fpath.Substring(tempPath.Length);
                    fpath = fpath.Trim('\\').Replace('\\', '/');

                    persona.Archive.FileSystem.Add(fpath, new MemoryStream(File.ReadAllBytes(file)));
                }

                if (File.Exists(Path.Combine(tempPath, MindPath)))
                {
                    var mindJson = File.ReadAllText(Path.Combine(tempPath, MindPath));
                    var mind = System.Text.Json.JsonSerializer.Deserialize<PersonaMind>(mindJson);
                    if (mind == null) throw new Exception("Failed to read Mind manifest");
                    persona.Mind = mind;
                }

                persona.LastSavePath = path;
                return persona;
            }
            finally
            {
                Directory.Delete(tempPath, true);
            }

            throw new Exception("Failed to load Persona");
        }
    }
}
