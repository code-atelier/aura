﻿namespace PersonaMaker.Forms
{
    partial class FAvatarEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbMats = new System.Windows.Forms.ListBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pbMatPreview = new System.Windows.Forms.PictureBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnMatClearDefault = new System.Windows.Forms.Button();
            this.btnMatSetDefault = new System.Windows.Forms.Button();
            this.btnRemoveMats = new System.Windows.Forms.Button();
            this.btnAddMats = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbLayers = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txNewLayerName = new System.Windows.Forms.TextBox();
            this.btnMoveLayerDown = new System.Windows.Forms.Button();
            this.btnMoveLayerUp = new System.Windows.Forms.Button();
            this.btnRemoveLayer = new System.Windows.Forms.Button();
            this.btnAddLayer = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgvSetTags = new System.Windows.Forms.DataGridView();
            this.cTag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Weight = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbSetMaterials = new System.Windows.Forms.GroupBox();
            this.pnlSetMaterialContainer = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lbSets = new System.Windows.Forms.ListBox();
            this.pnlSetPreview = new System.Windows.Forms.Panel();
            this.pbSetPreview = new System.Windows.Forms.PictureBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txSetName = new System.Windows.Forms.TextBox();
            this.btnSetRemove = new System.Windows.Forms.Button();
            this.btnSetAdd = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.pbRegionPreview = new System.Windows.Forms.PictureBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lbRegions = new System.Windows.Forms.ListBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.gbRegionTags = new System.Windows.Forms.GroupBox();
            this.dgvRegionTag = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel16 = new System.Windows.Forms.Panel();
            this.gbRegionSettings = new System.Windows.Forms.GroupBox();
            this.txRegionX = new System.Windows.Forms.NumericUpDown();
            this.rbPoint = new System.Windows.Forms.RadioButton();
            this.rbRectangle = new System.Windows.Forms.RadioButton();
            this.txRegionY = new System.Windows.Forms.NumericUpDown();
            this.rbCircle = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txRegionRadius = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.txRegionHeight = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txRegionWidth = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel19 = new System.Windows.Forms.Panel();
            this.cbRegionId = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnRegionRemove = new System.Windows.Forms.Button();
            this.btnRegionAdd = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnApplyScale = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.txScale = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txHeight = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.txWidth = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.pbRealPreview = new System.Windows.Forms.PictureBox();
            this.ofdPNG = new System.Windows.Forms.OpenFileDialog();
            this.tab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbMatPreview)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetTags)).BeginInit();
            this.gbSetMaterials.SuspendLayout();
            this.panel10.SuspendLayout();
            this.pnlSetPreview.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSetPreview)).BeginInit();
            this.panel7.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRegionPreview)).BeginInit();
            this.panel14.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.panel15.SuspendLayout();
            this.gbRegionTags.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegionTag)).BeginInit();
            this.panel16.SuspendLayout();
            this.gbRegionSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionRadius)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionWidth)).BeginInit();
            this.panel19.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel18.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txWidth)).BeginInit();
            this.panel9.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRealPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // tab
            // 
            this.tab.Controls.Add(this.tabPage1);
            this.tab.Controls.Add(this.tabPage2);
            this.tab.Controls.Add(this.tabPage3);
            this.tab.Controls.Add(this.tabPage4);
            this.tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab.Location = new System.Drawing.Point(0, 0);
            this.tab.Name = "tab";
            this.tab.SelectedIndex = 0;
            this.tab.Size = new System.Drawing.Size(1084, 561);
            this.tab.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1076, 533);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Layers";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupBox2);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(390, 3);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(8);
            this.panel3.Size = new System.Drawing.Size(683, 527);
            this.panel3.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbMats);
            this.groupBox2.Controls.Add(this.panel5);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(8, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox2.Size = new System.Drawing.Size(667, 477);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Layer Materials";
            // 
            // lbMats
            // 
            this.lbMats.AllowDrop = true;
            this.lbMats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMats.FormattingEnabled = true;
            this.lbMats.ItemHeight = 15;
            this.lbMats.Location = new System.Drawing.Point(8, 24);
            this.lbMats.Name = "lbMats";
            this.lbMats.Size = new System.Drawing.Size(285, 445);
            this.lbMats.TabIndex = 0;
            this.lbMats.SelectedIndexChanged += new System.EventHandler(this.lbMats_SelectedIndexChanged);
            this.lbMats.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbMats_DragDrop);
            this.lbMats.DragOver += new System.Windows.Forms.DragEventHandler(this.lbMats_DragOver);
            this.lbMats.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbMats_KeyDown);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pbMatPreview);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(293, 24);
            this.panel5.Name = "panel5";
            this.panel5.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.panel5.Size = new System.Drawing.Size(366, 445);
            this.panel5.TabIndex = 1;
            // 
            // pbMatPreview
            // 
            this.pbMatPreview.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pbMatPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbMatPreview.Location = new System.Drawing.Point(8, 0);
            this.pbMatPreview.Name = "pbMatPreview";
            this.pbMatPreview.Size = new System.Drawing.Size(358, 445);
            this.pbMatPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbMatPreview.TabIndex = 0;
            this.pbMatPreview.TabStop = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnMatClearDefault);
            this.panel4.Controls.Add(this.btnMatSetDefault);
            this.panel4.Controls.Add(this.btnRemoveMats);
            this.panel4.Controls.Add(this.btnAddMats);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(8, 485);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(667, 34);
            this.panel4.TabIndex = 2;
            // 
            // btnMatClearDefault
            // 
            this.btnMatClearDefault.Enabled = false;
            this.btnMatClearDefault.Location = new System.Drawing.Point(238, 5);
            this.btnMatClearDefault.Name = "btnMatClearDefault";
            this.btnMatClearDefault.Size = new System.Drawing.Size(88, 23);
            this.btnMatClearDefault.TabIndex = 3;
            this.btnMatClearDefault.Text = "Clear Default";
            this.btnMatClearDefault.UseVisualStyleBackColor = true;
            this.btnMatClearDefault.Click += new System.EventHandler(this.btnMatClearDefault_Click);
            // 
            // btnMatSetDefault
            // 
            this.btnMatSetDefault.Enabled = false;
            this.btnMatSetDefault.Location = new System.Drawing.Point(157, 5);
            this.btnMatSetDefault.Name = "btnMatSetDefault";
            this.btnMatSetDefault.Size = new System.Drawing.Size(75, 23);
            this.btnMatSetDefault.TabIndex = 2;
            this.btnMatSetDefault.Text = "Set Default";
            this.btnMatSetDefault.UseVisualStyleBackColor = true;
            this.btnMatSetDefault.Click += new System.EventHandler(this.btnMatSetDefault_Click);
            // 
            // btnRemoveMats
            // 
            this.btnRemoveMats.Enabled = false;
            this.btnRemoveMats.Location = new System.Drawing.Point(76, 5);
            this.btnRemoveMats.Name = "btnRemoveMats";
            this.btnRemoveMats.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveMats.TabIndex = 1;
            this.btnRemoveMats.Text = "Remove";
            this.btnRemoveMats.UseVisualStyleBackColor = true;
            this.btnRemoveMats.Click += new System.EventHandler(this.btnRemoveMats_Click);
            // 
            // btnAddMats
            // 
            this.btnAddMats.Enabled = false;
            this.btnAddMats.Location = new System.Drawing.Point(8, 5);
            this.btnAddMats.Name = "btnAddMats";
            this.btnAddMats.Size = new System.Drawing.Size(62, 23);
            this.btnAddMats.TabIndex = 0;
            this.btnAddMats.Text = "Add";
            this.btnAddMats.UseVisualStyleBackColor = true;
            this.btnAddMats.Click += new System.EventHandler(this.btnAddMats_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Margin = new System.Windows.Forms.Padding(8);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(8);
            this.panel1.Size = new System.Drawing.Size(387, 527);
            this.panel1.TabIndex = 3;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbLayers);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox1.Size = new System.Drawing.Size(371, 477);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Layers";
            // 
            // lbLayers
            // 
            this.lbLayers.AllowDrop = true;
            this.lbLayers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbLayers.FormattingEnabled = true;
            this.lbLayers.ItemHeight = 15;
            this.lbLayers.Location = new System.Drawing.Point(8, 24);
            this.lbLayers.Name = "lbLayers";
            this.lbLayers.Size = new System.Drawing.Size(355, 445);
            this.lbLayers.TabIndex = 0;
            this.lbLayers.SelectedIndexChanged += new System.EventHandler(this.lbLayers_SelectedIndexChanged);
            this.lbLayers.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbLayers_DragDrop);
            this.lbLayers.DragOver += new System.Windows.Forms.DragEventHandler(this.lbLayers_DragOver);
            this.lbLayers.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbLayers_KeyDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txNewLayerName);
            this.panel2.Controls.Add(this.btnMoveLayerDown);
            this.panel2.Controls.Add(this.btnMoveLayerUp);
            this.panel2.Controls.Add(this.btnRemoveLayer);
            this.panel2.Controls.Add(this.btnAddLayer);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(8, 485);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(371, 34);
            this.panel2.TabIndex = 1;
            // 
            // txNewLayerName
            // 
            this.txNewLayerName.Location = new System.Drawing.Point(8, 6);
            this.txNewLayerName.Name = "txNewLayerName";
            this.txNewLayerName.Size = new System.Drawing.Size(124, 23);
            this.txNewLayerName.TabIndex = 0;
            this.txNewLayerName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txNewLayerName_KeyDown);
            this.txNewLayerName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txNewLayerName_KeyUp);
            // 
            // btnMoveLayerDown
            // 
            this.btnMoveLayerDown.Enabled = false;
            this.btnMoveLayerDown.Location = new System.Drawing.Point(329, 5);
            this.btnMoveLayerDown.Name = "btnMoveLayerDown";
            this.btnMoveLayerDown.Size = new System.Drawing.Size(34, 23);
            this.btnMoveLayerDown.TabIndex = 4;
            this.btnMoveLayerDown.Text = "▼";
            this.btnMoveLayerDown.UseVisualStyleBackColor = true;
            this.btnMoveLayerDown.Click += new System.EventHandler(this.btnMoveLayerDown_Click);
            // 
            // btnMoveLayerUp
            // 
            this.btnMoveLayerUp.Enabled = false;
            this.btnMoveLayerUp.Location = new System.Drawing.Point(289, 5);
            this.btnMoveLayerUp.Name = "btnMoveLayerUp";
            this.btnMoveLayerUp.Size = new System.Drawing.Size(34, 23);
            this.btnMoveLayerUp.TabIndex = 3;
            this.btnMoveLayerUp.Text = "▲";
            this.btnMoveLayerUp.UseVisualStyleBackColor = true;
            this.btnMoveLayerUp.Click += new System.EventHandler(this.btnMoveLayerUp_Click);
            // 
            // btnRemoveLayer
            // 
            this.btnRemoveLayer.Enabled = false;
            this.btnRemoveLayer.Location = new System.Drawing.Point(208, 5);
            this.btnRemoveLayer.Name = "btnRemoveLayer";
            this.btnRemoveLayer.Size = new System.Drawing.Size(75, 23);
            this.btnRemoveLayer.TabIndex = 2;
            this.btnRemoveLayer.Text = "Remove";
            this.btnRemoveLayer.UseVisualStyleBackColor = true;
            this.btnRemoveLayer.Click += new System.EventHandler(this.btnRemoveLayer_Click);
            // 
            // btnAddLayer
            // 
            this.btnAddLayer.Enabled = false;
            this.btnAddLayer.Location = new System.Drawing.Point(138, 5);
            this.btnAddLayer.Name = "btnAddLayer";
            this.btnAddLayer.Size = new System.Drawing.Size(64, 23);
            this.btnAddLayer.TabIndex = 1;
            this.btnAddLayer.Text = "Add";
            this.btnAddLayer.UseVisualStyleBackColor = true;
            this.btnAddLayer.Click += new System.EventHandler(this.btnAddLayer_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.panel6);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1076, 533);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Material Sets";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.groupBox3);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(3, 3);
            this.panel6.Name = "panel6";
            this.panel6.Padding = new System.Windows.Forms.Padding(8);
            this.panel6.Size = new System.Drawing.Size(1070, 527);
            this.panel6.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.panel8);
            this.groupBox3.Controls.Add(this.pnlSetPreview);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(8, 8);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox3.Size = new System.Drawing.Size(1054, 477);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Sets";
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.panel11);
            this.panel8.Controls.Add(this.panel10);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(8, 24);
            this.panel8.Name = "panel8";
            this.panel8.Padding = new System.Windows.Forms.Padding(0, 0, 8, 0);
            this.panel8.Size = new System.Drawing.Size(680, 445);
            this.panel8.TabIndex = 0;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.gbSetMaterials);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel11.Location = new System.Drawing.Point(0, 187);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(672, 258);
            this.panel11.TabIndex = 1;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.groupBox5);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel12.Location = new System.Drawing.Point(356, 0);
            this.panel12.Name = "panel12";
            this.panel12.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.panel12.Size = new System.Drawing.Size(316, 258);
            this.panel12.TabIndex = 3;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgvSetTags);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(8, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox5.Size = new System.Drawing.Size(308, 258);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tag Weight";
            // 
            // dgvSetTags
            // 
            this.dgvSetTags.AllowUserToResizeColumns = false;
            this.dgvSetTags.AllowUserToResizeRows = false;
            this.dgvSetTags.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSetTags.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cTag,
            this.Weight});
            this.dgvSetTags.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSetTags.Enabled = false;
            this.dgvSetTags.Location = new System.Drawing.Point(8, 24);
            this.dgvSetTags.Name = "dgvSetTags";
            this.dgvSetTags.RowHeadersVisible = false;
            this.dgvSetTags.RowTemplate.Height = 25;
            this.dgvSetTags.Size = new System.Drawing.Size(292, 226);
            this.dgvSetTags.TabIndex = 0;
            this.dgvSetTags.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvSetTags_CellEndEdit);
            this.dgvSetTags.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgvSetTags_DataError);
            this.dgvSetTags.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvSetTags_KeyDown);
            // 
            // cTag
            // 
            this.cTag.HeaderText = "Tag";
            this.cTag.Name = "cTag";
            this.cTag.Width = 150;
            // 
            // Weight
            // 
            this.Weight.HeaderText = "Weight";
            this.Weight.Name = "Weight";
            // 
            // gbSetMaterials
            // 
            this.gbSetMaterials.Controls.Add(this.pnlSetMaterialContainer);
            this.gbSetMaterials.Dock = System.Windows.Forms.DockStyle.Left;
            this.gbSetMaterials.Location = new System.Drawing.Point(0, 0);
            this.gbSetMaterials.Name = "gbSetMaterials";
            this.gbSetMaterials.Padding = new System.Windows.Forms.Padding(8);
            this.gbSetMaterials.Size = new System.Drawing.Size(356, 258);
            this.gbSetMaterials.TabIndex = 2;
            this.gbSetMaterials.TabStop = false;
            this.gbSetMaterials.Text = "Set Material";
            // 
            // pnlSetMaterialContainer
            // 
            this.pnlSetMaterialContainer.AutoScroll = true;
            this.pnlSetMaterialContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSetMaterialContainer.Enabled = false;
            this.pnlSetMaterialContainer.Location = new System.Drawing.Point(8, 24);
            this.pnlSetMaterialContainer.Name = "pnlSetMaterialContainer";
            this.pnlSetMaterialContainer.Size = new System.Drawing.Size(340, 226);
            this.pnlSetMaterialContainer.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.lbSets);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this.panel10.Size = new System.Drawing.Size(672, 187);
            this.panel10.TabIndex = 0;
            // 
            // lbSets
            // 
            this.lbSets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSets.FormattingEnabled = true;
            this.lbSets.ItemHeight = 15;
            this.lbSets.Location = new System.Drawing.Point(0, 0);
            this.lbSets.Name = "lbSets";
            this.lbSets.Size = new System.Drawing.Size(672, 179);
            this.lbSets.TabIndex = 2;
            this.lbSets.SelectedIndexChanged += new System.EventHandler(this.lbSets_SelectedIndexChanged);
            this.lbSets.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbSets_KeyDown);
            // 
            // pnlSetPreview
            // 
            this.pnlSetPreview.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pnlSetPreview.Controls.Add(this.pbSetPreview);
            this.pnlSetPreview.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnlSetPreview.Location = new System.Drawing.Point(688, 24);
            this.pnlSetPreview.Name = "pnlSetPreview";
            this.pnlSetPreview.Size = new System.Drawing.Size(358, 445);
            this.pnlSetPreview.TabIndex = 1;
            // 
            // pbSetPreview
            // 
            this.pbSetPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbSetPreview.Location = new System.Drawing.Point(0, 0);
            this.pbSetPreview.Name = "pbSetPreview";
            this.pbSetPreview.Size = new System.Drawing.Size(358, 445);
            this.pbSetPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbSetPreview.TabIndex = 0;
            this.pbSetPreview.TabStop = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txSetName);
            this.panel7.Controls.Add(this.btnSetRemove);
            this.panel7.Controls.Add(this.btnSetAdd);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(8, 485);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1054, 34);
            this.panel7.TabIndex = 3;
            // 
            // txSetName
            // 
            this.txSetName.Location = new System.Drawing.Point(8, 6);
            this.txSetName.Name = "txSetName";
            this.txSetName.Size = new System.Drawing.Size(124, 23);
            this.txSetName.TabIndex = 5;
            this.txSetName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txSetName_KeyDown);
            this.txSetName.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txSetName_KeyUp);
            // 
            // btnSetRemove
            // 
            this.btnSetRemove.Enabled = false;
            this.btnSetRemove.Location = new System.Drawing.Point(219, 6);
            this.btnSetRemove.Name = "btnSetRemove";
            this.btnSetRemove.Size = new System.Drawing.Size(75, 23);
            this.btnSetRemove.TabIndex = 1;
            this.btnSetRemove.Text = "Remove";
            this.btnSetRemove.UseVisualStyleBackColor = true;
            this.btnSetRemove.Click += new System.EventHandler(this.btnSetRemove_Click);
            // 
            // btnSetAdd
            // 
            this.btnSetAdd.Enabled = false;
            this.btnSetAdd.Location = new System.Drawing.Point(138, 6);
            this.btnSetAdd.Name = "btnSetAdd";
            this.btnSetAdd.Size = new System.Drawing.Size(75, 23);
            this.btnSetAdd.TabIndex = 0;
            this.btnSetAdd.Text = "Add";
            this.btnSetAdd.UseVisualStyleBackColor = true;
            this.btnSetAdd.Click += new System.EventHandler(this.btnSetAdd_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.panel13);
            this.tabPage3.Controls.Add(this.panel14);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1076, 533);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Regions";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.AutoScroll = true;
            this.panel13.Controls.Add(this.pbRegionPreview);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Margin = new System.Windows.Forms.Padding(8);
            this.panel13.Name = "panel13";
            this.panel13.Padding = new System.Windows.Forms.Padding(8);
            this.panel13.Size = new System.Drawing.Size(387, 533);
            this.panel13.TabIndex = 4;
            // 
            // pbRegionPreview
            // 
            this.pbRegionPreview.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pbRegionPreview.Location = new System.Drawing.Point(11, 11);
            this.pbRegionPreview.Name = "pbRegionPreview";
            this.pbRegionPreview.Size = new System.Drawing.Size(268, 352);
            this.pbRegionPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRegionPreview.TabIndex = 1;
            this.pbRegionPreview.TabStop = false;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.groupBox6);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel14.Location = new System.Drawing.Point(387, 0);
            this.panel14.Margin = new System.Windows.Forms.Padding(8);
            this.panel14.Name = "panel14";
            this.panel14.Padding = new System.Windows.Forms.Padding(8);
            this.panel14.Size = new System.Drawing.Size(689, 533);
            this.panel14.TabIndex = 5;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lbRegions);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(8, 8);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox6.Size = new System.Drawing.Size(673, 267);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Regions";
            // 
            // lbRegions
            // 
            this.lbRegions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbRegions.FormattingEnabled = true;
            this.lbRegions.ItemHeight = 15;
            this.lbRegions.Location = new System.Drawing.Point(8, 24);
            this.lbRegions.Name = "lbRegions";
            this.lbRegions.Size = new System.Drawing.Size(657, 235);
            this.lbRegions.TabIndex = 0;
            this.lbRegions.SelectedIndexChanged += new System.EventHandler(this.lbRegions_SelectedIndexChanged);
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.gbRegionTags);
            this.panel15.Controls.Add(this.panel16);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel15.Location = new System.Drawing.Point(8, 275);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(673, 250);
            this.panel15.TabIndex = 1;
            // 
            // gbRegionTags
            // 
            this.gbRegionTags.Controls.Add(this.dgvRegionTag);
            this.gbRegionTags.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbRegionTags.Enabled = false;
            this.gbRegionTags.Location = new System.Drawing.Point(348, 0);
            this.gbRegionTags.Name = "gbRegionTags";
            this.gbRegionTags.Padding = new System.Windows.Forms.Padding(8);
            this.gbRegionTags.Size = new System.Drawing.Size(325, 250);
            this.gbRegionTags.TabIndex = 17;
            this.gbRegionTags.TabStop = false;
            this.gbRegionTags.Text = "Tag Weight";
            // 
            // dgvRegionTag
            // 
            this.dgvRegionTag.AllowUserToResizeColumns = false;
            this.dgvRegionTag.AllowUserToResizeRows = false;
            this.dgvRegionTag.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvRegionTag.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgvRegionTag.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvRegionTag.Location = new System.Drawing.Point(8, 24);
            this.dgvRegionTag.Name = "dgvRegionTag";
            this.dgvRegionTag.RowHeadersVisible = false;
            this.dgvRegionTag.RowTemplate.Height = 25;
            this.dgvRegionTag.Size = new System.Drawing.Size(309, 218);
            this.dgvRegionTag.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Tag";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 150;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Weight";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.gbRegionSettings);
            this.panel16.Controls.Add(this.panel19);
            this.panel16.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel16.Location = new System.Drawing.Point(0, 0);
            this.panel16.Name = "panel16";
            this.panel16.Padding = new System.Windows.Forms.Padding(0, 8, 8, 0);
            this.panel16.Size = new System.Drawing.Size(348, 250);
            this.panel16.TabIndex = 16;
            // 
            // gbRegionSettings
            // 
            this.gbRegionSettings.Controls.Add(this.txRegionX);
            this.gbRegionSettings.Controls.Add(this.rbPoint);
            this.gbRegionSettings.Controls.Add(this.rbRectangle);
            this.gbRegionSettings.Controls.Add(this.txRegionY);
            this.gbRegionSettings.Controls.Add(this.rbCircle);
            this.gbRegionSettings.Controls.Add(this.label1);
            this.gbRegionSettings.Controls.Add(this.txRegionRadius);
            this.gbRegionSettings.Controls.Add(this.label2);
            this.gbRegionSettings.Controls.Add(this.txRegionHeight);
            this.gbRegionSettings.Controls.Add(this.label3);
            this.gbRegionSettings.Controls.Add(this.txRegionWidth);
            this.gbRegionSettings.Controls.Add(this.label6);
            this.gbRegionSettings.Controls.Add(this.label5);
            this.gbRegionSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbRegionSettings.Enabled = false;
            this.gbRegionSettings.Location = new System.Drawing.Point(0, 8);
            this.gbRegionSettings.Name = "gbRegionSettings";
            this.gbRegionSettings.Size = new System.Drawing.Size(340, 206);
            this.gbRegionSettings.TabIndex = 31;
            this.gbRegionSettings.TabStop = false;
            this.gbRegionSettings.Text = "Region Settings";
            // 
            // txRegionX
            // 
            this.txRegionX.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txRegionX.Location = new System.Drawing.Point(43, 33);
            this.txRegionX.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txRegionX.Name = "txRegionX";
            this.txRegionX.Size = new System.Drawing.Size(60, 23);
            this.txRegionX.TabIndex = 1;
            this.txRegionX.ValueChanged += new System.EventHandler(this.txRegionX_ValueChanged);
            // 
            // rbPoint
            // 
            this.rbPoint.AutoSize = true;
            this.rbPoint.Location = new System.Drawing.Point(19, 171);
            this.rbPoint.Name = "rbPoint";
            this.rbPoint.Size = new System.Drawing.Size(53, 19);
            this.rbPoint.TabIndex = 8;
            this.rbPoint.Text = "Point";
            this.rbPoint.UseVisualStyleBackColor = true;
            this.rbPoint.CheckedChanged += new System.EventHandler(this.rbPoint_CheckedChanged);
            // 
            // rbRectangle
            // 
            this.rbRectangle.AutoSize = true;
            this.rbRectangle.Checked = true;
            this.rbRectangle.Location = new System.Drawing.Point(19, 60);
            this.rbRectangle.Name = "rbRectangle";
            this.rbRectangle.Size = new System.Drawing.Size(77, 19);
            this.rbRectangle.TabIndex = 3;
            this.rbRectangle.TabStop = true;
            this.rbRectangle.Text = "Rectangle";
            this.rbRectangle.UseVisualStyleBackColor = true;
            this.rbRectangle.CheckedChanged += new System.EventHandler(this.rbRectangle_CheckedChanged);
            // 
            // txRegionY
            // 
            this.txRegionY.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txRegionY.Location = new System.Drawing.Point(142, 33);
            this.txRegionY.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txRegionY.Name = "txRegionY";
            this.txRegionY.Size = new System.Drawing.Size(60, 23);
            this.txRegionY.TabIndex = 2;
            this.txRegionY.ValueChanged += new System.EventHandler(this.txRegionY_ValueChanged);
            // 
            // rbCircle
            // 
            this.rbCircle.AutoSize = true;
            this.rbCircle.Location = new System.Drawing.Point(19, 117);
            this.rbCircle.Name = "rbCircle";
            this.rbCircle.Size = new System.Drawing.Size(55, 19);
            this.rbCircle.TabIndex = 6;
            this.rbCircle.Text = "Circle";
            this.rbCircle.UseVisualStyleBackColor = true;
            this.rbCircle.CheckedChanged += new System.EventHandler(this.rbCircle_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 20;
            this.label1.Text = "Width";
            // 
            // txRegionRadius
            // 
            this.txRegionRadius.Enabled = false;
            this.txRegionRadius.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txRegionRadius.Location = new System.Drawing.Point(67, 142);
            this.txRegionRadius.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txRegionRadius.Name = "txRegionRadius";
            this.txRegionRadius.Size = new System.Drawing.Size(60, 23);
            this.txRegionRadius.TabIndex = 7;
            this.txRegionRadius.ValueChanged += new System.EventHandler(this.txRegionRadius_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 15);
            this.label2.TabIndex = 22;
            this.label2.Text = "Height";
            // 
            // txRegionHeight
            // 
            this.txRegionHeight.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txRegionHeight.Location = new System.Drawing.Point(184, 88);
            this.txRegionHeight.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txRegionHeight.Name = "txRegionHeight";
            this.txRegionHeight.Size = new System.Drawing.Size(60, 23);
            this.txRegionHeight.TabIndex = 5;
            this.txRegionHeight.ValueChanged += new System.EventHandler(this.txRegionHeight_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 15);
            this.label3.TabIndex = 24;
            this.label3.Text = "Radius";
            // 
            // txRegionWidth
            // 
            this.txRegionWidth.Increment = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txRegionWidth.Location = new System.Drawing.Point(64, 88);
            this.txRegionWidth.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.txRegionWidth.Name = "txRegionWidth";
            this.txRegionWidth.Size = new System.Drawing.Size(60, 23);
            this.txRegionWidth.TabIndex = 4;
            this.txRegionWidth.ValueChanged += new System.EventHandler(this.txRegionWidth_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 15);
            this.label6.TabIndex = 28;
            this.label6.Text = "X";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(117, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 15);
            this.label5.TabIndex = 30;
            this.label5.Text = "Y";
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.cbRegionId);
            this.panel19.Controls.Add(this.label4);
            this.panel19.Controls.Add(this.btnRegionRemove);
            this.panel19.Controls.Add(this.btnRegionAdd);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel19.Location = new System.Drawing.Point(0, 214);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(340, 36);
            this.panel19.TabIndex = 32;
            // 
            // cbRegionId
            // 
            this.cbRegionId.FormattingEnabled = true;
            this.cbRegionId.Items.AddRange(new object[] {
            "head",
            "neck",
            "chest",
            "breast-r",
            "breast-l",
            "hand-r",
            "hand-l",
            "stomach",
            "crotch",
            "thigh-r",
            "thigh-l",
            "leg-r",
            "leg-l",
            "foot-r",
            "foot-l",
            "baloon-r",
            "baloon-l"});
            this.cbRegionId.Location = new System.Drawing.Point(37, 8);
            this.cbRegionId.Name = "cbRegionId";
            this.cbRegionId.Size = new System.Drawing.Size(145, 23);
            this.cbRegionId.TabIndex = 0;
            this.cbRegionId.TextChanged += new System.EventHandler(this.cbRegionId_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(18, 15);
            this.label4.TabIndex = 26;
            this.label4.Text = "ID";
            // 
            // btnRegionRemove
            // 
            this.btnRegionRemove.Enabled = false;
            this.btnRegionRemove.Location = new System.Drawing.Point(258, 8);
            this.btnRegionRemove.Name = "btnRegionRemove";
            this.btnRegionRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRegionRemove.TabIndex = 10;
            this.btnRegionRemove.Text = "Remove";
            this.btnRegionRemove.UseVisualStyleBackColor = true;
            this.btnRegionRemove.Click += new System.EventHandler(this.btnRegionRemove_Click);
            // 
            // btnRegionAdd
            // 
            this.btnRegionAdd.Enabled = false;
            this.btnRegionAdd.Location = new System.Drawing.Point(188, 8);
            this.btnRegionAdd.Name = "btnRegionAdd";
            this.btnRegionAdd.Size = new System.Drawing.Size(64, 23);
            this.btnRegionAdd.TabIndex = 9;
            this.btnRegionAdd.Text = "Add";
            this.btnRegionAdd.UseVisualStyleBackColor = true;
            this.btnRegionAdd.Click += new System.EventHandler(this.btnRegionAdd_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel18);
            this.tabPage4.Controls.Add(this.panel9);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1076, 533);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Display";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.groupBox4);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel18.Location = new System.Drawing.Point(387, 0);
            this.panel18.Name = "panel18";
            this.panel18.Padding = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.panel18.Size = new System.Drawing.Size(689, 533);
            this.panel18.TabIndex = 6;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnApplyScale);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.txScale);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txHeight);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.txWidth);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(8, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox4.Size = new System.Drawing.Size(673, 533);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Display Configuration";
            // 
            // btnApplyScale
            // 
            this.btnApplyScale.Location = new System.Drawing.Point(169, 26);
            this.btnApplyScale.Name = "btnApplyScale";
            this.btnApplyScale.Size = new System.Drawing.Size(89, 23);
            this.btnApplyScale.TabIndex = 1;
            this.btnApplyScale.Text = "Apply Scale";
            this.btnApplyScale.UseVisualStyleBackColor = true;
            this.btnApplyScale.Click += new System.EventHandler(this.btnApplyScale_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(146, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 15);
            this.label11.TabIndex = 6;
            this.label11.Text = "%";
            // 
            // txScale
            // 
            this.txScale.Location = new System.Drawing.Point(80, 26);
            this.txScale.Name = "txScale";
            this.txScale.Size = new System.Drawing.Size(60, 23);
            this.txScale.TabIndex = 0;
            this.txScale.Text = "100";
            this.txScale.TextChanged += new System.EventHandler(this.txScale_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(17, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(34, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "Scale";
            // 
            // txHeight
            // 
            this.txHeight.Location = new System.Drawing.Point(80, 104);
            this.txHeight.Maximum = new decimal(new int[] {
            10240,
            0,
            0,
            0});
            this.txHeight.Name = "txHeight";
            this.txHeight.Size = new System.Drawing.Size(60, 23);
            this.txHeight.TabIndex = 3;
            this.txHeight.ValueChanged += new System.EventHandler(this.txHeight_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 106);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 15);
            this.label9.TabIndex = 2;
            this.label9.Text = "Height";
            // 
            // txWidth
            // 
            this.txWidth.Location = new System.Drawing.Point(80, 75);
            this.txWidth.Maximum = new decimal(new int[] {
            10240,
            0,
            0,
            0});
            this.txWidth.Name = "txWidth";
            this.txWidth.Size = new System.Drawing.Size(60, 23);
            this.txWidth.TabIndex = 2;
            this.txWidth.ValueChanged += new System.EventHandler(this.txWidth_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(39, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Width";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gainsboro;
            this.panel9.Controls.Add(this.label7);
            this.panel9.Controls.Add(this.panel17);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(8, 16, 8, 8);
            this.panel9.Name = "panel9";
            this.panel9.Padding = new System.Windows.Forms.Padding(8, 24, 8, 8);
            this.panel9.Size = new System.Drawing.Size(387, 533);
            this.panel9.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Real Size";
            // 
            // panel17
            // 
            this.panel17.AutoScroll = true;
            this.panel17.AutoSize = true;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.pbRealPreview);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel17.Location = new System.Drawing.Point(8, 24);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(371, 501);
            this.panel17.TabIndex = 0;
            // 
            // pbRealPreview
            // 
            this.pbRealPreview.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.pbRealPreview.Location = new System.Drawing.Point(0, 0);
            this.pbRealPreview.Name = "pbRealPreview";
            this.pbRealPreview.Size = new System.Drawing.Size(198, 264);
            this.pbRealPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbRealPreview.TabIndex = 9;
            this.pbRealPreview.TabStop = false;
            // 
            // ofdPNG
            // 
            this.ofdPNG.Filter = "PNG Image files|*.png";
            // 
            // FAvatarEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 561);
            this.Controls.Add(this.tab);
            this.MinimumSize = new System.Drawing.Size(1100, 600);
            this.Name = "FAvatarEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Avatar Editor";
            this.tab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbMatPreview)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSetTags)).EndInit();
            this.gbSetMaterials.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.pnlSetPreview.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbSetPreview)).EndInit();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRegionPreview)).EndInit();
            this.panel14.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.gbRegionTags.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvRegionTag)).EndInit();
            this.panel16.ResumeLayout(false);
            this.gbRegionSettings.ResumeLayout(false);
            this.gbRegionSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionRadius)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txRegionWidth)).EndInit();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txWidth)).EndInit();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbRealPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl tab;
        private TabPage tabPage1;
        private Panel panel3;
        private GroupBox groupBox2;
        private ListBox lbMats;
        private Panel panel5;
        private PictureBox pbMatPreview;
        private Panel panel4;
        private Button btnMatClearDefault;
        private Button btnMatSetDefault;
        private Button btnRemoveMats;
        private Button btnAddMats;
        private Panel panel1;
        private GroupBox groupBox1;
        private ListBox lbLayers;
        private Panel panel2;
        private Button btnMoveLayerDown;
        private Button btnMoveLayerUp;
        private Button btnRemoveLayer;
        private Button btnAddLayer;
        private TabPage tabPage2;
        private Panel panel6;
        private GroupBox groupBox3;
        private Panel pnlSetPreview;
        private Panel panel8;
        private Panel panel7;
        private Button btnSetRemove;
        private Button btnSetAdd;
        private TabPage tabPage3;
        private Panel panel10;
        private ListBox lbSets;
        private TextBox txNewLayerName;
        private TextBox txSetName;
        private Panel panel11;
        private GroupBox gbSetMaterials;
        private Panel panel12;
        private GroupBox groupBox5;
        private DataGridView dgvSetTags;
        private Panel panel13;
        private PictureBox pbRegionPreview;
        private Panel panel14;
        private GroupBox groupBox6;
        private ListBox lbRegions;
        private Panel panel15;
        private Panel panel16;
        private Label label5;
        private Label label6;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private RadioButton rbCircle;
        private RadioButton rbRectangle;
        private Button btnRegionRemove;
        private Button btnRegionAdd;
        private GroupBox gbRegionTags;
        private DataGridView dgvRegionTag;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private ComboBox cbRegionId;
        private OpenFileDialog ofdPNG;
        private Panel pnlSetMaterialContainer;
        private PictureBox pbSetPreview;
        private TabPage tabPage4;
        private Panel panel9;
        private Panel panel17;
        private PictureBox pbRealPreview;
        private Label label7;
        private Panel panel18;
        private GroupBox groupBox4;
        private NumericUpDown txWidth;
        private Label label8;
        private NumericUpDown txHeight;
        private Label label9;
        private NumericUpDown txRegionRadius;
        private NumericUpDown txRegionHeight;
        private NumericUpDown txRegionWidth;
        private NumericUpDown txRegionY;
        private NumericUpDown txRegionX;
        private RadioButton rbPoint;
        private Label label10;
        private TextBox txScale;
        private Label label11;
        private Button btnApplyScale;
        private DataGridViewTextBoxColumn cTag;
        private DataGridViewTextBoxColumn Weight;
        private GroupBox gbRegionSettings;
        private Panel panel19;
    }
}