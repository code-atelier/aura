﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace PersonaMaker.Forms
{
    public partial class FScheduleEditor : Form
    {
        public FScheduleEditor()
        {
            InitializeComponent();
        }

        private Schedule Schedule { get; set; } = new Schedule();

        public void Initialize(Persona persona)
        {
            Schedule = persona.Schedule;
            Clear();
            ReloadSchedule();
        }

        private void ReloadSchedule()
        {
            Schedule.Sort((a, b) => a.Name.CompareTo(b.Name));
            lbSchedules.Items.Clear();
            foreach (var entry in Schedule)
            {
                lbSchedules.Items.Add(entry);
            }
        }

        public void Clear()
        {
            txEnd.Clear();
            txName.Clear();
            txStart.Clear();
            txTags.Clear();

            cxDate.Checked = false;
            cxDoW.Checked = false;
            cxTime.Checked = false;
            for (var i = 0; i < clbDayOfWeek.Items.Count; i++)
                clbDayOfWeek.SetItemChecked(i, true);
            dtpDate.Value = DateTime.Today;

            txName.Focus();
        }

        private void cxTime_CheckedChanged(object sender, EventArgs e)
        {
            txStart.Enabled = cxTime.Checked;
            txEnd.Enabled = cxTime.Checked;
        }

        private void cxDoW_CheckedChanged(object sender, EventArgs e)
        {
            clbDayOfWeek.Enabled = cxDoW.Checked;
        }

        private void cxDate_CheckedChanged(object sender, EventArgs e)
        {
            dtpDate.Enabled = cxDate.Checked;
        }

        private void txName_TextChanged(object sender, EventArgs e)
        {
            btnAdd.Enabled = false;
            var name = txName.Text.Trim().ToLower();
            if (name == "")
            {
                btnAdd.Text = "Add New";
                return;
            }
            btnAdd.Enabled = true;
            if (Schedule.Any(x => x.Name == name))
                btnAdd.Text = "Save";
            else
                btnAdd.Text = "Add New";
        }

        private void lbSchedules_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnRemove.Enabled = false;
            if (lbSchedules != null && lbSchedules.SelectedItem is ScheduleEntry entry)
            {

                Clear();
                txName.Text = entry.Name;
                cxDate.Checked = entry.Date != null;
                cxDoW.Checked = entry.Days != null && entry.Days.Count > 0;
                cxTime.Checked = entry.Time != null;

                if (entry.Time != null)
                {
                    txStart.Text = entry.Time.Value.Start.Hours.ToString().PadLeft(2, '0') + ":" + entry.Time.Value.Start.Minutes.ToString().PadLeft(2, '0');
                    txEnd.Text = entry.Time.Value.End.Hours.ToString().PadLeft(2, '0') + ":" + entry.Time.Value.End.Minutes.ToString().PadLeft(2, '0');
                }
                if (entry.Date != null)
                {
                    dtpDate.Value = entry.Date.Value;
                }
                if (entry.Days != null)
                {
                    for (var i = 0; i < 7; i++)
                    {
                        var dow = (DayOfWeek)i;
                        clbDayOfWeek.SetItemChecked(i, entry.Days.Contains(dow));
                    }
                }
                btnRemove.Enabled = true;

                txTags.Text = string.Join(Environment.NewLine, entry.Tags);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string pattern = @"^[a-zA-Z0-9\:\-]+$";
            var name = txName.Text.Trim().ToLower();
            var existing = false;
            if (Schedule.Any(x => x.Name == name))
            {
                if (MessageBox.Show("Continue updating existing entry?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    return;
                existing = true;
            }

            if (!Regex.IsMatch(name, pattern))
            {
                MessageBox.Show($"'{name}' is not a valid schedule entry name", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var entry = Schedule.FirstOrDefault(x => x.Name == name) ?? new ScheduleEntry();
            entry.Name = name;
            entry.Tags = txTags.Lines.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x => x.Trim().ToLower()).ToList();

            if (cxTime.Checked)
            {
                TimeSpan start, end;
                if (!TimeSpan.TryParse(txStart.Text, out start))
                {
                    MessageBox.Show("Start time must be a valid time", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (!TimeSpan.TryParse(txEnd.Text, out end))
                {
                    MessageBox.Show("End time must be a valid time", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (start.Hours > 23 || start.Minutes > 59)
                {
                    MessageBox.Show("Maximum start time is 23:59", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (end.Hours > 23 || end.Minutes > 59)
                {
                    MessageBox.Show("Maximum end time is 23:59", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                entry.Time = new TimeRange()
                {
                    Start = start,
                    End = end,
                };
            }
            else
            {
                entry.Time = null;
            }

            if (cxDate.Checked)
            {
                entry.Date = dtpDate.Value.Date;
            }
            else
            {
                entry.Date = null;
            }

            if (cxDoW.Checked)
            {
                entry.Days = new List<DayOfWeek>();

                for (var i = 0; i < 7; i++)
                {
                    var dow = (DayOfWeek)i;
                    if (clbDayOfWeek.GetItemChecked(i))
                    {
                        entry.Days.Add(dow);
                    }
                }
            }
            else
            {
                entry.Days = null;
            }

            if (cxDateOfYear.Checked)
            {
                entry.DateOfYear = new DateOfYearRange()
                {
                    Start = dtpDRStart.Value.Date,
                    End = dtpDREnd.Value.Date,
                };
            }
            else
            {
                entry.DateOfYear = null;
            }

            foreach (var tag in entry.Tags)
            {
                if (!Regex.IsMatch(tag, pattern))
                {
                    MessageBox.Show($"'{tag}' is not a valid tag", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (!existing)
            {
                Schedule.Add(entry);
            }
            Clear();
            ReloadSchedule();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (lbSchedules != null && lbSchedules.SelectedItem is ScheduleEntry entry)
            {
                if (MessageBox.Show("Are you sure to delete '" + entry.Name + "'?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    Schedule.Remove(entry);
                    ReloadSchedule();
                }
            }
        }

        private void cxDateOfYear_CheckedChanged(object sender, EventArgs e)
        {
            dtpDREnd.Enabled = dtpDRStart.Enabled = cxDateOfYear.Checked;
        }
    }
}
