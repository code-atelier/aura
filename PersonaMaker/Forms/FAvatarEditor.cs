﻿using Aura.Models;
using Aura.Models.Avatar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.ComponentModel.Design.ObjectSelectorEditor;
using Label = System.Windows.Forms.Label;

namespace PersonaMaker.Forms
{
    public partial class FAvatarEditor : Form
    {
        public FAvatarEditor()
        {
            InitializeComponent();
            dgvSetTags.Columns[1].ValueType = typeof(int);
        }

        public AvatarManifest Manifest { get; set; } = new AvatarManifest();
        public AuraArchive Archive { get; set; } = new AuraArchive();

        public void AssignManifestAndArchive(AvatarManifest manifest, AuraArchive archive)
        {
            Manifest = manifest;
            Archive = archive;
            txWidth.Value = manifest.Display.Width;
            txHeight.Value = manifest.Display.Height;
            ReloadLayersList();
            ReloadSetList();
            ReloadRegionList();
            ReloadSetPreview();
        }

        #region Layers Controller
        private void txNewLayerName_KeyUp(object sender, KeyEventArgs e)
        {
            btnAddLayer.Enabled = !string.IsNullOrEmpty(txNewLayerName.Text);
        }
        private void txNewLayerName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                btnAddLayer.PerformClick();
            }
        }

        private void lbLayers_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnAddMats.Enabled = btnRemoveLayer.Enabled = lbLayers.SelectedIndex >= 0;
            btnMoveLayerUp.Enabled = lbLayers.SelectedIndex > 0;
            btnMoveLayerDown.Enabled = lbLayers.SelectedIndex < lbLayers.Items.Count - 1 && lbLayers.SelectedIndex >= 0;
            ReloadLayerMaterial();
        }

        private void btnRemoveLayer_Click(object sender, EventArgs e)
        {
            RemoveSelectedLayer();
        }

        private void RemoveSelectedLayer()
        {
            if (lbLayers.SelectedIndex >= 0 && MessageBox.Show("Are you sure to remove this layer?\r\nAll materials and set references will be lost.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var layerId = lbLayers.SelectedItem as string;
                if (layerId != null)
                {
                    var layer = Manifest.Layers[layerId];
                    foreach (var mat in layer.Materials)
                    {
                        try
                        {
                            var path = Path.Combine(layer.Path, mat).Replace("\\", "/");
                            Archive.FileSystem.Delete(path);
                        }
                        catch { }
                    }
                    Manifest.Layers.Remove(layerId);
                    lbLayers.Items.Remove(layerId);
                }
                RefreshLayerZIndex();
                ReloadSetMaterialList();
            }
        }

        private void btnMoveLayerUp_Click(object sender, EventArgs e)
        {
            MoveSelectedLayerUp();
        }

        private void MoveSelectedLayerUp()
        {
            if (lbLayers.SelectedIndex > 0)
            {
                var selIndex = lbLayers.SelectedIndex;
                var layerId = lbLayers.SelectedItem as string;
                if (layerId != null)
                {
                    lbLayers.Items.RemoveAt(selIndex);
                    lbLayers.Items.Insert(selIndex - 1, layerId);
                    lbLayers.SelectedIndex = selIndex - 1;
                }
                RefreshLayerZIndex();
                ReloadSetMaterialList();
            }
        }

        private void RefreshLayerZIndex()
        {
            var idx = 0;
            foreach (var itm in lbLayers.Items)
            {
                if (itm is string layerId)
                {
                    Manifest.Layers[layerId].ZIndex = idx++;
                }
            }
        }

        private void btnMoveLayerDown_Click(object sender, EventArgs e)
        {
            MoveSelectedLayerDown();
        }

        private void MoveSelectedLayerDown()
        {
            if (lbLayers.SelectedIndex >= 0 && lbLayers.SelectedIndex < lbLayers.Items.Count - 1)
            {
                var selIndex = lbLayers.SelectedIndex;
                var layerId = lbLayers.SelectedItem as string;
                if (layerId != null)
                {
                    lbLayers.Items.RemoveAt(selIndex);
                    lbLayers.Items.Insert(selIndex + 1, layerId);
                    lbLayers.SelectedIndex = selIndex + 1;
                }
                RefreshLayerZIndex();
                ReloadSetMaterialList();
            }
        }

        private void lbLayers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSelectedLayer();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Up && e.Control)
            {
                MoveSelectedLayerUp();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.Up && e.Control)
            {
                MoveSelectedLayerDown();
                e.Handled = true;
            }
        }
        private void btnAddLayer_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txNewLayerName.Text))
            {
                var layerName = txNewLayerName.Text.Trim().ToLower();
                string pattern = @"^[a-zA-Z0-9\-]+$";
                if (!Regex.IsMatch(layerName, pattern))
                {
                    MessageBox.Show("Invalid layer name '" + layerName + "'\r\nLayer name may only contains lower-case alphanumeric and dashes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Manifest.Layers.ContainsKey(layerName))
                {
                    MessageBox.Show("Layer '" + layerName + "' already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Manifest.Layers.Add(layerName, new Layer()
                {
                    Path = "res/img/mats/" + layerName,
                    Default = null,
                    ZIndex = 0,
                });
                ReloadLayersList(layerName);
                txNewLayerName.Clear();
            }
        }

        private void ReloadLayersList(string? selected = null)
        {
            var selectedLayerName = selected ?? lbLayers.SelectedItem as string;
            lbLayers.Items.Clear();
            var layers = Manifest.Layers.ToList();
            layers.Sort((a, b) => a.Value.ZIndex.CompareTo(b.Value.ZIndex));

            foreach (var layer in layers)
            {
                lbLayers.Items.Add(layer.Key);
            }
            lbLayers.SelectedItem = selectedLayerName;

            RefreshLayerZIndex();
            ReloadLayerMaterial();
        }
        #endregion

        #region Layer Materials
        class ListBoxLayerMaterial
        {
            public string MaterialName { get; set; } = string.Empty;
            public bool IsDefault { get; set; }

            public override string ToString()
            {
                return Path.GetFileNameWithoutExtension(MaterialName) + (IsDefault ? " (default)" : "");
            }
        }
        private void ReloadLayerMaterial(string? selected = null)
        {
            ListBoxLayerMaterial? prevSelection = null;
            var prevMat = selected ?? (lbMats.SelectedItem as ListBoxLayerMaterial)?.MaterialName;
            lbMats.Items.Clear();
            var layerId = lbLayers.SelectedItem as string;
            if (layerId != null)
            {
                var layer = Manifest.Layers[layerId];
                foreach (var mat in layer.Materials)
                {
                    var lbi = new ListBoxLayerMaterial() { MaterialName = mat, IsDefault = layer.Default == mat };
                    if (prevMat == mat)
                    {
                        prevSelection = lbi;
                    }
                    lbMats.Items.Add(lbi);
                }
            }
            if (prevSelection != null)
            {
                lbMats.SelectedItem = prevSelection;
            }
            else
            {
                LoadMaterialPreview();
            }
            ReloadSetMaterialList();
        }

        private string _validMaterialExtension = ".png";
        private void AddLayerMaterial(string path, string? forceLayerId = null)
        {
            var layerId = forceLayerId ?? lbLayers.SelectedItem as string;
            if (string.IsNullOrWhiteSpace(layerId)) return;
            if (!File.Exists(path))
            {
                MessageBox.Show("File not found: " + path, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var ext = Path.GetExtension(path)?.ToLower();
            if (ext != _validMaterialExtension)
            {
                MessageBox.Show("File extension is not valid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var fName = Path.GetFileName(path);
            var layer = Manifest.Layers[layerId];
            if (layer.Materials.Contains(fName))
            {
                if (MessageBox.Show("Material already exists: " + fName + "\r\nDo you want to replace existing material?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    layer.Materials.Remove(fName);
                    Archive.FileSystem.Delete(Path.Combine(layer.Path, fName).Replace("\\", "/"));
                }
                else
                    return;
            }
            layer.Materials.Add(fName);
            layer.Materials.Sort();
            using (var fi = File.OpenRead(path))
            {
                Archive.FileSystem.Add(Path.Combine(layer.Path, fName).Replace("\\", "/"), fi);
            }
            ReloadLayerMaterial(fName);
        }

        private void btnAddMats_Click(object sender, EventArgs e)
        {
            if (ofdPNG.ShowDialog() == DialogResult.OK)
            {
                AddLayerMaterial(ofdPNG.FileName);
            }
        }

        private void lbMats_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data?.GetDataPresent(DataFormats.FileDrop) == true)
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                e.Effect = DragDropEffects.None;
                if (files != null && files.Length > 0 && btnAddMats.Enabled)
                {
                    e.Effect = DragDropEffects.Copy;
                    foreach (var file in files)
                    {
                        var ext = Path.GetExtension(file)?.ToLower();
                        if (ext != _validMaterialExtension)
                        {
                            e.Effect = DragDropEffects.None;
                        }
                    }
                }
            }
        }
        private void lbMats_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data?.GetDataPresent(DataFormats.FileDrop) == true)
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                e.Effect = DragDropEffects.None;
                if (files != null && btnAddMats.Enabled)
                {
                    e.Effect = DragDropEffects.Copy;
                    foreach (var file in files)
                    {
                        var ext = Path.GetExtension(file)?.ToLower();
                        if (ext != _validMaterialExtension)
                        {
                            e.Effect = DragDropEffects.None;
                        }
                    }
                    if (e.Effect == DragDropEffects.Copy)
                    {
                        foreach (var file in files)
                        {
                            AddLayerMaterial(file);
                        }
                    }
                }
            }
        }

        private void lbMats_SelectedIndexChanged(object sender, EventArgs e)
        {
            var lbi = lbMats.SelectedItem as ListBoxLayerMaterial;
            btnRemoveMats.Enabled = lbMats.SelectedIndex >= 0;
            btnMatSetDefault.Enabled = lbi?.IsDefault == false;
            btnMatClearDefault.Enabled = false;
            foreach (var mat in lbMats.Items)
            {
                if (mat is ListBoxLayerMaterial lbx)
                {
                    if (lbx.IsDefault)
                    {
                        btnMatClearDefault.Enabled = true;
                    }
                }
            }
            LoadMaterialPreview();
        }

        private void LoadMaterialPreview()
        {
            var lbi = lbMats.SelectedItem as ListBoxLayerMaterial;
            pbMatPreview.Image = null;
            var layerId = lbLayers.SelectedItem as string;
            if (lbi != null && layerId != null)
            {
                var layer = Manifest.Layers[layerId];
                var path = Path.Combine(layer.Path, lbi.MaterialName).Replace("\\", "/");
                if (Archive.FileSystem.Exists(path))
                {
                    try
                    {
                        var strm = Archive.FileSystem[path];
                        var ms = new MemoryStream();
                        strm.Seek(0, SeekOrigin.Begin);
                        strm.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        pbMatPreview.Image = Image.FromStream(ms);
                    }
                    catch { }
                }
            }
        }
        private void btnRemoveMats_Click(object sender, EventArgs e)
        {
            RemoveSelectedLayerMaterial();
        }

        private void RemoveSelectedLayerMaterial()
        {
            if (lbMats.SelectedIndex >= 0 && MessageBox.Show("Are you sure to remove this layer material?\r\nAll set references will be lost.", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var lbi = lbMats.SelectedItem as ListBoxLayerMaterial;
                var layerId = lbLayers.SelectedItem as string;
                if (lbi != null && layerId != null)
                {
                    var layer = Manifest.Layers[layerId];
                    var path = Path.Combine(layer.Path, lbi.MaterialName).Replace("\\", "/");
                    layer.Materials.Remove(lbi.MaterialName);
                    Archive.FileSystem.Delete(path);
                    lbMats.Items.Remove(lbi);
                    if (layer.Default == lbi.MaterialName)
                    {
                        layer.Default = null; 
                        ReloadSetMaterial();
                    }
                }
            }
        }
        private void btnMatSetDefault_Click(object sender, EventArgs e)
        {
            var lbi = lbMats.SelectedItem as ListBoxLayerMaterial;
            var layerId = lbLayers.SelectedItem as string;
            if (lbi != null && layerId != null)
            {
                var layer = Manifest.Layers[layerId];
                layer.Default = lbi.MaterialName;
                ReloadLayerMaterial();
            }
        }

        private void btnMatClearDefault_Click(object sender, EventArgs e)
        {
            var layerId = lbLayers.SelectedItem as string;
            if (layerId != null)
            {
                var layer = Manifest.Layers[layerId];
                layer.Default = null;
                ReloadLayerMaterial();
            }
        }

        private void lbMats_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSelectedLayerMaterial();
            }
        }

        private Layer? SelectedLayer
        {
            get
            {
                var layerId = lbLayers.SelectedItem as string;
                if (layerId != null)
                {
                    return Manifest.Layers[layerId];
                }
                return null;
            }
        }
        #endregion

        #region Set
        private Set? SelectedSet
        {
            get
            {
                var setId = lbSets.SelectedItem as string;
                if (setId != null)
                {
                    return Manifest.Sets[setId];
                }
                return null;
            }
        }
        private void txSetName_KeyUp(object sender, KeyEventArgs e)
        {
            btnSetAdd.Enabled = !string.IsNullOrEmpty(txSetName.Text);
        }

        private Dictionary<string, string?> GenerateSetLayer()
        {
            var res = new Dictionary<string, string?>();
            foreach (var kv in Manifest.Layers)
            {
                res.Add(kv.Key, null);
            }
            return res;
        }

        private void btnSetAdd_Click(object sender, EventArgs e)
        {
            AddSet();
        }

        private void ReloadSetList(string? selected = null)
        {
            var selectedSetId = selected ?? lbSets.SelectedItem as string;
            lbSets.Items.Clear();
            foreach (var set in Manifest.Sets)
            {
                lbSets.Items.Add(set.Key);
            }
            lbSets.SelectedItem = selectedSetId;

            ReloadSetMaterial();
            ReloadSetTag();
        }

        Dictionary<string, ComboBox> _setComboBoxes = new Dictionary<string, ComboBox>();
        private void ReloadSetMaterialList()
        {
            _setComboBoxes.Clear();
            foreach (var set in Manifest.Sets)
            {
                var setV = set.Value;
                var setLayerKeys = setV.Layers.Keys.ToList();
                foreach (var lyr in setLayerKeys)
                {
                    if (!Manifest.Layers.ContainsKey(lyr))
                    {
                        setV.Layers.Remove(lyr);
                    }
                }
                foreach (var layer in Manifest.Layers)
                {
                    var layerId = layer.Key;
                    if (!setV.Layers.ContainsKey(layerId))
                    {
                        setV.Layers.Add(layerId, null);
                    }
                }
            }
            pnlSetMaterialContainer.Controls.Clear();
            var y = 20;
            var manLayers = Manifest.Layers.ToList();
            manLayers.Sort((a, b) => a.Value.ZIndex.CompareTo(b.Value.ZIndex));
            foreach (var layer in manLayers)
            {
                // create control
                var lb = new Label();
                lb.Text = layer.Key;
                lb.Left = 10;
                lb.Top = y + 3;
                lb.AutoSize = true;
                pnlSetMaterialContainer.Controls.Add(lb);

                var cb = new ComboBox();
                cb.Left = 100;
                cb.Top = y;
                cb.Width = 200;
                cb.DropDownStyle = ComboBoxStyle.DropDownList;
                cb.Items.Add("- Don't Change -");
                cb.Items.Add("- Remove -");
                cb.Tag = layer.Key;
                foreach (var mat in layer.Value.Materials)
                {
                    cb.Items.Add(mat);
                }
                cb.SelectedIndex = 0;
                pnlSetMaterialContainer.Controls.Add(cb);
                _setComboBoxes.Add(layer.Key, cb);
                cb.SelectedIndexChanged += Cb_SelectedIndexChanged;

                // add x
                y += 25;
            }
            ReloadSetMaterial();
        }

        private void Cb_SelectedIndexChanged(object? sender, EventArgs e)
        {
            if (sender is ComboBox cb && SelectedSet != null && cb.Tag is string layerId)
            {
                if (cb.SelectedIndex <= 0)
                    SelectedSet.Layers[layerId] = null;
                else if (cb.SelectedIndex == 1)
                    SelectedSet.Layers[layerId] = "";
                else
                    SelectedSet.Layers[layerId] = cb.SelectedItem.ToString();
                ReloadSetPreview();
            }
        }

        private void ReloadSetMaterial()
        {
            pnlSetMaterialContainer.Enabled = SelectedSet != null;
            _suppressReloadSetPreview = true;
            try
            {
                foreach (var kv in _setComboBoxes)
                {
                    var layerId = kv.Key;
                    if (SelectedSet != null && !string.IsNullOrEmpty(SelectedSet.Layers[layerId]))
                    {
                        kv.Value.SelectedIndex = kv.Value.Items.IndexOf(SelectedSet.Layers[layerId]);
                    }
                    else
                    {
                        kv.Value.SelectedIndex = 0;
                    }
                }
            }
            finally
            {
                _suppressReloadSetPreview = false;
            }
            ReloadSetPreview();
        }

        private void ReloadSetTag()
        {
            dgvSetTags.Enabled = SelectedSet != null;
            dgvSetTags.Rows.Clear();
            if (SelectedSet != null)
            {
                foreach (var tag in SelectedSet.Tags)
                {
                    var dgr = dgvSetTags.Rows[dgvSetTags.Rows.Add()];
                    dgr.Cells[0].Value = tag.Key;
                    dgr.Cells[1].Value = tag.Value;
                }
            }
        }

        private void txSetName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                AddSet();
                e.Handled = true;
            }
        }

        private void AddSet()
        {
            if (!string.IsNullOrEmpty(txSetName.Text))
            {
                var setName = txSetName.Text.Trim().ToLower();
                string pattern = @"^[a-zA-Z0-9\:\-]+$";
                if (!Regex.IsMatch(setName, pattern))
                {
                    MessageBox.Show("Invalid set name '" + setName + "'\r\nSet name may only contains lower-case alphanumeric, dashes and colons", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Manifest.Sets.ContainsKey(setName))
                {
                    MessageBox.Show("Set '" + setName + "' already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Manifest.Sets.Add(setName, new Set()
                {
                    Layers = GenerateSetLayer(),
                    Tags = new Dictionary<string, int>(),
                });
                ReloadSetList(setName);
                txSetName.Clear();
            }
        }

        private void lbSets_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReloadSetMaterial();
            ReloadSetTag();
            btnSetRemove.Enabled = lbSets.SelectedIndex >= 0;
        }

        Bitmap? _regionBaseImage = null;
        bool _suppressReloadSetPreview = false;
        private void ReloadSetPreview()
        {
            if (_suppressReloadSetPreview) return;
            pbSetPreview.Image = null;
            pbRegionPreview.Image = null;
            pbRealPreview.Image = null;
            try
            {
                List<Image> images = new List<Image>();
                List<Image> layerDefaults = new List<Image>();
                foreach (var cb in _setComboBoxes)
                {
                    var mat = cb.Value.SelectedItem as string;
                    var layer = Manifest.Layers[cb.Key];
                    if (cb.Value.SelectedIndex > 1 && !string.IsNullOrWhiteSpace(mat))
                    {
                        var arcPath = Path.Combine(layer.Path, mat).Replace("\\", "/");
                        if (Archive.FileSystem.Exists(arcPath))
                        {
                            var strm = Archive.FileSystem[arcPath];
                            strm.Seek(0, SeekOrigin.Begin);
                            using (var ms = new MemoryStream())
                            {
                                strm.CopyTo(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                var img = Image.FromStream(ms);
                                images.Add(img);
                                layerDefaults.Add(img);
                            }
                        }
                    }
                    else if (cb.Value.SelectedIndex == 0 && !string.IsNullOrWhiteSpace(layer.Default))
                    {
                        var arcPath = Path.Combine(layer.Path, layer.Default).Replace("\\", "/");
                        if (Archive.FileSystem.Exists(arcPath))
                        {
                            var strm = Archive.FileSystem[arcPath];
                            strm.Seek(0, SeekOrigin.Begin);
                            using (var ms = new MemoryStream())
                            {
                                strm.CopyTo(ms);
                                ms.Seek(0, SeekOrigin.Begin);
                                var img = Image.FromStream(ms);
                                layerDefaults.Add(img);
                            }
                        }
                    }
                }

                if (images.Count > 0)
                {
                    Bitmap final = new Bitmap(images.First().Width, images.First().Height, images.First().PixelFormat);
                    final.SetResolution(images.First().HorizontalResolution, images.First().VerticalResolution);
                    using (var g = Graphics.FromImage(final))
                    {
                        foreach (var img in images)
                        {
                            g.DrawImage(img, 0, 0);
                        }
                    }
                    pbSetPreview.Image = final;

                    if (txWidth.Value == 0)
                    {
                        txWidth.Value = final.Width;
                    }
                    if (txHeight.Value == 0)
                    {
                        txHeight.Value = final.Height;
                    }
                    UpdateRealSize();
                }

                if (layerDefaults.Count > 0)
                {
                    var region = new Bitmap(layerDefaults.First().Width, layerDefaults.First().Height, layerDefaults.First().PixelFormat);
                    region.SetResolution(layerDefaults.First().HorizontalResolution, layerDefaults.First().VerticalResolution);
                    using (var g = Graphics.FromImage(region))
                    {
                        foreach (var img in layerDefaults)
                        {
                            g.DrawImage(img, 0, 0, img.Width, img.Height);
                        }
                    }
                    pbRealPreview.Image = new Bitmap(region);
                    _regionBaseImage = region;
                    UpdateRegionImage();
                }
            }
            catch { }
        }

        private void dgvSetTags_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            MessageBox.Show("Invalid value", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            e.Cancel = true;
        }

        private void dgvSetTags_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            SaveSetTag();
        }

        private void SaveSetTag()
        {
            if (SelectedSet != null)
            {
                var tags = new Dictionary<string, int>();
                foreach (DataGridViewRow row in dgvSetTags.Rows)
                {
                    try
                    {
                        var key = (row.Cells[0].Value as string ?? "").ToLower().Trim();
                        var weight = int.Parse(row.Cells[1].Value?.ToString() ?? "");
                        string pattern = @"^[a-zA-Z0-9\:\-]+$";
                        if (!string.IsNullOrWhiteSpace(key))
                        {
                            if (!Regex.IsMatch(key, pattern))
                            {
                                MessageBox.Show("Invalid tag name '" + key + "'\r\nTag name may only contains lower-case alphanumeric, dashes and colon", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            if (!tags.ContainsKey(key.ToLower().Trim()))
                            {
                                tags.Add(key.ToLower().Trim(), weight);
                            }
                        }
                    }
                    catch
                    {

                    }
                }
                SelectedSet.Tags = tags;
            }
        }

        private void btnSetRemove_Click(object sender, EventArgs e)
        {
            RemoveSet();
        }

        private void RemoveSet()
        {

            if (lbSets.SelectedIndex >= 0 && MessageBox.Show("Are you sure to remove this set?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var lbi = lbSets.SelectedItem as string;
                if (lbi != null)
                {
                    Manifest.Sets.Remove(lbi);
                    lbSets.Items.Remove(lbi);
                }
            }
        }

        private void lbSets_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                RemoveSet();
            }
        }

        private void lbLayers_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data != null && e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files != null && files.Length > 0)
                {
                    e.Effect = DragDropEffects.Copy;
                    foreach (var dir in files)
                    {
                        if (!Directory.Exists(dir))
                        {
                            e.Effect = DragDropEffects.None;
                        }
                    }
                }
            }
        }

        private void lbLayers_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data != null && e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var files = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (files != null && files.Length > 0)
                {
                    e.Effect = DragDropEffects.Copy;
                    foreach (var dir in files)
                    {
                        if (!Directory.Exists(dir))
                        {
                            e.Effect = DragDropEffects.None;
                        }
                    }
                    if (e.Effect == DragDropEffects.Copy)
                    {
                        foreach (var dir in files)
                        {
                            var layerName = Path.GetFileName(dir).Trim().ToLower();

                            string pattern = @"^[a-zA-Z0-9\-]+$";
                            if (!Regex.IsMatch(layerName, pattern))
                            {
                                MessageBox.Show("Invalid layer name '" + layerName + "'\r\nLayer name may only contains lower-case alphanumeric and dashes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            if (Manifest.Layers.ContainsKey(layerName))
                            {
                                MessageBox.Show("Layer '" + layerName + "' already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return;
                            }
                            var layer = new Layer()
                            {
                                Path = "res/img/mats/" + layerName,
                                Default = null,
                                ZIndex = 0,
                            };
                            Manifest.Layers.Add(layerName, layer);

                            var fls = Directory.EnumerateFiles(dir).ToList();
                            foreach (var f in fls)
                            {
                                AddLayerMaterial(f, layerName);
                            }
                        }
                        ReloadLayersList();
                    }
                }
            }
        }

        private void txScale_TextChanged(object sender, EventArgs e)
        {
            btnApplyScale.Enabled = double.TryParse(txScale.Text, out _);
        }

        private void txWidth_ValueChanged(object sender, EventArgs e)
        {
            Manifest.Display.Width = (int)txWidth.Value;
            UpdateRealSize();
        }

        private void txHeight_ValueChanged(object sender, EventArgs e)
        {
            Manifest.Display.Height = (int)txHeight.Value;
            UpdateRealSize();
        }

        private void UpdateRealSize()
        {
            var width = Manifest.Display.Width;
            var height = Manifest.Display.Height;
            if (pbRealPreview.Image is Image img)
            {
                if (width == 0) width = img.Width;
                if (height == 0) height = img.Height;
            }

            pbRealPreview.Width = width;
            pbRealPreview.Height = height;
        }

        private void btnApplyScale_Click(object sender, EventArgs e)
        {
            if (pbRealPreview.Image is Image img)
            {
                var scale = double.Parse(txScale.Text) / 100;
                txWidth.Value = (int)Math.Ceiling(img.Width * scale);
                txHeight.Value = (int)Math.Ceiling(img.Height * scale);
            }
        }
        #endregion

        #region Region
        bool _suppressUpdateRegion = false;
        private void UpdateRegionImage()
        {
            if (_suppressUpdateRegion) return;
            if (_regionBaseImage == null)
            {
                pbRegionPreview.Image = null;
            }
            else
            {
                var resimg = new Bitmap(_regionBaseImage);

                if (SelectedRegion != null)
                {
                    var region = SelectedRegion;
                    using (var g = Graphics.FromImage(resimg))
                    {
                        var pen = new Pen(new SolidBrush(Color.Red), 2);
                        g.FillEllipse(new SolidBrush(Color.Red), (float)region.X - 3, (float)region.Y - 3, 6, 6);

                        if (region.Type == RegionType.Rectangle)
                        {
                            g.DrawRectangle(pen, (float)region.X, (float)region.Y, (float)region.Width, (float)region.Height);
                        }
                        else if (region.Type == RegionType.Radius)
                        {
                            g.DrawEllipse(pen, (float)(region.X - region.Radius), (float)(region.Y - region.Radius), (float)region.Radius * 2, (float)region.Radius * 2);
                        }
                    }
                }

                pbRegionPreview.Width = resimg.Width;
                pbRegionPreview.Height = resimg.Height;

                pbRegionPreview.Image = resimg;
            }
        }
        private void lbRegions_SelectedIndexChanged(object sender, EventArgs e)
        {
            gbRegionSettings.Enabled = gbRegionTags.Enabled =
            btnRegionRemove.Enabled = lbRegions.SelectedIndex >= 0;

            _suppressUpdateRegion = true;
            try
            {
                var regionId = lbRegions.SelectedItem as string;
                if (regionId != null)
                {
                    var region = Manifest.Regions[regionId].First();
                    txRegionHeight.Value = (decimal)region.Height;
                    txRegionWidth.Value = (decimal)region.Width;
                    txRegionRadius.Value = (decimal)region.Radius;
                    txRegionX.Value = (decimal)region.X;
                    txRegionY.Value = (decimal)region.Y;
                    rbRectangle.Checked = region.Type == RegionType.Rectangle;
                    rbCircle.Checked = region.Type == RegionType.Radius;
                    rbPoint.Checked = region.Type == RegionType.Point;
                }
                else
                {
                    rbRectangle.Checked = true;
                    txRegionHeight.Value = txRegionWidth.Value = txRegionRadius.Value = txRegionX.Value = txRegionY.Value = 0;
                    dgvRegionTag.Rows.Clear();
                }
            }
            finally
            {
                _suppressUpdateRegion = false;
            }
            UpdateRegionImage();
        }

        private void cbRegionId_TextChanged(object sender, EventArgs e)
        {
            btnRegionAdd.Enabled = !string.IsNullOrWhiteSpace(cbRegionId.Text);
        }

        private void btnRegionAdd_Click(object sender, EventArgs e)
        {
            AddRegion();
        }

        private void AddRegion()
        {
            var regionId = cbRegionId.Text.Trim().ToLower();
            if (!string.IsNullOrWhiteSpace(regionId))
            {
                string pattern = @"^[a-zA-Z0-9\-]+$";
                if (!Regex.IsMatch(regionId, pattern))
                {
                    MessageBox.Show("Invalid region id '" + regionId + "'\r\nRegion id may only contains lower-case alphanumeric and dashes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (Manifest.Regions.ContainsKey(regionId))
                {
                    MessageBox.Show("Region '" + regionId + "' already exists", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                Manifest.Regions.Add(regionId, new Regions()
                {
                    new Aura.Models.Avatar.Region()
                    {
                        Type = RegionType.Rectangle
                    }
                });
                ReloadRegionList(regionId);
                cbRegionId.Text = "";
            }
        }

        private void ReloadRegionList(string? selected = null)
        {
            var selectedRegionId = selected ?? lbRegions.SelectedItem as string;
            lbRegions.Items.Clear();

            foreach (var region in Manifest.Regions)
            {
                lbRegions.Items.Add(region.Key);
            }
            lbRegions.SelectedItem = selectedRegionId;

            UpdateRegionImage();
        }

        private void btnRegionRemove_Click(object sender, EventArgs e)
        {
            RemoveSelectedRegion();
        }

        private void RemoveSelectedRegion()
        {
            if (lbLayers.SelectedIndex >= 0 && MessageBox.Show("Are you sure to remove this region?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                var regionId = lbRegions.SelectedItem as string;
                if (regionId != null)
                {
                    Manifest.Regions.Remove(regionId);
                }
                ReloadRegionList();
            }
        }

        Aura.Models.Avatar.Region? SelectedRegion
        {
            get
            {
                var regionId = lbRegions.SelectedItem as string;
                if (regionId != null)
                {
                    var region = Manifest.Regions[regionId].First();
                    return region;
                }
                return null;
            }
        }
        private void txRegionX_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null)
            {
                SelectedRegion.X = (double)txRegionX.Value;
                UpdateRegionImage();
            }
        }
        private void txRegionY_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null)
            {
                SelectedRegion.Y = (double)txRegionY.Value;
                UpdateRegionImage();
            }
        }

        private void txRegionWidth_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null)
            {
                SelectedRegion.Width = (double)txRegionWidth.Value;
                UpdateRegionImage();
            }
        }

        private void txRegionHeight_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null)
            {
                SelectedRegion.Height = (double)txRegionHeight.Value;
                UpdateRegionImage();
            }
        }

        private void txRegionRadius_ValueChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null)
            {
                SelectedRegion.Radius = (double)txRegionRadius.Value;
                UpdateRegionImage();
            }
        }
        private void rbRectangle_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null && sender is RadioButton rb && rb.Checked)
            {
                SelectedRegion.Type = RegionType.Rectangle;
                UpdateRegionImage();
            }
            txRegionWidth.Enabled = txRegionHeight.Enabled = rbRectangle.Checked;
        }
        private void rbCircle_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null && sender is RadioButton rb && rb.Checked)
            {
                SelectedRegion.Type = RegionType.Radius;
                UpdateRegionImage();
            }
            txRegionRadius.Enabled = rbCircle.Checked;
        }

        private void rbPoint_CheckedChanged(object sender, EventArgs e)
        {
            if (SelectedRegion != null && sender is RadioButton rb && rb.Checked)
            {
                SelectedRegion.Type = RegionType.Point;
                UpdateRegionImage();
            }
        }
        #endregion

        private void dgvSetTags_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && dgvSetTags.SelectedCells.Count == 1 && dgvSetTags.SelectedCells[0].RowIndex != dgvSetTags.NewRowIndex)
            {
                var tagName = dgvSetTags.Rows[dgvSetTags.SelectedCells[0].RowIndex].Cells[0].Value?.ToString();
                if (MessageBox.Show("Are you sure to delete '" + tagName + "'?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    return;
                dgvSetTags.Rows.RemoveAt(dgvSetTags.SelectedCells[0].RowIndex);
                SaveSetTag();
            }
        }
    }
}
