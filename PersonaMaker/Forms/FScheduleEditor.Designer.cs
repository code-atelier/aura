﻿namespace PersonaMaker.Forms
{
    partial class FScheduleEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbSchedules = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRemove = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txTags = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cxTime = new System.Windows.Forms.CheckBox();
            this.cxDoW = new System.Windows.Forms.CheckBox();
            this.cxDate = new System.Windows.Forms.CheckBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.clbDayOfWeek = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txEnd = new System.Windows.Forms.MaskedTextBox();
            this.txStart = new System.Windows.Forms.MaskedTextBox();
            this.txName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cxDateOfYear = new System.Windows.Forms.CheckBox();
            this.dtpDRStart = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.dtpDREnd = new System.Windows.Forms.DateTimePicker();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(8, 8, 4, 8);
            this.panel1.Size = new System.Drawing.Size(285, 491);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbSchedules);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(8, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox1.Size = new System.Drawing.Size(273, 475);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Schedules";
            // 
            // lbSchedules
            // 
            this.lbSchedules.AllowDrop = true;
            this.lbSchedules.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSchedules.FormattingEnabled = true;
            this.lbSchedules.ItemHeight = 15;
            this.lbSchedules.Location = new System.Drawing.Point(8, 24);
            this.lbSchedules.Name = "lbSchedules";
            this.lbSchedules.Size = new System.Drawing.Size(257, 443);
            this.lbSchedules.TabIndex = 0;
            this.lbSchedules.SelectedIndexChanged += new System.EventHandler(this.lbSchedules_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(285, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(4, 8, 8, 8);
            this.panel2.Size = new System.Drawing.Size(429, 491);
            this.panel2.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtpDREnd);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cxDateOfYear);
            this.groupBox2.Controls.Add(this.dtpDRStart);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txTags);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cxTime);
            this.groupBox2.Controls.Add(this.cxDoW);
            this.groupBox2.Controls.Add(this.cxDate);
            this.groupBox2.Controls.Add(this.dtpDate);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.clbDayOfWeek);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txEnd);
            this.groupBox2.Controls.Add(this.txStart);
            this.groupBox2.Controls.Add(this.txName);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(4, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(8);
            this.groupBox2.Size = new System.Drawing.Size(417, 475);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Editor";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnRemove);
            this.panel4.Controls.Add(this.btnAdd);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(8, 433);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(401, 34);
            this.panel4.TabIndex = 19;
            // 
            // btnRemove
            // 
            this.btnRemove.Enabled = false;
            this.btnRemove.Location = new System.Drawing.Point(99, 5);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(75, 23);
            this.btnRemove.TabIndex = 1;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Enabled = false;
            this.btnAdd.Location = new System.Drawing.Point(8, 5);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(85, 23);
            this.btnAdd.TabIndex = 0;
            this.btnAdd.Text = "Add New";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 309);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 15);
            this.label4.TabIndex = 18;
            this.label4.Text = "(line-separated)";
            // 
            // txTags
            // 
            this.txTags.AcceptsReturn = true;
            this.txTags.Location = new System.Drawing.Point(118, 291);
            this.txTags.Multiline = true;
            this.txTags.Name = "txTags";
            this.txTags.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txTags.Size = new System.Drawing.Size(215, 131);
            this.txTags.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 294);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 15);
            this.label2.TabIndex = 16;
            this.label2.Text = "Active Tags";
            // 
            // cxTime
            // 
            this.cxTime.AutoSize = true;
            this.cxTime.Location = new System.Drawing.Point(11, 52);
            this.cxTime.Name = "cxTime";
            this.cxTime.Size = new System.Drawing.Size(52, 19);
            this.cxTime.TabIndex = 1;
            this.cxTime.Text = "Time";
            this.cxTime.UseVisualStyleBackColor = true;
            this.cxTime.CheckedChanged += new System.EventHandler(this.cxTime_CheckedChanged);
            // 
            // cxDoW
            // 
            this.cxDoW.AutoSize = true;
            this.cxDoW.Location = new System.Drawing.Point(11, 81);
            this.cxDoW.Name = "cxDoW";
            this.cxDoW.Size = new System.Drawing.Size(92, 19);
            this.cxDoW.TabIndex = 4;
            this.cxDoW.Text = "Day of Week";
            this.cxDoW.UseVisualStyleBackColor = true;
            this.cxDoW.CheckedChanged += new System.EventHandler(this.cxDoW_CheckedChanged);
            // 
            // cxDate
            // 
            this.cxDate.AutoSize = true;
            this.cxDate.Location = new System.Drawing.Point(11, 236);
            this.cxDate.Name = "cxDate";
            this.cxDate.Size = new System.Drawing.Size(50, 19);
            this.cxDate.TabIndex = 6;
            this.cxDate.Text = "Date";
            this.cxDate.UseVisualStyleBackColor = true;
            this.cxDate.CheckedChanged += new System.EventHandler(this.cxDate_CheckedChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd MMM yyyy";
            this.dtpDate.Enabled = false;
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(118, 233);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(137, 23);
            this.dtpDate.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(242, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(97, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "(24-hour system)";
            // 
            // clbDayOfWeek
            // 
            this.clbDayOfWeek.Enabled = false;
            this.clbDayOfWeek.FormattingEnabled = true;
            this.clbDayOfWeek.Items.AddRange(new object[] {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"});
            this.clbDayOfWeek.Location = new System.Drawing.Point(118, 79);
            this.clbDayOfWeek.Name = "clbDayOfWeek";
            this.clbDayOfWeek.Size = new System.Drawing.Size(137, 148);
            this.clbDayOfWeek.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(167, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "to";
            // 
            // txEnd
            // 
            this.txEnd.Enabled = false;
            this.txEnd.Location = new System.Drawing.Point(196, 50);
            this.txEnd.Mask = "00:00";
            this.txEnd.Name = "txEnd";
            this.txEnd.Size = new System.Drawing.Size(40, 23);
            this.txEnd.TabIndex = 3;
            this.txEnd.Text = "2400";
            this.txEnd.ValidatingType = typeof(System.DateTime);
            // 
            // txStart
            // 
            this.txStart.Enabled = false;
            this.txStart.Location = new System.Drawing.Point(118, 50);
            this.txStart.Mask = "00:00";
            this.txStart.Name = "txStart";
            this.txStart.Size = new System.Drawing.Size(40, 23);
            this.txStart.TabIndex = 2;
            this.txStart.ValidatingType = typeof(System.DateTime);
            // 
            // txName
            // 
            this.txName.Location = new System.Drawing.Point(118, 21);
            this.txName.Name = "txName";
            this.txName.Size = new System.Drawing.Size(215, 23);
            this.txName.TabIndex = 0;
            this.txName.TextChanged += new System.EventHandler(this.txName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // cxDateOfYear
            // 
            this.cxDateOfYear.AutoSize = true;
            this.cxDateOfYear.Location = new System.Drawing.Point(11, 265);
            this.cxDateOfYear.Name = "cxDateOfYear";
            this.cxDateOfYear.Size = new System.Drawing.Size(86, 19);
            this.cxDateOfYear.TabIndex = 8;
            this.cxDateOfYear.Text = "Date Range";
            this.cxDateOfYear.UseVisualStyleBackColor = true;
            this.cxDateOfYear.CheckedChanged += new System.EventHandler(this.cxDateOfYear_CheckedChanged);
            // 
            // dtpDRStart
            // 
            this.dtpDRStart.CustomFormat = "dd MMMM";
            this.dtpDRStart.Enabled = false;
            this.dtpDRStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDRStart.Location = new System.Drawing.Point(118, 262);
            this.dtpDRStart.Name = "dtpDRStart";
            this.dtpDRStart.Size = new System.Drawing.Size(118, 23);
            this.dtpDRStart.TabIndex = 9;
            this.dtpDRStart.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(242, 266);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(12, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "-";
            // 
            // dtpDREnd
            // 
            this.dtpDREnd.CustomFormat = "dd MMMM";
            this.dtpDREnd.Enabled = false;
            this.dtpDREnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDREnd.Location = new System.Drawing.Point(260, 262);
            this.dtpDREnd.Name = "dtpDREnd";
            this.dtpDREnd.Size = new System.Drawing.Size(118, 23);
            this.dtpDREnd.TabIndex = 10;
            this.dtpDREnd.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // FScheduleEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(714, 491);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FScheduleEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Schedule Editor";
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Panel panel1;
        private GroupBox groupBox1;
        private ListBox lbSchedules;
        private Panel panel2;
        private GroupBox groupBox2;
        private TextBox txName;
        private Label label1;
        private Label label3;
        private MaskedTextBox txEnd;
        private MaskedTextBox txStart;
        private CheckedListBox clbDayOfWeek;
        private Label label5;
        private DateTimePicker dtpDate;
        private CheckBox cxDate;
        private CheckBox cxDoW;
        private CheckBox cxTime;
        private TextBox txTags;
        private Label label2;
        private Label label4;
        private Panel panel4;
        private Button btnRemove;
        private Button btnAdd;
        private CheckBox cxDateOfYear;
        private DateTimePicker dtpDRStart;
        private DateTimePicker dtpDREnd;
        private Label label6;
    }
}