﻿namespace PersonaMaker
{
    partial class FMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miNewPersona = new System.Windows.Forms.ToolStripMenuItem();
            this.miOpenPersona = new System.Windows.Forms.ToolStripMenuItem();
            this.personaCompendiumToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.miSavePersona = new System.Windows.Forms.ToolStripMenuItem();
            this.miSavePersonaAs = new System.Windows.Forms.ToolStripMenuItem();
            this.miClosePersona = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miEdit = new System.Windows.Forms.ToolStripMenuItem();
            this.miAvatarEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.miScheduleEditor = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlPersonaManifest = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.txVersion = new System.Windows.Forms.TextBox();
            this.cxNSFW = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txCreator = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txNickname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txLastName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txFirstName = new System.Windows.Forms.TextBox();
            this.pbPersonaPreview = new System.Windows.Forms.PictureBox();
            this.sfdPersona = new System.Windows.Forms.SaveFileDialog();
            this.ofdPersona = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.pnlPersonaManifest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPersonaPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.miEdit,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miNewPersona,
            this.miOpenPersona,
            this.personaCompendiumToolStripMenuItem,
            this.toolStripSeparator1,
            this.miSavePersona,
            this.miSavePersonaAs,
            this.miClosePersona,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // miNewPersona
            // 
            this.miNewPersona.Name = "miNewPersona";
            this.miNewPersona.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.miNewPersona.Size = new System.Drawing.Size(240, 22);
            this.miNewPersona.Text = "New Persona";
            this.miNewPersona.Click += new System.EventHandler(this.miNewPersona_Click);
            // 
            // miOpenPersona
            // 
            this.miOpenPersona.Name = "miOpenPersona";
            this.miOpenPersona.ShortcutKeyDisplayString = "";
            this.miOpenPersona.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.miOpenPersona.Size = new System.Drawing.Size(240, 22);
            this.miOpenPersona.Text = "Open Persona";
            this.miOpenPersona.Click += new System.EventHandler(this.miOpenPersona_Click);
            // 
            // personaCompendiumToolStripMenuItem
            // 
            this.personaCompendiumToolStripMenuItem.Name = "personaCompendiumToolStripMenuItem";
            this.personaCompendiumToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.personaCompendiumToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.personaCompendiumToolStripMenuItem.Text = "Persona Compendium";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(237, 6);
            // 
            // miSavePersona
            // 
            this.miSavePersona.Enabled = false;
            this.miSavePersona.Name = "miSavePersona";
            this.miSavePersona.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.miSavePersona.Size = new System.Drawing.Size(240, 22);
            this.miSavePersona.Text = "Save Persona";
            this.miSavePersona.Click += new System.EventHandler(this.miSavePersona_Click);
            // 
            // miSavePersonaAs
            // 
            this.miSavePersonaAs.Enabled = false;
            this.miSavePersonaAs.Name = "miSavePersonaAs";
            this.miSavePersonaAs.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.miSavePersonaAs.Size = new System.Drawing.Size(240, 22);
            this.miSavePersonaAs.Text = "Save Persona As...";
            this.miSavePersonaAs.Click += new System.EventHandler(this.miSavePersonaAs_Click);
            // 
            // miClosePersona
            // 
            this.miClosePersona.Enabled = false;
            this.miClosePersona.Name = "miClosePersona";
            this.miClosePersona.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.miClosePersona.Size = new System.Drawing.Size(240, 22);
            this.miClosePersona.Text = "Close Persona";
            this.miClosePersona.Click += new System.EventHandler(this.miClosePersona_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(237, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeyDisplayString = "Alt+F4";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(240, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // miEdit
            // 
            this.miEdit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miAvatarEditor,
            this.miScheduleEditor});
            this.miEdit.Enabled = false;
            this.miEdit.Name = "miEdit";
            this.miEdit.Size = new System.Drawing.Size(39, 20);
            this.miEdit.Text = "Edit";
            // 
            // miAvatarEditor
            // 
            this.miAvatarEditor.Name = "miAvatarEditor";
            this.miAvatarEditor.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.miAvatarEditor.Size = new System.Drawing.Size(175, 22);
            this.miAvatarEditor.Text = "Avatar Editor";
            this.miAvatarEditor.Click += new System.EventHandler(this.miAvatarEditor_Click);
            // 
            // miScheduleEditor
            // 
            this.miScheduleEditor.Name = "miScheduleEditor";
            this.miScheduleEditor.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.miScheduleEditor.Size = new System.Drawing.Size(175, 22);
            this.miScheduleEditor.Text = "Schedule Editor";
            this.miScheduleEditor.Click += new System.EventHandler(this.miScheduleEditor_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // pnlPersonaManifest
            // 
            this.pnlPersonaManifest.BackColor = System.Drawing.SystemColors.Control;
            this.pnlPersonaManifest.Controls.Add(this.label5);
            this.pnlPersonaManifest.Controls.Add(this.txVersion);
            this.pnlPersonaManifest.Controls.Add(this.cxNSFW);
            this.pnlPersonaManifest.Controls.Add(this.label4);
            this.pnlPersonaManifest.Controls.Add(this.txCreator);
            this.pnlPersonaManifest.Controls.Add(this.label3);
            this.pnlPersonaManifest.Controls.Add(this.txNickname);
            this.pnlPersonaManifest.Controls.Add(this.label2);
            this.pnlPersonaManifest.Controls.Add(this.txLastName);
            this.pnlPersonaManifest.Controls.Add(this.label1);
            this.pnlPersonaManifest.Controls.Add(this.txFirstName);
            this.pnlPersonaManifest.Controls.Add(this.pbPersonaPreview);
            this.pnlPersonaManifest.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlPersonaManifest.Enabled = false;
            this.pnlPersonaManifest.Location = new System.Drawing.Point(0, 24);
            this.pnlPersonaManifest.Name = "pnlPersonaManifest";
            this.pnlPersonaManifest.Size = new System.Drawing.Size(246, 437);
            this.pnlPersonaManifest.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 314);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 15);
            this.label5.TabIndex = 11;
            this.label5.Text = "Version";
            // 
            // txVersion
            // 
            this.txVersion.Location = new System.Drawing.Point(93, 311);
            this.txVersion.Name = "txVersion";
            this.txVersion.Size = new System.Drawing.Size(74, 23);
            this.txVersion.TabIndex = 7;
            this.txVersion.TextChanged += new System.EventHandler(this.txVersion_TextChanged);
            // 
            // cxNSFW
            // 
            this.cxNSFW.AutoSize = true;
            this.cxNSFW.Location = new System.Drawing.Point(93, 370);
            this.cxNSFW.Name = "cxNSFW";
            this.cxNSFW.Size = new System.Drawing.Size(58, 19);
            this.cxNSFW.TabIndex = 10;
            this.cxNSFW.Text = "NSFW";
            this.cxNSFW.UseVisualStyleBackColor = true;
            this.cxNSFW.CheckedChanged += new System.EventHandler(this.cxNSFW_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 344);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 8;
            this.label4.Text = "Creator";
            // 
            // txCreator
            // 
            this.txCreator.Location = new System.Drawing.Point(93, 341);
            this.txCreator.Name = "txCreator";
            this.txCreator.Size = new System.Drawing.Size(138, 23);
            this.txCreator.TabIndex = 9;
            this.txCreator.TextChanged += new System.EventHandler(this.txCreator_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 285);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nickname";
            // 
            // txNickname
            // 
            this.txNickname.Location = new System.Drawing.Point(93, 282);
            this.txNickname.Name = "txNickname";
            this.txNickname.Size = new System.Drawing.Size(138, 23);
            this.txNickname.TabIndex = 5;
            this.txNickname.TextChanged += new System.EventHandler(this.txNickname_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 256);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Last Name";
            // 
            // txLastName
            // 
            this.txLastName.Location = new System.Drawing.Point(93, 253);
            this.txLastName.Name = "txLastName";
            this.txLastName.Size = new System.Drawing.Size(138, 23);
            this.txLastName.TabIndex = 3;
            this.txLastName.TextChanged += new System.EventHandler(this.txLastName_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 227);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 15);
            this.label1.TabIndex = 2;
            this.label1.Text = "First Name";
            // 
            // txFirstName
            // 
            this.txFirstName.Location = new System.Drawing.Point(93, 224);
            this.txFirstName.Name = "txFirstName";
            this.txFirstName.Size = new System.Drawing.Size(138, 23);
            this.txFirstName.TabIndex = 1;
            this.txFirstName.TextChanged += new System.EventHandler(this.txFirstName_TextChanged);
            // 
            // pbPersonaPreview
            // 
            this.pbPersonaPreview.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pbPersonaPreview.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbPersonaPreview.Location = new System.Drawing.Point(22, 10);
            this.pbPersonaPreview.Name = "pbPersonaPreview";
            this.pbPersonaPreview.Size = new System.Drawing.Size(200, 200);
            this.pbPersonaPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbPersonaPreview.TabIndex = 0;
            this.pbPersonaPreview.TabStop = false;
            // 
            // sfdPersona
            // 
            this.sfdPersona.Filter = "Persona Files|*.persona";
            // 
            // ofdPersona
            // 
            this.ofdPersona.Filter = "Persona Files|*.persona";
            // 
            // FMain
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 461);
            this.Controls.Add(this.pnlPersonaManifest);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "FMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Persona Maker";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FMain_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.pnlPersonaManifest.ResumeLayout(false);
            this.pnlPersonaManifest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPersonaPreview)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem miNewPersona;
        private ToolStripMenuItem miOpenPersona;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem miSavePersona;
        private ToolStripMenuItem miSavePersonaAs;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem miEdit;
        private ToolStripMenuItem miAvatarEditor;
        private ToolStripMenuItem personaCompendiumToolStripMenuItem;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem miClosePersona;
        private Panel pnlPersonaManifest;
        private PictureBox pbPersonaPreview;
        private Label label1;
        private TextBox txFirstName;
        private Label label2;
        private TextBox txLastName;
        private Label label3;
        private TextBox txNickname;
        private Label label4;
        private TextBox txCreator;
        private CheckBox cxNSFW;
        private SaveFileDialog sfdPersona;
        private OpenFileDialog ofdPersona;
        private ToolStripMenuItem miScheduleEditor;
        private Label label5;
        private TextBox txVersion;
    }
}