using Aura.Models;
using PersonaMaker.Forms;
using System.Drawing.Imaging;

namespace PersonaMaker
{
    public partial class FMain : Form
    {
        const string ApplicationName = "Persona Maker";
        static Version Version { get; } = new Version(1, 0, 3);

        bool _hasChanges = false;
        Persona? _persona;
        Persona? Persona
        {
            get => _persona; set
            {
                _persona = value;
                PersonaChanged();
            }
        }

        public FMain()
        {
            InitializeComponent();
            PersonaChanged();
        }

        private void PersonaChanged()
        {
            miClosePersona.Enabled = miSavePersona.Enabled = miSavePersonaAs.Enabled =
            pnlPersonaManifest.Enabled =
            miEdit.Enabled = Persona != null;
            Text = ApplicationName + " v" + Version.ToString() + (Persona?.LastSavePath != null ? " - " + Path.GetFileName(Persona.LastSavePath) : "");

            if (Persona != null)
            {
                txCreator.Text = Persona.Manifest.Creator;
                txFirstName.Text = Persona.Manifest.FirstName;
                txLastName.Text = Persona.Manifest.LastName;
                txNickname.Text = Persona.Manifest.Nickname;
                cxNSFW.Checked = Persona.Manifest.NSFW;
                txVersion.Text = Persona.Manifest.Version;
            }
            else
            {
                txCreator.Clear(); txFirstName.Clear();
                txLastName.Clear(); txNickname.Clear();
                txVersion.Clear();
                cxNSFW.Checked = false;
            }
            LoadPersonaPreview();
        }

        private bool ConfirmHasChanges()
        {
            if (_hasChanges && MessageBox.Show("This action will discard unsaved changes.\r\nAre you sure to continue?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                return false;
            return true;
        }

        private void miNewPersona_Click(object sender, EventArgs e)
        {
            if (!ConfirmHasChanges()) return;

            Persona = new Persona();
            _hasChanges = false;
        }

        private void miAvatarEditor_Click(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                using (var f = new FAvatarEditor())
                {
                    f.AssignManifestAndArchive(Persona.Avatar, Persona.Archive);
                    f.ShowDialog();
                    _hasChanges = true;
                    GeneratePersonaPreview();
                }
            }
        }

        private void miClosePersona_Click(object sender, EventArgs e)
        {
            if (!ConfirmHasChanges()) return;
            Persona = null;
            _hasChanges = false;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ConfirmHasChanges()) e.Cancel = true;
        }

        private void txFirstName_TextChanged(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                Persona.Manifest.FirstName = txFirstName.Text.Trim();
            }
        }

        private void txLastName_TextChanged(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                Persona.Manifest.LastName = txLastName.Text.Trim();
            }
        }

        private void txNickname_TextChanged(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                Persona.Manifest.Nickname = txNickname.Text.Trim();
            }
        }

        private void txCreator_TextChanged(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                Persona.Manifest.Creator = txCreator.Text.Trim();
            }
        }

        private void cxNSFW_CheckedChanged(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                Persona.Manifest.NSFW = cxNSFW.Checked;
            }
        }

        private void miSavePersona_Click(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                if (Persona.LastSavePath == null)
                {
                    miSavePersonaAs_Click(sender, e);
                }
                else
                {
                    try
                    {
                        SavePersonaPreview();
                        Persona.Save();
                        _hasChanges = false;
                        MessageBox.Show("Saved successfully!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void miSavePersonaAs_Click(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                sfdPersona.FileName = "";
                if (!string.IsNullOrWhiteSpace(txFirstName.Text))
                    sfdPersona.FileName = txFirstName.Text.Trim() + ".persona";
                if (sfdPersona.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        SavePersonaPreview();
                        Persona.Save(sfdPersona.FileName);
                        _hasChanges = false;
                        PersonaChanged();
                        MessageBox.Show("Saved successfully!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void miOpenPersona_Click(object sender, EventArgs e)
        {
            if (!ConfirmHasChanges()) return;
            try
            {
                if (ofdPersona.ShowDialog() == DialogResult.OK)
                {
                    Persona = Persona.Load(ofdPersona.FileName);
                    _hasChanges = false;
                    MessageBox.Show("Loaded successfully!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GeneratePersonaPreview()
        {
            pbPersonaPreview.Image = null;
            if (Persona != null)
            {
                try
                {
                    var blt = Persona.Avatar.Layers
                        .Select(x => !string.IsNullOrWhiteSpace(x.Value.Default) ? x.Value.GetMaterialPath(x.Value.Default) : null)
                        .Where(x => x != null && Persona.Archive.FileSystem.Exists(x)).Select(x => Persona.Archive.FileSystem[x ?? ""]).ToList();
                    if (blt.Count > 0)
                    {
                        Bitmap l0 = (Bitmap)Bitmap.FromStream(blt[0]);

                        var rect = new Rectangle(0, 0, l0.Width, l0.Height);

                        var head = (Persona.Avatar.Regions.ContainsKey("head") ? Persona.Avatar.Regions["head"] : null)?.FirstOrDefault();
                        if (head != null)
                        {
                            var x = (int)head.X;
                            var y = (int)head.Y;
                            var w = (int)head.Width;
                            var h = (int)head.Height;
                            var r = (int)head.Radius;
                            if (head.Type == Aura.Models.Avatar.RegionType.Point)
                            {
                                rect = new Rectangle(Math.Max(0, x - 100), Math.Max(0, y - 100), 200, 200);
                            }
                            else if (head.Type == Aura.Models.Avatar.RegionType.Rectangle)
                            {
                                rect = new Rectangle(Math.Max(0, x - 50), Math.Max(0, y - 50), w + 100, h + 100);
                            }
                            else if (head.Type == Aura.Models.Avatar.RegionType.Radius)
                            {
                                rect = new Rectangle(Math.Max(0, x - r / 2 - 50), Math.Max(0, y - r / 2 - 50), r + 100, r + 100);
                            }
                        }
                        var dest = new Rectangle(0, 0, rect.Width, rect.Height);

                        var bmp = new Bitmap(rect.Width, rect.Height, l0.PixelFormat);
                        using (var g = Graphics.FromImage(bmp))
                        {
                            for (var i = 0; i < blt.Count; i++)
                            {
                                Bitmap mrg = (Bitmap)Bitmap.FromStream(blt[i]);
                                g.DrawImage(mrg, dest, rect, GraphicsUnit.Pixel);
                            }
                        }
                        pbPersonaPreview.Image = bmp;
                    }
                }
                catch { }
            }
        }

        private void LoadPersonaPreview()
        {
            pbPersonaPreview.Image = null;
            if (Persona != null && Persona.Archive.FileSystem.Exists(Persona.PersonaPreviewPath))
            {
                try
                {
                    var strm = Persona.Archive.FileSystem[Persona.PersonaPreviewPath];
                    Bitmap bmp = (Bitmap)Bitmap.FromStream(strm);
                    pbPersonaPreview.Image = bmp;
                }
                catch { }
            }
        }

        private void SavePersonaPreview()
        {
            if (Persona != null && pbPersonaPreview.Image is Bitmap bmp)
            {
                try
                {
                    using (var ms = new MemoryStream())
                    {
                        bmp.Save(ms, ImageFormat.Png);
                        Persona.Archive.FileSystem.Add(Persona.PersonaPreviewPath, ms);
                    }
                }
                catch { }
            }
        }

        private void miScheduleEditor_Click(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                using (var f = new FScheduleEditor())
                {
                    f.Initialize(Persona);
                    f.ShowDialog();
                    _hasChanges = true;
                }
            }
        }

        private void txVersion_TextChanged(object sender, EventArgs e)
        {
            if (Persona != null)
            {
                Persona.Manifest.Version = txVersion.Text.Trim();
            }
        }
    }
}