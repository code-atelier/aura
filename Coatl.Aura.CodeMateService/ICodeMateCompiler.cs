﻿using Coatl.Aura.CodeMateService.JSCompile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService
{
    public interface ICodeMateCompiler
    {
        /// <summary>
        /// Detect whether a file is a compatible manifest or not
        /// </summary>
        /// <param name="path">Manifest file path</param>
        /// <returns>True if compatible, false otherwise</returns>
        ICodeMateCompilerManifest? DetectManifest(string path);

        /// <summary>
        /// Compile a code from manifest file
        /// </summary>
        /// <param name="manifestFile"></param>
        /// <param name="outputPath"></param>
        /// <returns>True if successful, false otherwise</returns>
        bool Compile(string manifestFile, string? outputPath = null);

        /// <summary>
        /// Compile a code from manifest file
        /// </summary>
        /// <param name="manifestFile"></param>
        /// <param name="outputPath"></param>
        /// <returns>True if successful, false otherwise</returns>
        Task CompileAsync(string manifestFile, string? outputPath = null);

        /// <summary>
        /// Gets the paths that needs to be monitored for auto-compile of a certain manifest file
        /// </summary>
        /// <param name="manifestFile"></param>
        /// <returns></returns>
        IEnumerable<string>? GetFileSystemMonitorPaths(string manifestFile);

        event EventHandler<JSCompileInfo>? CompileCompleted;

        event EventHandler<JSCompileInfo>? CompileFailed;
    }

    public interface ICodeMateCompilerManifest
    {
        string? ProjectName { get; }
    }

    public class JSCompileInfo
    {
        public ICodeMateCompilerManifest? Manifest { get; set; }

        public string? OutputPath { get; set; }

        public bool Success { get; set; }

        public Exception? Exception { get; set; }
    }
}
