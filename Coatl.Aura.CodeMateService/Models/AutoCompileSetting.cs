﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.Models
{
    public class AutoCompileSetting
    {
        [JsonPropertyName("active")]
        public bool Active { get; set; }

        [JsonPropertyName("delay")]
        public double Delay { get; set; } = 1;

        [JsonPropertyName("cooldown")]
        public double Cooldown { get; set; } = 3;

        [JsonPropertyName("monitor")]
        public List<string> Monitor { get; set; } = new List<string>();
    }
}
