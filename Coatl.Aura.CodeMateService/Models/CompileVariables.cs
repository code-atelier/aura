﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.Models
{
    public class CompileVariables: Dictionary<string, string>
    {
        public const string SourcePath = "SourcePath";
        public const string OutputPath = "OutputPath";
        public const string ProjectName = "ProjectName";
        public const string Version = "Version";
        public const string SolutionName = "SolutionName";
        public const string SolutionPath = "SolutionPath";

        public string Replace(string text)
        {
            foreach(var kv in this)
            {
                var key = "{" + kv.Key + "}";
                text = text.Replace(key, kv.Value);
            }
            return text;
        }
    }
}
