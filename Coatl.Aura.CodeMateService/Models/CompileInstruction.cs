﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.Models
{
    public class CompileInstruction
    {
        [JsonPropertyName("name")]
        public string SolutionName { get; set; } = string.Empty;

        [JsonIgnore]
        public string SolutionPath { get; set; } = string.Empty;

        [JsonPropertyName("targets")]
        public List<CompileTarget> Targets { get; set; } = new List<CompileTarget>();

        [JsonPropertyName("autoCompile")]
        public AutoCompileSetting AutoCompile { get; set; } = new AutoCompileSetting();

        public CompileVariables GenerateVariables(CompileTarget target)
        {
            var res = new CompileVariables();
            res[CompileVariables.Version] = target.Version;
            res[CompileVariables.ProjectName] = target.ProjectName;
            res[CompileVariables.OutputPath] = Path.GetFullPath(target.OutputPath);
            res[CompileVariables.SourcePath] = Path.GetFullPath(target.SourcePath);
            return res;
        }
    }
}
