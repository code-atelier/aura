﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.Models
{
    public class CompileTarget
    {
        [JsonPropertyName("name")]
        public string ProjectName { get; set; } = string.Empty;

        [JsonPropertyName("version")]
        public string Version { get; set; } = string.Empty;

        [JsonPropertyName("compilerGuid")]
        public string CompilerGuid { get; set; } = string.Empty;

        [JsonPropertyName("sourcePath")]
        public string SourcePath { get; set; } = string.Empty;

        [JsonPropertyName("outputPath")]
        public string OutputPath { get; set; } = string.Empty;

        [JsonPropertyName("arguments")]
        public Dictionary<string, string>? Arguments { get; set; }

        [JsonPropertyName("preCompile")]
        public string? PreCompile { get; set; }

        [JsonPropertyName("postCompile")]
        public string? PostCompile { get; set; }
    }
}
