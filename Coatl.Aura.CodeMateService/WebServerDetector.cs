﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService
{
    public static class WebServerDetector
    {
        public static string? DetectPhpExecutable(string path)
        {
            var paths = path.Split(new char[] { '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
            for(var i = paths.Length; i >= 0; i--)
            {
                var phppath = Path.Combine(string.Join("\\", paths[0..i]), "php", "php.exe");
                if (File.Exists(phppath))
                {
                    return phppath;
                }
            }
            var envPaths = Environment.GetEnvironmentVariable("PATH")?.Split(";", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
            if (envPaths != null)
            {
                foreach (var epath in envPaths)
                {
                    var phppath = Path.Combine(epath, "php.exe");
                    if (File.Exists(phppath))
                    {
                        return "php";
                    }
                }
            }
            return null;
        }
    }
}
