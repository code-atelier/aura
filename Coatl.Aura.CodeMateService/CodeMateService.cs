﻿using Aura.Models;
using Aura.Services;
using Coatl.Aura.CodeMateService.CMZMate;
using Coatl.Aura.CodeMateService.JSCompile;
using Coatl.Aura.CodeMateService.Laravel;
using Microsoft.VisualBasic;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Threading;

namespace Coatl.Aura.CodeMateService
{
    public class CodeMateService : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.codemate";
        public override string ServiceName => "Code Mate";
        public override string? ServiceIcon => "/Coatl.Aura.CodeMateService;Component/res/cm.ico";

        public List<ICodeMateCompiler> Compilers { get; } = new List<ICodeMateCompiler>();
        public List<string> AutoCompileProjects { get; } = new List<string>();
        public Dictionary<string, ICodeMateCompiler> HandleCompile { get; } = new Dictionary<string, ICodeMateCompiler>();
        public Dictionary<string, DateTime> LastCompile { get; } = new Dictionary<string, DateTime>();
        public Dictionary<FileSystemWatcher, string> FileSystemWatchers { get; } = new Dictionary<FileSystemWatcher, string>();
        public Dictionary<FileSystemWatcher, DateTime> CompileQueue { get; } = new Dictionary<FileSystemWatcher, DateTime>();
        public DispatcherTimer DispatcherTimer { get; } = new DispatcherTimer(DispatcherPriority.Background);

        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                Compilers.Add(new JSCompiler());
                DispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 500);
                DispatcherTimer.Tick += DispatcherTimer_Tick;
                DispatcherTimer.Start();

                foreach (var compiler in Compilers)
                {
                    compiler.CompileCompleted += Compiler_CompileCompleted;
                    compiler.CompileFailed += Compiler_CompileFailed;
                }
            });
        }

        private async void DispatcherTimer_Tick(object? sender, EventArgs e)
        {
            FileSystemWatcher? fsw = null;
            foreach (var kv in CompileQueue)
            {
                var delta = DateTime.Now - kv.Value;
                if (delta.TotalSeconds >= 1)
                {
                    fsw = kv.Key;
                    if (FileSystemWatchers.ContainsKey(fsw))
                    {
                        var manifestFile = FileSystemWatchers[fsw];
                        if (HandleCompile.ContainsKey(manifestFile) && LastCompile.ContainsKey(manifestFile))
                        {
                            var compiler = HandleCompile[manifestFile];
                            try
                            {
                                await compiler.CompileAsync(manifestFile);
                            }
                            catch { }
                        }
                    }
                }
            }
            if (fsw != null)
            {
                CompileQueue.Remove(fsw);
            }
        }

        private async void Compiler_CompileFailed(object? sender, JSCompileInfo e)
        {
            if (Manager != null)
            {
                var msg = this.CreateToastNotificationMessage(
                        "Code Mate",
                        $"Failed to compile {e.Manifest?.ProjectName ?? "your project"}: {e.Exception?.Message ?? "Fatal error"}",
                        ServiceID
                    );
                await Manager.BroadcastMessage(msg);
            }
        }

        private async void Compiler_CompileCompleted(object? sender, JSCompileInfo e)
        {
            if (Manager != null)
            {
                var arg =
                        e.OutputPath != null ?
                        new Dictionary<string, string>()
                        {
                            { "action", "show-in-explorer" },
                            { "paths", e.OutputPath.Trim('\\').Trim('/') }
                        } : null;
                var msg = this.CreateToastNotificationMessage(
                        "Code Mate",
                        $"{e.Manifest?.ProjectName ?? "Your project"} compiled successfully",
                        InternalServices.CoreService,
                        arg
                    );
                await Manager.BroadcastMessage(msg);
            }
        }

        private void Fsw_Changed(object sender, FileSystemEventArgs e)
        {
            if (sender is FileSystemWatcher fsw)
            {
                CompileQueue[fsw] = DateTime.Now;
            }
        }

        public bool IsCompatibleFile(string filePath)
        {
            if (LaravelMateDetector.DetectLaravelDirectory(filePath) != null)
                return true;
            if (CMZMateDetector.DetectCMZDirectory(filePath) != null)
                return true;
            if (Compilers.Any(x => x.DetectManifest(filePath) != null)) return true;

            return false;
        }

        public string? FirstCompatibleFile(IEnumerable<string> filePaths)
        {
            foreach (var filePath in filePaths)
            {
                if (IsCompatibleFile(filePath))
                    return filePath;
            }
            return null;
        }

        public async Task<bool> HandleFirstCompatibleFile(IEnumerable<string> filePaths)
        {
            bool handled = false;
            if (Manager == null) return handled;
            foreach (var file in filePaths)
            {
                var path = LaravelMateDetector.DetectLaravelDirectory(file);
                if (path != null)
                {
                    var msg = this.CreateMessage(InternalServices.MessageService,
                        $"Wow!\r\nA Laravel\r\nproject!");
                    var prevGuid = msg.Guid.ToString();
                    await Manager.BroadcastMessage(msg);
                    handled = true;

                    var f = new LaravelMate();
                    f.Owner = Manager.MainWindow;
                    f.Initialize(path);
                    f.Show();
                    return handled;
                }

                path = CMZMateDetector.DetectCMZDirectory(file);
                if (path != null)
                {
                    var msg = this.CreateMessage(InternalServices.MessageService,
                        $"Oh?\r\nA RPGMZ-CMZ\r\nproject!?");
                    var prevGuid = msg.Guid.ToString();
                    await Manager.BroadcastMessage(msg);
                    handled = true;

                    var f = new CMZMate.CMZMate();
                    f.Owner = Manager.MainWindow;
                    f.Initialize(path);
                    f.Show();
                    return handled;
                }

                var compile = Compilers.FirstOrDefault(x => x.DetectManifest(file) != null);
                if (compile != null)
                {
                    HandleCompile[file] = compile;
                    if (!LastCompile.ContainsKey(file))
                        LastCompile[file] = DateTime.MinValue;
                    var manifest = compile.DetectManifest(file);
                    if (manifest != null)
                    {
                        handled = true;
                        var autoCompile = compile.GetFileSystemMonitorPaths(file);
                        if (autoCompile != null && autoCompile.Count() > 0 && manifest.ProjectName != null)
                        {
                            var success = false;
                            if (!AutoCompileProjects.Contains(manifest.ProjectName))
                            {
                                foreach (var ac in autoCompile)
                                {
                                    try
                                    {
                                        var fsw = new FileSystemWatcher();
                                        fsw.Path = ac;
                                        fsw.IncludeSubdirectories = true;
                                        fsw.Changed += Fsw_Changed;
                                        fsw.EnableRaisingEvents = true;
                                        FileSystemWatchers.Add(fsw, file);
                                        success = true;
                                    }
                                    catch { }
                                }
                                AutoCompileProjects.Add(manifest.ProjectName);
                            }
                            else
                            {
                                success = true;
                            }
                            if (success)
                            {
                                var msg = this.CreateMessage(
                                    InternalServices.MessageService,
                                    $"Auto-compile\r\nfor this project\r\nis active");
                                await Manager.BroadcastMessage(msg);
                            }
                        }
                        else
                        {
                            var msg = this.CreateMessage(
                                InternalServices.MessageService,
                                $"I will\r\ncompile the project,\r\nplease wait");
                            await Manager.BroadcastMessage(msg);
                        }
                        return handled;
                    }
                }
            }
            return handled;
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            if (message.From(InternalSensors.DragDrop))
            {
                if (message.HeaderIs("event", "preview")
                    && message.Body is PoolRequest request
                    && request.RequestData is IDataObject data)
                {
                    try
                    {
                        if (data.GetDataPresent(DataFormats.FileDrop))
                        {
                            var fileList = data.GetData(DataFormats.FileDrop) as string[];
                            if (fileList != null && FirstCompatibleFile(fileList) != null)
                            {
                                request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                {
                                    IconUri = ServiceIcon
                                });
                            }
                        }
                    }
                    catch { }
                }
                if (message.HeaderIs("event", "drop")
                    && message.Body is IDataObject drop)
                {
                    try
                    {
                        if (drop.GetDataPresent(DataFormats.FileDrop))
                        {
                            var fileList = drop.GetData(DataFormats.FileDrop) as string[];
                            if (fileList != null)
                            {
                                if (await HandleFirstCompatibleFile(fileList))
                                    message.Handled = true;
                            }
                        }
                    }
                    catch { }
                }
            }
        }
    }
}