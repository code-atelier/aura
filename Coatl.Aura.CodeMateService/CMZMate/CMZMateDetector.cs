﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.CMZMate
{
    public static class CMZMateDetector
    {
        private const string AnchorFile = "game.rmmzproject";
        public static string? DetectCMZDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    path = Path.GetDirectoryName(path) ?? "";
                }

                while (!string.IsNullOrEmpty(path))
                {
                    var fpath = Path.Combine(path, AnchorFile);
                    if (File.Exists(fpath))
                    {
                        var cmzpath = Path.Combine(path, "js", "plugins", "cmz.core.js");
                        if (File.Exists(cmzpath))
                            return path;
                    }
                    path = Path.GetDirectoryName(path.Trim('\\').Trim('/')) ?? "";
                }
            }
            catch { }
            return null;
        }
    }
}
