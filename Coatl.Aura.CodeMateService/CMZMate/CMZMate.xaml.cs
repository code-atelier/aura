﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coatl.Aura.CodeMateService.CMZMate
{
    /// <summary>
    /// Interaction logic for CMZMate.xaml
    /// </summary>
    public partial class CMZMate : Window
    {
        public CMZMate()
        {
            InitializeComponent();
        }

        private void asdasd()
        {
            if (Clipboard.ContainsData("DataObject"))
            {
                var data = Clipboard.GetData("DataObject") as MemoryStream;
            }
            if (Clipboard.ContainsData("application/rpgmz-EventCommand"))
            {
                var data = Clipboard.GetData("application/rpgmz-EventCommand");
            }
            if (Clipboard.ContainsData("Ole Private Data"))
            {
                var data = Clipboard.GetData("Ole Private Data");
            }
        }

        public string BasePath { get; private set; } = string.Empty;
        public void Initialize(string path)
        {
            BasePath = path;
        }
    }
}
