﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.JSCompile
{
    public class JSCompileManifest: ICodeMateCompilerManifest
    {
        [JsonPropertyName("name")]
        public string? ProjectName { get; set; }

        [JsonPropertyName("sourcePath")]
        public string? SourcePath { get; set; }

        [JsonPropertyName("outputPath")]
        public string? OutputPath { get; set; }

        [JsonPropertyName("files")]
        public List<JSCompileFile> Files { get; set; } = new List<JSCompileFile>();

        [JsonPropertyName("autoCompile")]
        public bool AutoCompile { get; set; }
    }
}
