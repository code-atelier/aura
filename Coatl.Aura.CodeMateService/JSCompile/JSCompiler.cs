﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.JSCompile
{
    public class JSCompiler : ICodeMateCompiler
    {
        public event EventHandler<JSCompileInfo>? CompileCompleted;
        public event EventHandler<JSCompileInfo>? CompileFailed;

        public bool Compile(string manifestFile, string? outputPath = null)
        {
            JSCompileManifest? jsManifest = null;
            try
            {
                var json = File.ReadAllText(manifestFile);
                jsManifest = JsonSerializer.Deserialize<JSCompileManifest>(json);

                if (jsManifest == null || string.IsNullOrWhiteSpace(jsManifest.ProjectName)) throw new Exception("Invalid manifest file");
                if (!Directory.Exists(jsManifest.SourcePath)) throw new Exception("Source path does not exists: " + jsManifest.SourcePath);
                if (string.IsNullOrWhiteSpace(jsManifest.OutputPath)) throw new Exception("Output path cannot be empty");
                if (jsManifest.Files == null || jsManifest.Files.Count <= 0) throw new Exception("No file to compile");

                var basePath = jsManifest.SourcePath;
                var outputs = new Dictionary<string, string>();
                var fileIndex = 0;
                foreach (var file in jsManifest.Files)
                {
                    if (string.IsNullOrWhiteSpace(file.CompiledFileName)) throw new Exception($"File at index {fileIndex} does not have output file name");
                    if (file.SourceFiles == null || file.SourceFiles.Count <= 0) throw new Exception($"File {file.CompiledFileName} does not have any source file");

                    var oPath = Path.Combine(jsManifest.OutputPath, file.CompiledFileName);
                    var js = new List<string>();
                    foreach (var sf in file.SourceFiles)
                    {
                        var fullSourcePath = Path.Combine(basePath, sf);
                        if (Directory.Exists(fullSourcePath))
                        {
                            var files = Directory.EnumerateFiles(fullSourcePath, "*.js");
                            foreach (var f in files)
                            {
                                js.Add(File.ReadAllText(f));
                            }
                        }
                        else if (File.Exists(fullSourcePath))
                        {
                            js.Add(File.ReadAllText(fullSourcePath));
                        }
                        else
                        {
                            throw new Exception($"File not found for {file.CompiledFileName}: {sf}");
                        }
                    }

                    outputs.Add(oPath, string.Join(Environment.NewLine, js));

                    fileIndex++;
                }

                string lastOutputDir = "";
                foreach (var output in outputs)
                {
                    var dir = Path.GetDirectoryName(output.Key);
                    if (!string.IsNullOrWhiteSpace(dir) && !Directory.Exists(dir))
                        Directory.CreateDirectory(dir);
                    if (dir != null) 
                        lastOutputDir = dir;
                    if (File.Exists(output.Key))
                    {
                        File.Delete(output.Key);
                    }
                    File.WriteAllText(output.Key, output.Value);
                }

                CompileCompleted?.Invoke(this, new JSCompileInfo()
                {
                    Manifest = jsManifest,
                    OutputPath = lastOutputDir,
                    Success = true,
                });
                return true;
            }
            catch(Exception ex)
            {
                CompileFailed?.Invoke(this, new JSCompileInfo()
                {
                    Manifest = jsManifest,
                    Success = false,
                    Exception = ex,
                });
                return false;
            }
        }

        public async Task CompileAsync(string manifestFile, string? outputPath = null)
        {
            await Task.Run(() =>
            {
                Compile(manifestFile, outputPath);
            });
        }

        public ICodeMateCompilerManifest? DetectManifest(string path)
        {
            try
            {
                var ext = Path.GetExtension(path);
                if (ext?.ToLower() != ".json") return null;
                var json = File.ReadAllText(path);
                var jsManifest = JsonSerializer.Deserialize<JSCompileManifest>(json);
                if (jsManifest != null)
                {
                    return jsManifest;
                }
            }
            catch { }
            return null;
        }

        public IEnumerable<string>? GetFileSystemMonitorPaths(string manifestFile)
        {
            try
            {
                var res = new List<string>();
                var json = File.ReadAllText(manifestFile);
                var jsManifest = JsonSerializer.Deserialize<JSCompileManifest>(json);
                if (jsManifest != null && Directory.Exists(jsManifest.SourcePath))
                {
                    res.Add(jsManifest.SourcePath);
                    return res;
                }
            }
            catch { }
            return null;
        }
    }
}
