﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.JSCompile
{
    public class JSCompileFile
    {
        [JsonPropertyName("output")]
        public string? CompiledFileName { get; set; }

        [JsonPropertyName("sources")]
        public List<string> SourceFiles { get; set; } = new List<string>();
    }
}
