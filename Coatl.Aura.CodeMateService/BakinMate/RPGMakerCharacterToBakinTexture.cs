﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Chat;

namespace Coatl.Aura.CodeMateService.BakinMate
{
    public static class RPGMakerCharacterToBakinTexture
    {
        public static bool Compatible(string path, bool sizeCheck)
        {
            var ext = Path.GetExtension(path)?.ToLower();
            if (ext != ".png") return false;
            if (!sizeCheck) return false;

            try
            {
                using (var bmp = (Bitmap)Bitmap.FromFile(path))
                {
                    return bmp.Width == bmp.Height * 3 / 2;
                }
            }
            catch
            {
                return false;
            }
        }

        public static string Convert(string path)
        {
            using (var bmp = (Bitmap)Bitmap.FromFile(path))
            {
                var imgWidth = bmp.Width;
                var imgHeight = bmp.Height;
                var basePath = Path.GetDirectoryName(path) ?? "";
                var fName = Path.GetFileNameWithoutExtension(path);
                var fExt = Path.GetExtension(path);

                bool multiChara = false;
                if (imgWidth > imgHeight && !fName.StartsWith("$"))
                {
                    multiChara = true;
                }

                string outfile = "";

                if (!multiChara)
                {
                    ExportWalk(bmp, new Rectangle(0, 0, imgWidth, imgHeight), Path.Combine(basePath, fName, fName.Replace("_", "") + "walk" + fExt));
                    outfile = ExportWait(bmp, new Rectangle(0, 0, imgWidth, imgHeight), Path.Combine(basePath, fName, fName.Replace("_", "") + "wait" + fExt));
                }
                else
                {
                    var partHeight = imgHeight / 2;
                    var partWidth = imgWidth / 4;
                    int cnt = 0;
                    for (var row = 0; row < 2; row++)
                    {
                        for (var col = 0; col < 4; col++)
                        {
                            var srcRect = new Rectangle(col * partWidth, row * partHeight, partWidth, partHeight);
                            var fname = Path.Combine(basePath, fName, fName.Replace("_", "") + cnt + "_walk" + fExt);
                            ExportWalk(bmp, srcRect, fname);
                            fname = Path.Combine(basePath, fName, fName.Replace("_", "") + cnt + "_wait" + fExt);
                            outfile = ExportWait(bmp, srcRect, fname);
                            cnt++;
                        }
                    }
                }
                return outfile;
            }
        }

        static string ExportWalk(Bitmap bitmap, Rectangle sourceRect, string dest)
        {
            var destDir = Path.GetDirectoryName(dest) ?? "";
            if (!Directory.Exists(destDir))
                Directory.CreateDirectory(destDir);
            var rowHeight = sourceRect.Height / 4;
            using (var bmp = new Bitmap(sourceRect.Width, sourceRect.Height, bitmap.PixelFormat))
            {
                using (var g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.Transparent);

                    // copy first 2 row
                    g.DrawImage(
                        bitmap,
                        new Rectangle(0, 0, sourceRect.Width, rowHeight * 2),
                        new Rectangle(sourceRect.Left, sourceRect.Top, sourceRect.Width, rowHeight * 2),
                        GraphicsUnit.Pixel
                        );

                    // copy 3rd row to 4th row
                    g.DrawImage(
                        bitmap,
                        new Rectangle(0, rowHeight * 3, sourceRect.Width, rowHeight),
                        new Rectangle(sourceRect.Left, sourceRect.Top + rowHeight * 2, sourceRect.Width, rowHeight),
                        GraphicsUnit.Pixel
                        );

                    // copy 4th row to 3rd row
                    g.DrawImage(
                        bitmap,
                        new Rectangle(0, rowHeight * 2, sourceRect.Width, rowHeight),
                        new Rectangle(sourceRect.Left, sourceRect.Top + rowHeight * 3, sourceRect.Width, rowHeight),
                        GraphicsUnit.Pixel
                        );
                    // flush
                    g.Flush();
                }
                if (File.Exists(dest))
                    File.Delete(dest);
                bmp.Save(dest);
                return dest;
            }
        }
        static string ExportWait(Bitmap bitmap, Rectangle sourceRect, string dest)
        {
            var rowHeight = sourceRect.Height / 4;
            var colWidth = sourceRect.Width / 3;
            using (var bmp = new Bitmap(colWidth, sourceRect.Height, bitmap.PixelFormat))
            {
                using (var g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.Transparent);

                    // copy first 2 row
                    g.DrawImage(
                        bitmap,
                        new Rectangle(0, 0, colWidth, rowHeight * 2),
                        new Rectangle(sourceRect.Left + colWidth, sourceRect.Top, colWidth, rowHeight * 2),
                        GraphicsUnit.Pixel
                        );

                    // copy 3rd row to 4th row
                    g.DrawImage(
                        bitmap,
                        new Rectangle(0, rowHeight * 3, colWidth, rowHeight),
                        new Rectangle(sourceRect.Left + colWidth, sourceRect.Top + rowHeight * 2, colWidth, rowHeight),
                        GraphicsUnit.Pixel
                        );

                    // copy 4th row to 3rd row
                    g.DrawImage(
                        bitmap,
                        new Rectangle(0, rowHeight * 2, colWidth, rowHeight),
                        new Rectangle(sourceRect.Left + colWidth, sourceRect.Top + rowHeight * 3, colWidth, rowHeight),
                        GraphicsUnit.Pixel
                        );
                    // flush
                    g.Flush();
                }
                if (File.Exists(dest))
                    File.Delete(dest);
                bmp.Save(dest);
                return dest;
            }
        }
    }
}
