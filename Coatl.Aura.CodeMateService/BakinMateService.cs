﻿using Aura.Models;
using Aura.Services;
using Coatl.Aura.CodeMateService.BakinMate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Coatl.Aura.CodeMateService
{
    public class BakinMateService : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.bakinmate";

        public override string ServiceName => "Bakin Mate";

        private bool AnyCompatibleFile(IEnumerable<string> paths)
        {
            return paths.Any(x => RPGMakerCharacterToBakinTexture.Compatible(x, false));
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            if (message.From(InternalSensors.DragDrop))
            {
                if (message.HeaderIs("event", "preview")
                    && message.Body is PoolRequest request
                    && request.RequestData is IDataObject data)
                {
                    try
                    {
                        if (data.GetDataPresent(DataFormats.FileDrop))
                        {
                            var fileList = data.GetData(DataFormats.FileDrop) as string[];
                            if (fileList != null && AnyCompatibleFile(fileList))
                            {
                                request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                {
                                    Text = "RPGM to Bakin Character",
                                });
                            }
                        }
                    }
                    catch { }
                }
                if (message.HeaderIs("event", "drop")
                    && message.Body is IDataObject drop)
                {
                    try
                    {
                        if (drop.GetDataPresent(DataFormats.FileDrop))
                        {
                            var fileList = drop.GetData(DataFormats.FileDrop) as string[];
                            if (fileList != null && Manager != null)
                            {
                                Dictionary<string, int> successfulOperation = new Dictionary<string, int>();
                                Dictionary<string, string> lastOutputFile = new Dictionary<string, string>();
                                foreach (var file in fileList)
                                {
                                    if (RPGMakerCharacterToBakinTexture.Compatible(file, true))
                                    {
                                        try
                                        {
                                            var outFile = RPGMakerCharacterToBakinTexture.Convert(file);
                                            if (!string.IsNullOrWhiteSpace(outFile))
                                                lastOutputFile["bakinconvert"] = outFile;
                                            if (!successfulOperation.ContainsKey("bakinconvert"))
                                                successfulOperation["bakinconvert"] = 0;
                                            successfulOperation["bakinconvert"]++;
                                        }
                                        catch (Exception ex)
                                        {
                                            var msg = this.CreateToastNotificationMessage(
                                                "RPGMaker to Bakin Character Converter",
                                                 $"Failed to convert '{Path.GetFileName(file)}': " + ex.Message,
                                                 ServiceID);
                                            await Manager.BroadcastMessage(msg);
                                        }
                                    }
                                }

                                foreach (var op in successfulOperation)
                                {
                                    string title = ServiceName;
                                    string body = "";
                                    switch (op.Key)
                                    {
                                        case "bakinconvert":
                                            title = "RPGMaker to Bakin Character Converter";
                                            body = op.Value > 0 ? $"{(op.Value > 1 ? op.Value + " files" : "File")} converted successfully" : "No compatible file found";
                                            break;
                                    }

                                    var arg = lastOutputFile.ContainsKey(op.Key) ? new Dictionary<string, string>()
                                            {
                                                { "action", "show-in-explorer" },
                                                { "paths", lastOutputFile[op.Key] }
                                            } : null;

                                    var msg = this.CreateToastNotificationMessage(
                                            title, body, InternalServices.CoreService, arg
                                        );
                                    await Manager.BroadcastMessage(msg);
                                }
                            }
                        }
                    }
                    catch { }
                }
            }
        }
    }
}
