﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Coatl.Aura.CodeMateService.Laravel
{
    /// <summary>
    /// Interaction logic for LaravelMate.xaml
    /// </summary>
    public partial class LaravelMate : Window
    {
        public LaravelMate()
        {
            InitializeComponent();
            BaseDirectory = "";
        }

        public string BaseDirectory
        {
            get; set;
        }
        private string PHPPath { get; set; } = string.Empty;

        public void Initialize(string baseDir)
        {
            Title = "Laravel Mate @" + baseDir;
            BaseDirectory = baseDir;
            var path = WebServerDetector.DetectPhpExecutable(baseDir);
            while (path == null)
            {
                path = "";
                MessageBox.Show("PHP executable directory cannot be found.", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            PHPPath = path;
            txConsole.AppendText("Welcome to LaravelMate!" + Environment.NewLine);
            txConsole.AppendText("Your Laravel directory is " + baseDir + Environment.NewLine);
            txConsole.AppendText("Detected PHP executable is " + PHPPath + Environment.NewLine);

            var packjson = System.IO.Path.Combine(baseDir, "package.json");
            var compojson = System.IO.Path.Combine(baseDir, "package.json");
            btnComposer.IsEnabled = File.Exists(compojson);
            btnNPMRunDev.IsEnabled = btnNPMRunProd.IsEnabled = File.Exists(packjson);
        }

        private void txMigrationName_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnExecuteMakeMigration.IsEnabled = !string.IsNullOrWhiteSpace(txMigrationName.Text);
        }

        private void btnExecuteMakeMigration_Click(object sender, RoutedEventArgs e)
        {
            var migrationName = cbMigrationType.Text + txMigrationName.Text + (cbMigrationType.Text == "alter_" ? "" : "_table");
            while (migrationName.Contains("  ") || migrationName.Contains("\t"))
            {
                migrationName = migrationName.Replace("  ", " ").Replace("\t", " ");
            }
            migrationName = migrationName.Replace(" ", "_");

            RunArtisan("make:migration " + migrationName);
            txMigrationName.Clear();
        }

        private void WriteLog(string log)
        {
            Dispatcher.BeginInvoke(() =>
            {
                txConsole.AppendText("> " + log + Environment.NewLine);
                txConsole.ScrollToEnd();
            });
        }

        private async void RunArtisan(string command)
        {
            try
            {
                var psi = new ProcessStartInfo(PHPPath);
                psi.WorkingDirectory = BaseDirectory;
                psi.Arguments = " artisan " + command;
                WriteLog(psi.Arguments.Trim());
                psi.UseShellExecute = false;
                psi.WindowStyle = ProcessWindowStyle.Hidden;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;
                psi.CreateNoWindow = true;
                var proc = new Process();
                proc.StartInfo = psi;
                proc.Start();
                await proc.WaitForExitAsync();
                var so = proc.StandardOutput.ReadToEnd();
                var lines = so?.Split(new[] { '\r', '\n' }, StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                if (lines != null)
                    foreach (var line in lines)
                    {
                        WriteLog(line);
                    }
            }
            catch (Exception ex)
            {
                WriteLog("Error: " + ex.Message);
            }
        }

        private void txMigrationName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                if (btnExecuteMakeMigration.IsEnabled)
                    btnExecuteMakeMigration_Click(sender, e);
            }
        }

        private void btnMigrateCommandExecute_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure to execute '" + cbMigrateCommand.Text + "'?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                return;
            RunArtisan(cbMigrateCommand.Text);
        }

        private void txExecuteCB_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnExecuteCB.IsEnabled = !string.IsNullOrWhiteSpace(txExecuteCB.Text);
        }

        private void btnExecuteCB_Click(object sender, RoutedEventArgs e)
        {
            var cmd = cbExecute.Text;
            var args = txExecuteCB.Text.Trim();

            var fullCommand = cmd.Contains(" ") ? args : cmd + " " + args;
            if (MessageBox.Show("Are you sure to execute '" + fullCommand + "'?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                return;
            RunArtisan(fullCommand);
            txExecuteCB.Clear();
        }

        private async void btnDirectConsoleExec(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is Button btn && btn.Content is string str)
                {
                    if (MessageBox.Show("Are you sure to execute '" + str + "'?", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) != MessageBoxResult.Yes)
                        return;
                    var psi = new ProcessStartInfo("cmd");
                    psi.WorkingDirectory = BaseDirectory;
                    psi.Arguments = "/c " + str;
                    WriteLog(str);
                    psi.UseShellExecute = false;
                    var proc = new Process();
                    proc.StartInfo = psi;
                    proc.Start();
                    await proc.WaitForExitAsync();
                }
            }
            catch (Exception ex)
            {
                WriteLog("Error: " + ex.Message);
            }
        }

        private void txExecuteCB_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                if (btnExecuteCB.IsEnabled)
                {
                    btnExecuteCB_Click(sender, e);
                }
            }
        }

        private void cbMigrationType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbTable != null)
            {
                if (cbMigrationType.Text == "alter_")
                {
                    lbTable.Visibility = Visibility.Visible;
                }
                else
                {
                    lbTable.Visibility = Visibility.Collapsed;
                }
            }
        }
    }
}
