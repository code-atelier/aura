﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.CodeMateService.Laravel
{
    public static class LaravelMateDetector
    {
        public static string? DetectLaravelDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    path = Path.GetDirectoryName(path) ?? "";
                }
                path = Path.Combine(path, "artisan");
                if (File.Exists(path))
                {
                    var str = File.ReadAllText(path).ToLower();
                    if (str.Contains("laravel"))
                        return Path.GetDirectoryName(path);
                }
            }
            catch { }
            return null;
        }        
    }
}
