﻿Aura - Copyright(c)2023 CodeAtelier

THIS APPLICATION IS A FREEWARE, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
Project Repository: https://gitlab.com/code-atelier/aura

Prerequisites:
Aura is built on .Net 6, you have to install the .Net 6 runtime from https://dotnet.microsoft.com/en-us/download/dotnet/6.0
To start using Aura, you have to own a Persona file.
You can create a new persona by using the Persona Maker program in personamaker folder.

Basic Operations:
- Drag and dropping files on top of Aura will open a drop menu that shows possible actions for those files
- Right clicking Aura will open up a context menu:
  - Console: You can issue a command or chat with Aura using chatbot service
  - Config: Basic Aura configuration
  - Widgets: Contains widgets offered by services
  - Applications: Currently only contains Persona Maker
  - Shortcuts: Create shortcuts to a file or folder, or access the item by clicking on its shortcut
  - Service Manager: Shows a list of available services and their status. You can also start/stop services and configure them.
  - Debug: Show debug information
  - Exit: I assume you know what this for
- Clicking on region: Depends on the Persona and services that you use, clicking on a Persona region might trigger something

New Update:
- v1.1.1.176
  - RandomTag will automatically refresh after updating JSON
  - AliceBot will automatically refresh after updating JSON
  - Added Service Manager
  - Persona Maker now supports date range
  - Bugfixes
  - QoL Update