﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public static class AuraStatic
    {
        public static Version Version { get; } = new Version(1, 1, 1, 176);

        public static ServiceManager ServiceManager { get; } = new ServiceManager();

        public static bool QuickStartup { get; set; }
    }
}
