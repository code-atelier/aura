﻿using Aura.Models;
using Aura.Models.Avatar;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura
{
    /// <summary>
    /// Interaction logic for Debug.xaml
    /// </summary>
    public partial class Debug : Window
    {
        DispatcherTimer tmrRefresher = new DispatcherTimer();

        ObservableCollection<AvatarTableRow> SetTable { get; } = new ObservableCollection<AvatarTableRow>();
        ObservableCollection<AvatarTableRow> MaterialTable { get; } = new ObservableCollection<AvatarTableRow>();

        public Debug()
        {
            InitializeComponent();
            tmrRefresher.Interval = new TimeSpan(0, 0, 0, 0, 250);
            tmrRefresher.Tick += TmrRefresher_Tick;
            tmrRefresher.Start();

            var svcTag = AuraStatic.ServiceManager.GetService<TagService>();
            if (svcTag != null)
            {
                var iddbg = "debug";
                var allTags = new List<string>();
                if (svcTag.ActiveTags.ContainsKey(iddbg))
                    allTags.AddRange(svcTag.ActiveTags[iddbg]);
                if (svcTag.TimedTags.ContainsKey(iddbg))
                    allTags.AddRange(svcTag.TimedTags[iddbg].Where(x => x.Expire > DateTime.Now).Select(x => x.Tag));
                allTags = allTags.Distinct().ToList();
                txDebugTags.Text = String.Join(Environment.NewLine, allTags);
            }

            itSet.Items.Clear();
            itSet.ItemsSource = SetTable;
            itMat.Items.Clear();
            itMat.ItemsSource = MaterialTable;

            dgSystemInfo.ItemsSource = SystemInfoEntries;

            LoadTagPools();
            LoadSets();
            LoadSchedule();
            GenerateSystemInfo();
        }

        int ticks = 0;

        private void TmrRefresher_Tick(object? sender, EventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                if (ticks % 20 == 0)
                {
                    LoadTagPools();
                }
                else
                {
                    RefreshShownTags();
                }
                RefreshSets();
                RefreshSchedule();
                RefreshEmotionWheel();
                ticks++;
            });
        }

        private void RefreshEmotionWheel()
        {
            var emo = AuraStatic.ServiceManager.GetService<EmotionService>();
            if (emo != null)
            {
                elEmoIndicator.Margin = new Thickness(emo.EmotionWheel.X * 200 - 6 + 200, 0, 0, emo.EmotionWheel.Y * 200 - 6 + 200);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            tmrRefresher.Stop();
        }

        #region Tags
        private void LoadTagPools()
        {
            if (lbTags != null)
            {
                var lastSelected = lbTags.SelectedItem as string;
                lbTags.Items.Clear();
                var svcTag = AuraStatic.ServiceManager.GetService<TagService>();
                if (svcTag != null)
                {
                    var poolIds = svcTag.PoolIDs.ToList();
                    poolIds.Sort();
                    foreach (var poolId in poolIds)
                    {
                        lbTags.Items.Add(poolId);
                    }
                }

                lbTags.SelectedItem = lastSelected;
            }
        }

        private void RefreshShownTags()
        {
            var selected = lbTags?.SelectedItem as string;
            var svcTag = AuraStatic.ServiceManager.GetService<TagService>();
            if (svcTag != null)
            {
                if (selected != null)
                {
                    var allTags = new List<string>();
                    if (svcTag.ActiveTags.ContainsKey(selected))
                        allTags.AddRange(svcTag.ActiveTags[selected]);
                    if (svcTag.TimedTags.ContainsKey(selected))
                        allTags.AddRange(svcTag.TimedTags[selected].Where(x => x.Expire > DateTime.Now).Select(x => x.Tag));
                    allTags = allTags.Distinct().ToList();

                    txTags.Text = String.Join(Environment.NewLine, allTags);
                }
                else
                {
                    txTags.Clear();
                }
                txActiveTags.Text = String.Join(Environment.NewLine, svcTag.GetTags());
            }
        }

        private void UpdateDebugTags()
        {
            if (lbTags != null)
            {
                var svcTag = AuraStatic.ServiceManager.GetService<TagService>();
                if (svcTag != null)
                {
                    string pattern = @"^[a-zA-Z0-9\:\-]+$";
                    var spl = txDebugTags.Text.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries).Where(x => Regex.IsMatch(x, pattern)).Select(x => x.ToLower()).Distinct().ToList();
                    svcTag.ClearTags("debug");
                    svcTag.ClearExpiringTags("debug");
                    foreach (var tag in spl)
                    {
                        svcTag.SetTag("debug", tag);
                    }
                    LoadTagPools();
                }
            }
        }

        private void btnApplyDebugTag_Click(object sender, RoutedEventArgs e)
        {
            UpdateDebugTags();
        }

        private void lbTags_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RefreshShownTags();
        }
        #endregion

        #region Avatar
        private void LoadSets()
        {
            if (lbSets != null)
            {
                lbSets.Items.Clear();

                var svcAva = AuraStatic.ServiceManager.GetService<AvatarControllerService>();
                if (svcAva?.Avatar != null)
                { 
                    var sorted = svcAva.Avatar.Sets.Keys.ToList();
                    sorted.Sort((a, b) => a.Contains(':') && !b.Contains(':') ? - 1 : !a.Contains(':') && b.Contains(':') ? 1 : a.CompareTo(b));
                    foreach (var set in sorted)
                    {
                        lbSets.Items.Add(new Label()
                        {
                            Content = set,
                            Tag = set,
                        });
                    }

                    var cats = svcAva.Avatar.Sets.GetCategories(true);
                    foreach (var cat in cats)
                    {
                        var row = new AvatarTableRow()
                        {
                            Layer = cat,
                            Options = svcAva.Avatar.Sets.GetByCategory(cat.Trim(':')).Select(x => x.Key).ToList(),
                        };
                        SetTable.Add(row);
                    }
                    foreach (var set in svcAva.Avatar.Sets.Keys.Where(x => !x.Contains(':')))
                    {
                        var row = new AvatarTableRow()
                        {
                            Layer = set,
                            Options = new List<string>() { "- put on -" }
                        };
                        SetTable.Add(row);
                    }

                    foreach (var layer in svcAva.Avatar.Layers)
                    {
                        var row = new AvatarTableRow()
                        {
                            Layer = layer.Key,
                            Options = layer.Value.Materials.ToList(),
                        };
                        MaterialTable.Add(row);
                    }
                }
            }
        }

        SolidColorBrush brBlack = new SolidColorBrush(Colors.Black);
        SolidColorBrush brBlue = new SolidColorBrush(Colors.Blue);
        private void RefreshSets()
        {
            var svcAva = AuraStatic.ServiceManager.GetService<AvatarControllerService>();
            var svcTag = AuraStatic.ServiceManager.GetService<TagService>();
            if (svcAva?.Avatar != null && svcTag != null && lbSets != null)
            {
                var tags = svcTag.GetTags();
                var activeSets = svcAva.GetMostActiveSets(tags, false);
                var visibleSets = svcAva.GetMostActiveSets(tags);
                foreach (var item in lbSets.Items)
                {
                    if (item is Label label && label.Tag is string set)
                    {
                        var setInstance = svcAva.Avatar.Sets[set];
                        if (setInstance != null)
                        {
                            label.Content = set + " (" + tags.CalculateWeight(setInstance.Tags) + ")";
                        }
                        var matches = activeSets.Any(x => x.Key == set);
                        label.Foreground = matches ? brBlue : brBlack;
                        label.FontWeight = matches ? FontWeights.Bold : FontWeights.Normal;
                    }
                }
                foreach (var row in SetTable)
                {
                    if (activeSets.Any(x => (x.Key.Contains(':') ? x.Key.StartsWith(row.Layer) : x.Key == row.Layer)))
                    {
                        var set = activeSets.First(x => (x.Key.Contains(':') ? x.Key.StartsWith(row.Layer) : x.Key == row.Layer));
                        if (set.Key.Contains(':'))
                            row.OriginalValue = set.Key;
                        else
                            row.OriginalValue = "On";
                    }
                    else
                    {
                        row.OriginalValue = row.Layer.Contains(':') ? "(none)" : "Off";
                    }

                    if (!svcAva.ForcedSets.ContainsKey(row.Layer))
                        row.SelectedItem = "- don't change -";
                    else
                    {
                        var val = svcAva.ForcedSets[row.Layer];
                        if (string.IsNullOrWhiteSpace(val))
                            row.SelectedItem = "- remove -";
                        else
                        {
                            if (row.Layer.Contains(':'))
                                row.SelectedItem = val;
                            else
                                row.SelectedItem = val == "on" ? "- put on -" : "- remove -";
                        }
                    }


                    if (visibleSets.Any(x => (x.Key.Contains(':') ? x.Key.StartsWith(row.Layer) : x.Key == row.Layer)))
                    {
                        var set = visibleSets.First(x => (x.Key.Contains(':') ? x.Key.StartsWith(row.Layer) : x.Key == row.Layer));
                        if (set.Key.Contains(':'))
                            row.FinalValue = set.Key;
                        else
                            row.FinalValue = "On";
                    }
                    else
                    {
                        row.FinalValue = row.Layer.Contains(':') ? "(none)" : "Off";
                    }
                }

                var orderedLayers = svcAva.Avatar.Layers.OrderBy(x => x.Value.ZIndex).ToList();
                var visibleLayers = new Dictionary<string, string?>();
                var activeLayers = new Dictionary<string, string?>();
                foreach (var layer in orderedLayers)
                {
                    var set = visibleSets.Select(x => x.Value.Item1).LastOrDefault(x => x.Layers[layer.Key] != null);
                    var source = set?.Layers[layer.Key] ?? layer.Value.Default;
                    if (source == "") source = null;
                    activeLayers.Add(layer.Key, source);

                    set = visibleSets.Select(x => x.Value.Item1).LastOrDefault(x => x.Layers[layer.Key] != null);
                    source = set?.Layers[layer.Key] ?? layer.Value.Default;
                    if (svcAva.ForcedMaterials.ContainsKey(layer.Key))
                        source = svcAva.ForcedMaterials[layer.Key];
                    if (source == "") source = null;
                    visibleLayers.Add(layer.Key, source);
                }
                foreach (var row in MaterialTable)
                {
                    row.OriginalValue = activeLayers[row.Layer] ?? "(none)";
                    row.FinalValue = visibleLayers[row.Layer] ?? "(none)";
                    row.SelectedItem = svcAva.ForcedMaterials.ContainsKey(row.Layer) ? 
                        (svcAva.ForcedMaterials[row.Layer] == "" ? "- remove -" : svcAva.ForcedMaterials[row.Layer]) 
                        : "- don't change -";
                }
            }
        }
        private void cbForceSet_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var svcAva = AuraStatic.ServiceManager.GetService<AvatarControllerService>();
            if (sender is ComboBox cb && cb.Tag is string catId && svcAva != null)
            {
                var val = cb.SelectedItem?.ToString();
                if (val == null || val == "- don't change -")
                {
                    if (svcAva.ForcedSets.ContainsKey(catId))
                        svcAva.ForcedSets.Remove(catId);
                }
                else if (val == "- remove -")
                {
                    svcAva.ForcedSets[catId] = "";
                }
                else
                {
                    svcAva.ForcedSets[catId] = val == "- put on -" ? "on" : val;
                }
                svcAva.RequestUpdate();
            }
        }
        private void cbForceMat_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var svcAva = AuraStatic.ServiceManager.GetService<AvatarControllerService>();
            if (sender is ComboBox cb && cb.Tag is string layerId && svcAva != null)
            {
                var val = cb.SelectedItem?.ToString();
                if (val == null || val == "- don't change -")
                {
                    if (svcAva.ForcedMaterials.ContainsKey(layerId))
                        svcAva.ForcedMaterials.Remove(layerId);
                }
                else if (val == "- remove -")
                {
                    svcAva.ForcedMaterials[layerId] = "";
                }
                else
                {
                    svcAva.ForcedMaterials[layerId] = val;
                }
                svcAva.RequestUpdate();
            }
        }
        private void lbSets_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbSetTags != null)
            {
                lbSetTags.Items.Clear();
                var svcAva = AuraStatic.ServiceManager.GetService<AvatarControllerService>();
                if (svcAva?.Avatar != null && lbSets.SelectedItem is Label label 
                    && label.Tag is string setId
                    && svcAva.Avatar.Sets.ContainsKey(setId)
                    )
                {
                    var set = svcAva.Avatar.Sets[setId];
                    foreach(var tag in set.Tags)
                    {
                        lbSetTags.Items.Add(tag.Key + " (" + tag.Value + ")");
                    }
                }
            }
        }
        #endregion

        #region Schedule
        private void LoadSchedule()
        {
            lbSchedule.Items.Clear();
            var svcCore = AuraStatic.ServiceManager.GetService<CoreService>();
            if (svcCore?.Persona?.Schedule != null)
            {
                var sorted = svcCore.Persona.Schedule.ToList();
                sorted.Sort((a, b) => a.Name.CompareTo(b.Name));
                foreach (var schedule in sorted)
                {
                    var lb = new Label();
                    lb.Content = schedule.Name;
                    lb.Tag = schedule;
                    lbSchedule.Items.Add(lb);
                }
            }
        }

        private void RefreshSchedule()
        {
            var now = DateTime.Now;
            var time = now.ToString("HH:mm:ss");
            if (now.Millisecond >= 500)
                time = time.Replace(":", " ");
            lbTime.Content = time;
            lbDay.Content = now.ToString("dddd");

            foreach(var child in lbSchedule.Items)
            {
                if (child is Label lb && lb.Tag is ScheduleEntry entry)
                {
                    if (entry.Contains(now))
                    {
                        lb.Foreground = brBlue;
                        lb.FontWeight = FontWeights.Bold;
                    }
                    else
                    {
                        lb.Foreground = brBlack;
                        lb.FontWeight = FontWeights.Normal;
                    }
                }
            }
        }
        private void lbSchedule_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbScheduleTag != null)
            {
                lbScheduleTag.Items.Clear();
                if (lbSchedule?.SelectedItem is Label label && label.Tag is ScheduleEntry entry)
                {
                    foreach (var tag in entry.Tags)
                        lbScheduleTag.Items.Add(tag);
                }
            }
        }
        #endregion

        #region System Info
        private ObservableCollection<SystemInfoEntry> SystemInfoEntries { get; } = new ObservableCollection<SystemInfoEntry>();
        private void GenerateSystemInfo()
        {
            SystemInfoEntries.Clear();
            var sie = SystemInfoEntries;
            sie.Add(new SystemInfoEntry("Aura Version", AuraStatic.Version.ToString()));
            sie.Add(new SystemInfoEntry("Working Directory", Environment.CurrentDirectory)
            {
                ActionName = "Open",
                Action = (entry) =>
                {
                    Process.Start(new ProcessStartInfo()
                    {
                        FileName = entry.Value,
                        UseShellExecute = true,
                    });
                },
            });
        }

        private void btnSystemInfoAction_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn && btn.Tag is SystemInfoEntry sie)
            {
                sie.InvokeAction();
            }
        }
        #endregion
    }

    public class AvatarTableRow : INotifyPropertyChanged
    {
        private List<string> options = new List<string>();
        private string originalValue = String.Empty;
        private string finalValue = String.Empty;

        public string Layer { get; set; } = String.Empty;

        public string OriginalValue
        {
            get => originalValue; set
            {
                if (originalValue != value)
                {
                    originalValue = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("OriginalValue"));
                }
            }
        }

        public List<string> Options
        {
            get => options; set
            {
                if (options != value)
                {
                    options = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Options"));
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Items"));
                }
            }
        }

        public List<string> Items
        {
            get
            {
                var res = new List<string>();
                res.Add("- don't change -");
                res.Add("- remove -");
                if (Options != null)
                    res.AddRange(Options);
                return res;
            }
        }

        public string? SelectedItem { get; set; }

        public string FinalValue
        {
            get => finalValue; set
            {
                if (finalValue != value)
                {
                    finalValue = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FinalValue"));
                }
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;
    }
}
