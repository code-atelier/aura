﻿using Aura.Models;
using Aura.Services;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura
{
    /// <summary>
    /// Interaction logic for DropZoneWindow.xaml
    /// </summary>
    public partial class DropZoneWindow : Window
    {
        public DropZoneWindow(ServiceManager serviceManager)
        {
            InitializeComponent();
            Opacity = 0;
            ServiceManager = serviceManager;
        }

        public ServiceManager ServiceManager { get; set; }

        public void Populate(IEnumerable<FileDropData> services)
        {
            foreach (var service in services)
                itmControl.Items.Add(new ServiceItem(service));
        }

        private void Border_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            e.Handled = true;
        }

        private async void Border_Drop(object sender, DragEventArgs e)
        {
            StartClosing(true);
            if (sender is Border border && border.Tag is FileDropData data)
            {
                e.Handled = true;
                e.Effects = DragDropEffects.Copy;
                var msg = new ServiceMessage(InternalSensors.DragDrop, data.Service.ServiceID, e.Data);
                msg["tag"] = data.Data?.Tag;
                msg["event"] = "drop";
                if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                    msg.SetHeader("ctrl");
                if (Keyboard.IsKeyDown(Key.LeftAlt) || Keyboard.IsKeyDown(Key.RightAlt))
                    msg.SetHeader("alt");
                if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                    msg.SetHeader("shift");
                await ServiceManager.BroadcastMessage(msg);
            }
        }

        bool allowClose = false;
        private void Window_Activated(object sender, EventArgs e)
        {
            if (Owner is MainWindow main)
            {
                Left = main.Left - Width;
                Top = main.Top - Height + Height / 2;
            }
            var fadeinAnim = new DoubleAnimation()
            {
                From = 0,
                To = 1,
                Duration = new TimeSpan(0, 0, 0, 0, 300),
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(fadeinAnim);
            Storyboard.SetTargetProperty(fadeinAnim, new PropertyPath(Window.OpacityProperty));
            storyboard.Completed += Storyboard_Completed;
            storyboard.Begin(this);

        }

        private void Storyboard_Completed(object? sender, EventArgs e)
        {
            allowClose = true;
        }

        public void StartClosing(bool fast = false)
        {
            if (!allowClose || ClosingStoryboard != null) return;
            if (fast)
            {
                BeginClose(fast);
            }
            else
            {
                var dt = new DispatcherTimer();
                dt.Interval = new TimeSpan(0, 0, 2);
                dt.Tick += dtClosing;
                dt.Start();
            }
        }

        private void dtClosing(object? sender, EventArgs e)
        {
            if (sender is DispatcherTimer dt) dt.Stop();
            var delta = (DateTime.Now - LastKeepAlive)?.TotalSeconds ?? 0;
            if (delta >= 2)
            {
                Dispatcher.Invoke(() =>
                {
                    if (ClosingStoryboard == null)
                    {
                        BeginClose();
                    }
                });
            }
        }

        private void BeginClose(bool fast = false)
        {
            var fadeoutAnim = new DoubleAnimation()
            {
                From = 1,
                To = 0,
                Duration = new TimeSpan(0, 0, 0, fast ? 0 : 1, fast ? 300 : 0),
            };
            ClosingStoryboard = new Storyboard();
            ClosingStoryboard.Children.Add(fadeoutAnim);
            Storyboard.SetTargetProperty(fadeoutAnim, new PropertyPath(Window.OpacityProperty));
            ClosingStoryboard.Completed += ClosingStoryboard_Completed;
            ClosingStoryboard.Begin(this, true);
        }


        DateTime? LastKeepAlive = null;
        public void StopClosing()
        {
            LastKeepAlive = DateTime.Now;
        }

        private void ClosingStoryboard_Completed(object? sender, EventArgs e)
        {
            Close();
        }

        Storyboard? ClosingStoryboard;

        private void Border_DragEnter(object sender, DragEventArgs e)
        {
            StopClosing();
        }

        private void Border_DragLeave(object sender, DragEventArgs e)
        {
            StartClosing();
        }
    }

    class ServiceItem
    {
        public IAuraService Service { get; }

        public string ServiceName { get; }

        public ImageSource? ImageSource { get; }

        public FileDropData Data { get; }

        public ServiceItem(FileDropData data)
        {
            Data = data;
            Service = data.Service;
            ServiceName = data.Data?.Text ?? data.Service.ServiceName;
            try
            {
                var uri = data.Data?.IconUri ?? data.Service.ServiceIcon;
                if (uri != null)
                {
                    var bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.UriSource = new Uri("pack://application:,,," + uri);
                    bmp.EndInit();
                    ImageSource = bmp;
                    ServiceName = "";
                }
            }
            catch { }
        }
    }
}
