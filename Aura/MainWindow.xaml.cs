﻿using Aura.Console;
using Aura.Forms;
using Aura.Models;
using Aura.Models.Avatar;
using Aura.Services;
using Aura.Services.Windows;
using MathNet.Numerics.LinearAlgebra.Factorization;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Reflection;
using System.Reflection.Emit;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Xps.Serialization;
using Windows.Storage.Pickers;
using static System.Net.WebRequestMethods;
using File = System.IO.File;
using Path = System.IO.Path;

namespace Aura
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomain_AssemblyResolve;

            var path = "personamaker\\PersonaMaker.exe";
            if (!File.Exists(path))
                miApps.Items.Remove(miPersonaMaker);
            try
            {
                RegistryKey? reg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (reg != null)
                {
                    cmiAutoRunOnLogon.IsChecked = reg.GetValue("Aura") != null;
                }
            }
            catch
            {
            }

            ignoreLogonCheck = false;
            miVersion.Header = "Aura v" + AuraStatic.Version;
        }

        private Assembly? CurrentDomain_AssemblyResolve(object? sender, ResolveEventArgs args)
        {
            try
            {
                string folderPath = Path.GetFullPath("data\\libs");
                string assemblyPath = Path.Combine(folderPath, new AssemblyName(args.Name).Name + ".dll");
                if (!File.Exists(assemblyPath)) return null;
                Assembly assembly = Assembly.LoadFrom(assemblyPath);
                return assembly;
            }
            catch
            {
                return null;
            }
        }

        #region Main Logic
        DispatcherTimer? _uiPoolingTimer;

        ServiceManager ServiceManager { get => AuraStatic.ServiceManager; }

        private Configuration? Configuration
        {
            get
            {
                if (CoreService != null)
                {
                    return CoreService.Configuration;
                }
                return null;
            }
        }

        private CoreService? CoreService
        {
            get
            {
                return ServiceManager.GetService<CoreService>();
            }
        }

        private void windowMain_Closed(object sender, EventArgs e)
        {
            try
            {
                ServiceManager.ShutdownServices();
            }
            catch { }
        }

        bool ignoreLogonCheck = true;
        private void cmiAutoRunOnLogon_Checked(object sender, RoutedEventArgs e)
        {
            if (ignoreLogonCheck) return;
            try
            {
                RegistryKey? reg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (reg != null)
                    reg.SetValue("Aura", "\"" + Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Aura.exe" + "\" --quick");
            }
            catch
            {
                ignoreLogonCheck = true;
                cmiAutoRunOnLogon.IsChecked = false;
                ignoreLogonCheck = false;
            }
        }

        private void cmiAutoRunOnLogon_Unchecked(object sender, RoutedEventArgs e)
        {
            if (ignoreLogonCheck) return;
            try
            {
                RegistryKey? reg = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (reg != null)
                    reg.DeleteValue("Aura");
            }
            catch
            {
                ignoreLogonCheck = true;
                cmiAutoRunOnLogon.IsChecked = true;
                ignoreLogonCheck = false;
            }
        }

        private void windowMain_LocationChanged(object sender, EventArgs e)
        {
            EnsureSnap();
        }

        private void InitializeMainComponents()
        {
            ServiceManager.InitializeServiceManager(this);
            _uiPoolingTimer = new DispatcherTimer(new TimeSpan(0, 0, 0, 0, 32), DispatcherPriority.Render, OnHandleUIPool, Dispatcher);
            _uiPoolingTimer?.Start();

            var core = ServiceManager.GetService<Services.CoreService>();
            if (core?.Configuration != null && (string.IsNullOrWhiteSpace(core.PersonaFilePath) || !File.Exists(core.PersonaFilePath)))
            {
                var ofd = new OpenFileDialog();
                ofd.Filter = "Persona Files|*.persona";
                ofd.Title = "Import New Persona";
                if (ofd.ShowDialog() == true)
                {
                    // import persona file
                    var imported = core.ImportPersonaFile(ofd.FileName);
                    if (imported == null)
                    {
                        MessageBox.Show("Invalid Persona file", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        noConfirmClose = true;
                        Application.Current.Shutdown(1);
                        return;
                    }
                    var fname = System.IO.Path.GetFileName(imported);
                    core.Configuration.Persona = fname;
                    core.SaveConfiguration();
                }
                else
                {
                    noConfirmClose = true;
                    Application.Current.Shutdown(1);
                    return;
                }
            }

            ServiceManager.StartServices();

            if (core?.Configuration != null)
            {
                cmiSnapDesktop.IsChecked = core.Configuration.SnapToScreen;
                cmiLockPosition.IsChecked = core.Configuration.LockPosition;
            }

            // transition
            Cursor = Cursors.Arrow;
            FadeOutSplash();
        }

        private void FadeOutSplash()
        {
            if (AuraStatic.QuickStartup)
            {
                SplashFadedOut(this, new EventArgs());
                return;
            }
            var fadeOutAnim = new DoubleAnimation()
            {
                From = 1.0,
                To = 0.0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 1)),
                BeginTime = new TimeSpan(0, 0, 0, 1, 500),
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(fadeOutAnim);
            Storyboard.SetTargetProperty(fadeOutAnim, new PropertyPath(Grid.OpacityProperty));

            storyboard.Completed += SplashFadedOut;
            storyboard.Begin(imgSplash);
        }

        private void SplashFadedOut(object? sender, EventArgs e)
        {
            imgSplash.Opacity = 1;
            imgSplash.Background = new SolidColorBrush(Colors.Transparent);
            imgSplash.Visibility = Visibility.Collapsed;
            gridMain.Children.Remove(imgSplash);
            ctxMenu.IsEnabled = true;
            ctxMenu.Visibility = Visibility.Visible;

            var core = ServiceManager.GetService<Services.CoreService>();
            ignoreStayOnTopChange = true;
            if (core?.Configuration != null)
            {
                Topmost = cmiStayOnTop.IsChecked = core.Configuration.StayOnTop;
            }
            else
            {
                Topmost = cmiStayOnTop.IsChecked = true;
            }
            ignoreStayOnTopChange = false;

            _isUIReady = true;
        }

        private void windowMain_Loaded(object sender, RoutedEventArgs e)
        {
            InitializeMainComponents();
        }

        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        bool noConfirmClose = false;
        private void windowMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!noConfirmClose && MessageBox.Show("Are you sure to exit?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
            {
                e.Cancel = true;
                return;
            }
            if (!ServiceManager.IsSafeToShutdown())
            {
                MessageBox.Show("Services are busy, cannot shutdown.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                e.Cancel = true;
                return;
            }
            if (CoreService != null)
            {
                CoreService.SaveConfiguration();
            }
        }

        private void ctxMenu_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            ignoreStayOnTopChange = true;
            cmiStayOnTop.IsChecked = Topmost;
            ignoreStayOnTopChange = false;
            miWidgets.Items.Clear();

            foreach (var svc in ServiceManager.Services)
            {
                if (svc.Widgets.Count > 0)
                {
                    var miSvcWidget = new MenuItem();
                    miSvcWidget.Header = svc.ServiceName;
                    if (svc.ServiceIcon != null)
                        miSvcWidget.Icon = CreateIconImage(svc.ServiceIcon);
                    foreach (var widget in svc.Widgets)
                    {
                        var miWgt = new MenuItem();
                        miWgt.Header = widget.Name;
                        miWgt.Tag = widget;
                        if (!string.IsNullOrEmpty(widget.IconUri))
                        {
                            miWgt.Icon = CreateIconImage(widget.IconUri);
                        }
                        miWgt.Click += MiWgt_Click;
                        miSvcWidget.Items.Add(miWgt);
                    }
                    miWidgets.Items.Add(miSvcWidget);
                }
            }

            while (miShortcut.Items.Count > 1)
            {
                miShortcut.Items.RemoveAt(0);
            }
            var sc = ShortcutManager.Get();
            sc.Sort((a, b) => a.Name.CompareTo(b.Name));
            foreach (var s in sc)
            {
                if (File.Exists(s.Target) || Directory.Exists(s.Target))
                {
                    var miShortcutItem = new MenuItem()
                    {
                        Header = s.Name,
                        Tag = s,
                        Icon = new Image()
                        {
                            Source = s.Icon
                        }
                    };
                    miShortcutItem.KeyDown += MiShortcutItem_KeyDown;
                    miShortcutItem.Click += MiShortcutItem_Click;
                    miShortcut.Items.Insert(miShortcut.Items.Count - 1, miShortcutItem);
                }
            }
        }
        ServiceManagerWindow? ServiceManagerWindow;
        private void MiServiceManager_Click(object sender, RoutedEventArgs e)
        {
            if (ServiceManagerWindow == null)
            {
                ServiceManagerWindow = new ServiceManagerWindow();
                ServiceManagerWindow.Closed += ServiceManagerWindow_Closed;
                ServiceManagerWindow.Show();
            }
            else
            {
                try
                {
                    ServiceManagerWindow.WindowState = WindowState.Normal;
                    ServiceManagerWindow.Topmost = true;
                    ServiceManagerWindow.Topmost = false;
                    ServiceManagerWindow.BringIntoView();
                }
                catch { }
            }
        }

        private void ServiceManagerWindow_Closed(object? sender, EventArgs e)
        {
            ServiceManagerWindow = null;
        }

        private void MiShortcutItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete && sender is MenuItem mi && Tag is ShortcutItem si)
            {

            }
        }

        private void miFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "All Files|*.*";
            ofd.Title = "Select shortcut target";
            if (ofd.ShowDialog() == true)
            {
                ShortcutManager.Add(ofd.FileName);
            }
        }

        private void miFolder_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ShortcutManager.Add(fbd.SelectedPath);
            }
        }

        private async void MiShortcutItem_Click(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem mi && mi.Tag is ShortcutItem si && si.Target is string target)
            {
                if (Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl))
                {
                    if (MessageBox.Show("Delete this shortcut?\r\n" + si.Name, "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        ShortcutManager.Remove(si);
                    }
                }
                else if (Keyboard.IsKeyDown(Key.LeftShift) || Keyboard.IsKeyDown(Key.RightShift))
                {
                    var msg = new ServiceMessage(InternalSensors.ContextMenu, InternalServices.CoreService, target);
                    msg["action"] = "show-in-explorer";
                    await ServiceManager.BroadcastMessage(msg);
                }
                else
                {
                    var msg = new ServiceMessage(InternalSensors.ContextMenu, InternalServices.CoreService, target);
                    msg["action"] = "open";
                    await ServiceManager.BroadcastMessage(msg);
                }
            }
        }

        private void MiWgt_Click(object sender, RoutedEventArgs e)
        {
            if (sender is MenuItem mi && mi.Tag is IAuraWidget widget)
            {
                ServiceManager.Widgets.Show(widget);
            }
        }

        private Image CreateIconImage(string iconUri)
        {
            var iconPath = iconUri.StartsWith("/") ? "pack://application:,,," + iconUri : iconUri;
            var bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.UriSource = new Uri(iconPath);
            bmp.EndInit();
            var img = new Image();
            img.Source = bmp;
            return img;
        }

        private void cmiSnapDesktop_Unchecked(object sender, RoutedEventArgs e)
        {
            if (Configuration != null)
            {
                Configuration.SnapToScreen = cmiSnapDesktop.IsChecked;
                SavePositionConfig();
            }
        }

        private void cmiLockPosition_Checked(object sender, RoutedEventArgs e)
        {
            if (Configuration != null)
            {
                Configuration.LockPosition = cmiLockPosition.IsChecked;
                SavePositionConfig();
            }
        }

        Debug? debugWindow = null;
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (debugWindow != null)
            {
                try
                {
                    debugWindow.BringIntoView();
                }
                catch { }
            }
            else
            {
                var f = new Debug();
                f.Owner = this;
                f.Closed += FDebug_Closed;
                debugWindow = f;
                f.Show();
            }
        }

        private void FDebug_Closed(object? sender, EventArgs e)
        {
            debugWindow = null;
        }

        private async void miPersonaMaker_Click(object sender, RoutedEventArgs e)
        {
            var path = "personamaker\\PersonaMaker.exe";
            if (File.Exists(path))
            {
                path = Path.GetFullPath(path);
                var msg = new ServiceMessage(InternalServices.UI, InternalServices.MessageService,
                    $"Opening\r\nPersona\r\nMaker");
                await ServiceManager.BroadcastMessage(msg);
                Process.Start(new ProcessStartInfo()
                {
                    FileName = path,
                    WorkingDirectory = Path.GetDirectoryName(path),
                });
            }
            else
            {
                var msg = new ServiceMessage(InternalServices.UI, InternalServices.MessageService,
                    $"I cannot\r\nfind the\r\napplication,\r\nplease check\r\nyour\r\ninstallation");
                await ServiceManager.BroadcastMessage(msg);
            }
        }
        #endregion

        #region Draw Avatar
        bool _isUIReady = false;
        bool _isUpdatingUI = false;
        DateTime? _lastUIUpdate = null;
        private void OnHandleUIPool(object? sender, EventArgs args)
        {
            ServiceManager.InvokeOnMainUIClockTick();
            if (_isUpdatingUI || !_isUIReady) return;
            var svcAvatar = ServiceManager.GetService<AvatarControllerService>();
            if (svcAvatar == null || svcAvatar.State != AuraServiceState.Running) return;
            if (_lastUIUpdate == null || svcAvatar.LastUpdate > _lastUIUpdate || (DateTime.Now - _lastUIUpdate)?.TotalSeconds >= 5)
            {
                if (svcAvatar.RefreshMaterial())
                {
                    _lastUIUpdate = DateTime.Now;
                    RefreshAvatar(svcAvatar);
                }
            }
        }

        string? _lastPersona = null;
        List<KeyValuePair<string, Image>> LayerControls { get; set; } = new List<KeyValuePair<string, Image>>();
        private void RefreshAvatar(AvatarControllerService service)
        {
            if (_isUpdatingUI || !_isUIReady) return;

            bool noFade = true;
            try
            {
                var personaChanged = _lastPersona != service.PersonaGuid;
                _isUpdatingUI = true;
                noFade = !personaChanged && service.LayerChanged?.Length > 0;
                RefreshLayers(service);
                _lastPersona = service.PersonaGuid;
            }
            finally
            {
                if (noFade)
                    _isUpdatingUI = false;
            }
        }

        bool shouldReadPositionFromConfig = true;
        private void RefreshLayers(AvatarControllerService service)
        {
            var personaChanged = _lastPersona != service.PersonaGuid;
            if (personaChanged)
            {
                grLayerContainer.Opacity = 0;

                // update layer controls
                for (var i = 0; i < service.LayerMaterials.Count; i++)
                {
                    var layer = service.LayerMaterials[i];
                    if (i < LayerControls.Count)
                    {
                        var lc = LayerControls[i];
                        LayerControls[i] = new KeyValuePair<string, Image>(layer.Key, lc.Value);
                    }
                    else
                    {
                        var img = new Image()
                        {
                            Stretch = Stretch.Fill,
                            SnapsToDevicePixels = false,
                        };
                        LayerControls.Add(new KeyValuePair<string, Image>(layer.Key, img));
                        grLayerContainer.Children.Insert(grLayerContainer.Children.Count - 1, img);
                    }
                }
                while (LayerControls.Count > service.LayerMaterials.Count)
                {
                    var last = LayerControls.Last();
                    LayerControls.RemoveAt(LayerControls.Count - 1);
                    grLayerContainer.Children.Remove(last.Value);
                }
            }

            Size? avatarImageSize = null;

            // update the layers image
            for (var i = 0; i < LayerControls.Count; i++)
            {
                var material = service.LayerMaterials[i];
                var layer = LayerControls[i];
                var materialPath = material.Value;
                var basePath = service.Avatar?.Layers[layer.Key].Path;
                ImageSource? res = layer.Value.Source;
                if (basePath != null && materialPath != null)
                {
                    var path = System.IO.Path.Combine(basePath, materialPath).Replace("\\", "/");
                    if (personaChanged || service.LayerChanged.Contains(layer.Key))
                    {
                        // update image
                        grLayerContainer.Opacity = 0;
                        if (service.Archive?.FileSystem.Exists(path) == true)
                        {
                            var ss = service.Archive.FileSystem[path];
                            var ms = new MemoryStream();
                            ss.Seek(0, SeekOrigin.Begin);
                            ss.CopyTo(ms);
                            ms.Seek(0, SeekOrigin.Begin);

                            var bmp = new BitmapImage();
                            bmp.BeginInit();
                            bmp.StreamSource = ms;
                            bmp.EndInit();
                            avatarImageSize = new Size(bmp.Width, bmp.Height);
                            res = bmp;
                        }
                    }
                }
                else if (string.IsNullOrWhiteSpace(materialPath))
                {
                    res = null;
                }
                layer.Value.Source = res;
            }

            if (avatarImageSize == null)
            {
                avatarImageSize = service.GetAvatarImageSize();
            }

            // update window size when avatar changed
            if (personaChanged)
            {
                Width = service.Avatar?.Display.Width > 0 ? service.Avatar.Display.Width : avatarImageSize?.Width ?? 0;
                Height = service.Avatar?.Display.Height > 0 ? service.Avatar.Display.Height : avatarImageSize?.Height ?? 0;
                ResetPosition(shouldReadPositionFromConfig);
                shouldReadPositionFromConfig = false;
            }

            // regenerate regions
            grRegionContainer.Children.Clear();
            if (avatarImageSize != null && service.Avatar != null)
            {
                foreach (var reg in service.Avatar.Regions)
                {
                    var scaleX = Width / avatarImageSize.Value.Width;
                    var scaleY = Height / avatarImageSize.Value.Height;

                    var region = reg.Value.FirstOrDefault();
                    if (region != null)
                    {
                        if (region.Type == RegionType.Rectangle)
                        {
                            var re = new Rectangle()
                            {
                                Margin = new Thickness(region.X * scaleX, region.Y * scaleY, 0, 0),
                                Height = region.Height * scaleY,
                                Width = region.Width * scaleX,
                                HorizontalAlignment = HorizontalAlignment.Left,
                                VerticalAlignment = VerticalAlignment.Top,
                                Cursor = Cursors.Hand,
                                Fill = new SolidColorBrush(Color.FromArgb(128, 230, 20, 20)),
                                Opacity = cmiRegionVisible.IsChecked ? 1 : 0.001,
                                Tag = reg.Key,
                                ToolTip = new ToolTip()
                                {
                                    Visibility = cmiRegionVisible.IsChecked ? Visibility.Visible : Visibility.Collapsed,
                                    Content = new TextBlock()
                                    {
                                        Text = reg.Key,
                                    }
                                },
                            };
                            re.TouchDown += RegionTouchDown;
                            re.TouchUp += RegionTouchUp;
                            re.StylusDown += RegionStylusDown;
                            re.StylusUp += RegionStylusUp;
                            re.MouseLeftButtonDown += RegionMouseLeftDown;
                            re.MouseLeftButtonUp += RegionMouseLeftUp;
                            grRegionContainer.Children.Add(re);
                        }
                        else if (region.Type == RegionType.Radius)
                        {
                            var el = new Ellipse()
                            {
                                Margin = new Thickness((region.X - region.Radius) * scaleX, (region.Y - region.Radius) * scaleY, 0, 0),
                                Height = region.Radius * 2 * scaleY,
                                Width = region.Radius * 2 * scaleX,
                                HorizontalAlignment = HorizontalAlignment.Left,
                                VerticalAlignment = VerticalAlignment.Top,
                                Cursor = Cursors.Hand,
                                Fill = new SolidColorBrush(Color.FromArgb(128, 230, 20, 20)),
                                Opacity = cmiRegionVisible.IsChecked ? 1 : 0.001,
                                Tag = reg.Key,
                                ToolTip = new ToolTip()
                                {
                                    Visibility = cmiRegionVisible.IsChecked ? Visibility.Visible : Visibility.Collapsed,
                                    Content = new TextBlock()
                                    {
                                        Text = reg.Key,
                                    }
                                },
                            };
                            el.TouchUp += RegionTouchUp;
                            el.TouchDown += RegionTouchDown;
                            el.StylusDown += RegionStylusDown;
                            el.StylusUp += RegionStylusUp;
                            el.MouseLeftButtonDown += RegionMouseLeftDown;
                            el.MouseLeftButtonUp += RegionMouseLeftUp;
                            grRegionContainer.Children.Add(el);
                        }
                    }
                }
            }

            if (personaChanged)
            {
                FadeInAvatar(0.2);
            }
            else
            {
                FadeInLayers(service.LayerChanged);
                grLayerContainer.Opacity = 1;
            }
        }

        private void FadeInLayers(string[] layers)
        {
            bool anyUpdate = false;
            foreach (var control in LayerControls)
            {
                if (layers.Contains(control.Key))
                {
                    var storyboard = new Storyboard();
                    var fadeOutAnim = new DoubleAnimation()
                    {
                        From = 0.5,
                        To = 1.0,
                        Duration = new Duration(new TimeSpan(0, 0, 0, 0, 150)),
                    };

                    storyboard.Children.Add(fadeOutAnim);
                    Storyboard.SetTargetProperty(fadeOutAnim, new PropertyPath(Grid.OpacityProperty));
                    storyboard.Completed += AvatarFadedIn;
                    storyboard.Begin(control.Value);
                    anyUpdate = true;
                }
            }
            if (!anyUpdate)
                _isUpdatingUI = false;
        }

        private void FadeInAvatar(double from = 0)
        {
            var fadeOutAnim = new DoubleAnimation()
            {
                From = from,
                To = 1.0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 150)),
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(fadeOutAnim);
            Storyboard.SetTargetProperty(fadeOutAnim, new PropertyPath(Grid.OpacityProperty));

            storyboard.Completed += AvatarFadedIn;
            storyboard.Begin(grLayerContainer);
        }

        private void AvatarFadedIn(object? sender, EventArgs e)
        {
            _isUpdatingUI = false;
        }
        #endregion

        #region Sensor - Touch
        string? _regionDown = null;
        string? _regionDownSender = null;
        DateTime _regionDownStart = DateTime.MinValue;
        Point? _regionDownPoint = null;

        private void BeginRegionDown(object? sender, string inputType, Point point)
        {
            _regionDown = inputType;
            _regionDownSender = (sender as FrameworkElement)?.Tag?.ToString();
            _regionDownStart = DateTime.Now;
            _regionDownPoint = point;
        }

        private void RegionMouseLeftDown(object sender, MouseButtonEventArgs e)
        {
            BeginRegionDown(sender, "mouse", e.GetPosition(sender as IInputElement));
            e.Handled = true;
        }

        private void RegionStylusDown(object sender, StylusDownEventArgs e)
        {
            BeginRegionDown(sender, "stylus", e.GetPosition(sender as IInputElement));
            e.Handled = true;
        }

        private void RegionTouchDown(object? sender, TouchEventArgs e)
        {
            BeginRegionDown(sender, "touch", e.GetTouchPoint(sender as IInputElement).Position);
            e.Handled = true;
        }

        private const int TriggerMaximumDelay = 300;
        private const int DragMaximumDelay = 1000;
        private const double TriggerMaximumDrag = 20;

        private double pointDistance(Point a, Point b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

        private bool ProcessRegionTrigger(object? sender, string inputType, Point? point)
        {
            var tag = (sender as FrameworkElement)?.Tag?.ToString();
            if (_regionDown != inputType || tag != _regionDownSender)
                return false;

            var delta = DateTime.Now - _regionDownStart;
            var distance = point.HasValue && _regionDownPoint.HasValue ? pointDistance(point.Value, _regionDownPoint.Value) : 0;

            if (delta.TotalMilliseconds <= TriggerMaximumDelay)
            {
                if (distance <= TriggerMaximumDrag)
                {
                    RegionTriggered(sender, inputType);
                    return true;
                }
            }
            else if (delta.TotalMilliseconds <= DragMaximumDelay && _regionDownPoint.HasValue && point.HasValue)
            {
                RegionDragTriggered(sender, inputType, _regionDownPoint.Value - point.Value);
                return true;
            }
            return false;
        }

        private void CleanupRegionTrigger()
        {
            _regionDown = null;
            _regionDownStart = DateTime.Now;
            _regionDownSender = null;
            _regionDownPoint = null;
        }

        private void RegionMouseLeftUp(object sender, MouseButtonEventArgs e)
        {
            e.Handled = true;

            if (!ProcessRegionTrigger(sender, "mouse", e.GetPosition(sender as IInputElement)))
            {

            }

            CleanupRegionTrigger();
        }

        private void RegionStylusUp(object sender, StylusEventArgs e)
        {
            e.Handled = true;

            if (!ProcessRegionTrigger(sender, "stylus", e.GetPosition(sender as IInputElement)))
            {

            }

            CleanupRegionTrigger();
        }

        private void RegionTouchUp(object? sender, TouchEventArgs e)
        {
            e.Handled = true;

            if (!ProcessRegionTrigger(sender, "mouse", e.GetTouchPoint(sender as IInputElement).Position))
            {

            }

            CleanupRegionTrigger();
        }

        private async void RegionTriggered(object? sender, string device)
        {
            if (sender is FrameworkElement ele && ele.Tag is string regionId && !string.IsNullOrWhiteSpace(regionId))
            {
                var msg = new ServiceMessage(InternalSensors.Region)
                {
                    Body = regionId,
                };
                msg.Headers["device"] = device;
                await ServiceManager.BroadcastMessage(msg);
            }
        }
        private async void RegionDragTriggered(object? sender, string device, Vector direction)
        {
            if (sender is FrameworkElement ele && ele.Tag is string regionId && !string.IsNullOrWhiteSpace(regionId))
            {
                var msg = new ServiceMessage(InternalSensors.RegionDrag)
                {
                    Body = regionId,
                };
                msg.Headers["device"] = device;
                msg.Headers["directionX"] = direction.X.ToString();
                msg.Headers["directionY"] = direction.Y.ToString();
                msg.Headers["angle"] = getAngleDegrees(0, 0, direction.X, direction.Y).ToString();
                await ServiceManager.BroadcastMessage(msg);
            }
        }
        private double getAngleDegrees(double fromX, double fromY, double toX, double toY, bool force360 = true)
        {
            var deltaX = fromX - toX;
            var deltaY = fromY - toY; // reverse
            var radians = Math.Atan2(deltaY, deltaX);
            var degrees = (radians * 180) / Math.PI - 90; // rotate
            if (force360)
            {
                while (degrees >= 360) degrees -= 360;
                while (degrees < 0) degrees += 360;
            }
            return degrees;
        }
        #endregion

        #region Sensor - File Drop
        private void Layer_DragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Link;
            e.Handled = true;
        }

        private void Layer_Drop(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.None;
            e.Handled = true;
        }

        DropZoneWindow? wDropZone;
        DateTime? lastSentDragEvent = null;
        private async void Layer_DragEnter(object sender, DragEventArgs e)
        {
            if (wDropZone != null)
            {
                try
                {
                    wDropZone.StopClosing();
                    return;
                }
                catch { }
            }

            var eventTime = DateTime.Now;
            if (lastSentDragEvent != null && (eventTime - lastSentDragEvent.Value).TotalMilliseconds < 500)
            {
                e.Effects = DragDropEffects.None;
                e.Handled = true;
                return;
            }

            var poolRequest = new PoolRequest()
            {
                RequestData = e.Data
            };
            var msg = new ServiceMessage(InternalSensors.DragDrop);
            msg.Headers["event"] = "preview";
            msg.Kind = ServiceMessageKind.Pool;
            msg.Body = poolRequest;
            lastSentDragEvent = eventTime;
            await ServiceManager.BroadcastMessage(msg);

            List<FileDropData> compatibleServices = new List<FileDropData>();
            foreach (var data in poolRequest.PoolData)
            {
                var svc = ServiceManager.GetService(data.ServiceID);
                if (svc != null)
                {
                    compatibleServices.Add(new FileDropData(svc, data.Data as FileDropReceiverData));
                }
            }

            if (compatibleServices.Count > 0)
            {
                wDropZone = new DropZoneWindow(ServiceManager);
                wDropZone.Populate(compatibleServices);
                wDropZone.Owner = this;
                wDropZone.Show();
                wDropZone.Closed += DropZoneWindow_Closed;
                e.Effects = DragDropEffects.Link;
            }
            else
            {
                e.Effects = DragDropEffects.None;
            }
            e.Handled = true;
        }

        private void DropZoneWindow_Closed(object? sender, EventArgs e)
        {
            wDropZone = null;
        }

        private void Layer_DragLeave(object sender, DragEventArgs e)
        {
            if (wDropZone != null)
            {
                try
                {
                    wDropZone.StartClosing();
                }
                catch { }
            }
        }
        #endregion

        #region Config
        bool ignoreStayOnTopChange = false;
        private void cmiStayOnTop_Checked(object sender, RoutedEventArgs e)
        {
            if (!_isUIReady || ignoreStayOnTopChange) return;
            Topmost = cmiStayOnTop.IsChecked;
            if (Configuration != null)
            {
                Configuration.StayOnTop = cmiStayOnTop.IsChecked;
                CoreService?.SaveConfiguration();
            }
        }
        private void cmiStayOnTop_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!_isUIReady || ignoreStayOnTopChange) return;
            Topmost = cmiStayOnTop.IsChecked;
            if (Configuration != null)
            {
                Configuration.StayOnTop = cmiStayOnTop.IsChecked;
                CoreService?.SaveConfiguration();
            }
        }
        #endregion

        #region Region Control
        private void cmiRegionVisible_Checked(object sender, RoutedEventArgs e)
        {
            if (grRegionContainer != null)
            {
                foreach (var child in grRegionContainer.Children)
                {
                    if (child is Shape shape)
                    {
                        shape.Opacity = 1;
                        if (shape.ToolTip is ToolTip tlt)
                        {
                            tlt.Visibility = Visibility.Visible;
                        }
                    }
                }
            }
        }

        private void cmiRegionVisible_Unchecked(object sender, RoutedEventArgs e)
        {
            if (grRegionContainer != null)
            {
                foreach (var child in grRegionContainer.Children)
                {
                    if (child is Shape shape)
                    {
                        shape.Opacity = 0.001;
                        if (shape.ToolTip is ToolTip tlt)
                        {
                            tlt.Visibility = Visibility.Collapsed;
                        }
                    }
                }
            }
        }
        #endregion

        #region Drag and Snap
        private void gridMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isUIReady || cmiLockPosition.IsChecked) return;
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Cursor = Cursors.ScrollAll;
                DragMove();
                Cursor = Cursors.Arrow;
                SavePositionConfig();
            }
        }

        private void SavePositionConfig()
        {
            try
            {
                if (!_isUIReady) return;
                if (Configuration != null && CoreService != null)
                {
                    Configuration.PositionLeft = Left;
                    Configuration.PositionTop = Top;
                    CoreService.SaveConfiguration();
                }
            }
            catch { }
        }

        private Rect GetCurrentScreenRect()
        {
            var center = new Point(Left + Width / 2, Top + Height / 2);
            var rect = new Rect(Left, Top, Width, Height);
            var scrnRects = new List<Rect>();
            foreach (var scrn in System.Windows.Forms.Screen.AllScreens)
            {
                scrnRects.Add(new Rect(scrn.WorkingArea.Left, scrn.WorkingArea.Top, scrn.WorkingArea.Width, scrn.WorkingArea.Height));
            }
            if (scrnRects.Any(x => x.Contains(center)))
                return scrnRects.Where(x => x.Contains(center)).First();
            if (scrnRects.Any(x => x.IntersectsWith(rect) || x.Contains(rect)))
                return scrnRects.Where(x => x.IntersectsWith(rect) || x.Contains(rect)).First();
            var primScreen = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea;
            return new Rect(primScreen.Left, primScreen.Top, primScreen.Width, primScreen.Height);
        }

        private void ResetPosition(bool setPositionFromConfig = false)
        {
            if (!setPositionFromConfig)
            {
                var wa = GetCurrentScreenRect();

                Left = wa.Right - Width;
                Top = wa.Bottom - Height;
            }
            else
            {
                if (Configuration != null && Configuration.PositionTop.HasValue && Configuration.PositionLeft.HasValue)
                {
                    Left = Configuration.PositionLeft.Value;
                    Top = Configuration.PositionTop.Value;
                }
                else
                {
                    var wa = GetCurrentScreenRect();

                    Left = wa.Right - Width;
                    Top = wa.Bottom - Height;
                }
            }

            EnsureSnap();
        }

        private void EnsureSnap()
        {
            if (!cmiSnapDesktop.IsChecked)
            {
                return;
            }
            var rect = new Rect(Left, Top, Width, Height);
            var scrnRects = new List<Rect>();
            foreach (var scrn in System.Windows.Forms.Screen.AllScreens)
            {
                scrnRects.Add(new Rect(scrn.WorkingArea.Left, scrn.WorkingArea.Top, scrn.WorkingArea.Width, scrn.WorkingArea.Height));
            }
            if (scrnRects.Any(x =>
                    x.Contains(rect)
                ) ||
                (
                    scrnRects.Any(x => x.Contains(rect.TopLeft))
                    && scrnRects.Any(x => x.Contains(rect.TopRight))
                    && scrnRects.Any(x => x.Contains(rect.BottomLeft))
                    && scrnRects.Any(x => x.Contains(rect.BottomRight))
                )
                )
            {
                return;
            }

            var currentScreen = GetCurrentScreenRect();
            var shouldMoveX = currentScreen.Left > rect.Left || currentScreen.Right < rect.Right;
            var shouldMoveY = currentScreen.Top > rect.Top || currentScreen.Bottom < rect.Bottom;

            if (shouldMoveX)
            {
                Left = Math.Max(currentScreen.Left, Math.Min(currentScreen.Right - Width, Left));
            }
            if (shouldMoveY)
            {
                Top = Math.Max(currentScreen.Top, Math.Min(currentScreen.Bottom - Height, Top));
            }
        }

        private void cmiSnapDesktop_Checked(object sender, RoutedEventArgs e)
        {
            EnsureSnap();
            if (Configuration != null)
            {
                Configuration.SnapToScreen = cmiSnapDesktop.IsChecked;
                SavePositionConfig();
            }
        }
        #endregion

        AuraConsole? AuraConsole { get; set; }
        private void miConsole_Click(object sender, RoutedEventArgs e)
        {
            if (AuraConsole == null)
            {
                AuraConsole = new AuraConsole();
                AuraConsole.Closed += AuraConsole_Closed;
                AuraConsole.Show();
            }
            AuraConsole.BringIntoView();
            AuraConsole.Topmost = true;
            AuraConsole.Topmost = false;
        }

        private void AuraConsole_Closed(object? sender, EventArgs e)
        {
            AuraConsole = null;
        }
    }
}
