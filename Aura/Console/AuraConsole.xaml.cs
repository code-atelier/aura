﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.Console
{
    /// <summary>
    /// Interaction logic for AuraConsole.xaml
    /// </summary>
    public partial class AuraConsole : Window
    {
        public ObservableCollection<ConsoleEntry> Entries { get; } = new ObservableCollection<ConsoleEntry>();

        public AuraConsole()
        {
            Initialize();
            InitializeComponent();
            itemChat.ItemsSource = Entries;

            Entries.Add(new ConsoleEntry()
            {
                Text = string.Join(Environment.NewLine, new[] {
                    "Hi, welcome to Aura Console.",
                    "There are 2 ways to use the console:",
                    "- Chat with chatbot:",
                    "  - Just type your message and press enter to send",
                    "- Console commands:",
                    "  - Start your message with forward-slash character (/)",
                    "  - Then type the console commands and press enter to send",
                    "  - For Example: /say Hello world!",
                }),
                SentByAura = true,
            });

            RichTextBoxHelper.OwnChatClicked += RichTextBoxHelper_OwnChatClicked;
            txMessage.Focus();
        }

        private void RichTextBoxHelper_OwnChatClicked(object? sender, string? e)
        {
            if (e != null)
            {
                txMessage.Text = e;
                txMessage.Select(e.Length, 0);
                txMessage.Focus();
            }
        }

        private void txMessage_TextChanged(object sender, TextChangedEventArgs e)
        {
            lbTypeMessage.Visibility = string.IsNullOrEmpty(txMessage.Text) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void txMessage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                e.Handled = true;
                if (IsBusy) return;

                var message = txMessage.Text.Trim();
                txMessage.Clear();
                if (string.IsNullOrWhiteSpace(message)) return;

                Entries.Add(new ConsoleEntry()
                {
                    Text = message,
                    SentByAura = false,
                });

                if (message.StartsWith("/"))
                {
                    HandleConsoleCommand(message.Substring(1));
                }
                else
                {
                    HandleChat(message);
                }
            }
        }

        ConsoleEntry? thinkingEntry = null;
        bool _isBusy = false;
        private bool IsBusy
        {
            get => _isBusy; set
            {
                if (value != _isBusy)
                {
                    Dispatcher.Invoke(() =>
                    {
                        if (value)
                        {
                            thinkingEntry = new ConsoleEntry()
                            {
                                Text = "Thinking...",
                                SentByAura = true,
                                Thinking = true,
                            };
                            Entries.Add(thinkingEntry);
                        }
                        else if (thinkingEntry != null) 
                        {
                            Entries.Remove(thinkingEntry);
                            thinkingEntry = null;
                        }
                    });
                    _isBusy = value;
                }
            }
        }

        private async void HandleConsoleCommand(string command)
        {
            IsBusy = true;
            try
            {
                var spl = command.Split(' ', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);
                var id = spl.FirstOrDefault()?.ToLower() ?? "";
                if (id == "help")
                {
                    var cmds = Processors.Keys.ToList();
                    cmds.Add("cls");
                    cmds.Add("help");
                    cmds.Sort();
                    Dispatcher.Invoke(() =>
                    {
                        Entries.Add(new ConsoleEntry()
                        {
                            Text = "Console commands found: " + string.Join(", ", cmds),
                            SentByAura = true,
                        });
                    });
                    return;
                }
                if (id == "cls")
                {
                    Dispatcher.Invoke(() =>
                    {
                        Entries.Clear();
                    });
                    return;
                }
                if (Processors.ContainsKey(id))
                {
                    var res = await Processors[id].Process(command.Trim());
                    if (!string.IsNullOrWhiteSpace(res))
                    {
                        Dispatcher.Invoke(() =>
                        {
                            Entries.Add(new ConsoleEntry()
                            {
                                Text = res,
                                SentByAura = true,
                            });
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(() =>
                {
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = "Error: " + ex.Message,
                        SentByAura = true,
                    });
                });
            }
            finally
            {
                IsBusy = false;
                Dispatcher.Invoke(() =>
                {
                    svChat.ScrollToEnd();
                });
            }
        }

        private async void HandleChat(string text)
        {
            IsBusy = true;
            try
            {
                var svcs = AuraStatic.ServiceManager.Services.Where(x => x is IChatbotService && x.State == AuraServiceState.Running);
                if (svcs.Count() == 0) throw new Exception("Cannot find any running Chatbot service");

                var svc = svcs.First() as IChatbotService;

                if (svcs.Count() == 0) throw new Exception("Cannot find any running Chatbot service");

                var msg = await svc.ProcessMessageAsync(text);

                if (msg == null) throw new Exception("Failed to process message");

                Dispatcher.Invoke(() =>
                {
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = msg,
                        SentByAura = true,
                    });
                });
            }
            catch(Exception ex)
            {
                Dispatcher.Invoke(() =>
                {
                    Entries.Add(new ConsoleEntry()
                    {
                        Text = "Error: " + ex.Message,
                        SentByAura = true,
                    });
                });
            }
            finally
            {
                IsBusy = false;
                Dispatcher.Invoke(() =>
                {
                    svChat.ScrollToEnd();
                });
            }
        }

        private static bool _initialized = false;
        public static void Initialize()
        {
            if (_initialized) return;

            var processorType = typeof(IAuraConsoleProcessor);
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            Processors.Clear();
            foreach (var assembly in assemblies)
            {
                try
                {
                    var classes = assembly.GetExportedTypes().Where(x => processorType.IsAssignableFrom(x) && !x.IsAbstract && x.IsClass);
                    foreach (var type in classes)
                    {
                        try
                        {
                            var processor = Activator.CreateInstance(type) as IAuraConsoleProcessor;
                            if (processor != null)
                            {
                                processor.ServiceManager = AuraStatic.ServiceManager;
                                var id = processor.Identifier.ToLower().Trim();
                                if (!Processors.ContainsKey(id))
                                    Processors.Add(id, processor);
                            }
                        }
                        catch { }
                    }
                }
                catch { }
            }
            _initialized = true;
        }

        public static Dictionary<string, IAuraConsoleProcessor> Processors { get; } = new Dictionary<string, IAuraConsoleProcessor>();

        private void Window_Closed(object sender, EventArgs e)
        {
            RichTextBoxHelper.OwnChatClicked -= RichTextBoxHelper_OwnChatClicked;
        }
    }

    public class RichTextBoxHelper : DependencyObject
    {
        public static string GetDocumentMarkdown(DependencyObject obj)
        {
            return (string)obj.GetValue(DocumentMarkdownProperty);
        }

        public static void SetDocumentMarkdown(DependencyObject obj, string value)
        {
            obj.SetValue(DocumentMarkdownProperty, value);
        }

        public static readonly DependencyProperty DocumentMarkdownProperty =
            DependencyProperty.RegisterAttached(
                "DocumentMarkdown",
                typeof(string),
                typeof(RichTextBoxHelper),
                new FrameworkPropertyMetadata
                {
                    BindsTwoWayByDefault = false,
                    PropertyChangedCallback = (obj, e) =>
                    {
                        var richTextBox = (RichTextBox)obj;
                        var entry = (ConsoleEntry)richTextBox.Tag;
                        var md = GetDocumentMarkdown(richTextBox);
                        var senderIsAura = entry.SentByAura;
                        if (senderIsAura)
                        {
                            var doc = RichTextDocumentParser.Parse(md);
                            richTextBox.Document = doc;
                        }
                        else
                        {
                            var doc = new FlowDocument();
                            var p = new Paragraph();
                            p.Tag = md;
                            p.Cursor = Cursors.Hand;
                            p.Inlines.Add(new Run(md));
                            p.MouseLeftButtonDown += OwnChatTrigger;
                            doc.Blocks.Add(p);
                            richTextBox.Document = doc;
                        }
                    }
                });

        private static void OwnChatTrigger(object sender, MouseButtonEventArgs e)
        {
            var para = (Paragraph)sender;
            OwnChatClicked?.Invoke(sender, para.Tag as string);
        }

        public static event EventHandler<string?>? OwnChatClicked;
    }
}
