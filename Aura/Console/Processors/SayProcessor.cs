﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Console.Processors
{
    public class SayProcessor : IAuraConsoleProcessor
    {
        public string Identifier => "say";

        public ServiceManager? ServiceManager { get; set; }

        public async Task<string?> Process(string input)
        {
            if (ServiceManager != null)
            {
                var message = input.Substring(4);
                await ServiceManager.BroadcastMessage(new ServiceMessage("Console", InternalServices.MessageService, message));
            }
            return null;
        }
    }
}
