﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Sources;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using Windows.ApplicationModel.Chat;

namespace Aura.Console
{
    public class ConsoleEntry
    {
        public static Color AuraBackgroundColor { get; } = Colors.White;
        public static Color UserBackgroundColor { get; } = Colors.Honeydew;
        public static Color AuraBorderColor { get; } = Color.FromRgb(0x99, 0x99, 0x99);
        public static Color UserBorderColor { get; } = Color.FromRgb(0x99, 0xbb, 0x99);

        public string Text { get; set; } = string.Empty;

        public bool SentByAura { get; set; }
        public bool Thinking { get; set; }

        public SolidColorBrush ForegroundColor { get => !Thinking ? new SolidColorBrush(Color.FromArgb(0xff, 0x33, 0x33, 0x33)) : new SolidColorBrush(Colors.Gray); }
        public SolidColorBrush BackgroundColor { get => new SolidColorBrush(SentByAura ? AuraBackgroundColor : UserBackgroundColor); }
        public SolidColorBrush BorderColor { get => new SolidColorBrush(SentByAura ? AuraBorderColor : UserBorderColor); }
        public HorizontalAlignment HorizontalAlignment { get => SentByAura ? HorizontalAlignment.Left : HorizontalAlignment.Right; }
        public Thickness Margin { get => SentByAura ? new Thickness(5, 5, 100, 5) : new Thickness(100, 5, 5, 5); }
    }

    public static class RichTextDocumentParser
    {
        public static FlowDocument Parse(string text)
        {
            ResetVars();
            var lines = text.Split(new[] { "\r\n", "\n", "\r" }, StringSplitOptions.None);
            // Create a FlowDocument to contain content for the RichTextBox.
            FlowDocument doc = new FlowDocument();

            foreach (var line in lines)
            {
                var block = ParseLine(line);
                if (block != null)
                    doc.Blocks.AddRange(block);
            }

            var finalBlock = Cleanup();
            if (finalBlock != null)
                doc.Blocks.AddRange(finalBlock);

            return doc;
        }

        private static void ResetVars()
        {
            _list.Clear();
            codeStarted = false;
            CodeBlocks.Clear();
        }

        static Stack<List> _list { get; } = new Stack<List>();

        private static IEnumerable<Block>? Cleanup()
        {
            var res = new List<Block>();
            List? finalList = null;
            while (_list.Count > 0)
            {
                finalList = PopList();
            }
            if (finalList != null)
            {
                res.Add(finalList);
            }

            ResetVars();
            return res;
        }

        private static List? PopList()
        {
            if (_list.Count < 1) return null;
            var popped = _list.Pop();
            if (_list.Count < 1) return popped;
            var parentList = _list.Peek();
            if (popped != null)
            {
                parentList.ListItems.LastListItem.Blocks.Add(popped);
            }
            return popped;
        }

        static bool codeStarted = false;
        static List<Paragraph> CodeBlocks { get; } = new List<Paragraph>();

        private static IEnumerable<Block>? ParseLine(string line)
        {
            line = line.Replace("\t", "  ");
            if (codeStarted)
            {
                if (line.Trim().StartsWith("```"))
                {
                    codeStarted = false;
                    var blocks = CodeBlocks.ToArray();
                    CodeBlocks.Clear();
                    return blocks;
                }
                else
                {
                    var pCode = new Paragraph()
                    {
                        Padding = new Thickness(4, 2, 4, 2),
                        Margin = new Thickness(0),
                    };
                    pCode.FontFamily = new FontFamily("Consolas");
                    pCode.FontSize = 11;
                    pCode.Inlines.Add(new Run(line));
                    pCode.Background = new SolidColorBrush(Colors.AliceBlue);
                    CodeBlocks.Add(pCode);
                    return null;
                }
            }
            else
            {
                if (line.Trim().StartsWith("```"))
                {
                    codeStarted = true;
                    CodeBlocks.Clear();
                    return null;
                }
            }
            if (line.TrimStart().StartsWith("- "))
            {
                var nestLevel = line.IndexOf('-') / 2 + 1;
                while (_list.Count < nestLevel)
                {
                    _list.Push(new List()
                    {
                        Margin = new Thickness(0, 0, 0, 4),
                        Padding = new Thickness(8, 0, 0, 0)
                    });
                }
                while (_list.Count > nestLevel)
                {
                    PopList();
                }
                var list = _list.Peek();
                list.MarkerStyle = TextMarkerStyle.Disc;
                list.ListItems.Add(new ListItem(new Paragraph(ParseInline(line.TrimStart().Substring(2)))));
                return null;
            }

            var res = new List<Block>();

            var popped = PopList();
            while (popped != null)
            {
                var lastPopped = popped;
                popped = PopList();
                if (popped == null)
                {
                    res.Add(lastPopped);
                }
            }

            var p = new Paragraph(ParseInline(line))
            {
                Margin = new Thickness(0, 0, 0, 4),
            };
            res.Add(p);
            return res;
        }

        private static Inline? ParseInline(string line)
        {
            return new Run(line);
        }
    }
}
