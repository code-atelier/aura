﻿using Aura.Models;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Versioning;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (OperatingSystem.IsWindowsVersionAtLeast(10, 0, 17763, 0))
            {
                ToastNotificationManagerCompat.OnActivated += ToastNotificationManagerCompat_OnActivated;
            }

            var curdir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (curdir != null)
                Environment.CurrentDirectory = curdir;
            if (e.Args != null)
            {
                var args = e.Args.ToList();
                if (args.Contains("--quick"))
                {
                    AuraStatic.QuickStartup = true;
                }
            }
        }

        // https://learn.microsoft.com/en-us/windows/apps/design/shell/tiles-and-notifications/send-local-toast?tabs=desktop
        [SupportedOSPlatform("windows10.0.17763.0")]
        private void ToastNotificationManagerCompat_OnActivated(ToastNotificationActivatedEventArgsCompat e)
        {
            if (AuraStatic.ServiceManager != null)
            {
                try
                {
                    var args = ToastArguments.Parse(e.Argument);
                    var toastActivationData = new ToastActivationData();
                    var receiver = "*";
                    foreach(var arg in args)
                    {
                        if (arg.Key.StartsWith("$"))
                        {
                            if (arg.Key == "$receiver")
                                receiver = arg.Value;
                        }
                        else
                        {
                            toastActivationData.Arguments.Add(arg.Key, arg.Value);
                        }
                    }
                    foreach(var val in e.UserInput)
                    {
                        toastActivationData.UserInput.Add(val.Key, val.Value);
                    }

                    var msg = new ServiceMessage(InternalSensors.Toast, receiver, toastActivationData);
                    AuraStatic.ServiceManager.BroadcastMessage(msg);
                }
                catch { }
            }
        }
    }
}
