﻿using Aura.Models;
using Aura.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura
{
    public class FileDropData
    {
        public FileDropReceiverData? Data { get; set; }

        public IAuraService Service { get; set; }

        public FileDropData(IAuraService service, FileDropReceiverData? data = null)
        {
            Data = data;
            Service = service;
        }
    }
}
