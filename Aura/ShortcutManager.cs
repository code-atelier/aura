﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Windows.Services.Maps;

namespace Aura
{
    public static class ShortcutManager
    {
        public const string ShortcutStorage = "shortcuts.json";

        public static List<ShortcutItem> Get()
        {
            if (File.Exists(ShortcutStorage))
            {
                var json = File.ReadAllText(ShortcutStorage);
                var res = JsonSerializer.Deserialize<List<ShortcutItem>>(json);
                if (res != null)
                {
                    foreach (var ele in res)
                    {
                        try
                        {
                            if (File.Exists(ele.Target))
                            {
                                var icon = Icon.ExtractAssociatedIcon(ele.Target);
                                if (icon != null)
                                {
                                    var ms = new MemoryStream();
                                    icon.ToBitmap().Save(ms, ImageFormat.Png);
                                    ms.Seek(0, SeekOrigin.Begin);

                                    var bmp = new BitmapImage();
                                    bmp.BeginInit();
                                    bmp.StreamSource = ms;
                                    bmp.EndInit();
                                    ele.Icon = bmp;
                                }
                            }
                            else if (Directory.Exists(ele.Target))
                            {
                                var bmp = new BitmapImage();
                                bmp.BeginInit();
                                bmp.UriSource = new Uri("pack://application:,,,/Aura;component/res/img/folder.png");
                                bmp.EndInit();
                                ele.Icon = bmp;
                            }
                        }
                        catch { }
                    }
                    return res;
                }
            }
            return new List<ShortcutItem>();
        }

        public static void Add(ShortcutItem item)
        {
            var current = Get();
            if (!current.Any(x => x.Name == item.Name && x.Target == item.Target))
            {
                current.Add(item);
                var json = JsonSerializer.Serialize(current);
                File.WriteAllText(ShortcutStorage, json);
            }
        }

        public static void Add(string path)
        {
            Add(new ShortcutItem()
            {
                Name = Path.GetFileNameWithoutExtension(path) + (Directory.Exists(path) ? "/" : ""),
                Target = path,
            });
        }

        public static void Remove(ShortcutItem item)
        {
            var current = Get();
            current.RemoveAll(x => x.Name == item.Name && x.Target == item.Target);
            var json = JsonSerializer.Serialize(current);
            File.WriteAllText(ShortcutStorage, json);
        }
    }

    public class ShortcutItem
    {
        public string Name { get; set; } = string.Empty;

        public string Target { get; set; } = string.Empty;

        [JsonIgnore]
        public BitmapImage? Icon { get; set; }
    }
}
