﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    public class SystemInfoEntry
    {
        public string Key { get; set; }

        public string Value { get; set; }

        public string? ActionName { get; set; }

        public Visibility ActionVisibility { get => !string.IsNullOrWhiteSpace(ActionName) ? Visibility.Visible : Visibility.Collapsed; }

        public Action<SystemInfoEntry>? Action { get; set; }

        public void InvokeAction()
        {
            Action?.Invoke(this);
        }

        public SystemInfoEntry(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
