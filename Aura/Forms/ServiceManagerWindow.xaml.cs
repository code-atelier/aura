﻿using Aura.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Aura.Forms
{
    /// <summary>
    /// Interaction logic for ServiceManagerWindow.xaml
    /// </summary>
    public partial class ServiceManagerWindow : Window
    {
        DispatcherTimer Timer { get; } = new DispatcherTimer(DispatcherPriority.Render);
        ServiceManager Manager { get; }
        static CoreService? CoreService { get; set; }
        ObservableCollection<ServiceGridItem> GridItems { get; } = new ObservableCollection<ServiceGridItem>();

        public ServiceManagerWindow()
        {
            InitializeComponent();
            Manager = AuraStatic.ServiceManager;
            CoreService = Manager.GetService<CoreService>();
            dgvServices.ItemsSource = GridItems;
            ReloadGrid();
            Timer.Interval = new TimeSpan(0, 0, 1);
            Timer.Start();
            Timer.Tick += Timer_Tick;
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            RefreshGrid();
        }

        private static bool CanAutoStart(IAuraService service)
        {
            if (ServiceManager.IsSystemService(service)) return true;
            if (CoreService?.Configuration != null && CoreService.Configuration.AllowAutoStart != null)
            {
                if (CoreService.Configuration.AllowAutoStart.ContainsKey(service.ServiceID))
                {
                    return CoreService.Configuration.AllowAutoStart[service.ServiceID];
                }
            }
            return true;
        }

        private void ReloadGrid()
        {
            var sorted = Manager.Services.ToList();
            sorted.Sort((a, b) =>
            {
                if (ServiceManager.IsSystemService(a) && !ServiceManager.IsSystemService(b)) return -1;
                if (!ServiceManager.IsSystemService(a) && ServiceManager.IsSystemService(b)) return 1;
                return a.ServiceName.CompareTo(b.ServiceName);
            });

            var selIndex = dgvServices.SelectedIndex;
            GridItems.Clear();
            foreach (var svc in sorted)
            {
                GridItems.Add(new ServiceGridItem(svc));
            }
            dgvServices.SelectedIndex = selIndex;
        }

        private void RefreshGrid()
        {
            foreach (var item in GridItems)
            {
                item.RaisePropertyChanged("Status");
                item.RaisePropertyChanged("StartupType");
            }
#pragma warning disable CS8625 // Cannot convert null literal to non-nullable reference type.
            dgvServices_SelectionChanged(this, null);
#pragma warning restore CS8625 // Cannot convert null literal to non-nullable reference type.
        }

        class ServiceGridItem : INotifyPropertyChanged
        {
            public IAuraService Service { get; }

            public ServiceGridItem(IAuraService service)
            {
                Service = service;
            }

            public string Name { get => Service.ServiceName; }
            public string ID { get => Service.ServiceID; }
            public string Status { get => Service.State.ToString(); }
            public string Configurable { get => Service.HasConfiguration ? "yes" : "no"; }
            public string StartupType { get => Service.AutoStart && CanAutoStart(Service) ? "Automatic" + (IsSystem ? " (core)" : "") : "Manual"; }
            public SolidColorBrush Foreground
            {
                get =>
                    Service.State == AuraServiceState.Running ? new SolidColorBrush(Colors.Lime)
                    : new SolidColorBrush(Colors.Black);
            }
            public bool IsSystem { get => ServiceManager.IsSystemService(Service); }
            public bool CanShutdown { get => Service.CanUserShutdown; }


            public event PropertyChangedEventHandler? PropertyChanged;

            public void RaisePropertyChanged(string propertyName)
            {
                if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void dgvServices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem service)
            {
                btnConfiguration.IsEnabled = service.Service.HasConfiguration;
                btnDisableAuto.Visibility = service.StartupType == "Automatic" && !service.IsSystem ? Visibility.Visible : Visibility.Collapsed;
                btnEnableAuto.Visibility = btnDisableAuto.Visibility != Visibility.Visible && !service.IsSystem ? Visibility.Visible : Visibility.Collapsed;
                btnStart.Visibility = service.Service.State == AuraServiceState.Stopped && !service.IsSystem ? Visibility.Visible : Visibility.Collapsed;
                btnStop.Visibility = btnStart.Visibility != Visibility.Visible && !service.IsSystem && service.CanShutdown ? Visibility.Visible : Visibility.Collapsed;
            }
            else
            {
                btnConfiguration.IsEnabled = false;
                btnEnableAuto.Visibility =
                btnStart.Visibility =
                btnStop.Visibility =
                btnDisableAuto.Visibility = Visibility.Collapsed;
            }
        }

        private void btnConfiguration_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem service && service.Service.HasConfiguration)
            {
                service.Service.OpenConfiguration();
            }
        }

        private async void btnStop_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem service)
            {
                try
                {
                    await service.Service.Shutdown();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private async void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem service)
            {
                try
                {
                    await service.Service.Start();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Timer.Stop();
        }

        private void btnDisableAuto_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem service)
            {
                if (CoreService?.Configuration != null)
                {
                    CoreService.Configuration.AllowAutoStart[service.Service.ServiceID] = false;
                    CoreService.SaveConfiguration();
                }
            }
        }

        private void btnEnableAuto_Click(object sender, RoutedEventArgs e)
        {
            if (dgvServices.SelectedItem is ServiceGridItem service)
            {
                if (CoreService?.Configuration != null)
                {
                    CoreService.Configuration.AllowAutoStart[service.Service.ServiceID] = true;
                    CoreService.SaveConfiguration();
                }
            }
        }
    }

}
