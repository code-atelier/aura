﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public class TagService : AuraServiceBase
    {
        public Dictionary<string, List<string>> ActiveTags { get; } = new Dictionary<string, List<string>>();

        public Dictionary<string, List<TimedTag>> TimedTags { get; } = new Dictionary<string, List<TimedTag>>();

        public IEnumerable<string> PoolIDs
        {
            get
            {
                List<string> res = new List<string>();

                res.AddRange(ActiveTags.Keys);
                res.AddRange(TimedTags.Keys);
                res = res.Distinct().ToList();

                return res;
            }
        }

        public override string ServiceID => "svc.coatl.tag";
        public override string ServiceName => "Aura Tag";

        public override AuraServiceMessagePriority MessagePriority => AuraServiceMessagePriority.High;

        public TagCollection GetTags()
        {
            var res = new List<string>();
            foreach (var at in ActiveTags)
            {
                res.AddRange(at.Value);
            }
            foreach (var tt in TimedTags)
            {
                tt.Value.RemoveAll(x => x.Expire <= DateTime.Now);
                res.AddRange(tt.Value.Select(x => x.Tag));
            }
            res = res.Distinct().ToList();
            return new TagCollection(res);
        }

        public void SetTag(string poolId, string tag, bool active = true)
        {
            lock (ActiveTags)
            {
                if (!ActiveTags.ContainsKey(poolId))
                    ActiveTags.Add(poolId, new List<string>());


                tag = tag.ToLower().Trim();
                if (active)
                {
                    if (!ActiveTags[poolId].Contains(tag))
                        ActiveTags[poolId].Add(tag);
                }
                else
                {
                    if (ActiveTags[poolId].Contains(tag))
                        ActiveTags[poolId].Remove(tag);
                }
            }
            SendUIRefreshRequest();
        }

        public void SetExpiringTag(string poolId, string tag, TimeSpan expireIn)
        {
            if (!TimedTags.ContainsKey(poolId))
                TimedTags.Add(poolId, new List<TimedTag>());

            tag = tag.ToLower().Trim();
            TimedTags[poolId].RemoveAll(x => x.Tag == tag);
            TimedTags[poolId].Add(new TimedTag()
            {
                Tag = tag,
                Expire = DateTime.Now + expireIn,
            });
            foreach (var tt in TimedTags)
            {
                tt.Value.RemoveAll(x => x.Expire <= DateTime.Now);
            }
            SendUIRefreshRequest();
        }

        public void ClearTags(string poolId)
        {
            if (ActiveTags.ContainsKey(poolId))
                ActiveTags[poolId].Clear();
        }

        public void ClearExpiringTags(string poolId)
        {
            if (TimedTags.ContainsKey(poolId))
                TimedTags[poolId].Clear();
        }

        private void SendUIRefreshRequest()
        {
            var svcAvatar = Manager?.GetService<AvatarControllerService>();
            if (svcAvatar != null && svcAvatar.State == AuraServiceState.Running)
            {
                svcAvatar.RequestUpdate();
            }
        }
    }
}
