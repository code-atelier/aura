﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public class ServiceManager
    {
        bool _initialized = false;

        public WidgetManager Widgets { get; }
        public List<IAuraService> Services { get; } = new List<IAuraService>();

        public void InitializeServiceManager(System.Windows.Window? mainWindow)
        {
            if (_initialized) return;
            RegisterRequiredServices();
            LoadExternalServices();
            MainWindow = mainWindow;
            _initialized = true;
        }

        public System.Windows.Window? MainWindow { get; private set; }

        public ServiceManager()
        {
            Widgets = new WidgetManager(this);
        }

        private void RegisterRequiredServices()
        {
            RegisterService(new CoreService());
            RegisterService(new AvatarControllerService());
            RegisterService(new TagService());
            RegisterService(new MessageService());
            RegisterService(new ScheduleService());
            RegisterService(new EmotionService());
        }

        private void LoadExternalServices()
        {
            var path = "data\\services";
            if (Directory.Exists(path)) {
                var dlls = Directory.EnumerateFiles(path, "*.dll");
                foreach(var dll in dlls)
                {
                    try
                    {
                        var asm = Assembly.LoadFrom(dll);
                        var auraSvc = typeof(IAuraService);
                        var svcs = asm.ExportedTypes.Where(x => auraSvc.IsAssignableFrom(x)).ToList();
                        foreach(var svc in svcs)
                        {
                            var instance = Activator.CreateInstance(svc);
                            if (instance is IAuraService asvc)
                                RegisterService(asvc);
                        }
                    }
                    catch { }
                }
            }
        }

        public static bool IsSystemService(IAuraService service)
        {
            return service is CoreService
                || service is MessageService
                || service is AvatarControllerService
                || service is TagService
                || service is ScheduleService
                || service is EmotionService;
        }

        private bool CanAutoStart(IAuraService service)
        {
            if (IsSystemService(service)) return true;
            var cs = GetService<CoreService>();
            if (cs?.Configuration?.AllowAutoStart != null && cs.Configuration.AllowAutoStart.ContainsKey(service.ServiceID))
            {
                return cs.Configuration.AllowAutoStart[service.ServiceID];
            }
            return true;
        }

        public async void StartServices()
        {
            foreach(var service in Services)
            {
                if (service.AutoStart && CanAutoStart(service))
                {
                    try
                    {
                        await service.Start();
                    }
                    catch { }
                }
            }
        }

        public void ShutdownServices()
        {
            foreach (var service in Services)
            {
                if (service.State == AuraServiceState.Running || service.State == AuraServiceState.Busy || service.State == AuraServiceState.Starting)
                {
                    try
                    {
                        service.Shutdown();
                    }
                    catch { }
                }
                    
            }
            Widgets.Shutdown();
        }

        public void RegisterService<T>(T service) where T : class, IAuraService
        {
            try
            {
                service.Initialize(this);
                Services.Add(service);
            }
            catch { }
        }

        public T? GetService<T>() where T: class, IAuraService
        {
            return Services.FirstOrDefault(x => x.GetType() == typeof(T)) as T;
        }
        public IAuraService? GetService(string id)
        {
            return Services.FirstOrDefault(x => x.ServiceID == id);
        }

        public async Task BroadcastMessage(ServiceMessage message)
        {
            var kind = message.Kind;
            var sortedService = Services.ToList();
            sortedService.OrderBy(x => (int)x.MessagePriority);
            foreach(var svc in sortedService)
            {
                if (svc.State == AuraServiceState.Error || svc.State == AuraServiceState.Stopped) continue;
                if (message.IntendedReceiver == "*" || svc.ServiceID == message.IntendedReceiver)
                {
                    await svc.ReceiveBroadcastMessage(message);
                    if (message.Handled && kind == ServiceMessageKind.Event)
                        break;
                }
            }
            if (!message.Handled && kind == ServiceMessageKind.Event)
            {
                OnUnhandledEventMessage?.Invoke(this, new UnhandledMessageEventArgs(message));
            }
        }

        public bool IsSafeToShutdown()
        {
            var safe = true;
            foreach(var svc in Services)
            {
                if (!svc.CheckSafeShutdown())
                    safe = false;
            }
            return safe;
        }

        public void InvokeOnMainUIClockTick()
        {
            OnMainUIClockTick?.Invoke(this, new EventArgs());
        }

        public event EventHandler? OnMainUIClockTick;

        public event EventHandler<UnhandledMessageEventArgs>? OnUnhandledEventMessage;
    }

    public class UnhandledMessageEventArgs: EventArgs
    {
        public ServiceMessage Message { get; }

        public UnhandledMessageEventArgs(ServiceMessage message)
        {
            Message = message;
        }
    }
}
