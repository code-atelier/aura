﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.Services
{
    public class WidgetManager
    {
        List<Window> widgets = new List<Window>();
        public IReadOnlyList<Window> ShownWidgetWindows { get => widgets; }

        public ServiceManager ServiceManager { get; }

        public WidgetManager(ServiceManager serviceManager)
        {
            ServiceManager = serviceManager;
        }

        public bool Show(IAuraWidget widget)
        {
            if (!widget.AllowMultiple)
            {
                var window = ShownWidgetWindows.FirstOrDefault(x => x.Tag is IAuraWidget w && w.Guid == widget.Guid);
                if (window != null)
                {
                    try
                    {
                        window.Activate();
                    }
                    catch { }
                    return false;
                }
            }

            try
            {
                widget.ServiceManager = ServiceManager;

                var ww = widget.CreateWidgetWindow();
                ww.Closed += Ww_Closed;
                ww.Show();
                widgets.Add(ww);

                return true;
            }
            catch { }
            return false;
        }

        private void Ww_Closed(object? sender, EventArgs e)
        {
            if (sender is Window ww && widgets.Contains(ww))
            {
                widgets.Remove(ww);
            }
        }

        public void Shutdown()
        {
            foreach(var ww in widgets)
            {
                try
                {
                    ww.Close();
                }
                catch { }
            }
        }
    }
}
