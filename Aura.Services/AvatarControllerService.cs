﻿using Aura.Models;
using Aura.Models.Avatar;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Region = Aura.Models.Avatar.Region;

namespace Aura.Services
{
    public class AvatarControllerService : AuraServiceBase
    {
        public override string ServiceID => InternalServices.AvatarService;
        public override string ServiceName => "Aura Avatar Controller";
        public AvatarManifest? Avatar { get; private set; }
        public AuraArchive? Archive { get; private set; }
        public string? PersonaGuid { get; private set; }

        public Dictionary<string, string> ForcedSets { get; } = new Dictionary<string, string>();
        public Dictionary<string, string> ForcedMaterials { get; } = new Dictionary<string, string>();

        public override AuraServiceMessagePriority MessagePriority => AuraServiceMessagePriority.High;
        public DateTime LastUpdate { get; private set; } = DateTime.MinValue;
        public string[] LayerChanged { get; private set; } = new string[0];
        public List<KeyValuePair<string, string?>> LayerMaterials { get; } = new List<KeyValuePair<string, string?>>();

        public void RequestUpdate()
        {
            LastUpdate = DateTime.Now;
        }

        public async void LoadAvatar(string personaGuid, AvatarManifest manifest, AuraArchive archive)
        {
            _avatarSize = null;
            await RunBusyActionAsync(() =>
            {
                Avatar = manifest;
                Archive = archive;
                PersonaGuid = personaGuid;
                LastUpdate = DateTime.Now;
            });
        }

        public Dictionary<string, Tuple<Set, int>> GetMostActiveSets(TagCollection tags, bool considerForced = true)
        {
            if (Avatar != null)
            {
                var activeSets = Avatar.Sets.GetMostActiveSets(tags);
                if (considerForced)
                {
                    foreach(var forced in ForcedSets)
                    {
                        if (forced.Key.Contains(':'))
                        {
                            var key = activeSets.Keys.FirstOrDefault(x => x.StartsWith(forced.Key));
                            if (key != null)
                            {
                                activeSets.Remove(key);
                            }
                            if (!string.IsNullOrEmpty(forced.Value))
                                activeSets[forced.Value] = new Tuple<Set, int>(Avatar.Sets[forced.Value], -1);
                        }
                        else
                        {
                            if (forced.Value == "on")
                                activeSets[forced.Key] = new Tuple<Set, int>(Avatar.Sets[forced.Key], -1);
                            else if (activeSets.ContainsKey(forced.Key))
                                activeSets.Remove(forced.Key);
                        }
                    }
                }
                return activeSets;
            }
            else
            {
                return new Dictionary<string, Tuple<Set, int>>();
            }
        }

        public bool RefreshMaterial()
        {
            var svcTag = Manager?.GetService<TagService>();
            if (svcTag == null || svcTag.State != AuraServiceState.Running)
                return false;

            var changedLayers = new List<string>();
            var oldMaterials = LayerMaterials.ToList().ToDictionary(x => x.Key, x => x.Value);
            LayerMaterials.Clear();

            if (Avatar != null)
            {
                var tags = svcTag.GetTags();
                var activeSets = GetMostActiveSets(tags);
                var orderedLayers = Avatar.Layers.OrderBy(x => x.Value.ZIndex).ToList();
                foreach (var layer in orderedLayers)
                {
                    var set = activeSets.Select(x => x.Value.Item1).LastOrDefault(x => x.Layers[layer.Key] != null);
                    var source = set?.Layers[layer.Key] ?? layer.Value.Default;

                    if (ForcedMaterials.ContainsKey(layer.Key))
                    {
                        source = ForcedMaterials[layer.Key];
                    }
                    if (source == "") source = null;

                    LayerMaterials.Add(new KeyValuePair<string, string?>(layer.Key, source));
                    if (!oldMaterials.ContainsKey(layer.Key) || oldMaterials[layer.Key] != source)
                    {
                        changedLayers.Add(layer.Key);
                    }
                }
            }

            LayerChanged = changedLayers.ToArray();
            return true;
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            if (message.IntendedReceiver == ServiceID && message.Body is string msg)
            {
                if (msg == "refresh-tags")
                {
                    await RunBusyActionAsync(() =>
                    {
                        LastUpdate = DateTime.Now;
                    });
                }
            }
        }

        private System.Windows.Size? _avatarSize = null;
        public System.Windows.Size? GetAvatarImageSize()
        {
            if (_avatarSize != null)
            {
                return _avatarSize;
            }
            try
            {
                if (Avatar != null && Archive != null)
                {
                    var layer = Avatar.Layers.First().Value;
                    var mat = layer.Materials.First();
                    var path = layer.GetMaterialPath(mat);
                    if (path != null && Archive.FileSystem.Exists(path))
                    {
                        var ss = Archive.FileSystem[path];
                        ss.Seek(0, SeekOrigin.Begin);
                        var ms = new MemoryStream();
                        ss.Seek(0, SeekOrigin.Begin);
                        ss.CopyTo(ms);
                        ms.Seek(0, SeekOrigin.Begin);

                        var bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.StreamSource = ms;
                        bmp.EndInit();
                        _avatarSize = new System.Windows.Size(bmp.PixelWidth, bmp.PixelHeight);
                        return _avatarSize;
                    }
                }
            }
            catch { }
            return null;
        }

        public System.Windows.Point? GetCurrentOrigin(string regionId)
        {
            if (Avatar != null  && Avatar.Regions.ContainsKey(regionId) && Avatar.Regions[regionId]?.Count > 0)
            {
                return GetCurrentOrigin(Avatar.Regions[regionId][0]);
            }
            return null;
        }

        public System.Windows.Point? GetCurrentOrigin(Region? region = null)
        {
            if (Manager?.MainWindow != null && Avatar != null)
            {
                var x = Manager.MainWindow.Left;
                var y = Manager.MainWindow.Top;
                var avaOriginalSize = GetAvatarImageSize() ?? new System.Windows.Size(1, 1);
                var scaleX = Avatar.Display.Width / avaOriginalSize.Width;
                var scaleY = Avatar.Display.Height / avaOriginalSize.Height;

                var ptX = region?.X ?? Manager.MainWindow.Width / 2;
                var ptY = region?.Y ?? Manager.MainWindow.Height / 2;

                return new System.Windows.Point(ptX * scaleX + x, ptY * scaleY + y);
            }
            return null;
        }
    }
}
