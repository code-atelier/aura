﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Chat;

namespace Aura.Services
{
    public static class ServiceMessageExtension
    {
        public static ServiceMessage CreateMessage(this AuraServiceBase service, string intendedReceiver = "*", object? body = null)
        {
            return new ServiceMessage(service.ServiceID, intendedReceiver, body);
        }

        public static async Task<ServiceMessage> CreateAndBroadcastMessage(this AuraServiceBase service, string intendedReceiver = "*", object? body = null)
        {
            var msg = service.CreateMessage(intendedReceiver, body);
            if (service.Manager != null)
            {
                await service.Manager.BroadcastMessage(msg);
            }
            return msg;
        }

        public static ServiceMessage CreateToastNotificationMessage(this AuraServiceBase service, string title, string body, string recipient = "*", Dictionary<string, string>? arguments = null)
        {
            arguments = arguments ?? new Dictionary<string, string>();
            arguments["$receiver"] = recipient;
            var msg = service.CreateMessage(InternalServices.MessageService, body);
            msg["as"] = "toast";
            msg["toast-title"] = title;
            msg["toast-arguments"] = System.Text.Json.JsonSerializer.Serialize(arguments);
            return msg;
        }

        public static bool IsRecipientOf(this AuraServiceBase service, ServiceMessage message, bool strict = false)
        {
            return message.IntendedReceiver == service.ServiceID || (!strict && message.IntendedReceiver == "*");
        }
    }
}
