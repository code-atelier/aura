﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura.Services
{
    public interface IAuraWidget
    {
        Guid Guid { get; }

        string Name { get; }

        string IconUri { get; }

        bool AllowMultiple { get; }

        ServiceManager? ServiceManager { get; set; }

        Window CreateWidgetWindow();
    }
}
