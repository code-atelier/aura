﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public interface IChatbotService
    {
        Task<string?> ProcessMessageAsync(string message); 
    }
}
