﻿using Aura.AI.EmotionModel;
using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public class EmotionService : AuraServiceBase
    {
        public override string ServiceID { get; } = InternalServices.EmotionService;

        public override string ServiceName => "Aura Emotion";

        DateTime _lastUpdate = DateTime.MinValue;
        string _lastEmotions = "";

        public EmotionWheel EmotionWheel { get; } = new EmotionWheel();

        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                if (Manager != null)
                {
                    EmotionWheel.Reset();
                    _lastUpdate = DateTime.Now;
                    Manager.OnMainUIClockTick += Manager_OnMainUIClockTick;
                }
            });
        }

        public void LoadPersona(Persona? persona = null)
        {
            EmotionWheel.Reset(persona?.Mind.Personality);
            var svcTag = Manager?.GetService<TagService>();
            if (svcTag == null) return;
            svcTag.ClearExpiringTags(ServiceID);
            svcTag.ClearTags(ServiceID);
        }

        private void Manager_OnMainUIClockTick(object? sender, EventArgs e)
        {
            var svcTag = Manager?.GetService<TagService>();
            if (svcTag == null) return;

            var delta = DateTime.Now - _lastUpdate;
            _lastUpdate = DateTime.Now;
            EmotionWheel.Decay((float)delta.TotalSeconds);
            var emos = EmotionWheel.GetActivatedEmotions();
            emos.Sort();
            var emotionlist = string.Join(",", emos);
            if (emotionlist != _lastEmotions)
            {
                // update
                svcTag.ClearExpiringTags(ServiceID);
                svcTag.ClearTags(ServiceID);
                foreach (var tag in emos)
                    svcTag.SetTag(ServiceID, tag);
            }
            _lastEmotions = emotionlist;
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            await Task.Run(() =>
            {
            });
        }
    }
}
