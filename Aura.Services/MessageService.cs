﻿using Aura.Models;
using Aura.Services.Windows;
using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Versioning;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using static System.Net.Mime.MediaTypeNames;

namespace Aura.Services
{
    public class MessageService : AuraServiceBase
    {
        public override string ServiceID => Models.InternalServices.MessageService;
        public override string ServiceName => "Aura Message";

        public override AuraServiceMessagePriority MessagePriority => AuraServiceMessagePriority.High;

        const int DelayBetweenMessages = 2000;
        List<KeyValuePair<ServiceMessage, DateTime>> PendingMessages { get; } = new List<KeyValuePair<ServiceMessage, DateTime>>();

        DateTime? _lastShownMessage = null;
        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            await Task.Run(() =>
            {
                if (message.IntendedReceiver == ServiceID)
                {
                    if (message.Body is string)
                    {
                        var hdrAs = message["as"];
                        if (hdrAs == null || hdrAs == "speech")
                        {
                            var delta = (DateTime.Now - _lastShownMessage)?.TotalMilliseconds ?? DelayBetweenMessages;
                            if (delta >= DelayBetweenMessages && !message.Headers.ContainsKey("after") && !message.Headers.ContainsKey("delay"))
                            {
                                ShowMessage(message);
                            }
                            else
                            {
                                int delay;
                                if (!message.Headers.ContainsKey("delay")
                                    || !int.TryParse(message.Headers["delay"], out delay))
                                {
                                    delay = 0;
                                }
                                PendingMessages.Add(new KeyValuePair<ServiceMessage, DateTime>(message, DateTime.Now.AddMilliseconds(delay)));
                            }
                        }
                        else if (hdrAs == "toast")
                        {
                            try
                            {
                                if (OperatingSystem.IsWindowsVersionAtLeast(10, 0, 17763, 0))
                                {
                                    Dictionary<string, string>? arg = null;
                                    if (message.ContainsHeader("toast-arguments"))
                                    {
                                        var hdr = message["toast-arguments"];
                                        if (hdr != null)
                                        {
                                            try
                                            {
                                                arg = JsonSerializer.Deserialize<Dictionary<string, string>>(hdr);
                                            }
                                            catch { }
                                        }
                                    }
                                    ShowToast(message["toast-title"] ?? "Aura", message.Body?.ToString() ?? "", arg);
                                }
                            }
                            catch { }
                        }
                    }
                }
            });
        }

        [SupportedOSPlatform("windows10.0.17763.0")]
        // https://learn.microsoft.com/en-us/windows/apps/design/shell/tiles-and-notifications/send-local-toast?tabs=desktop
        private void ShowToast(string title, string body, Dictionary<string, string>? arguments = null)
        {
            var toast = new ToastContentBuilder();
            toast.AddText(title);
            if (!string.IsNullOrWhiteSpace(body))
                toast.AddText(body);
            if (arguments != null)
            {
                foreach (var arg in arguments)
                {
                    toast.AddArgument(arg.Key, arg.Value);
                }
            }
            toast.Show();
        }

        List<string> Showing { get; } = new List<string>();

        private void ShowMessage(ServiceMessage message)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(() =>
            {
                if (message.Body is string text && Manager != null)
                {
                    _lastShownMessage = DateTime.Now;
                    var window = new MessageWindow();
                    window.Owner = Manager.MainWindow;
                    window.Init(Manager, text, message.Headers.ContainsKey("confirm"), message.Sender, message.Guid.ToString());
                    window.Tag = message.Guid;
                    Showing.Add(message.Guid.ToString());
                    window.Closed += Window_Closed;
                    window.Show();
                }
            });
        }

        private void Window_Closed(object? sender, EventArgs e)
        {
            if (sender is MessageWindow window && window.Tag is Guid guid)
            {
                Showing.Remove(guid.ToString());
            }
        }

        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                if (Manager != null)
                {
                    Manager.OnMainUIClockTick += Manager_OnMainUIClockTick;
                }
            });
        }

        private void Manager_OnMainUIClockTick(object? sender, EventArgs e)
        {
            try
            {
                var delta = (DateTime.Now - _lastShownMessage)?.TotalMilliseconds ?? DelayBetweenMessages;
                if (delta < DelayBetweenMessages) return;

                var viable = PendingMessages
                    .Where(x => x.Value < DateTime.Now && (!x.Key.Headers.ContainsKey("after") || !Showing.Contains(x.Key.Headers["after"])))
                    .MinBy(x => x.Value);
                if (viable.Key != null)
                {
                    PendingMessages.RemoveAll(x => x.Key == viable.Key);
                    ShowMessage(viable.Key);
                }
            }
            catch { }
        }
    }
}
