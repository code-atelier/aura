﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using Windows.Devices.Geolocation;

namespace Aura.Services.Windows
{
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window
    {
        const int MillisecondPerCharacter = 120;
        const int WaitTimeMillisecond = 1500;
        const int PadText = 20;
        const double BubbleLineThickness = 4;

        public ServiceManager? ServiceManager { get; private set; }

        public MessageWindow()
        {
            InitializeComponent();
            Activated += MessageWindow_Activated;
            pathBubble.StrokeThickness = BubbleLineThickness;
            Timer.Tick += Timer_Tick;
        }

        DispatcherTimer Timer { get; } = new DispatcherTimer();
        bool _initialized = false;
        private void MessageWindow_Activated(object? sender, EventArgs e)
        {
            if (_initialized) return;
            _initialized = true;

            var txWidth = txMessage.ActualWidth;
            var txHeight = txMessage.ActualHeight;

            Width = txWidth + PadText * 2 + BubbleLineThickness;
            Height = txHeight + PadText * 2 + BubbleLineThickness;

            if (ServiceManager != null)
            {

                var head = ServiceManager.GetService<AvatarControllerService>()?.GetCurrentOrigin("head");
                if (head != null && ServiceManager.MainWindow is Window w)
                {
                    Left = Math.Abs(w.Left - head.Value.X) / 3 + w.Left - Width;
                    Top = Math.Abs(w.Top - head.Value.Y) / 2 + w.Top - Height;
                }
            }

            DrawBubble(new Point(0, 0));

            FadeIn();
        }

        private void DrawBubble(Point headPosition)
        {
            var geo = new PathGeometry();
            var fig = new PathFigure();
            var edgeOffset = BubbleLineThickness / 2;


            var arcPortion = 0.9;
            var arcWidth = Width / 2 * arcPortion;
            var lineWidth = Width / 2 * (1 - arcPortion);
            var arcHeight = Height / 2 * arcPortion;
            var lineHeight = Height / 2 * (1- arcPortion);

            var qWidth = Width / 2 - edgeOffset;
            var qHeight = Height / 2 - edgeOffset;
            var yScale = qHeight / qWidth;

            var rotW = Rotate(37, new Point(qWidth, 0));
            var rotH = Rotate(37, new Point(qHeight, 0));
            var arcSegmentX = qWidth - rotW.X;
            var arcSegmentY = rotW.Y;
            var sarcSegmentX = arcSegmentX * yScale;
            var sarcSegmentY = arcSegmentY * yScale;

            fig.StartPoint = new Point(edgeOffset, arcHeight);
            fig.Segments.Add(new ArcSegment(new Point(arcWidth, edgeOffset), new Size(arcWidth, arcHeight), 0, false, SweepDirection.Clockwise, true));
            fig.Segments.Add(new LineSegment(new Point(Width - arcWidth, edgeOffset), true));
            fig.Segments.Add(new ArcSegment(new Point(Width - edgeOffset, arcHeight), new Size(arcWidth, arcHeight), 0, false, SweepDirection.Clockwise, true));
            fig.Segments.Add(new LineSegment(new Point(Width - edgeOffset, Height - arcHeight), true));
            fig.Segments.Add(new ArcSegment(new Point(Width - arcSegmentX, Height / 2 + sarcSegmentY), new Size(arcSegmentX, sarcSegmentY), 0, false, SweepDirection.Clockwise, true));
            fig.Segments.Add(new LineSegment(new Point(Width - edgeOffset, Height - edgeOffset), true));
            fig.Segments.Add(new LineSegment(new Point(Width / 2 + arcSegmentY, Height - sarcSegmentX), true));
            fig.Segments.Add(new ArcSegment(new Point(Width - edgeOffset - arcWidth, Height - edgeOffset), new Size(arcSegmentY, sarcSegmentX), 0, false, SweepDirection.Clockwise, true));
            fig.Segments.Add(new LineSegment(new Point(arcWidth, Height - edgeOffset), true));
            fig.Segments.Add(new ArcSegment(new Point(edgeOffset, Height - arcHeight), new Size(arcWidth, arcHeight), 0, false, SweepDirection.Clockwise, true));

            fig.IsClosed = true;
            geo.Figures.Add(fig);
            pathBubble.Data = geo;
        }

        private Point Rotate(double angle, Point point, Point? around = null)
        {
            Point pivot = around ?? new Point(0, 0);
            var rad = angle * (Math.PI / 180);
            var sinT = Math.Sin(rad);
            var cosT = Math.Cos(rad);

            return new Point(
                cosT * (point.X - pivot.X) - sinT * (point.Y - pivot.Y) + pivot.X,
                sinT * (point.X - pivot.X) + cosT * (point.Y - pivot.Y) + pivot.Y
                );
        }

        private void FadeIn()
        {
            var fadeInAnim = new DoubleAnimation()
            {
                From = 0.0,
                To = 1.0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(fadeInAnim);
            Storyboard.SetTargetProperty(fadeInAnim, new PropertyPath(Grid.OpacityProperty));

            storyboard.Completed += WindowFadedIn;
            storyboard.Begin(grContainer);
        }

        private void WindowFadedIn(object? sender, EventArgs e)
        {
            _fadingIn = false;
            Timer.Start();
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            Timer.Stop();
            StartClose();
        }

        public void StartClose()
        {
            if (_fadingIn || _canClose || _fadingOut) return;
            _fadingOut = true;
            var fadeOutAnim = new DoubleAnimation()
            {
                From = 1.0,
                To = 0.0,
                Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
            };

            var storyboard = new Storyboard();
            storyboard.Children.Add(fadeOutAnim);
            Storyboard.SetTargetProperty(fadeOutAnim, new PropertyPath(Grid.OpacityProperty));

            storyboard.Completed += WindowFadedOut;
            storyboard.Begin(grContainer);
        }

        string senderId = string.Empty;
        string messageGuid = string.Empty;
        bool _fadingIn = true;
        bool _fadingOut = false;
        bool _canClose = false;
        private void WindowFadedOut(object? sender, EventArgs e)
        {
            _canClose = true;
            Close();
        }

        public void Init(ServiceManager serviceManager, string message, bool confirm, string senderId, string msgGuid)
        {
            ServiceManager = serviceManager;
            txMessage.Text = message;
            this.senderId = senderId;
            this.messageGuid = msgGuid;
            Timer.Interval = new TimeSpan(0, 0, 0, 0, MillisecondPerCharacter * message.Length + WaitTimeMillisecond);
        }

        public void RefreshTailOrientation(Point? towards = null)
        {
            /*
            if (ServiceManager == null) return;
            towards = towards ?? ServiceManager.GetService<AvatarControllerService>()?.GetCurrentOrigin("head");
            if (towards == null) return;
            DrawBubble(towards.Value);
            */
        }

        private void Window_LocationChanged(object sender, EventArgs e)
        {
            RefreshTailOrientation();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_canClose) e.Cancel = true;
        }

        private void grContainer_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StartClose();
        }

        private void grContainer_StylusDown(object sender, StylusDownEventArgs e)
        {
            StartClose();
        }

        private void grContainer_TouchDown(object sender, TouchEventArgs e)
        {
            StartClose();
        }
    }
}
