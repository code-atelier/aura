﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WinRT;

namespace Aura.Services.Windows
{
    /// <summary>
    /// Interaction logic for AvatarPreviewWindow.xaml
    /// </summary>
    public partial class AvatarPreviewWindow : Window
    {
        public AvatarPreviewWindow()
        {
            InitializeComponent();
        }

        Persona? Persona { get; set; }

        public bool Load(string path)
        {
            try
            {
                Persona = Persona.Load(path);
                string name = (Persona.Manifest.FirstName + " " + Persona.Manifest.LastName).Trim();
                if (!string.IsNullOrWhiteSpace(Persona.Manifest.Nickname))
                {
                    if (string.IsNullOrWhiteSpace(name)) name = Persona.Manifest.Nickname;
                    else name += " (" + Persona.Manifest.Nickname + ")";
                }
                lbName.Content = name;
                lbAuthor.Content = Persona.Manifest.Creator;
                lbVersion.Content = Persona.Manifest.Version;
                cbNSFW.Content = Persona.Manifest.NSFW ? "Yes" : "No";
                cbNSFW.Foreground = new SolidColorBrush(Persona.Manifest.NSFW ? Colors.Crimson : Colors.Green);
                if (Persona.Archive.FileSystem.Exists(Persona.PersonaPreviewPath))
                {
                    var strm = Persona.Archive.FileSystem[Persona.PersonaPreviewPath];
                    strm.Seek(0, System.IO.SeekOrigin.Begin);
                    var ms = new MemoryStream();
                    strm.CopyTo(ms);
                    var bmp = new BitmapImage();
                    bmp.BeginInit();
                    bmp.StreamSource = ms;
                    bmp.EndInit();
                    imgPreview.Source = bmp;
                }
            }
            catch { }
            return false;
        }

        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
