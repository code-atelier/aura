﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public abstract class AuraConsoleProcessor : IAuraConsoleProcessor
    {
        public abstract string Identifier { get; }

        public ServiceManager? ServiceManager { get; set; }

        public virtual async Task<string?> Process(string input)
        {
            var spl = input.Split(' ', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            return await OnProcess(spl);
        }

        public virtual Task<string?> OnProcess(string[] input)
        {
            return Task.FromResult(null as string);
        }
    }
}
