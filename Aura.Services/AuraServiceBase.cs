﻿using Aura.Models;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public abstract class AuraServiceBase : IAuraService
    {
        bool _initialized = false;
        public AuraServiceState State { get; protected set; }

        public ServiceManager? Manager { get; private set; }

        public abstract string ServiceID { get; }

        public virtual AuraServiceMessagePriority MessagePriority { get; } = AuraServiceMessagePriority.Medium;

        public abstract string ServiceName { get; }

        public virtual string? ServiceIcon { get => null; }

        public virtual bool AutoStart { get => true; }

        public virtual bool HasConfiguration { get => false; }

        public virtual IReadOnlyList<IAuraWidget> Widgets { get; } = new List<IAuraWidget>();
        public virtual bool CanUserShutdown { get; } = false;

        public virtual bool CheckSafeShutdown()
        {
            return State != AuraServiceState.Busy;
        }

        public async Task Initialize(ServiceManager manager)
        {
            if (_initialized) return;
            Manager = manager;
            await OnInitialize();
        }

        protected virtual async Task OnInitialize()
        {
            await Task.Run(() => { });
        }

        public async Task Shutdown()
        {
            if (!CheckSafeShutdown()) return;
            try
            {
                State = AuraServiceState.Stopping;
                await OnShutdown();
                State = AuraServiceState.Stopped;
            }
            catch
            {
                State = AuraServiceState.Error;
                throw;
            }
        }

        protected virtual async Task OnShutdown()
        {
            await Task.Run(() => { });
        }

        public async Task Start()
        {
            if (State != AuraServiceState.Stopped)
            {
                return;
            }
            State = AuraServiceState.Starting;
            try
            {
                await OnStart();
                State = AuraServiceState.Running;
            }
            catch
            {
                State = AuraServiceState.Stopped;
                throw;
            }
        }

        protected virtual async Task OnStart()
        {
            await Task.Run(() => { });
        }

        private volatile int _busyCounter = 0;
        protected void RunBusyAction(Action action)
        {
            try
            {
                if (State != AuraServiceState.Stopped && State != AuraServiceState.Error)
                {
                    State = AuraServiceState.Busy;
                    _busyCounter++;
                }
                action();
            }
            finally
            {
                if (State != AuraServiceState.Stopped && State != AuraServiceState.Error)
                {
                    if (--_busyCounter <= 0 && State == AuraServiceState.Busy)
                    {
                        State = AuraServiceState.Running;
                    }
                }
            }
        }
        protected async Task RunBusyActionAsync(Action action)
        {
            try
            {
                if (State != AuraServiceState.Stopped && State != AuraServiceState.Stopping && State != AuraServiceState.Starting && State != AuraServiceState.Error)
                {
                    State = AuraServiceState.Busy;
                    _busyCounter++;
                }
                await Task.Run(action);
            }
            finally
            {
                if (State != AuraServiceState.Stopped && State != AuraServiceState.Stopping && State != AuraServiceState.Error)
                {
                    if (--_busyCounter <= 0 && State == AuraServiceState.Busy)
                    {
                        State = AuraServiceState.Running;
                    }
                }
            }
        }

        public virtual async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            await Task.Run(() => { });
        }

        public virtual async Task OpenConfiguration()
        {
            await Task.Run(() => { });
        }
    }
}
