﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public interface IAuraConsoleProcessor
    {
        /// <summary>
        /// Gets or sets the service manager
        /// </summary>
        ServiceManager? ServiceManager { get; set; }

        /// <summary>
        /// Command identifier of this processor
        /// </summary>
        string Identifier { get; }

        /// <summary>
        /// Process a console input
        /// </summary>
        Task<string?> Process(string input);
    }
}
