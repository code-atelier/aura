﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public interface IAuraService
    {
        /// <summary>
        /// Gets the service ID
        /// </summary>
        string ServiceID { get; }

        /// <summary>
        /// Gets the service name
        /// </summary>
        string ServiceName { get; }

        /// <summary>
        /// Gets the service icon Uri
        /// </summary>
        string? ServiceIcon { get; }

        /// <summary>
        /// Gets whether this service can be manually shutdown by user
        /// </summary>
        bool CanUserShutdown { get; }

        /// <summary>
        /// Initialize the service. This will only called once.
        /// </summary>
        Task Initialize(ServiceManager manager);

        /// <summary>
        /// Starts the service.
        /// </summary>
        Task Start();

        /// <summary>
        /// Request service to check whether it is safe to shut it down.
        /// </summary>
        /// <returns>True when safe, false when unsafe.</returns>
        bool CheckSafeShutdown();

        /// <summary>
        /// Send a shutdown signal to the service.
        /// </summary>
        Task Shutdown();

        /// <summary>
        /// Gets the current state of the service.
        /// </summary>
        AuraServiceState State { get; }

        /// <summary>
        /// Gets the priority of this service when receiving broadcast messages.
        /// </summary>
        AuraServiceMessagePriority MessagePriority { get; }

        /// <summary>
        /// Gets the manager of this service.
        /// </summary>
        ServiceManager? Manager { get; }

        /// <summary>
        /// Gets whether this service should start automatically.
        /// </summary>
        bool AutoStart { get; }

        /// <summary>
        /// This method will be called when service manager broadcasts a message.
        /// </summary>
        Task ReceiveBroadcastMessage(ServiceMessage message);

        /// <summary>
        /// Gets whether this service has a configuration window or not.
        /// </summary>
        bool HasConfiguration { get; }

        /// <summary>
        /// Opens the configuration window of this service if applicable.
        /// </summary>
        Task OpenConfiguration();

        /// <summary>
        /// Gets the widgets offered by this service
        /// </summary>
        IReadOnlyList<IAuraWidget> Widgets { get; }
    }

    public enum AuraServiceState
    {
        Stopped,
        Starting,
        Running,
        Busy,
        Stopping,
        Error,
    }

    public enum AuraServiceMessagePriority: int
    {
        High = 1,
        Medium = 2,
        Low = 3,
    }
}
