﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services.Emotion.NeuralNetworks
{
    public class SpeechEmotionNeuralNetwork
    {
        private static readonly string[] inputs = new[] {
            "positive_event",
            "neutral_event",
            "negative_event",

            EmotionConstants.Emotions.Fear,
            EmotionConstants.Emotions.Anticipation,
            EmotionConstants.Emotions.Sadness,
            EmotionConstants.Emotions.Joy,
            EmotionConstants.Emotions.Disgust,
            EmotionConstants.Emotions.Surprise,
            EmotionConstants.Emotions.Trust,
            EmotionConstants.Emotions.Anticipation,
        };

        private static readonly string[] outputs = new[] {
            EmotionConstants.SpeechEmotions.IndifferentPositive,
            EmotionConstants.SpeechEmotions.IndifferentNeutral,
            EmotionConstants.SpeechEmotions.IndifferentNegative,
            EmotionConstants.SpeechEmotions.NeutralPositive,
            EmotionConstants.SpeechEmotions.TrueNeutral,
            EmotionConstants.SpeechEmotions.NeutralNegative,
            EmotionConstants.SpeechEmotions.CaringPositive,
            EmotionConstants.SpeechEmotions.CaringNeutral,
            EmotionConstants.SpeechEmotions.CaringNegative,
        };

        private static readonly int[] hiddenLayers = new[] { 32, 16 };

    }
}
