﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services.Emotion
{
    public static class EmotionConstants
    {
        public static class Emotions
        {
            public const string Fear = "fear";
            public const string Anger = "anger";
            public const string Trust = "trust";
            public const string Disgust = "disgust";
            public const string Joy = "joy";
            public const string Sadness = "sadness";
            public const string Surprise = "surprise";
            public const string Anticipation = "anticipation";
            public const string Lust = "lust";
        }

        public static class SpeechEmotions
        {
            public const string IndifferentPositive = "indifferent_positive";
            public const string IndifferentNeutral = "indifferent_neutral";
            public const string IndifferentNegative = "indifferent_negative";
            public const string NeutralPositive = "neutral_positive";
            public const string TrueNeutral = "true_neutral";
            public const string NeutralNegative = "neutral_negative";
            public const string CaringPositive = "caring_positive";
            public const string CaringNeutral = "caring_neutral";
            public const string CaringNegative = "caring_negative";
        }
    }
}
