﻿using Aura.Models;
using Aura.Services.Windows;
using System.IO;
using System.Text.Json;
using System.Windows;

namespace Aura.Services
{
    public class CoreService : AuraServiceBase
    {
        public static string DefaultConfigPath = "config.json";
        public static string PersonaCompendiumPath = "data\\compendium";
        public static string MemoryStorePath = "data\\memories";
        public static string ServiceExtensionsPath = "data\\services";

        public override AuraServiceMessagePriority MessagePriority => AuraServiceMessagePriority.High;
        public override string ServiceID => InternalServices.CoreService;
        public override string ServiceName => "Aura Core";
        public Configuration? Configuration { get; private set; }
        public Persona? Persona { get; private set; }

        public string? PersonaFilePath { get => !string.IsNullOrWhiteSpace(Configuration?.Persona) ? Path.Combine(PersonaCompendiumPath, Configuration.Persona) : null; }

        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                Configuration = LoadConfiguration();
                SaveConfiguration();

                if (!Directory.Exists(PersonaCompendiumPath))
                    Directory.CreateDirectory(PersonaCompendiumPath);
                if (!Directory.Exists(MemoryStorePath))
                    Directory.CreateDirectory(MemoryStorePath);
                if (!Directory.Exists(ServiceExtensionsPath))
                    Directory.CreateDirectory(ServiceExtensionsPath);
            });
        }

        protected override async Task OnStart()
        {
            await Task.Run(() =>
            {
                LoadPersona();
            });
        }

        private void LoadPersona(string? persona = null)
        {
            if (persona != null && Configuration != null)
            {
                Configuration.Persona = persona;
            }
            if (!string.IsNullOrWhiteSpace(Configuration?.Persona))
            {
                var path = Path.Combine(PersonaCompendiumPath, Configuration.Persona);
                Persona = Persona.Load(path);

                var ac = Manager?.GetService<AvatarControllerService>();
                if (ac != null && Persona != null)
                {
                    ac.LoadAvatar(Persona.Manifest.Guid, Persona.Avatar, Persona.Archive);
                }
                var sc = Manager?.GetService<ScheduleService>();
                if (sc != null && Persona?.Schedule != null)
                {
                    sc.LoadSchedule(Persona.Schedule);
                }
                var ec = Manager?.GetService<EmotionService>();
                if (ec != null)
                {
                    ec.LoadPersona(Persona);
                }
            }
            else throw new Exception("Persona is not configured");
        }

        private Configuration LoadConfiguration(string? path = null)
        {
            path = path ?? DefaultConfigPath;
            if (!File.Exists(path))
                return new Configuration();
            var json = File.ReadAllText(path);
            return JsonSerializer.Deserialize<Configuration>(json) ?? new Configuration();
        }

        public void SaveConfiguration(string? path = null)
        {
            path = path ?? DefaultConfigPath;
            if (File.Exists(path)) File.Delete(path);
            var json = JsonSerializer.Serialize(Configuration);
            File.WriteAllText(path, json);
        }

        public string? ImportPersonaFile(string path)
        {
            try
            {
                var persona = Persona.Load(path);
                var fname = persona.Manifest.Guid + ".persona";
                var targetPath = Path.Combine(PersonaCompendiumPath, fname);
                if (File.Exists(targetPath)) File.Delete(targetPath);
                File.Copy(path, targetPath);
                return targetPath;
            }
            catch
            {
                return null;
            }
        }

        public override async Task ReceiveBroadcastMessage(ServiceMessage message)
        {
            if (message.From(InternalSensors.DragDrop))
            {
                if (message.HeaderIs("event", "preview")
                    && message.Body is PoolRequest request
                    && request.RequestData is IDataObject data)
                {
                    try
                    {
                        if (data.GetDataPresent(DataFormats.FileDrop))
                        {
                            var fileList = data.GetData(DataFormats.FileDrop) as string[];
                            if (fileList != null)
                            {
                                foreach (var file in fileList)
                                {
                                    var ext = Path.GetExtension(file).ToLower();
                                    if (ext == ".persona")
                                    {
                                        var fname = Path.GetFileNameWithoutExtension(file);
                                        request.PoolData.Add(ServiceID, new FileDropReceiverData()
                                        {
                                            Text = "Load Persona '" + fname + "'",
                                            Tag = "loadpersona:" + fname
                                        });
                                    }
                                }
                            }
                        }
                    }
                    catch { }
                }
                if (message.HeaderIs("event", "drop")
                    && message.Body is IDataObject drop)
                {
                    try
                    {
                        if (message["tag"]?.StartsWith("loadpersona:") == true)
                        {
                            var selPersona = message["tag"]?.Substring("loadpersona:".Length);
                            if (!string.IsNullOrEmpty(selPersona) && drop.GetDataPresent(DataFormats.FileDrop))
                            {
                                var fileList = drop.GetData(DataFormats.FileDrop) as string[];
                                string? lastPersona = null;
                                if (fileList != null)
                                {
                                    foreach (var file in fileList)
                                    {
                                        var fname = Path.GetFileNameWithoutExtension(file);
                                        if (fname != selPersona) continue;

                                        var ext = Path.GetExtension(file).ToLower();
                                        if (ext == ".persona")
                                        {
                                            var confirm = new AvatarPreviewWindow();
                                            confirm.Owner = Manager?.MainWindow;
                                            confirm.Load(file);
                                            if (confirm.ShowDialog() == true)
                                            {
                                                var imported = ImportPersonaFile(file);
                                                lastPersona = Path.GetFileName(imported);
                                                message.Handled = true;
                                            }
                                        }
                                    }
                                }
                                if (lastPersona != null)
                                {
                                    if (Manager != null)
                                    {
                                        var msg = this.CreateMessage(InternalServices.MessageService,
                                            $"Transform!").SetHeader("delay", "1000");
                                        await Manager.BroadcastMessage(msg);
                                    }
                                    LoadPersona(lastPersona);
                                    SaveConfiguration();
                                }
                            }
                        }
                    }
                    catch { }
                }
            }

            if (message.From(InternalSensors.Toast) && message.Body is ToastActivationData toastData)
            {
                if (toastData.Arguments.ContainsKey("action"))
                {
                    var action = toastData.Arguments["action"];
                    if (action == "show-in-explorer"
                        && toastData.Arguments.ContainsKey("paths"))
                    {
                        var paths = toastData.Arguments["paths"].Split(';');
                        if (paths.Length > 0)
                        {
                            var lastpath = paths.Last();
                            ShowInExplorer(lastpath);
                        }
                    }
                }
            }

            if (message.Body is string body)
            {
                var action = message["action"];
                if (action == "show-in-explorer")
                {
                    ShowInExplorer(body);
                }
                if (action == "open")
                {
                    Open(body);
                }
            }
        }

        private void ShowInExplorer(string path)
        {
            if (File.Exists(path) || Directory.Exists(path))
            {
                try
                {
                    System.Diagnostics.Process.Start("explorer.exe", string.Format("/select,\"{0}\"", path));
                }
                catch { }
            }
        }

        private void Open(string path)
        {
            if (File.Exists(path) || Directory.Exists(path))
            {
                try
                {
                    System.Diagnostics.Process.Start(
                        new System.Diagnostics.ProcessStartInfo()
                        {
                            WorkingDirectory = Path.GetDirectoryName(path),
                            UseShellExecute = true,
                            FileName = path,
                        }
                        );
                }
                catch { }
            }
        }
    }
}