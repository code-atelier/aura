﻿using Aura.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aura.Services
{
    public class ScheduleService : AuraServiceBase
    {
        public override string ServiceID => InternalServices.ScheduleService;

        public override string ServiceName => "Aura Schedule";

        private Schedule? Schedule { get; set; }

        public DateTime? FixDateTime { get; set; }

        /// <summary>
        /// Gets or sets the refresh rate in milliseconds
        /// </summary>
        public int UpdateEvery { get; set; } = 100;

        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                if (Manager != null)
                {
                    Manager.OnMainUIClockTick += Manager_OnMainUIClockTick;
                }
            });
        }

        public void LoadSchedule(Schedule schedule)
        {
            Schedule = schedule;
            UpdateTagBasedOnSchedule();
        }

        private void Manager_OnMainUIClockTick(object? sender, EventArgs e)
        {
            var delta = (DateTime.Now - LastUpdate)?.TotalMilliseconds;
            if (delta == null || delta >= UpdateEvery)
            {
                LastUpdate = DateTime.Now;
                UpdateTagBasedOnSchedule();
            }
        }

        DateTime? LastUpdate = null;
        private void UpdateTagBasedOnSchedule()
        {
            if (Schedule == null) return;
            var svcTag = Manager?.GetService<TagService>();
            if (svcTag == null) return;
            svcTag.ClearExpiringTags(ServiceID);
            svcTag.ClearTags(ServiceID);

            var now = FixDateTime ?? DateTime.Now;

            var entries = Schedule.Where(x => x.Contains(now)).SelectMany(x => x.Tags).Distinct().ToList();
            foreach (var entry in entries)
                svcTag.SetTag(ServiceID, entry, true);
        }
    }
}
