﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.DNSOverHttps
{
    public interface IDNSOverHttpsResolver
    {
        string Name { get; }

        Task<DnsJson?> Resolve(DnsJsonQuestion question);
    }
}
