﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.DNSOverHttps
{
    public class DNSOverHttpsServiceLogger
    {
        public Queue<DNSOverHttpsServiceLogEntry> Entries { get; } = new Queue<DNSOverHttpsServiceLogEntry>();

        public int MaxLogEntries { get; set; } = 500;

        public void Add(DNSOverHttpsServiceLogEntry entry)
        {
            Entries.Enqueue(entry);
            while(Entries.Count > MaxLogEntries)
                Entries.Dequeue();
        }

        public void Add(IPEndPoint endPoint, DNSOverHttpsServiceLogLevel level, string message)
        {
            Add(new DNSOverHttpsServiceLogEntry(endPoint, level, message));
        }
    }

    public struct DNSOverHttpsServiceLogEntry
    {
        public static int GlobalSequence { get; private set; }
        public int Sequence { get; }

        public DateTime DateTime { get; }

        public string ServiceIPEndPoint { get; }

        public DNSOverHttpsServiceLogLevel Level { get; }

        public string Message { get; }

        public DNSOverHttpsServiceLogEntry(IPEndPoint endPoint, DNSOverHttpsServiceLogLevel level, string message)
        {
            Sequence = GlobalSequence++;
            DateTime = DateTime.Now;
            ServiceIPEndPoint = endPoint.Address.ToString() + ":" + endPoint.Port;
            Level = level;
            Message = message;
        }

        public override string ToString()
        {
            return ToString("[{time}] {level}: {message}");
        }

        public string ToString(string format)
        {
            var text = format.Replace("{message}", Message);
            text = text.Replace("{level}", Level.ToString());
            text = text.Replace("{service}", ServiceIPEndPoint);
            text = text.Replace("{time}", DateTime.ToString("yyyy-MM-dd HH:mm:ss"));
            return text;
        }
    }

    public enum DNSOverHttpsServiceLogLevel
    {
        Debug = 0,

        Notification = 1,
        Warning = 2,

        Error = 3,
    }
}
