﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.DNSOverHttps
{
    public static class DNSOverHttpsServiceStatics
    {
        public static Version Version { get; } = new Version(2, 0, 2);

        public static string DataPath = "data/doh";
    }
}
