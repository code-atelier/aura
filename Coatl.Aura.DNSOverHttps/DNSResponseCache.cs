﻿using ARSoft.Tools.Net.Dns;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coatl.Aura.DNSOverHttps
{
    public class DNSResponseCache
    {
        public List<DNSResponseCacheEntry> Data { get; } = new List<DNSResponseCacheEntry>();

        public int MaxCachedResponse { get; set; } = 1000;

        public int Expire { get; set; } = 60;

        public void RemoveExpired()
        {
            Data.RemoveAll(x => x.Expire <= DateTime.Now);
        }

        public void Clear()
        {
            Data.Clear();
        }

        public void Add(DnsMessage message)
        {
            var entry = new DNSResponseCacheEntry(message, Expire);
            Data.RemoveAll(x => x.Id == entry.Id);
            Data.Add(entry);
            RemoveExpired();
            while (Data.Count > MaxCachedResponse)
            {
                Data.RemoveAt(0);
            }
        }

        public DNSResponseCacheEntry? Get(DnsQuestion question)
        {
            RemoveExpired();
            var id = (question.Name + ":" + question.RecordType).ToLower();
            return Data.FirstOrDefault(x => x.Id.Contains(id));
        }
    }

    public struct DNSResponseCacheEntry
    {
        public string Id { get; }

        public DateTime Expire { get; }

        public DnsMessage DnsMessage { get; }

        public DNSResponseCacheEntry(DnsMessage dnsMessage, int expireSeconds)
        {
            Id = ("|" + string.Join("|", dnsMessage.Questions.Select(x => x.Name.ToString() + ":" + x.RecordType)) + "|").ToLower();
            DnsMessage = dnsMessage;
            Expire = DateTime.Now.AddSeconds(expireSeconds);
        }
    }
}
