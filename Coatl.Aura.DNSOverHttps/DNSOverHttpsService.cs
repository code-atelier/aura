﻿using ARSoft.Tools.Net;
using ARSoft.Tools.Net.Dns;
using Aura.Services;
using MathNet.Numerics.LinearAlgebra.Factorization;
using RestSharp;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Threading;

namespace Coatl.Aura.DNSOverHttps
{
    public class DNSOverHttpsService : AuraServiceBase
    {
        public override string ServiceID => "svc.coatl.doh";

        public override string ServiceName => "Code Atelier DNS";

        public override bool CanUserShutdown => true;

        public override bool AutoStart => true;

        public DNSOverHttpsServiceLogger Log { get; } = new DNSOverHttpsServiceLogger();

        public DNSOverHttpsServiceConfig Config { get; private set; } = new DNSOverHttpsServiceConfig();

        public Dictionary<DnsServer, IPEndPoint> Servers { get; } = new Dictionary<DnsServer, IPEndPoint>();

        public DNSResponseCache Cache { get; } = new DNSResponseCache();

        public List<IDNSOverHttpsResolver> Resolvers { get; } = new List<IDNSOverHttpsResolver>();

        public override bool HasConfiguration => true;

        public override async Task OpenConfiguration()
        {
            await Task.Run(() =>
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    DOHConfiguration conf = new DOHConfiguration(this);
                    //conf.Owner = Manager?.MainWindow;
                    conf.ShowDialog();
                });
            });
        }

        protected override async Task OnInitialize()
        {
            await Task.Run(() =>
            {
                Config = DNSOverHttpsServiceConfig.Load(Path.Combine(DNSOverHttpsServiceStatics.DataPath, "config.json"));
                AuthorityDomains.Clear();
                if (Config.DomainNames != null)
                {
                    foreach (var domain in Config.DomainNames)
                    {
                        try
                        {
                            AuthorityDomains.Add(DomainName.Parse(domain));
                        }
                        catch { }
                    }
                }
                Cache.Expire = Config.CacheExpire;
                Cache.MaxCachedResponse = Config.MaxCachedResponse;
                foreach (var resolv in Config.GenericResolvers)
                {
                    try
                    {
                        var rslv = new GenericDOHResolver(resolv);
                        Resolvers.Add(rslv);
                        Log.Add(new IPEndPoint(IPAddress.Loopback, 0), DNSOverHttpsServiceLogLevel.Notification, "Generic resolver [" + rslv.Name + "] is registered");
                    }
                    catch { }
                }
                foreach (var endPoint in Config.EndPoints)
                {
                    /*
                    if (endPoint.Address.AddressFamily != System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Warning, "IPv6 is currently not supported");
                        continue;
                    }
                    */
                    var dns = new DnsServer(endPoint, Config.UDPListenerCount, Config.TCPListenerCount);
                    dns.ClientConnected += Dns_ClientConnected;
                    dns.QueryReceived += Dns_QueryReceived;
                   // dns.InvalidSignedMessageReceived += Dns_InvalidSignedMessageReceived;
                    dns.ExceptionThrown += Dns_ExceptionThrown;
                    Servers.Add(dns, endPoint);
                    Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Debug, "Service [{service}] initialized");
                }
            });
        }

        private async Task Dns_InvalidSignedMessageReceived(object sender, InvalidSignedMessageEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                if (sender is DnsServer server && Servers.ContainsKey(server))
                {
                    var endPoint = Servers[server];
                    Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Error, "Service [{service}] received invalid signed message");
                }
            });
        }

        private async Task Dns_ExceptionThrown(object sender, ExceptionEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                if (sender is DnsServer server && Servers.ContainsKey(server))
                {
                    var endPoint = Servers[server];
                    Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Error, "Service [{service}] thrown an exception: " + eventArgs.Exception.Message);
                }
            });
        }

        private async Task Dns_ClientConnected(object sender, ClientConnectedEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                if (sender is DnsServer server && Servers.ContainsKey(server))
                {
                    var endPoint = Servers[server];
                    if (Config.ClientFilter.Allowed(eventArgs.RemoteEndpoint))
                    {
                        Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Debug, "[" + eventArgs.RemoteEndpoint.Address.ToString() + "] connected to service [{service}]");
                    }
                    else
                    {
                        eventArgs.RefuseConnect = true;
                        Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Warning, "[" + eventArgs.RemoteEndpoint.Address.ToString() + "] tried to connect to service [{service}] but denied");
                    }
                }
            });
        }

        private async Task Dns_QueryReceived0(object sender, QueryReceivedEventArgs e)
        {
            var query = e.Query as DnsMessage;

            if (query == null) return;

            var response = query.CreateResponseInstance();
            DnsMessage? message = null;

            response.ReturnCode = ReturnCode.FormatError;
            if (response.Questions.Count > 0)
            {
                var question = response.Questions[0];

                message = await ResolveQuestion(question);                

                if (message != null)
                {
                    response.AnswerRecords.Clear();
                    response.AnswerRecords.AddRange(message.AnswerRecords);
                    response.AdditionalRecords.Clear();
                    response.AdditionalRecords.AddRange(message.AdditionalRecords);
                    response.AuthorityRecords.Clear();
                    response.AuthorityRecords.AddRange(message.AuthorityRecords);
                    response.IsAuthenticData = message.IsAuthenticData;
                    response.IsCheckingDisabled = message.IsCheckingDisabled;
                    response.IsTruncated = message.IsTruncated;
                    response.IsRecursionDesired = message.IsRecursionDesired;
                    response.IsRecursionAllowed = message.IsRecursionAllowed;
                    response.ReturnCode = message.ReturnCode;
                    e.Response = response;
                }
                else
                {
                    response.ReturnCode = ReturnCode.NotZone;
                }
            }
        }

        private async Task Dns_QueryReceived(object sender, QueryReceivedEventArgs eventArgs)
        {
            if (sender is DnsServer server && Servers.ContainsKey(server))
            {
                var endPoint = Servers[server];
                if (eventArgs.Query is DnsMessage message)
                {
                    Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Notification, "[" + eventArgs.RemoteEndpoint.Address.ToString() + "] queries [" + string.Join(", ", message.Questions.Select(x => x.Name + ":" + x.RecordType)) + "]");
                    var response = message.CreateResponseInstance();
                    response.AnswerRecords.Clear();
                    response.AdditionalRecords.Clear();
                    response.AuthorityRecords.Clear();
                    //response.Questions.AddRange(message.Questions);
                    response.IsTruncated = false;
                    response.IsRecursionDesired = true;
                    response.IsRecursionAllowed = true;
                    response.IsAuthenticData = false;
                    response.IsCheckingDisabled = false;
                    response.IsAuthoritiveAnswer = false;

                    bool hasAnySuccessReturn = false;
                    ReturnCode? lastErrorMessage = null;
                    foreach (var question in message.Questions)
                    {
                        var timeStart = DateTime.Now;
                        var msg = await ResolveQuestion(question);
                        var delta = DateTime.Now - timeStart;
                        if (msg != null)
                        {
                            Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Debug,
                                "resolved [" +
                                string.Join(", ", message.Questions.Select(x => x.Name + ":" + x.RecordType)) + "] in " + Math.Round(delta.TotalMilliseconds, 1) + "ms => "
                                + msg.ReturnCode);
                            if (msg.ReturnCode == ReturnCode.NoError)
                            {
                                response.AnswerRecords.AddRange(msg.AnswerRecords);
                                response.AdditionalRecords.AddRange(msg.AdditionalRecords);
                                response.AuthorityRecords.AddRange(msg.AuthorityRecords);
                                response.IsAuthenticData = msg.IsAuthenticData;
                                response.IsCheckingDisabled = msg.IsCheckingDisabled;
                                response.IsTruncated = msg.IsTruncated;
                                response.IsRecursionDesired = msg.IsRecursionDesired;
                                response.IsRecursionAllowed = msg.IsRecursionAllowed;
                                response.ReturnCode = msg.ReturnCode;
                                hasAnySuccessReturn = true;
                            }
                            else
                            {
                                lastErrorMessage = msg.ReturnCode;
                            }
                        }
                        else
                        {
                            Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Debug, "cannot resolve [" + string.Join(", ", message.Questions.Select(x => x.Name + ":" + x.RecordType)) + "]");
                        }
                    }
                    if (hasAnySuccessReturn)
                    {
                        response.ReturnCode = ReturnCode.NoError;
                        eventArgs.Response = response;
                        Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Notification, "[" + eventArgs.RemoteEndpoint.Address.ToString() + "] response for [" + string.Join(", ", message.Questions.Select(x => x.Name + ":" + x.RecordType)) + "] sent");
                    }
                    else
                    {
                        if (lastErrorMessage.HasValue)
                            response.ReturnCode = lastErrorMessage.Value;
                        else
                            response.ReturnCode = ReturnCode.NotZone;
                        eventArgs.Response = response;
                        Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Warning, "[" + eventArgs.RemoteEndpoint.Address.ToString() + "] error reponse for [" + string.Join(", ", message.Questions.Select(x => x.Name + ":" + x.RecordType)) + "]");
                    }
                }
                else
                {
                    Log.Add(endPoint, DNSOverHttpsServiceLogLevel.Warning, "[" + eventArgs.RemoteEndpoint.Address.ToString() + "] sent invalid request");
                    eventArgs.Response = FailureMessage(ReturnCode.Refused);
                }
            }
        }

        DomainName inv4AddrTLD = DomainName.Parse("in-addr.arpa");
        DomainName inv6AddrTLD = DomainName.Parse("ip6.arpa");
        List<DomainName> AuthorityDomains { get; } = new List<DomainName>();
        List<IPAddress> loopbackBoundAddress = new List<IPAddress>();
        private async Task<DnsMessage?> ResolveQuestion(DnsQuestion question)
        {
            // intercept local inverse address
            if (question.RecordType == RecordType.Ptr && (
                question.Name.IsSubDomainOf(inv4AddrTLD) || question.Name.IsSubDomainOf(inv6AddrTLD)))
            {
                var lbl = question.Name.Labels.ToArray();
                Array.Reverse(lbl);
                string ip;
                if (lbl.Length > 6) // should be ipv6
                {
                    var lst = new List<string>();
                    for (var i = 2; i < lbl.Length - 3; i += 4)
                    {
                        lst.Add(lbl[i] + lbl[i + 1] + lbl[i + 2] + lbl[i + 3]);
                    }
                    ip = string.Join(":", lst);
                }
                else
                {
                    ip = string.Join(".", lbl[2..]);
                }

                IPAddress? addr;
                if (IPAddress.TryParse(ip, out addr) &&
                    (addr.Equals(IPAddress.Loopback)
                    || addr.Equals(IPAddress.IPv6Loopback)
                    || loopbackBoundAddress.Any(x => addr.Equals(x))
                    ))
                {
                    var msg = new DnsMessage()
                    {
                        ReturnCode = ReturnCode.NoError
                    };
                    msg.AnswerRecords.Add(new PtrRecord(question.Name, Config.ForceTTL ? Config.TTL : 200, DomainName.Parse(!string.IsNullOrWhiteSpace(Config.ServerName) ? Config.ServerName : Environment.MachineName)));
                    return msg;
                }
            }

            bool isAuth = false;
            var q = DnsJsonQuestion.FromQuestion(question);
            try
            {
                var dom = DomainName.Parse(q.Name);
                var authDom = AuthorityDomains.FirstOrDefault(x => dom.IsSubDomainOf(x));
                if (authDom != null)
                {
                    isAuth = true;
                    q.Name = new DomainName(dom.Labels[0..(dom.LabelCount - authDom.LabelCount)]).ToString();
                }
            }
            catch { }

            if (!Config.DomainFilter.Allowed(q.Name))
            {
                return FailureMessage(ReturnCode.Refused);
            }
            if (Config.UseCache)
            {
                var cached = Cache.Get(question);
                if (cached.HasValue && cached.Value.Id != null)
                {
                    return cached.Value.DnsMessage;
                }
            }
            foreach (var resolver in Resolvers)
            {
                try
                {
                    var res = await resolver.Resolve(q);
                    if (res != null)
                    {
                        var ret = res.ToMessage(Config.ForceTTL ? Config.TTL : null);

                        if (ret != null && Config.UseCache && ret.ReturnCode == ReturnCode.NoError)
                        {
                            Cache.Add(ret);
                        }

                        return ret;
                    }
                }
                catch { }
            }
            return null;
        }

        private DnsMessage FailureMessage(ReturnCode returnCode, params DnsQuestion[] questions)
        {
            var res = new DnsMessage()
            {
                ReturnCode = returnCode,
                IsTruncated = false,
                IsRecursionDesired = true,
                IsRecursionAllowed = true,
                IsAuthenticData = true,
                IsCheckingDisabled = false,
            };
            res.Questions.AddRange(questions);
            res.AuthorityRecords.Add(new SoaRecord(
                DomainName.Parse("."),
                86398,
                DomainName.Parse("a.root-servers.net."),
                DomainName.Parse("nstld.verisign-grs.com."),
                2023010600,
                1800,
                900,
                604800,
                86400
                ));
            return res;
        }

        Dictionary<DnsServer, IPEndPoint> started = new Dictionary<DnsServer, IPEndPoint>();

        public string? IPV4NIC { get; set; }
        public string? IPV6NIC { get; set; }

        protected override async Task OnStart()
        {
            await Task.Run(() =>
            {
                loopbackBoundAddress.Clear();
                foreach (var server in Servers)
                {
                    try
                    {
                        Log.Add(server.Value, DNSOverHttpsServiceLogLevel.Debug, "Starting service [{service}]...");
                        server.Key.Start();
                        Log.Add(server.Value, DNSOverHttpsServiceLogLevel.Notification, "Service [{service}] started successfully");
                        started.Add(server.Key, server.Value);
                        if (server.Value.Address.Equals(IPAddress.Any) || server.Value.Address.Equals(IPAddress.IPv6Any))
                        {
                            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
                            {
                                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                                {
                                    foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                                    {
                                        if (ip.Address.AddressFamily == server.Value.Address.AddressFamily)
                                        {
                                            loopbackBoundAddress.Add(ip.Address);
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            loopbackBoundAddress.Add(server.Value.Address);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Add(server.Value, DNSOverHttpsServiceLogLevel.Error, "Failed to start service [{service}]: " + ex.Message);
                        Log.Add(server.Value, DNSOverHttpsServiceLogLevel.Warning, "Stopping started servers...");
                        foreach (var svc in started)
                        {
                            try
                            {
                                Log.Add(svc.Value, DNSOverHttpsServiceLogLevel.Debug, "Stopping service [{service}]...");
                                svc.Key.Stop();
                                Log.Add(svc.Value, DNSOverHttpsServiceLogLevel.Notification, "Service [{service}] stopped successfully");
                            }
                            catch (Exception xx)
                            {
                                Log.Add(svc.Value, DNSOverHttpsServiceLogLevel.Warning, "Failed to stop service [{service}]: " + xx.Message);
                            }
                        }
                        SaveLog();
                        State = AuraServiceState.Error;
                        return;
                    }
                }
            });
        }

        private void SaveLog()
        {
            try
            {
                var logOut = Path.Combine(DNSOverHttpsServiceStatics.DataPath, "session.log");
                var str = string.Join(Environment.NewLine, Log.Entries);
                str = "Log saved at " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + Environment.NewLine + str;
                if (!Directory.Exists(DNSOverHttpsServiceStatics.DataPath))
                    Directory.CreateDirectory(DNSOverHttpsServiceStatics.DataPath);
                File.WriteAllText(logOut, str);
            }
            catch { }
        }

        protected override async Task OnShutdown()
        {
            await Task.Run(() =>
            {
                foreach (var server in started)
                {
                    try
                    {
                        Log.Add(server.Value, DNSOverHttpsServiceLogLevel.Debug, "Stopping service [{service}]...");
                        server.Key.Stop();
                        Log.Add(server.Value, DNSOverHttpsServiceLogLevel.Notification, "Service [{service}] stopped successfully");
                    }
                    catch (Exception xx)
                    {
                        Log.Add(server.Value, DNSOverHttpsServiceLogLevel.Error, "Failed to stop service [{service}]: " + xx.Message);
                    }
                }

                try
                {
                    Log.Add(new IPEndPoint(IPAddress.Any, 0), DNSOverHttpsServiceLogLevel.Debug, "Saving configuration...");
                    Config.Save();
                    Log.Add(new IPEndPoint(IPAddress.Any, 0), DNSOverHttpsServiceLogLevel.Notification, "Configuration saved successfully");
                }
                catch (Exception xx)
                {
                    Log.Add(new IPEndPoint(IPAddress.Any, 0), DNSOverHttpsServiceLogLevel.Error, "Failed to save configuration: " + xx.Message);
                }

                SaveLog();
                started.Clear();
            });
        }
    }
}