﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Coatl.Aura.DNSOverHttps
{
    /// <summary>
    /// Interaction logic for DOHConfiguration.xaml
    /// </summary>
    public partial class DOHConfiguration : Window
    {
        public DNSOverHttpsService? Service { get; }

        private DispatcherTimer Timer { get; } = new DispatcherTimer();

        public DOHConfiguration(DNSOverHttpsService? service = null)
        {
            InitializeComponent();
            Service = service;

            tabConfig.Visibility =
            tabLog.Visibility = service != null ? Visibility.Visible : Visibility.Collapsed;

            Title = "Coatl DNS v" + DNSOverHttpsServiceStatics.Version;
            Timer.Interval = new TimeSpan(0, 0, 0, 0, 250);
            Timer.Tick += Timer_Tick;
            Timer.Start();

            cbFilter.Items.Add(DNSOverHttpsServiceLogLevel.Debug);
            cbFilter.Items.Add(DNSOverHttpsServiceLogLevel.Notification);
            cbFilter.Items.Add(DNSOverHttpsServiceLogLevel.Warning);
            cbFilter.Items.Add(DNSOverHttpsServiceLogLevel.Error);
            cbFilter.SelectedIndex = 2;

            ReloadNetworkInterfaces();
            AddShieldIconTo(imgShieldSetup);
            AddShieldIconTo(imgShieldReset);

            if (service?.Config != null)
            {
                txServiceName.Text = service.Config.ServerName;
                txListenEndpoints.Text = string.Join(Environment.NewLine, service.Config.EndPoints.Select(p => p.ToString()));
                txTCPCount.Text = service.Config.TCPListenerCount.ToString();
                txUDPCount.Text = service.Config.UDPListenerCount.ToString();
                txTimeout.Text = service.Config.Timeout.ToString();
                txTTL.Text = service.Config.TTL.ToString();
                cxForceTTL.IsChecked = service.Config.ForceTTL;
                txMaxCache.Text = service.Config.MaxCachedResponse.ToString();
                txCacheExpire.Text = service.Config.CacheExpire.ToString();
            }
        }

        private void btnSaveConfig_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Service != null)
                {
                    Service.Config.ServerName = txServiceName.Text;
                    var eps = new List<IPEndPoint>();
                    foreach (var line in txListenEndpoints.Text.Split('\n', StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries))
                    {
                        try
                        {
                            eps.Add(IPEndPoint.Parse(line));
                        }
                        catch
                        {
                            throw new Exception($"'{line}' is not a valid IP end point");
                        }
                    }
                    if (eps.Count > 0)
                    {
                        Service.Config.EndPoints.Clear();
                        Service.Config.EndPoints.AddRange(eps);
                    }
                    else
                    {
                        throw new Exception("IP End Points cannot be empty");
                    }
                    try
                    {
                        var udp = int.Parse(txUDPCount.Text);
                        if (udp < 0) throw new Exception();
                        Service.Config.UDPListenerCount = udp;
                    }
                    catch { throw new Exception($"{txUDPCount.Text} is not a valid UDP Listener Count value"); };
                    try
                    {
                        var tcp = int.Parse(txTCPCount.Text);
                        if (tcp < 0) throw new Exception();
                        Service.Config.UDPListenerCount = tcp;
                    }
                    catch { throw new Exception($"{txTCPCount.Text} is not a valid TCP Listener Count value"); };
                    try
                    {
                        var val = int.Parse(txTimeout.Text);
                        if (val < 0) throw new Exception();
                        Service.Config.Timeout = val;
                    }
                    catch { throw new Exception($"{txTimeout.Text} is not a valid Timeout value"); };
                    try
                    {
                        var val = int.Parse(txTTL.Text);
                        if (val < 0) throw new Exception();
                        Service.Config.TTL = val;
                    }
                    catch { throw new Exception($"{txTTL.Text} is not a valid TTL value"); };
                    Service.Config.ForceTTL = cxForceTTL.IsChecked == true;
                    try
                    {
                        var val = int.Parse(txMaxCache.Text);
                        if (val < 0) throw new Exception();
                        Service.Config.MaxCachedResponse = val;
                    }
                    catch { throw new Exception($"{txMaxCache.Text} is not a valid Max Cached Records value"); };
                    try
                    {
                        var val = int.Parse(txCacheExpire.Text);
                        if (val < 0) throw new Exception();
                        Service.Config.CacheExpire = val;
                    }
                    catch { throw new Exception($"{txCacheExpire.Text} is not a valid Cache Expire value"); };

                    Service.Config.Save();
                }
                MessageBox.Show($"DOH Server Configuration saved!", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddShieldIconTo(Image image)
        {
            try
            {
                var identity = WindowsIdentity.GetCurrent();
                if (identity != null)
                {
                    var principal = new WindowsPrincipal(identity);
                    if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
                    {
                        BitmapSource shieldSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(
                            System.Drawing.SystemIcons.Shield.Handle,
                            Int32Rect.Empty,
                            BitmapSizeOptions.FromEmptyOptions());
                        image.Source = shieldSource;
                    }
                }
            }
            catch { }
        }

        int lastSequence = -1;
        private void Timer_Tick(object? sender, EventArgs e)
        {
            Timer.Stop();
            try
            {
                if (Service == null) return;
                var scroll = IsScrolledToEnd(txLog);
                var level = cbFilter.SelectedItem is DNSOverHttpsServiceLogLevel ? (DNSOverHttpsServiceLogLevel)cbFilter.SelectedItem : DNSOverHttpsServiceLogLevel.Warning;
                try
                {
                    var coll = Service.Log.Entries.Where(x => x.Sequence > lastSequence &&  x.Level >= level).ToList();
                    if (coll.Count > 0)
                    {
                        lastSequence = coll.Max(x => x.Sequence);
                        var nText = string.Join(Environment.NewLine, coll) + Environment.NewLine;
                        txLog.AppendText(nText);
                        if (scroll)
                            txLog.ScrollToEnd();
                    }
                }
                catch { }
            }
            finally
            {
                Timer.Start();
            }
        }
        public bool IsScrolledToEnd(TextBox textBox)
        {
            return textBox.VerticalOffset + textBox.ViewportHeight == textBox.ExtentHeight || textBox.ViewportHeight < textBox.ExtentHeight;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Timer.Stop();
        }

        private void btnClearLogs_Click(object sender, RoutedEventArgs e)
        {
            txLog.Clear();
        }

        public void ReloadNetworkInterfaces(string? selectByName = null)
        {
            var nics = NetworkInterface.GetAllNetworkInterfaces()
                    .Where(ni =>
                    ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet
                    || ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211
                    ).Select(x => new NICWrapper(x)).ToList();
            cbNICs.ItemsSource = nics;
            if (nics.Count > 0)
            {
                if (selectByName != null)
                {
                    cbNICs.SelectedItem = nics.Where(x => x.NetworkInterface.Name == selectByName).FirstOrDefault();
                }
                else
                {
                    cbNICs.SelectedItem = nics.Where(x => x.NetworkInterface.OperationalStatus == OperationalStatus.Up).FirstOrDefault(x => x.NetworkInterface.GetIPProperties().GatewayAddresses.Any(x => x?.Address != null));
                }
            }
        }

        private void cbNICs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            grNICInfo.DataContext = null;
            if (cbNICs.SelectedItem is NICWrapper nicw)
            {
                grNICInfo.DataContext = nicw;
            }
        }

        private void btnSetupDOHv4_Click(object sender, RoutedEventArgs e)
        {
            if (cbNICs.SelectedItem is NICWrapper nicw)
            {
                try
                {
                    var dns = nicw.NetworkInterface.GetIPProperties().DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).Select(x => x.ToString()).ToList();
                    if (!dns.Contains("127.0.0.1"))
                        dns.Insert(0, "127.0.0.1");
                    if (nicw.IP4DOHReady == "No")
                        SetupDOH("ipv4", nicw.Name, dns.ToArray());
                    else
                        SetupDOH("ipv4", nicw.Name);
                }
                catch
                {
                    if (nicw.IP4DOHReady == "No")
                        MessageBox.Show("Failed to setup DNS Over Https", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    else
                        MessageBox.Show("Failed to reset interface configuration", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    ReloadNetworkInterfaces(nicw.Name);
                }
            }
        }

        private void SetupDOH(string ipv, string name, params string?[] address)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.FileName = "cmd";
            startInfo.Verb = "runas";
            startInfo.CreateNoWindow = true;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "/c netsh interface " + ipv + " set dnsserver name=\"" + name + "\"";
            if (address != null && address.Length > 0)
            {
                startInfo.Arguments += " source=static address=" + address[0];
                for(var i = 1; i < address.Length; i++)
                {
                    startInfo.Arguments += " &&";
                    startInfo.Arguments += " netsh interface " + ipv + " add dnsserver name=\"" + name + "\"";
                    startInfo.Arguments += " address=" + address[i];
                }
            }
            else
            {
                startInfo.Arguments += " source=dhcp";
            }
            Process? p = Process.Start(startInfo);
            if (p != null)
            {
                p.WaitForExit();
                if (p.ExitCode != 0)
                    throw new Exception();
            }
            else throw new Exception();
        }

        private void btnSetupDOHv6_Click(object sender, RoutedEventArgs e)
        {
            if (cbNICs.SelectedItem is NICWrapper nicw)
            {
                try
                {
                    var dns = nicw.NetworkInterface.GetIPProperties().DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6).Select(x => x.ToString()).ToList();
                    if (!dns.Contains("::1"))
                        dns.Insert(0, "::1");
                    if (nicw.IP6DOHReady == "No")
                        SetupDOH("ipv6", nicw.Name, "::1");
                    else
                        SetupDOH("ipv6", nicw.Name);
                }
                catch
                {
                    if (nicw.IP6DOHReady == "No")
                        MessageBox.Show("Failed to setup DNS Over Https", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    else
                        MessageBox.Show("Failed to reset interface configuration", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    ReloadNetworkInterfaces(nicw.Name);
                }
            }
        }

        private void btnClearDNSCache_Click(object sender, RoutedEventArgs e)
        {
            if (Service != null)
            {
                var count = Service.Cache.Data.Count;
                Service.Cache.Clear();
                MessageBox.Show($"DNS Resolver Cache has been cleared. ({count} records)", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnDomainFilter_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Coming soon :)", "Oops...", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }

    public class NICWrapper
    {
        public NetworkInterface NetworkInterface { get; }

        public string Name { get => NetworkInterface.Name; }
        public string Description { get => NetworkInterface.Description; }
        public OperationalStatus OperationalStatus { get => NetworkInterface.OperationalStatus; }
        public NetworkInterfaceType NetworkInterfaceType { get => NetworkInterface.NetworkInterfaceType; }

        public string IP4Address { get => NetworkInterface.GetIPProperties()?.UnicastAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?.Address.ToString() ?? "-"; }
        public string IP6Address { get => NetworkInterface.GetIPProperties()?.UnicastAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)?.Address.ToString() ?? "-"; }
        public string IP4Gateway { get => NetworkInterface.GetIPProperties()?.GatewayAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?.Address.ToString() ?? "-"; }
        public string IP6Gateway { get => NetworkInterface.GetIPProperties()?.GatewayAddresses.FirstOrDefault(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)?.Address.ToString() ?? "-"; }
        public string IP4DNS
        {
            get
            {
                var ips = NetworkInterface.GetIPProperties()?.DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).Select(x => x.ToString()).ToList() ?? new List<string>();
                return ips.Count > 0 ? string.Join(", ", ips) : "-";
            }
        }
        public string IP6DNS
        {
            get
            {
                var ips = NetworkInterface.GetIPProperties()?.DnsAddresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6).Select(x => x.ToString()).ToList() ?? new List<string>();
                return ips.Count > 0 ? string.Join(", ", ips) : "-";
            }
        }

        public string IP4DOHReady { get => NetworkInterface.GetIPProperties()?.DnsAddresses.Any(x => x.ToString() == "127.0.0.1") == true ? "Yes" : "No"; }
        public string IP6DOHReady { get => NetworkInterface.GetIPProperties()?.DnsAddresses.Any(x => x.ToString() == "::1") == true ? "Yes" : "No"; }

        public bool IP4CanSetup { get => NetworkInterface.OperationalStatus == OperationalStatus.Up; }
        public bool IP6CanSetup { get => NetworkInterface.OperationalStatus == OperationalStatus.Up; }

        public string IP4SetupText { get => IP4DOHReady == "Yes" ? "Reset IPv4 Configuration" : "Setup DNS Over Https for IPv4"; }
        public string IP6SetupText { get => IP6DOHReady == "Yes" ? "Reset IPv6 Configuration" : "Setup DNS Over Https for IPv6"; }

        public NICWrapper(NetworkInterface networkInterface)
        {
            NetworkInterface = networkInterface;
        }

        public override string ToString()
        {
            return NetworkInterface.Name + " (" + NetworkInterface.Description + ")";
        }
    }
}
