﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.DNSOverHttps
{
    public class GenericDOHResolverConfig
    {
        [JsonPropertyName("name")]
        public string Name { get; set; } = string.Empty;

        [JsonPropertyName("uri")]
        public string Uri { get; set; } = string.Empty;

        [JsonPropertyName("method")]
        public GenericDOHResolverRequestMethod Method { get; set; } = GenericDOHResolverRequestMethod.GET;

        [JsonPropertyName("queryFields")]
        public Dictionary<string, string> QueryFields { get; set; } = new Dictionary<string, string>();

        [JsonPropertyName("headers")]
        public Dictionary<string, string> Headers { get; set; } = new Dictionary<string, string>();
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum GenericDOHResolverRequestMethod
    {
        GET,
        POST
    }
}
