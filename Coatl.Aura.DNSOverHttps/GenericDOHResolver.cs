﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Windows.Media.Protection.PlayReady;

namespace Coatl.Aura.DNSOverHttps
{
    public class GenericDOHResolver : IDNSOverHttpsResolver
    {
        public GenericDOHResolverConfig Config { get; }

        public RestClient Client { get; }

        public HttpClient HttpClient { get; }

        public string Name => Config.Name;

        public bool UseHttpClient { get; set; } = false;

        public GenericDOHResolver(GenericDOHResolverConfig config)
        {
            Config = config;
            HttpClient = new HttpClient();
            Client = new RestClient();
            foreach (var hdr in Config.Headers)
            {
                try
                {
                    if (hdr.Key.Trim().ToLower() != "user-agent")
                        Client.AddDefaultHeader(hdr.Key, hdr.Value);
                }
                catch { }
            }
            Client.AddDefaultHeader("User-Agent", "CoatlDNS");
        }

        public async Task<DnsJson?> Resolve(DnsJsonQuestion question)
        {
            try
            {
                if (UseHttpClient)
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    var uri = Config.Uri;
                    uri += "?" + (Config.QueryFields.ContainsKey("name") ? Config.QueryFields["name"] : "name") + "=" + question.Name;
                    uri += "&" + (Config.QueryFields.ContainsKey("type") ? Config.QueryFields["type"] : "type") + "=" + question.Type;
                    var res = await HttpClient.GetAsync(uri);
                    if (res.IsSuccessStatusCode)
                    {
                        var json = await res.Content.ReadAsStringAsync();
                        try
                        {
                            var result = JsonSerializer.Deserialize<DnsJson>(json ?? "null");
                            return result;
                        }
                        catch { return null; }
                    }
                    else
                    {
                        throw new Exception("DNS returned http error " + res.StatusCode);
                    }
                }
                else
                {
                    var req = new RestRequest(Config.Uri, Config.Method == GenericDOHResolverRequestMethod.GET ? Method.Get : Method.Post);
                    if (Config.Method == GenericDOHResolverRequestMethod.GET)
                    {
                        var fieldName = "name";
                        if (Config.QueryFields.ContainsKey(fieldName))
                            fieldName = Config.QueryFields[fieldName];
                        req.AddQueryParameter(fieldName, question.Name);
                        fieldName = "type";
                        if (Config.QueryFields.ContainsKey(fieldName))
                            fieldName = Config.QueryFields[fieldName];
                        req.AddQueryParameter(fieldName, question.Type);
                        var response = await Client.ExecuteAsync(req);
                        if (response.IsSuccessStatusCode)
                        {
                            var result = JsonSerializer.Deserialize<DnsJson>(response.Content ?? "null");
                            return result;
                        }
                    }
                    else
                    {
                        throw new NotImplementedException();
                    }
                }
            }
            catch { }
            return null;
        }
    }
}
