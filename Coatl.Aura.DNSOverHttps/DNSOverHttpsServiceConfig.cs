﻿using ARSoft.Tools.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Coatl.Aura.DNSOverHttps
{
    public class DNSOverHttpsServiceConfig
    {
        [JsonPropertyName("name")]
        public string? ServerName { get; set; }

        [JsonPropertyName("domains")]
        public List<string> DomainNames { get; set; } = new List<string>();

        [JsonPropertyName("endpoints")]
        public List<IPEndPoint> EndPoints { get; set; } = new List<IPEndPoint>()
        {
            //new IPEndPoint(IPAddress.IPv6Any, 53),
            new IPEndPoint(IPAddress.Any, 53),
        };

        [JsonPropertyName("udpListenerCount")]
        public int UDPListenerCount { get; set; } = 30;

        [JsonPropertyName("tcpListenerCount")]
        public int TCPListenerCount { get; set; } = 30;

        /// <summary>
        /// Timeout value in milliseconds
        /// </summary>
        [JsonPropertyName("timeout")]
        public int Timeout { get; set; } = 5000;

        [JsonPropertyName("forceTtl")]
        public bool ForceTTL { get; set; } = false;

        [JsonPropertyName("ttl")]
        public int TTL { get; set; } = 200;

        [JsonPropertyName("useCache")]
        public bool UseCache { get; set; } = true;

        /// <summary>
        /// Cache expiration in seconds
        /// </summary>
        [JsonPropertyName("cacheExpire")]
        public int CacheExpire { get; set; } = 60;

        /// <summary>
        /// Maximum number of response being stored in cache
        /// </summary>
        [JsonPropertyName("maxCachedResponse")]
        public int MaxCachedResponse { get; set; } = 1000;

        [JsonPropertyName("domainFilter")]
        public HostFilteringRule DomainFilter { get; set; } = new HostFilteringRule();

        [JsonPropertyName("clientFilter")]
        public HostFilteringRule ClientFilter { get; set; } = new HostFilteringRule();

        [JsonPropertyName("genericResolvers")]
        public List<GenericDOHResolverConfig> GenericResolvers { get; set; } = new List<GenericDOHResolverConfig>()
        {
            new GenericDOHResolverConfig()
            {
                Name = "Google",
                Uri = "https://8.8.8.8/resolve"
            },
            new GenericDOHResolverConfig()
            {
                Name = "Cloudflare",
                Uri = "https://1.1.1.1/dns-query",
                Headers = new Dictionary<string, string>()
                {
                    { "Accept", "application/dns-json" }
                }
            },
        };

        [JsonIgnore]
        public string? ConfigPath { get; private set; } = null;

        public static DNSOverHttpsServiceConfig Load(string path)
        {
            try
            {
                path = Path.GetFullPath(path);
                var json = File.ReadAllText(path);
                var opt = new JsonSerializerOptions();
                opt.Converters.Add(new IPEndPointJsonConverter());
                var config = JsonSerializer.Deserialize<DNSOverHttpsServiceConfig>(json, opt)
                    ?? new DNSOverHttpsServiceConfig();
                config.ConfigPath = path;
                return config;
            }
            catch { }
            return new DNSOverHttpsServiceConfig() { ConfigPath = path };
        }

        public void Save(string? path = null)
        {
            path = path ?? ConfigPath;
            if (path == null)
                throw new ArgumentNullException(nameof(path));
            var dir = Path.GetDirectoryName(path);
            if (dir != null && !Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            var opt = new JsonSerializerOptions();
            opt.Converters.Add(new IPEndPointJsonConverter());
            var json = JsonSerializer.Serialize(this, opt);
            File.WriteAllText(path, json);
        }
    }

    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum FilteringRule
    {
        AllowAll,

        Blacklist,
        Whitelist,
    }

    public class HostFilteringRule
    {
        [JsonPropertyName("rule")]
        public FilteringRule Rule { get; set; } = FilteringRule.AllowAll;

        [JsonPropertyName("whitelistedHosts")]
        public List<string> WhitelistedHosts { get; set; } = new List<string>();

        [JsonPropertyName("blacklistedHosts")]
        public List<string> BlacklistedHosts { get; set; } = new List<string>();

        public bool Allowed(IPEndPoint endPoint)
        {
            if (Rule == FilteringRule.AllowAll) return true;
            var full = endPoint.Address.ToString().ToLower() + ":" + endPoint.Port;
            var hostOnly = endPoint.Address.ToString();
            if (Rule == FilteringRule.Whitelist)
            {
                if (WhitelistedHosts.Contains(full) || WhitelistedHosts.Contains(hostOnly))
                    return true;
            }
            if (Rule == FilteringRule.Blacklist)
            {
                if (!BlacklistedHosts.Contains(full) && !BlacklistedHosts.Contains(hostOnly))
                    return true;
            }
            return false;
        }
        public bool Allowed(string domainName)
        {
            if (Rule == FilteringRule.AllowAll) return true;
            try
            {
                var domName = DomainName.Parse(domainName);
                if (Rule == FilteringRule.Whitelist)
                {
                    if (WhitelistedHosts.Any(x =>
                    {
                        try
                        {
                            return DomainName.Parse(x).IsEqualOrSubDomainOf(domName);
                        }
                        catch
                        {
                            return false;
                        }
                    }))
                        return true;
                }
                if (Rule == FilteringRule.Blacklist)
                {
                    if (!BlacklistedHosts.Any(x =>
                    {
                        try
                        {
                            return DomainName.Parse(x).IsEqualOrSubDomainOf(domName);
                        }
                        catch
                        {
                            return false;
                        }
                    }))
                        return true;
                }
            }
            catch { }
            return false;
        }
    }
}
