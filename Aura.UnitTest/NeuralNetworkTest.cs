namespace Aura.UnitTest
{
    [TestClass]
    public class NeuralNetworkTest
    {
        const int TrainingCount = 250;
        const double LearningRate = 0.1;
        const double TargetMSE = 0.01;

        [TestMethod]
        public void Run_Success()
        {
        }

        [TestMethod]
        public void Train_Success()
        {
            /*
            var nn = new NeuralNetwork(
                "xor_test",
                new string[] { "a", "b" },
                new int[] { 7, 12, 4 },
                new string[] { "result" }
            );
            nn.PerLayerActivationFunctions = new List<ActivationFunction>()
            {
                ActivationFunction.Sigmoid,
                ActivationFunction.ReLU,
                ActivationFunction.Sigmoid,
                ActivationFunction.Sigmoid
            };

            var dataset = GenerateDatasetFromTrueData();
            dataset.Randomize();
            NeuralNetwork trainedNN = nn;
            var nnTrainer = new NeuralNetworkTrainer(nn);
            var mse = nnTrainer.MSE(dataset).Average();
            var lastMSE = mse;
            Console.WriteLine("Training NN for " + TrainingCount + " iterations");
            Console.WriteLine("Original NN MSE: " + mse);

            var sameMSE = 0;
            for (var epoch = 1; epoch <= TrainingCount; epoch++)
            {
                dataset.Randomize();
                trainedNN = nnTrainer.Train(dataset, LearningRate);
                nnTrainer = new NeuralNetworkTrainer(trainedNN);
                mse = nnTrainer.MSE(dataset).Average();
                var diff = mse - lastMSE;
                Console.WriteLine($"Epoch #{epoch} NN MSE: " + Math.Round(mse, 5) + " => " +
                    ( diff > 0 ? "+" : diff == 0 ? "~" : "")
                    + Math.Round(diff, 5));
                
                if (diff == 0)
                {
                    sameMSE++; 
                    if (sameMSE >= 3)
                    {
                        Console.WriteLine("Stopping training as MSE does not improve");
                        break;
                    }
                }
                else
                {
                    sameMSE = 0;
                }
                if (mse <= TargetMSE)
                {
                    Console.WriteLine("Stopping training as target MSE achieved");
                    break;
                }
                lastMSE = mse;
            }

            var test = GenerateTrueData();
            foreach (var t in test)
            {
                var res = trainedNN.Run(t.InputValues);
                var y = res["result"];
                y = y < 0.5 ? 0 : 1;
                var a = t.ExpectedOutputValues["result"];
                Console.WriteLine(t.ToString() + " <> " + res.ToString() + " " + (y != a ? "(FAIL)" : "(SUCCESS)" ));
                Assert.AreEqual(y, a);
            }
            // */
        }

    }
}